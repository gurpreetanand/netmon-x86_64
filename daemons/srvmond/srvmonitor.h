#ifndef __SRV_MONITOR__
#define __SRV_MONITOR__

#include "checker.h"
#include "msgqueue.h"
#include "sigtermhandler.h"

namespace netmon
{
  class db;
  class daemon;
  class srv_alert_manager;

  class srvmonitor
  {
    public:
      srvmonitor (db *, daemon *);
      ~srvmonitor ();

      void run ();

    protected:
      void init ();
      void getServices (servicelist &);
      void checkServers ();
      void updateDB (srvchecker *);
      void checkServiceAlert (srvchecker::servicestatus *, bool);
      void checkLatencyAlert (srvchecker::servicestatus *);
      const char *formatLatency (unsigned int);

      db *m_db;
      daemon *m_daemon;
      srv_alert_manager *m_alertManager;
      sig_term_handler m_sigHandler;

    private:
      struct daemon_msg : public msg
      {
        char m_data[20];
      };

      static const unsigned long queue_id = 0xff00cacf;
      msgqueue m_queue;
  };
}

#endif // __SRV_MONITOR__
