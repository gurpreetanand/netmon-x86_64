#ifndef __SRV_ALERT_MANAGER__
#define __SRV_ALERT_MANAGER__

#include "srvalertmanager.h"
#include "alertutil.h"

namespace netmon
{
  class srv_alert_manager : public service_alert_manager
  {
    public:
      using alert_manager::getAlerts;
      srv_alert_manager (db *);

      unsigned int getPingAlerts (unsigned int, bool, values_map &);
      unsigned int getServiceAlerts (unsigned int, bool, values_map &);

    protected:
      using alert_manager::createAlertHandler;

      enum service_type { ePingAlert, eServiceAlert };
      unsigned int getAlerts (unsigned int, bool, service_type, values_map &);
      alerthandler *createAlertHandler (const alert_map::iterator &, bool, service_type);

      virtual void loadStaticData ();

      virtual void getReferenceTable (std::string &s)
      {
        s = "servers";
      }

      virtual void getOverName (std::string &s)
      {
        s = "SRV_LATENCY_ABOVE_THRESHOLD";
      }

      virtual void getBelowName (std::string &s)
      {
        s = "SRV_LATENCY_BELOW_THRESHOLD";
      }

      virtual void getUpName (std::string &s)
      {
        s = "SRV_SERVICE_UP";
      }

      virtual void getDownName (std::string &s)
      {
        s = "SRV_SERVICE_DOWN";
      }

      void getNoPingName (std::string &s)
      {
        s = "SRV_NO_PING";
      }

      void getGotPingName (std::string &s)
      {
        s = "SRV_GOT_PING";
      }

      // these are specific for srvmond alerts
      std::string m_noPingSubject;
      std::string m_gotPingSubject;
      std::string m_noPingTemplate;
      std::string m_gotPingTemplate;
  };
}

#endif // __SRV_ALERT_MANAGER__
