#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <fcntl.h>
#include <event.h>

#include "checker.h"

#define MAXPACKET        4096
#define SIZE_ICMP_HDR    8
#define SIZE_TIME_DATA   8
#define DEF_DATALEN      56

namespace netmon
{
  srvchecker::servicestatus::servicestatus (const servicedata &data)
  {
    m_port = data.m_port;
    m_ip = data.m_ip;
    m_serverName = data.m_serverName;
    m_id = data.m_id;
    m_up = data.m_up;
    m_logtimeout = data.m_logtimeout;
    m_interval = data.m_interval;
    m_latency = 0;
    m_isok = false;
    m_timeout = data.m_timeout;
    m_timestamp = data.m_timestamp;
    m_err = 0;

    bzero ((void *) &(m_srvaddr), sizeof (m_srvaddr));
    m_srvaddr.sin_family = AF_INET;
    if (inet_aton (data.m_ip.c_str(), &(m_srvaddr.sin_addr)) == 0)
    {
      throw (errno);
      return;
    }
  }

  srvchecker::srvchecker ()
    : m_sequence (0)
  {
    init ();
  }

  srvchecker::srvchecker (const servicelist &srvlist)
    : m_sequence (0)
  {
    init ();
    addServices (srvlist);
  }

  srvchecker::~srvchecker ()
  {
    for (sockmap::iterator it = m_socks.begin (); it != m_socks.end (); ++it)
    {
      delete it->second;
      close (it->first);
    }
    m_socks.clear ();
  }

  void srvchecker::check (int sec /* = 5 */, int usec /* = 0 */)
  {
    unsigned int size = m_socks.size ();
    if (size == 0)
      return;
    sendRequests ();

    timeval timeout;

    timeout.tv_sec = sec; // 5 seconds timeout
    timeout.tv_usec = usec;

    poll (&timeout);
  }

  void srvchecker::store ()
  {
    for (sockmap::iterator it = m_socks.begin (); it != m_socks.end (); ++it)
    {
      printf ("%s ", it->second->m_ip.c_str());
      if (it->second->m_isok == false)
        printf ("error: %s\n", strerror (it->second->m_err));
      else
      {
        if (it->second->m_port == -1)
          printf ("ping time ");
        else
          printf ("port %d, latency ", it->second->m_port);
        printf ("%d\n", it->second->m_latency);
      }
    }
    purge ();
  }

  void srvchecker::init ()
  {
    event_init ();
    protoent *proto;

    if ((proto = getprotobyname ("icmp")) == NULL)
      m_ICMPprotocol = 1; // assume it's 1
    else
      m_ICMPprotocol = proto->p_proto;
  }

  void srvchecker::addServices (const servicelist &srvlist)
  {
    for (servicelist::const_iterator it = srvlist.begin (); it != srvlist.end (); ++it)
      createSocket (*it);
  }

  void srvchecker::createSocket (const servicedata &data)
  {
    int sockfd;
//printf ("in create socket\n");
    servicestatus *ss = new servicestatus (data);

    if (data.m_port == -1) // ICMP
    {
      /* Open a RAW socket */
      if ((sockfd = socket (AF_INET, SOCK_RAW, m_ICMPprotocol)) < 0)
      {
        delete ss;
        throw (errno);
      }
    }
    else
    {
      ss->m_srvaddr.sin_port = htons (data.m_port);
      if ((sockfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)
      {
        delete ss;
        throw (errno);
      }
    }

    installSockFlags (sockfd);

    if (data.m_port == -1) // ICMP
      event_set (&(ss->m_event), sockfd, EV_READ | EV_PERSIST, handleICMP, this);
    else
      event_set (&(ss->m_event), sockfd, EV_READ | EV_WRITE | EV_PERSIST, handleConnect, this);

    event_add (&(ss->m_event), NULL);

    m_socks[sockfd] = ss;
  }

  void srvchecker::installSockFlags (int sockfd)
  {
    int flags;
    flags = fcntl (sockfd, F_GETFL);
    flags |= O_NONBLOCK | O_ASYNC;
    fcntl (sockfd, F_SETFL, flags);
  }

  void srvchecker::purge ()
  {
  printf ("map size %d\n", (int) m_socks.size());
    std::list<sockmap::iterator> ilist;
    for (sockmap::iterator it = m_socks.begin (); it != m_socks.end (); ++it)
    {
    printf ("service id: %d ", it->second->m_id);
      if (it->second->m_timeout < 0)
        closeSocket (it);
      if (it->second->m_isok || it->second->m_timeout < 0)
      {
        if (it->second->m_isok)
          printf ("service ok? ");
        printf ("to delete %d\n", it->second->m_id);
        delete it->second;
        //m_socks.erase (it);
        ilist.push_back (it);
      }
    }
    for (std::list<sockmap::iterator>::iterator it = ilist.begin(); it != ilist.end(); ++it)
      m_socks.erase (*it);
  }

  void srvchecker::sendRequests ()
  {
    for (sockmap::iterator it = m_socks.begin (); it != m_socks.end (); ++it)
    {
      if (it->second->m_isok)
        continue;
      if (it->second->m_port != -1) // TCP port
      {
        if (connect (it->first, (sockaddr *) &(it->second->m_srvaddr), sizeof (it->second->m_srvaddr)) == 0)
        {
        printf ("first time connect!\n");
          it->second->m_isok = true;
          it->second->m_latency = 100;
          closeSocket (it);
        }
      }
      else
      {
        sendICMPRequest (it->first, (sockaddr *) &(it->second->m_srvaddr), sizeof (it->second->m_srvaddr));
      }
      gettimeofday (&(it->second->m_tvstart), 0);
    }
  }

  void srvchecker::poll (timeval *timeout)
  {
    event_loopexit (timeout);
    event_dispatch ();
  }

  void srvchecker::handleICMP (int fd, short flags, void *t)
  {
    srvchecker *c = reinterpret_cast<srvchecker *> (t);
    sockmap &map = c->m_socks;
    sockmap::iterator it = map.find (fd);
    if (it != map.end())
    {
      c->handleICMP (it);
    }
    else
      return;
  }

  void srvchecker::handleConnect (int fd, short flags, void *t)
  {
    srvchecker *c = reinterpret_cast<srvchecker *> (t);
    sockmap &map = c->m_socks;
    sockmap::iterator it = map.find (fd);
    if (it != map.end())
    {
      c->handleConnect (it);
    }
    else
      return;
  }

  void srvchecker::handleICMP (sockmap::iterator &it)
  {
    int len;
    u_char recvpack[MAXPACKET];
    bzero ((void *) recvpack, MAXPACKET);
    sockaddr_in from;
    int fromlen = sizeof (from);

    if ((len = recvfrom (it->first, recvpack, sizeof(recvpack), 0,
                  (struct sockaddr *) &from, (socklen_t *) &fromlen)) < 0) 
    {
      printf ("failed\n");
      return;
    }

    timeval tvend;
    gettimeofday (&tvend, 0);

    /* We got reply, check if it is OK */

    if (!ICMPOK ((char *) recvpack, len, &(it->second->m_srvaddr)))
    {
      return;
    }

    closeSocket (it);

    it->second->m_latency = (tvend.tv_sec - it->second->m_tvstart.tv_sec) * 1000000 +
                            (tvend.tv_usec - it->second->m_tvstart.tv_usec);
//printf ("setting flag to true for %d\n", it->second->m_id);
    it->second->m_isok = true;
//    printf ("icmp ok\n");
  }

  void srvchecker::handleConnect (sockmap::iterator &it)
  {
    int ret;
    int size = sizeof (int);
    timeval tvend;
    gettimeofday (&tvend, 0);

    if (getsockopt (it->first, SOL_SOCKET, SO_ERROR, (void *) &ret, (socklen_t *) &size) < 0)
    {
      printf ("getsockopt error (sock %d).\n", it->first);
      perror ("");
      return;
    }
//    printf ("getsockopt for sock %d: %d\n", it->first, ret);
    if (ret == 0) // no error, we're connected
    {
//    printf ("connected\n");
      it->second->m_latency = (tvend.tv_sec - it->second->m_tvstart.tv_sec) * 1000000 +
                             (tvend.tv_usec - it->second->m_tvstart.tv_usec);
      it->second->m_isok = true;
      closeSocket (it);
    }
    else
    {
      it->second->m_err = ret;
//      closeSocket (it);
      event_del (&(it->second->m_event));
    }
  }

  void srvchecker::closeSocket (const sockmap::iterator & it)
  {
    int flags;

    // before we close this socket we'll remove us from any further
    // notifications
    if (fcntl (it->first, F_GETFL, &flags) != -1)
    {
      flags &= ~O_ASYNC;
      fcntl (it->first, F_SETFL, flags);
    }
    close (it->first);
    event_del (&(it->second->m_event));
  }

  void srvchecker::sendICMPRequest (int sockfd, sockaddr *addr, int size)
  {
    struct icmp *icp;       /* ICMP header */
    u_char      *uptr;      /* start of user data */
    u_char      sendpack[MAXPACKET];

    icp = (struct icmp *) sendpack;   /* pointer to ICMP header */
    icp->icmp_type  = ICMP_ECHO;
    icp->icmp_code  = 0;
    icp->icmp_cksum = 0;
    icp->icmp_id    = getpid();
    icp->icmp_seq   = m_sequence++;

    /* Add the time stamp of when we sent it */

    gettimeofday ((struct timeval *) &sendpack[SIZE_ICMP_HDR], 0);

    /* Fill the remainder of the packet with the user data */

    uptr = &sendpack[SIZE_ICMP_HDR + SIZE_TIME_DATA];
    for (int i = SIZE_TIME_DATA; i < DEF_DATALEN; i++)
      *uptr++ = i;

    /* Compute and store ICMP checksum */

    icp->icmp_cksum = checksum ((u_short *) icp, DEF_DATALEN);

    /* Now send the datagram */
    int ret = sendto (sockfd, sendpack, DEF_DATALEN, 0, addr, size);

    if (ret < 0 || ret != DEF_DATALEN)
    {
      perror ("sendto");
      close(sockfd);
    }
  }

  int srvchecker::checksum (u_short *ptr, int nbytes)
  {
    long    sum = 0;                  /* assumes long == 32 bits */
    u_short oddbyte;
    u_short answer;	              /* assumes u_short == 16 bits */

    while (nbytes > 1)
    {
      sum += *ptr++;
      nbytes -= 2;
    }

    /* mop up an odd byte, if necessary */
    if (nbytes == 1)
    {
      oddbyte = 0;                               /* make sure top half is zero */
      *((u_char *) &oddbyte) = *(u_char *)ptr;   /* one byte only */
      sum += oddbyte;
    }

    sum  = (sum >> 16) + (sum & 0xffff);	       /* add high-16 to low-16 */
    sum += (sum >> 16);                          /* add carry */
    answer = ~sum;                               /* ones-complement, then truncate to 16 bits */

    return answer;
  }

  bool srvchecker::ICMPOK (char *buf, int size, sockaddr_in *in)
  {
    int  iphdrlen;
    ip   *ip;		/* ptr to IP header */
    icmp *icp;		/* ptr to ICMP header */

    /*
     * We have to look at the IP header, to get its length.
     * We also verify that what follows the IP header contains at
     * least an ICMP header (8 bytes minimum).
     */

    ip = (struct ip *) buf;
    iphdrlen = ip->ip_hl << 2;	/* convert # 16-bit words to #bytes */

    if (ip->ip_src.s_addr != in->sin_addr.s_addr)
      return false;

    if (size < iphdrlen + ICMP_MINLEN)
      return false;

    size -= iphdrlen;

    icp = (icmp *) (buf + iphdrlen);
    if ((icp->icmp_type != ICMP_ECHOREPLY) && (icp->icmp_type != ICMP_ECHO))
      return false;

    /*
     * See if we sent the packet, and if not, just ignore it.
     */

    if (icp->icmp_id != getpid())
      return false;

    return true;
  }

}

#ifdef __TEST__

int main ()
{
  using namespace netmon;
  servicelist l;
  l.push_back (servicedata (0, "127.0.0.1", 22, true));
  l.push_back (servicedata (1, "127.0.0.1", 23, true));
  l.push_back (servicedata (2, "10.0.2.218", -1, true));
  l.push_back (servicedata (3, "10.0.2.202", -1, true));
  l.push_back (servicedata (4, "10.0.2.27", -1, true));
  l.push_back (servicedata (5, "10.0.2.8", -1, true, 10));

  srvchecker s(l);
  s.check ();
  s.store ();

  l.clear ();
  l.push_back (servicedata (6, "10.0.2.26", 22, true));
  s.addServices (l);
  s.check ();
  s.store ();
}

#endif // __TEST__
