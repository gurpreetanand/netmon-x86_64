#include "syslogmond.h"
#include "syslogmonitor.h"

namespace netmon
{
  syslogmon_daemon::syslogmon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  syslogmon_daemon::~syslogmon_daemon ()
  {
    delete m_syslogMonitor;
  }

  void syslogmon_daemon::init ()
  {
    daemon::init();

    m_syslogMonitor = new syslog_monitor (m_db, this);
  }

  int syslogmon_daemon::run ()
  {
    m_syslogMonitor->run();

    return 0; // never reached
  }
}
