#include "syslogalertmgr.h"
#include "alerts.h"
#include "alertthrottler.h"

#include <sstream>
#include <pcrecpp.h>

namespace netmon
{
  unsigned int syslog_alert_manager::getAlerts (unsigned int id, unsigned int severity, const char *msg, values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    unsigned int cnt = 0;

    for (; i != end; ++i)
    {
      if (i->second->m_threshold < severity)
        continue;
      if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
        if (match (i->second->m_pattern.c_str(), msg))
        {
          alerthandler *h = createAlertHandler (i->second, false);
          getReferenceData (id, h->getMap());
          h->addKeywordValue (alerthandler::eSysLogMsg, msg);
          if (cnt != 0)
          {
            buf.str("");
            buf << std::endl << std::endl << "(Last message repeated " << cnt << " times)";
            h->getTemplate() += buf.str();
          }
          dispatchAlert (h, vmap);
          ++ret;
        }
    }
    return ret;
  }

  bool syslog_alert_manager::match (const char *pattern, const char *value)
  {
    if (value == 0 || strlen (value) == 0)
      return true;

    pcrecpp::RE_Options opt;
    opt.set_caseless (true);
    pcrecpp::RE ptrn (pattern, opt);
    if (ptrn.PartialMatch ((const char *) value))
      return true;
    return false;
  }
}
