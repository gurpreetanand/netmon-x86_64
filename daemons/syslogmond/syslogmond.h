#ifndef __SYSLOGMON_DAEMON__
#define __SYSLOGMON_DAEMON__

#include "daemon.h"

namespace netmon
{
  class syslog_monitor;

  class syslogmon_daemon : public daemon
  {
    public:
      syslogmon_daemon (int, char **);
      ~syslogmon_daemon ();

      virtual void init ();

      bool termSignaled ()
      {
        return isTermSignaled();
      }

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "syslogmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/syslogmond";
      }

      syslog_monitor *m_syslogMonitor;
  };
}

#endif //__SYSLOGMON_DAEMON__
