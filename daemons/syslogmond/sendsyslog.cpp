#include <syslog.h>

int main()
{
  openlog("Test syslog", LOG_PID, LOG_LOCAL4);

  syslog(LOG_DEBUG, "Test, ' test, test.");
  
  return 0;
}
