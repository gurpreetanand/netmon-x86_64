\documentclass{article}
\usepackage{tocloft}

\title {Netmon Daemons}
\author {Damir Ned\v zibovi\' c\\
e-mail: $<$damir@netmon.ca$>$\\
}

\begin{document}

\maketitle

\tableofcontents

\newpage

\section {arpmond}
\textbf {arpmond} is Netmon's auto discovery service - its main functionality
is to discover new devices on LANs and to pair IP addresses with MAC addresses.
A device is considered to be "new" if and only if its MAC address is new to
arpmond (i.e. not seen before).

On start up, arpmond will read the content of
\textbf {arptable} db table and create a memory cache from its content
(which is basically a map of MAC and IP addresses). arpmond will also
read the content of \textbf {localnets} db table and then will scan each
local net for new devices.
It does so by sending ARP requests to each IP address from local segments.
If a response is received from an IP address, arpmond will pair the MAC
address from the response with its IP address and save it in its memory cache.
If that MAC address is new, arpmond will insert the MAC-IP pair into
\textbf {arptable} db table and set \textbf {is\_new} flag to \textbf {true}
(which means that this device is newly discovered, since MAC addresses are 
unique).

At this point arpmond will dispatch alerts for new devices if any are
configured. Another possibility is that this MAC address is not new (i.e. it's
known by arpmond) but its corresponding IP address has changed; in that case
this pair will also be inserted into the db, but \textbf {is\_new} flag will
be set to \textbf {false} (this can happen when a device changes its IP
address , for instance when using DHCP).

All IP addresses found are sent by POSIX message queue to \textbf {resolvemond}
to be resolved by DNS and NetBIOS. Message queue ID is \textbf {0xff00caca}
and its format is represented by a C struct:

\vskip 0.5cm
\noindent
\verb+ struct message+\\
\verb+ {+\\
\verb+   long m_type;+\\
\verb+   unsigned long m_ip;+\\
\verb+ };+
\vskip 0.5cm

arpmond will set \textbf {m\_type} to \textbf {1} which will let
\textbf {resolvemond} know that this message comes from arpmond.


Scans are done continuously, i.e. arpmond starts over when it finishes a
scan, so new devices should be found in a couple of minutes as they appear
on the local net.

\section {dfmond}

\textbf {dfmond} is a partition monitor daemon which monitors UNIX and
UNIX-like servers and their exported partitions. Before dfmond can access
those partitions one needs to add \textbf {df} command to inetd on the target
server, so once dfmond connects to a given TCP port on the target server,
df command will be executed and its output will be read by dfmond. dfmond
will then parse the output and calculate the usage statistics from the data.

On start up, dfmond reads the content of \textbf {df\_servers} db table and
then scans each of the servers and partitions configured by Netmon user.
After each scan, df\_servers is updated with the current value of available
and used space on the target partition (or if the server or partition is not
available, field \textbf {message} is set to  error message.). The result of
each scan is also inserted into \textbf {df\_server\_log} db table. When
dfmond detects that available free space on the target partition is lower
than any threshold set in the alerts for a partition it will trigger an
alert. Alert is also triggered once the free space goes over the threshold
value.

\section {emailmond}

\textbf {emailmond} will dispatch e-mail pending alerts. Its only duty is to
monitor \textbf {alert\_pending} db table and once it detects that there are
new pending (e-mail) alerts it will try to dispatch the queued alert via the
user configured SMTP server. If the alert is successfully dispatched,
emailmond will set the \textbf {sent} bit in alert\_pending db table to
\textbf {1} to indicate that the alert has been dispatched. It will also
update the \textbf {dispatch\_timestamp}. If the alert has not been
dispatched, emailmond will increase the value of \textbf
{retries\_processed}. It will try to dispatch the alert for as many times as
is the value of \textbf {required\_retries} field in \textbf {alert\_handlers}
db table.

\section {netflowd}

\textbf {netflowd} is Netflow collector - it will receive netflow packets,
parse them and aggregate the collected data. Currently it supports v1, v5
and v7 flows (which netflow version is in question is determined by reading
the content of netflow header, as per netflow specification).
Its main duty is to aggregate data collected from netflow and store
it in the db and SHM segments.

Upon start, netflowd will read the content of \textbf {devices} and
\textbf {interfaces} db tables in order to create a simple cache of netflow
devices and their interfaces from which netflowd will collect data. Of course,
Netmon users need to configure their devices to export netflow data to Netmon
box before data can be collected. netflowd will also insert new devices into
\textbf {devices} db table if it starts to receive netflow data from the
device, but that device has not been configured in the GUI (in that case that
device will appear as 'new' in the GUI and Netmon user can enable netflow
for it). 

netflowd creates three types of data from the data it receives:

\begin {itemize}
\item Protocol break down data, which basically represents IP flows per each
of the protocols (i.e. web data which goes over port 80). This data is
periodically inserted into \textbf {protocol\_breakdown} db table.
\item Live data which is inserted into shared memory. For each interface
netflowd will create two SHM segments - one for the stream data (which
represents insight into ongoing IP connections) and the other for protocol
break down data. SHM keys start from \textbf {0xf4000006} and are increased
for each interface (the first interface will use key 0xf4000006 for stream
data and key 0xf4000007 for protocol break down data, second interface will
use key 0xf4000008 for stream data and so on).
\item Netflow data. This data is aggregated data from netflow (if that is
possible) and is basically inserted "as-is" into \textbf {netflow} db table.
\end {itemize}

\section {netmond}
Network sniffers in Netmon are implemented as simple plug ins which
are basically dlls (or .so files) with simple interface. To be more
precise, \textbf {netmond} is Netmon's network sniffer and also a plug in
manager. It will load all plug ins on start up and then it will collect network
packets from eth cards and redistribute them to each of the plug ins.
netmond plug ins:

\subsection {mod\_eth}
\textbf {mod\_eth} collects eth packets from the network and creates two
types of statistical data: stream conversations on eth level (i.e. between
the network hosts) and protocol statistics (i.e. how much data was
transferred via UDP protocol). Both are periodically inserted into the SHM
segments (every 60 seconds), where stream conversations are inserted into
SHM key \textbf {0xf5000000} and protocol statistics to SHM key
\textbf {0xf5000001}.

\subsection {mod\_http}
\textbf {mod\_http} analyzes HTTP network traffic and collects data on web
servers and clients (i.e. which documents were requested, what was their
size and content type). Collected data is periodically inserted into \textbf
{web\_traffic} db table (every 120 seconds) and live data is inserted into
SHM segments (every 20 seconds): top web servers into SHM key \textbf {0xf4000001} and top web
clients into SHM key {0xf4000002}.

\subsection {mod\_ip}
\textbf {mod\_ip} is used for analyzing the IP network protocol. Its primary
duty is to keep the track of ongoing IP connections (or streams)
between the hosts in the local network. mod\_ip also collects data on so
called protocol breakdown (which is basically a bandwidth usage across
different TCP/UDP ports).

Periodically, mod\_ip will inserted the aggregated (more precisely:
connections which are marked as 'finished' - IP flags FIN or RST are set, or
if the connection is long lasting, its 10 minute state is inserted) 
data into the db - connection details are inserted into \textbf {netflow} db
table (with \textbf {in\_iface} and \textbf {out\_iface} fields set to \textbf
{NULL}) while protocol data is inserted into \textbf {protocol\_breakdown}
db table. Data is inserted into the database every 60 seconds.
mod\_ip will also insert so called real time data into the SHM:
so called IP conversations (which are basically cumulative data transferred
between two hosts) are inserted into the segment with key \textbf {0xf4000000},
IP streams (data transferred between two IP ports on two hosts) into
\textbf {0xf4000003} and protocol breakdown into \textbf {0xf4000004}. This
live data is inserted into SHM segments every 20 seconds. 

mod\_ip will send each IP addresses found to POSIX message queue to
\textbf {resolvemond} to be resolved by DNS and NetBIOS. Message queue ID is
\textbf {0xff00caca} (the same queue used by arpmond, but \textbf {m\_type}
is set to \textbf {2}).

mod\_ip will dispatch protocol activity alerts when it discovers that there
is network traffic on user and alert defined ports.

\section {netscand}
\textbf {netscand} is Netmon's port scanner. It will periodically (once
every six hours) scan local network range for new open ports. On start up,
netscand will read the content of \textbf {scan\_log} db table and create
a cache of IP addresses and open ports. After each scan, netscand will update
its cache and if new open ports are found will insert those ports into
scan\_log. netscand will also trigger alerts for new open ports if any is
configured.

\section {pagermond}
\textbf {pagermond} will dispatch pager alerts. It works in a very similar
fashion to emailmond while the only difference being it dispatches alerts
via pager terminal.

\section {procmond}
\textbf {procmond} is the core process in Netmon. When the Netmon box is
booted, init.d script will launch procmond. Once started, procmond will
first check if the license is valid (by reading the content of \textbf
{netmon} db table) and if it is, it will launch all other Netmon's daemons.
The list of the daemons to be started is obtained from \textbf {daemons} db
table - each daemon that has its flag \textbf {start\_auto} set to \textbf
{true} will be started automatically.

procmond also creates a UNIX domain
socket \textbf {/var/run/.netmon.s} which is used by the front end to
communicate with procmond. This communication is very simple: client (in
this case the front end) send one line commands (terminated by the \ n sign)
and procmond returns the result and then closes the connection. Results
begin with "OK", which means that the command has been successfully
executed, or with "ERR" followed by the error message which, of course, means
that the command has failed. Commands are:

\begin {itemize}
\item start - start will start a daemon if it's not already started. For
example: "start netmond" will start netmond.
\item stop - stop stops a daemon if it's already started. For example: "stop
emailmond" will stop emailmond.
\item status - returns the status of all daemons and plug ins in the form of
an XML file.
\item restart - restarts a daemon. Basically procmond will stop and then
start the daemon.
\end {itemize}

\section {resolvemond}
\textbf {resolvemond} is Netmon's IP resolver. When started, resolvemond
reads the content of \textbf {hosts} db table and creates a memory cache of
IPs and their corresponding host names. All IPs sent by arpmond and
netmond end up in resolvemond's POSIX message queue, from which IPs are
retrieved and send to the DNS server to be resolved or, if the IP belongs to
the local network range, resolvemond will try to resolve it via NetBIOS
protocol. In order to prevent excessive look ups, resolvemond will re-request
a resolved name once every hour. If the IP has been successfully resolved,
resolvemond will update its memory cache and if there are any changes to be
made (for instance, the IP has new host name) the IP and its new host name
will be inserted into hosts db table.

\section {smbmond}
Just as dfmond monitors UNIX partitions, \textbf {smbmond} monitors windows
shares for disk usage. smbmond works in the same manner as dfmond, the only
difference is that it reads server info from \textbf {smb\_servers} and uses
SMB protocol to access the partition info on windows shares.

\section {oidmond}
\textbf {oidmond} is Netmon's SNMP OID tracker daemon. oidmond will read the
content of \textbf {oids} and \textbf {devices} db tables and use SNMP get
method to retrieve the values of OIDs which are being tracked. If the value
of the OID is below (or the value doesn't match the threshold value -
according to the matching operator configured in the alert) the threshold,
oidmond will dispatch an alert. After each scan, the current OID value is
inserted into \textbf {oid\_log} db table (that is, only if \textbf
{enable\_logging} flag in devices is set to \textbf {true}) and oids table
updated with the current OID value.

\section {snmpautod}
\textbf {snmpautod} is SNMP auto discovery daemon - it will try to detect
new SNMP agents on the local network segment. It reads the content of
\textbf {localnets} db table and then sends SNMP broadcast requests to the
whole sub net and after it receives responses (if any) will send SNMP get
request to each of the IPs from the local network. Each IP address from
which it received a response will be inserted into \textbf {devices} db
table (if the IP is not already inserted, of course).

\section {snmpproxyd}
\textbf {snmpproxyd} is a wrapper around net-snmp lib which acts like a
proxy for all SNMP get and walk requests. When started, snmpproxyd will
create a UNIX domain socket \textbf {/var/run/.snmp\_proxy.s} by which all
communication between the replacement utilities \textbf {snmpget} and
\textbf {snmpwalk} goes. The replacement utilities are invoked by the front
end in the same manner as ordinary tools from net-snmp package and they send
all command line switches to snmpproxyd via the mentioned socket. snmpproxyd
parses each command and then sends SNMP requests to the destination IP
address. Responses are returned to the replacement utilities via the same
socket and in the same manner as they are received.

\section {snmptrapd}
\textbf {snmptrapd} is Netmon's SNMP trap receiver. It opens UDP port 162
(the default SNMP trap port), binds to it and listens to incoming SNMP
traps. It will read the content of \textbf {snmpoids} db table and create a
memory cache of IP addresses and SNMP OIDs (which are in fast SNMP trap
OIDs) which is updated as new SNMP traps (either from different IPs or traps
with different SNMP OID) are received.

If a new type of SNMP trap is received it will be inserted into the same db
table. SNMP traps which are flagged to be logged into the db are inserted into
\textbf {snmptrap\_log} and their content (trap variables) are inserted into
\textbf {snmptrapoids} db table. snmptrapd will also dispatch alerts if a
received trap matches the alert's threshold.

\section {srvmond}
\textbf {srvmond} is Netmon's service monitor. It will monitor predefined
hosts for ICMP responses or openness of TCP ports (in other words in will
monitor whether a service which listens on a given TCP ports still works).
A list of servers which need to be checked is read from \textbf {servers} db
table, servers are checked and servers table is updated will the result of
the check. If the latency is greater than the value of \textbf
{log\_timeout} field, the result is also logged into \textbf {server\_log}
db table. srvmond will dispatch alerts if the service is down (or ICMP
response hasn't been received) or its latency is greater than the value of
alert threshold.

\section {syslogmond}
\textbf {syslogmond} is Netmon's syslog daemon. In other words, syslogmond
will collect UNIX syslog messages and store them into \textbf {syslog} db
table. On start up, syslogmond will create a list of hosts which are
permitted to send their syslog messages to Netmon box by reading the content
of \textbf {syslog\_access} db table. Netmon user can define the facility
and severity of syslog messages which will be received and logged from each
host. Syslog messages that have different facility or lower severity will
not be logged into the db. syslogmond will dispatch alerts when a message
matches the facility and severity predefined by Netmon user.

\section {webmond}
\textbf {webmond} is HTTP server monitor; basically it connects to user
configured URLs, downloads the web page and checks whether its content
matches the predefined regular expression (web page is viewed as a text
document, so it's possible to match markup tags as well as document content).
webmond reads a list of URLs from \textbf {urls} db table and then performs
a check on each URL. If \textbf {enable\_logging} flag is set to
\textbf {true}, then each result will be logged into \textbf {url\_log} db
table. Alerts are dispatched when page content doesn't match the predefined
pattern.

\end{document}
