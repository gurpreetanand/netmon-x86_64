#ifndef __SCANNER__
#define __SCANNER__

#include <set>
#include <list>
#include <string>
#include <sstream>
#include <stdint.h>

#include "db.h"
#include "portscan.h"
#include "portlist.h"
#include "inetaddr.h"

namespace netmon
{
  struct hostentry
  {
    std::set<unsigned int> m_ports;
    time_t m_time;
    int m_id;
  };
}

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
typedef __gnu_cxx::hash_map <uint32_t, netmon::hostentry> scannedservers;
#else
#include <hash_map>
typedef std::hash_map <uint32_t, netmon::hostentry> scannedservers;
#endif 

namespace netmon
{
  typedef std::pair <std::string, std::string> localnet;
  typedef std::list <localnet> netlist;

  class scanner
  {
    public:
      scanner (db &);
      virtual ~scanner ();

      void scan (int &);
      void dump ();
      virtual void reload ();

    protected:
      void init ();
      bool scanHost (const inetaddr &);
      void checkNewPorts (const inetaddr &, const std::set<unsigned int> &);
      void getServers ();
      void getNetworks ();
      void logServer (uint32_t);
      void deleteServer (uint32_t);
      void removeOldServers ();
      virtual void queueAlert (const inetaddr &, const std::set<unsigned int> &) {}
      bool isAddressSeen (const inetaddr &);

      scannedservers m_servers;
      netlist m_networks;
      db &m_db;
      portlist m_plist;

      typedef std::pair <inetaddr, inetaddr> inet_pair;
      typedef std::list <inet_pair> inet_list;
      inet_list m_visitedNetworks;
  };
}

#endif // __SCANNER__
