#ifndef __PORT_LIST__
#define __PORT_LIST__

#include <string>
#include <istream>
#include <vector>

namespace netmon
{
  /*
   * Data collected from one line of well-know ports list is stored here
   */
  struct service
  {
    enum eproto {_eTCP, _eUDP};
    service (const char *name, unsigned int port, eproto proto)
     : m_name (name), m_port (port), m_proto (proto)
    {
    }
    std::string m_name;
    unsigned int m_port;
    eproto m_proto;
  };

  class portlist
  {
    public:
      portlist ();
      ~portlist ();

      /*
       * Read and parse a list of well-known ports. Eliminate duplicates
       */
      void addPorts (std::istream &);

      /*
       * Parse port or ports range from string
       */
      void addPorts (const char *);

      /*
       * Adds range of ports
       */
      void addPorts (int, int);
      void addPort (int);

    protected:
      typedef std::vector<service *> servicevec;
      servicevec m_portlist;
      friend class portscanner;
  };
}

#endif // __PORT_LIST__
