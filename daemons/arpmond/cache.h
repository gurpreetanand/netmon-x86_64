#ifndef __CACHE__
#define __CACHE__

#include <cstring>
#include <string>
#include <set>
#include <stdio.h>
#include <stdint.h>

namespace netmon
{
  struct MACKey
  {
    MACKey ()
    {
      memset (m_addr, 0, 6);
    }
    MACKey (uint8_t *mac)
    {
      memcpy (m_addr, mac, 6);
    }
    std::string format () const
    {
      char buf[18];
      snprintf (buf, 18, "%02X:%02X:%02X:%02X:%02X:%02X", m_addr[0], m_addr[1], m_addr[2], m_addr[3], m_addr[4], m_addr[5]);
      return std::string (buf);
    }
    uint8_t m_addr[6];
  };
  typedef MACKey MACAddr;

  struct MACEntry
  {
    MACEntry (uint8_t *mac, const char *ip)
    {
      memcpy (m_addr, mac, 6);
      m_IPs.insert (std::string (ip));
    }
    MACEntry (const MACAddr &mac, const char *ip)
    {
      memcpy (m_addr, mac.m_addr, 6);
      m_IPs.insert (std::string (ip));
    }
    uint8_t m_addr[6];
    std::set <std::string> m_IPs;
  };

  struct MACKeyEq
  {
    bool operator() (const MACKey &k1, const MACKey &k2) const
    {
      int i = memcmp ((void *) k1.m_addr, (void *) k2.m_addr, 6);
      return i == 0;
    }
  };
}

#if __GNUC__ && (((__GNUC__ >= 3) && (__GNUC_MINOR__ >= 2)) || (__GNUC__ >= 4))
#include <ext/hash_map>
using __gnu_cxx::hash_map;
#else
#include <hash_map>
using std::hash_map;
#endif

namespace netmon
{
  class cache
  {
    public:
      cache ();
      ~cache ();
      enum MACStatus { _eNew, _eNewIP, _eOld };
      MACStatus addMAC (u_char *, const char *);
      MACStatus addMAC (const MACAddr &, const char *);

    private:
      MACStatus addMACImpl (const MACAddr &, const char *);

    protected:
      struct MACHash
      {
        size_t operator() (const MACKey &__s) const
        {
          size_t tmp = 0;
          for (int i = 2; i < 6; ++i)
            tmp = (tmp << 8) | __s.m_addr[i];
          return tmp;
        }
      };

      typedef hash_map<MACKey, MACEntry *, MACHash, MACKeyEq> machash;
      machash m_hash;
  };
}

#endif // __CACHE__
