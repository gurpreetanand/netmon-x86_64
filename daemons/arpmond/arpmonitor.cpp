#include "arpquery.h"
#include "arpmonitor.h"
#include "localaddr.h"
#include "alerts.h"
#include "log.h"
#include "arpalertmngr.h"

#include <sstream>
#include <cstdio>
#include <errno.h>
#include <unistd.h>

/*
 * Reads MAC address in form of XX:XX:XX:XX:XX:XX
 * Returns raw bytes of address
 */
static void readMAC (const char *in, u_char *out)
{
  const char *p = in;
  bool first = true;
  int cnt = 0;
  u_char prev = 0;

  while (*p)
  {
    if (*p == ':')
    {
      ++p;
      continue;
    }
    u_char i = 0;
    if (*p >= 'a' && *p <= 'f')
    {
      i = *p - 'a' + 10;
    }
    else if (*p >= 'A' && *p <= 'F')
    {
      i = *p - 'A' + 10;
    }
    else
    {
      i = *p - '0';
    }
    if (first)
    {
      first = false;
      i <<= 4;
      prev = i;
      ++p;
      continue;
    }
    *(out + cnt) = i | prev;
    ++cnt;
    first = true;
    prev = 0;
    ++p;
  }
}

namespace netmon
{
  const unsigned int arpmonitor::m_maxPollIPs;

  arpmonitor::arpmonitor (db *mydb, const strlist &devices)
    : m_db (mydb), m_devices (devices), m_run (0)
  {
    init();
  }

  arpmonitor::~arpmonitor ()
  {
    m_db->close();
    delete m_db;
    delete m_alertManager;
  }

  void arpmonitor::run () throw (std::string)
  {
    ++m_run;
    m_alertManager->reload();
    if (m_networks.empty())
    {
      usleep (60);
      getNetworks();
      return;
    }

    for (netlist::iterator it = m_networks.begin(); it != m_networks.end(); ++it)
    {
      inetaddr from ((*it).first.c_str());
      inetaddr to ((*it).second.c_str());
      std::string dev;
      uint32_t localip = 0;

      for (std::list<localaddr>::iterator i = m_localaddrs.begin(); i != m_localaddrs.end(); ++i)
        if (from <= (*i).m_addr && (*i).m_addr <= to)
        {
          dev = (*i).m_device;
          localip = (*i).m_addr.iaddr();
          break;
        }

      if (dev.size() == 0)
      {
        dev = m_localaddrs.front().m_device;
        localip = m_localaddrs.front().m_addr.iaddr();
      }

      arpquery q (dev.c_str(), localip);

      if (from == to)
        return;

      std::list<std::string> ips;
      std::list<netmon::ip_mac> res;

      while (true)
      {
        ips.clear();
        for (unsigned int i = 0; i < m_maxPollIPs; ++i)
        {
          if (to < from)
            break;
          ips.push_back (from.addr());
          ++from;
        }
        if (ips.empty())
          break;
        res.clear();
        q.query (ips);
        q.poll (3);
        q.getResults (res);

        for (std::list<netmon::ip_mac>::iterator i = res.begin(); i != res.end(); ++i)
        {
          inetaddr ip ((*i).m_ip);
          updateCache ((*i).m_mac, ip); 
        }
      }
    }
    addLocalAddr();
  }

  void arpmonitor::updateCache (const MACAddr &mac, const inetaddr &from)
  {
    static std::ostringstream ss;
    cache::MACStatus status = m_cache.addMAC (mac, from.addr());

    if (status == cache::_eNew || status == cache::_eNewIP || m_run > 10)
    {
      uint32_t ip = from.iaddr();
      mq_send (m_queue, (const char *) &ip, sizeof (ip), 0);
      if (m_run > 10)
        m_run = 0;
    }

    if (status == cache::_eNew || status == cache::_eNewIP)
    {
      ss.str("");
      ss << "DELETE FROM ARPTABLE WHERE mac = '" << mac.format() << "'";
      m_db->execSQL (ss.str().c_str());
    }

    ss.str("");
    if (status == cache::_eNew)
    {
      ss << "INSERT INTO ARPTABLE (ip, mac, timestamp) VALUES ('"
         << from.addr() << "', '" << mac.format() << "', "
         << (int) time (0) << ")";
      log::instance()->add(_eLogDebug, ss.str().c_str());
      m_db->execSQL (ss.str().c_str());
      checkAlerts (mac, from);
    }  
    if (status == cache::_eNewIP)
    {
      ss << "INSERT INTO ARPTABLE (ip, mac, timestamp, is_new) VALUES ('"
         << from.addr() << "', '" << mac.format() << "', "
         << (int) time (0) << ", 'f')";
      log::instance()->add(_eLogDebug, ss.str().c_str());
      m_db->execSQL (ss.str().c_str());
    }
  }

  void arpmonitor::checkAlerts (const MACAddr &mac, const inetaddr &addr)
  {
    values_map vals;
    std::string dev;
    std::string iface;

    getDeviceAndInterface (mac, dev, iface);

    vals[alerthandler::eHostIP] = keyword_value (addr.addr());
    vals[alerthandler::eMACAddr] = keyword_value (mac.format());
    vals[alerthandler::eDevice] = keyword_value (dev);
    vals[alerthandler::eInterface] = keyword_value (iface);

    m_alertManager->getAlerts ((unsigned) -1, vals);
  }

  void arpmonitor::getDeviceAndInterface (const MACAddr &mac, std::string &dev, std::string &iface)
  {
    static std::ostringstream sql;

    sql.str("");
    sql << "select ip_address, label, interfaces.name, interface from interfaces, devices where mac = '"
        << mac.format() << "' and device_id = devices.id";

    PGresult *res = m_db->execQuery (sql.str().c_str());

    if (PQntuples (res) == 0)
    {
      dev = "N/A";
      iface = "N/A";
    }
    else
    {
      dev = std::string (PQgetvalue (res, 0, 1)) + " (" + std::string (PQgetvalue (res, 0, 0)) + ")";
      iface = std::string (PQgetvalue (res, 0, 2)) + " (" + std::string (PQgetvalue (res, 0, 3)) + ")";
    }
    PQclear (res);
  }

  void arpmonitor::addLocalAddr ()
  {
    for (std::list<localaddr>::iterator i = m_localaddrs.begin(); i != m_localaddrs.end(); ++i)
    {
      uint32_t ip = (*i).m_addr.iaddr();
      mq_send (m_queue, (const char *) &ip, sizeof (ip), 0);
      updateCache ((*i).m_mac, (*i).m_addr);
    }
  }

  void arpmonitor::getLocalAddresses ()
  {
    for (std::list<std::string>::iterator i = m_devices.begin(); i != m_devices.end(); ++i)
    {
      MACAddr mac;
      struct in_addr addr;

      if (getlocaladdr (0, (*i).c_str(), (u_char *) &addr.s_addr) == 0)
        if (getlocaladdr (1, (*i).c_str(), mac.m_addr) == 0)
          m_localaddrs.push_back (localaddr (*i, mac, addr));
    }
  }

  void arpmonitor::init ()
  {
    event_init();
    m_alertManager = new arp_alert_manager (m_db);
    m_alertManager->init();
    getNetworks();
    populate();
    getLocalAddresses();

    mq_attr attr;
    attr.mq_maxmsg = 16384;
    attr.mq_msgsize = sizeof (uint32_t);
    m_queue = mq_open ("/ipqueue", O_CREAT | O_NONBLOCK | O_WRONLY, 0777, &attr);
    if (m_queue == (mqd_t) -1)
    {
      perror ("mq_open");
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      log::instance()->add(_eLogFatal, strerror (errno));
    }
  }

  void arpmonitor::populate ()
  {
    PGresult *res = m_db->execQuery ("SELECT IP, MAC FROM ARPTABLE");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      u_char mac[6];
      ::readMAC (PQgetvalue (res, i, 1), mac);
      m_cache.addMAC (mac, PQgetvalue (res, i, 0));
    }
    PQclear (res);
  }

  void arpmonitor::getNetworks ()
  {
    PGresult *res = m_db->execQuery ("SELECT NETWORK, BROADCAST FROM LOCALNETS");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_networks.push_back (std::make_pair (std::string (PQgetvalue (res, i, 0)),
                                            std::string (PQgetvalue (res, i, 1))));
    }
    PQclear (res);
  }
}
