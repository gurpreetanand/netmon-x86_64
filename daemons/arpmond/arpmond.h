#ifndef __ARP_DAEMON__
#define __ARP_DAEMON__

#include "../lib/daemon.h"

namespace netmon
{
  class arpmonitor;

  class arpmon_daemon : public daemon
  {
    public:
      arpmon_daemon (int, char **);
      ~arpmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "arpmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/arpmond";
      }

      arpmonitor *m_arpMonitor;
  };
}

#endif //__ARP_DAEMON__
