#ifndef __LOCAL_ADDR__
#define __LOCAL_ADDR__

/*
 *  Gets local addresses: if type = 0, local IP will be returned in buf
 *  otherwise, local MAC addr is returned
 */
long getlocaladdr (u_char type, const char *ethdev, u_char *buf);

#endif // __LOCAL_ADDR__
