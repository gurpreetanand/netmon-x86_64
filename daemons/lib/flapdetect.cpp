#include "flapdetect.h"

namespace netmon
{
  void flapping_detector::addState (uint32_t id, bool up)
  {
    history_map_t::iterator i = m_history.find (id);
    if (i == m_history.end())
    {
      history_data data;
      data.addState (up);
      m_history[id] = data;
    }
    else
      i->second.addState (up);

    checkStatus (id);
  }

  flapping_detector::status flapping_detector::getStatus (uint32_t id)
  {
    history_map_t::iterator i = m_history.find (id);
    if (i == m_history.end())
      return eNotFlapping;
    return i->second.m_status;
  }

  bool flapping_detector::isFlapping (uint32_t id)
  {
    status tmp = getStatus (id);
    if (tmp == eStartedToFlap || tmp == eFlapping)
      return true;
      return false;
  }

  void flapping_detector::checkStatus (uint32_t id)
  {
    history_map_t::iterator i = m_history.find (id);

    float fr = calculateFlappingRatio (i->second);
    if (fr >= m_upper)
    {
      if (i->second.m_status == eNotFlapping)
        i->second.m_status = eStartedToFlap;
      else
        i->second.m_status = eFlapping;
      return;
    }
    if (i->second.m_status == eFlapping)
    {
      if (fr <= m_lower)
      {
        i->second.m_status = eNotFlapping;
        return;
      }
    }
  }

  float flapping_detector::calculateFlappingRatio (history_data &data)
  {
    if (data.m_size < history_data::eMaxHistoryData / 2)
      return 0.0;

    uint8_t size = 0;
    bool_list_t &list = data.m_list;
    bool_list_t::iterator i = list.begin();
    bool prev = *i;
    ++i;
    bool_list_t::iterator end = list.end();

    while (i != end)
    {
      if (prev != *i)
        ++size;
      prev = *i;
      ++i;
    }

    return (float) size / (float) data.m_size;
  }
}

#ifdef __TEST__

#include <iostream>

using namespace netmon;
using namespace std;

class test : public flapping_detector
{
  public:
    float getFlappintRatio (uint32_t id)
    {
      history_map_t::iterator i = m_history.find (id);
      if (i == m_history.end())
        return 0.0;
      print (i->second);
      return calculateFlappingRatio (i->second);
    }

  protected:
    void print (history_data &d)
    {
      for (bool_list_t::iterator i = d.m_list.begin(); i != d.m_list.end(); ++i)
        cout << *i << " ";
      cout << endl;
    }
};

void print (bool f)
{
  cout << (f == true ? "Flapping" : "Not Flapping") << endl;
}

void test1()
{
  cout << "test1()" << endl;
  flapping_detector f (0.4);
  for (int i = 0; i < 22; ++i)
    f.addState (0, i % 2 == 0);

  print (f.isFlapping (0));
}

void test2()
{
  cout << "test2()" << endl;

  flapping_detector f (0.4, 0.3);

  for (int i = 0; i < 22; ++i)
    f.addState (0, i % 2 == 0);

  print (f.isFlapping (0));

  for (int i = 0; i < 15; ++i)
    f.addState (0, true);

  print (f.isFlapping (0));
}

void test3()
{
  cout << "test3()" << endl; 
  test t;
  for (int i = 0; i < 22; ++i)
  {
    t.addState (0, i % 2 == 0);
    cout << t.getFlappintRatio (0) << endl;
  }

  cout << "Second round" << endl;
  for (int i = 0; i < 15; ++i)
  {
    t.addState (0, true);
    cout << t.getFlappintRatio (0) << endl;
  }
}

int main()
{
  test1();
  test2();
  test3();
  return 0;
}

#endif // __TEST__
