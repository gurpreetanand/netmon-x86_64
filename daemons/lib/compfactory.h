#ifndef __COMPARE_FACTORY__
#define __COMPARE_FACTORY__

#include <cstring>

namespace netmon
{
  template <typename T>
  class comparator
  {
    public:
      virtual ~comparator () {}
      virtual bool compare (const T &a, const T &b) const = 0;
  };

  template <typename T>
  class true_function : public comparator<T>
  {
    public:
      virtual bool compare (const T &, const T &) const
      {
        return true;
      }
  };

  template <typename T>
  class equal : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return (a == b);
      }
  };

  template <typename T>
  class not_equal : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return (a != b);
      }
  };

  template <typename T>
  class less : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return a < b;
      }
  };

  template <typename T>
  class greater : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return a > b;
      }
  };

  template <typename T>
  class greater_equal : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return a >= b;
      }
  };

  template <typename T>
  class less_equal : public comparator<T>
  {
    public:
      virtual bool compare (const T &a, const T &b) const
      {
        return a <= b;
      }
  };

  template <typename T>
  class compare_factory
  {
    public:
      typedef comparator<T> comparator_t;
      comparator_t *createComparator (const char *expr)
      {
        if (expr == 0 || strlen (expr) == 0)
          return new greater<T>();
        if (strcmp (expr, "<") == 0)
          return new less<T>();
        if (strcmp (expr, "<=") == 0)
          return new less_equal<T>();
        if (strcmp (expr, ">") == 0)
          return new greater<T>();
        if (strcmp (expr, ">=") == 0)
          return new greater_equal<T>();
        if (strcmp (expr, "==") == 0)
          return new equal<T>();
        if (strcmp (expr, "!=") == 0)
          return new not_equal<T>();
        return new true_function<T>();
      }
  };
}

#endif // __COMPARE_FACTORY__
