#ifndef __SIGNAL_H__
#define __SIGNAL_H__

#include <signal.h>
#include <sys/types.h>
#include <event.h>

namespace netmon
{
  class event_handler
  {
    public:
      virtual ~event_handler () {}
      virtual int handle_signal (int) = 0;
  };

  class signal_handler
  {
    public:
      static signal_handler *instance ();
      event_handler *register_handler (int, event_handler *);
      event_handler *register_handler_ev (int, event_handler *);
      void remove_handler (int);

    private:
      signal_handler ();

      static signal_handler *m_instance;
      static void dispatcher (int);
      static void ev_dispatcher (int, short, void *);
      static event_handler *m_handlers[NSIG];
      static event *m_events[NSIG];
  };
}

#endif // __SIGNAL_H__
