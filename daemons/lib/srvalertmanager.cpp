#include "srvalertmanager.h"
#include "condmanager.h"
#include "alertthrottler.h"

#include <sstream>

namespace netmon
{
  service_alert_manager::service_alert_manager (db *mydb, bool useUpDownAlerts /* = true */, bool useUpAlert /* = true */)
    : alert_manager (mydb, useUpDownAlerts), m_useUpAlert (useUpAlert)
  {
    m_condManager = new conditional_manager (mydb);
  }

  service_alert_manager::~service_alert_manager ()
  {
    delete m_condManager;
  }

  void service_alert_manager::reload ()
  {
    alert_manager::reload();
    m_condManager->reload();
  }

  void service_alert_manager::loadStaticData ()
  {
    alert_manager::loadStaticData();
    std::string tmpl;
    if (m_useUpAlert)
    {
      getUpName (tmpl);
      alert_manager::loadStaticData (tmpl, m_upTemplate, m_upSubject);
    }
    getDownName (tmpl);
    alert_manager::loadStaticData (tmpl, m_downTemplate, m_downSubject);
  }

  bool service_alert_manager::isAlertValid (alert_data *d)
  {
    return m_condManager->conditionalValid (d->m_conditionalID.c_str());
  }

  unsigned int service_alert_manager::getDownAlerts (unsigned int id, const values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i;

    i = m_alerts.find (id);

    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    unsigned int cnt = 0;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;
      if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
      {
        alerthandler *h = createDownAlertHandler (i);
        if (cnt != 0)
        {
          buf.str("");
          buf << std::endl << std::endl << "(Last message repeated " << cnt << " times)";
          h->getTemplate() += buf.str();
        }
        dispatchAlert (h, vmap);
        ++ret;
      }
    }
    return ret;
  }

  alerthandler *service_alert_manager::createDownAlertHandler (const alert_map::iterator &i)
  {
    alerthandler *h = createAlertHandler (i->second);

    h->setSubject (m_downSubject);
    h->setTemplate (m_downTemplate);

    return h;
  }
}
