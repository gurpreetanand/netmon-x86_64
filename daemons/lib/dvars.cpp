#include "dvars.h"
#include <cstring>
#include <cstdlib>

namespace netmon
{
  dvars::dvars (db &mydb, const char *dname)
    : m_db (mydb), m_dname (dname)
  {
    populate();
  }

  dvars::~dvars ()
  {
    m_vars.clear();
  }

  bool dvars::get (const char *key, std::string &val)
  {
    varmap::iterator it = m_vars.find (std::string(key));
    if (it == m_vars.end())
      return false;
    val = it->second;
    return true;
  }

  bool dvars::get (const char *key, int &val)
  {
    varmap::iterator it = m_vars.find (std::string(key));
    if (it == m_vars.end())
      return false;
    char *err = 0;
    int ret = strtol (it->second.c_str(), &err, 10);
    if (err != 0 && strlen (err) > 0)
      return false;
    val = ret;
    return true;
  }

  void dvars::reload ()
  {
    m_vars.clear();
    populate();
  }

  void dvars::populate ()
  {
    std::string sql("SELECT B.VAR, B.VALUE FROM DAEMONS A, DAEMONSCONFIG B WHERE A.NAME ='");
    sql += m_dname + "' AND B.DAEMON_ID = A.ID";

    PGresult *res = m_db.execQuery (sql.c_str());
    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_vars[std::string (PQgetvalue (res, i, 0))] = std::string (PQgetvalue (res, i, 1));
    }
    PQclear (res);
  }
}

#ifdef __TEST__

#include <cstdio>

int main ()
{
  using namespace netmon;

  try
  {
    db mdb;
    mdb.connect ("netmon35", "root");
    dvars v(mdb, "snmpautodiscoverd");
    std::string val;
    int port;
    if (v.get ("port", port))
      printf ("port: %d\n", port);
    if (v.get ("community", val))
      printf ("community: %s\n", val.c_str());
  }
  catch (db_exception &e)
  {
    printf ("error: %s\n", e.what());
    return -1;
  }
}

#endif
