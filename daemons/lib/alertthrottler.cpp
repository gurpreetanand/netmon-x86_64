#include "alertthrottler.h"

namespace netmon
{
  alert_throttler::alert_throttler ()
  {
  }

  alert_throttler::~alert_throttler ()
  {
    clear ();
  }

  void alert_throttler::clear ()
  {
    for (throttle_map::iterator i = m_tmap.begin(); i != m_tmap.end(); ++i)
      delete i->second;
    m_tmap.clear ();
  }

  void alert_throttler::updateAlert (unsigned int id, unsigned int interval)
  {
    throttle_map::iterator i = m_tmap.find (id);
    if (i != m_tmap.end())
    {
      i->second->m_interval = interval;
      return;
    }
    m_tmap[id] = new alert_data (interval);
  }

  bool alert_throttler::toTrigger (unsigned int id, const values_map &vmap, unsigned int &cnt)
  {
    throttle_map::iterator i = m_tmap.find (id);
    if (i == m_tmap.end())
      return true; // we haven't been notified about this alert, dispatch it

    unsigned int now = (unsigned int) time (0);

    values_list::iterator vit = findValues (i, vmap);
    if (vit == i->second->m_values.end()) // we don't have this set of values, dispatch the alert
    {
      alert_values *v = new alert_values;
      v->m_values = vmap;
      v->m_timestamp = now;
      i->second->m_values.push_back (v);
      return true;
    }

    if ((*vit)->m_timestamp == 0)
    {
      (*vit)->m_timestamp = now;
      return true; // this is the first time we see this alert
    }

    if ((now - (*vit)->m_timestamp) >= i->second->m_interval)
    {
      (*vit)->m_timestamp = now;
      cnt = (*vit)->m_counter;
      (*vit)->m_counter = 0;
//      delete *vit;
//      i->second->m_values.erase(vit);
      return true; // throttling interval has passed
    }

    (*vit)->m_counter++;
    return false;
  }

  void alert_throttler::removeOldEntries ()
  {
    values_list::iterator j, k;
    unsigned int now = (unsigned int) time(0);

    for (throttle_map::iterator i = m_tmap.begin(); i != m_tmap.end(); ++i)
    {
      k = i->second->m_values.end();
      for (j = i->second->m_values.begin(); j != k; )
      {
        if (((now - (*j)->m_timestamp) >= i->second->m_interval) && (*j)->m_counter == 0)
        {
          delete *j;
          j = i->second->m_values.erase(j);
        }
        else
          ++j;
      }
    }
  }

  values_list::iterator alert_throttler::findValues (throttle_map::iterator i, const values_map &vmap)
  {
    values_list &l = i->second->m_values;
    values_list::iterator j = l.begin();
    values_list::iterator end = l.end();

    for (; j != end; ++j)
    {
      values_map &m = (*j)->m_values;
      if (m == vmap)
        return j;
    }
    return end;
  }
}
