#include "serial.h"
#include "exception.h"
#include "log.h"
#include "db.h"

#include <pcrecpp.h>
#include <openssl/md5.h>

namespace netmon
{
  serial_number::serial_number (db *mydb)
    : m_db (mydb), m_firstDev (true), m_devLimit (-1), m_timeStamp (0)
  {
    init();
  }

  serial_number::~serial_number ()
  {
  }

  bool serial_number::valid ()
  {
    PGresult *res = 0;

    try
    {
      res = m_db->execQuery ("SELECT registration_key, devices, activation_key, is_trial, expires, EXTRACT('epoch' FROM expires) FROM NETMON");
    }
    catch (netmon::db_exception &e)
    {
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      fprintf (stderr, "%s\n", e.what());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
      PQclear (res);
      return false;
    }

    MD5_CTX ctx;
    MD5_Init (&ctx);
    MD5_Update (&ctx, (const void *) m_mac, 3);
    MD5_Update (&ctx, (const void *) PQgetvalue (res, 0, 0), strlen (PQgetvalue (res, 0, 0)));
    MD5_Update (&ctx, (const void *) (m_mac + 3), 1);
    MD5_Update (&ctx, (const void *) PQgetvalue (res, 0, 1), strlen (PQgetvalue (res, 0, 1)));
    MD5_Update (&ctx, (const void *) PQgetvalue (res, 0, 3), strlen (PQgetvalue (res, 0, 3)));
    if (*PQgetvalue (res, 0, 3) == '1')
      MD5_Update (&ctx, (const void *) PQgetvalue (res, 0, 4), strlen (PQgetvalue (res, 0, 4)));
    MD5_Update (&ctx, (const void *) (m_mac + 4), 2);

    unsigned char final[16];
    char hex[32];

    MD5_Final (final, &ctx);

    for (int i = 0; i < 16; ++i)
      sprintf (hex + i * 2, "%02x", final[i]);

    if (memcmp ((void *) hex, (void *) PQgetvalue (res, 0, 2), 32) == 0)
    {
      m_devLimit = atoi (PQgetvalue (res, 0, 1));
      m_timeStamp = atoi (PQgetvalue (res, 0, 5));
      bool is_trial = *PQgetvalue (res, 0, 3) == '1';
      PQclear (res);
      if (is_trial && m_timeStamp <= (uint32_t) time (0)) // trial
      {
        printf ("Serial number has expired.\n");
        netmon::log::instance()->add(netmon::_eLogFatal, "Serial number has expired.");
        return false;
      }
      return true;
    }
    else
    {
      PQclear (res);
      printf ("Invalid serial number.But we bypass this condition for now\n");
      //added by sameer on 28 Dec
      m_devLimit = 1000; // hardcoded value to bypass invalid serial
      netmon::log::instance()->add(netmon::_eLogFatal, "Invalid serial number. But we bypass this condition for now");
      return true;
      // return false;
    }
  }

  void serial_number::init ()
  {
    intf_t *intf;
    if ((intf = intf_open()) == 0)
      throw netmon_exception ("Cannot open network interfaces");
    intf_loop (intf, ifCallback, this);
    intf_close (intf);
  }

  void serial_number::checkInterface (const intf_entry *e)
  {
    if (e->intf_type == INTF_TYPE_LOOPBACK ||  // we don't need loopback interfaces
        e->intf_link_addr.addr_type != ADDR_TYPE_ETH || // or interfaces with no MAC address
       (e->intf_flags & INTF_FLAG_UP) == 0 ) // or interfaces which is not UP
      return;
    uint32_t i;
    pcrecpp::RE re ("eth(\\d+)");
    if (re.FullMatch (e->intf_name, &i))
    {
      if (m_firstDev || i < m_devIndex)
      {
        m_firstDev = false;
        m_devIndex = i;
        memcpy ((void *) m_mac, e->intf_link_addr.addr_eth.data, sizeof (m_mac));
        return;
      }
    }
    if (!m_firstDev) // we already have eth device
      return;
    pcrecpp::RE rew ("wlan(\\d+)");
    if (rew.FullMatch (e->intf_name, &i))
    {
      if (m_firstDev || i < m_devIndex)
      {
        m_firstDev = false;
        m_devIndex = i;
        memcpy ((void *) m_mac, e->intf_link_addr.addr_eth.data, sizeof (m_mac));
        return;
      }
    }
  }

  int serial_number::ifCallback (const intf_entry *e, void *me)
  {
    serial_number *s = reinterpret_cast<serial_number *> (me);
    s->checkInterface (e);

    return 0;
  }
}
