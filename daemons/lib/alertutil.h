#ifndef __ALERT_UTIL__
#define __ALERT_UTIL__

#include "alerts.h"

#include <map>
#include <string>

namespace netmon
{
  struct keyword_value
  {
    enum type { _eInt, _eString, _eNull };

    keyword_value () {}
    keyword_value (int i)
      : m_int (i), m_type (_eInt)
    {}
    keyword_value (const std::string &s)
      : m_string (s), m_type (_eString)
    {}
    keyword_value (const char *s)
      : m_string (s), m_type (_eString)
    {}
    int m_int;
    std::string m_string;
    type m_type;

    friend bool operator== (const keyword_value &a, const keyword_value &b)
    {
      if (a.m_type != b.m_type)
        return false;
      if (a.m_type == keyword_value::_eInt)
        return (a.m_int == b.m_int);
      if (a.m_type == keyword_value::_eString)
        return (a.m_string == b.m_string);
      return false;
    }
  };

  typedef std::map<alerthandler::TemplateKeywords, keyword_value> values_map;
}

#endif // __ALERT_UTIL__
