#include "shm.h"

#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <cstring>
  
namespace netmon
{
  shm::shm (int size, bool todestroy)
    : m_addr ((void *) -1), m_isAttached (false), m_toDestroy (todestroy)
  {
    m_id = create (IPC_PRIVATE, size, IPC_CREAT | 0600);
    if (m_id < 0)
      throw (errno);
  }

  shm::shm (key_t key, int size, int flags, bool todestroy)
    : m_addr ((void *) -1), m_isAttached (false), m_toDestroy (todestroy)
  {
    m_id = create (key, size, flags);
    if (m_id < 0)
      throw (errno);
  }

  shm::~shm ()
  {
    if (m_addr != (void *) -1)
      ::shmdt (m_addr);
    if (m_toDestroy)
    {
      shmid_ds dummy;
      ::shmctl (m_id, IPC_RMID, &dummy);
    }
  }

  int shm::create (key_t key, int size, int flags)
  {
    int id = ::shmget (key, size, flags);
    if (id < 0 && errno == EINVAL)
    {
      int old = ::shmget (key, 1, flags);
      shmid_ds dummy;
      ::shmctl (old, IPC_RMID, &dummy);
      id = ::shmget (key, size, flags);
    }
    return id;
  }

  char *shm::buffer ()
  {
    if (m_isAttached)
      return static_cast<char *> (m_addr);

    m_addr = ::shmat (m_id, 0, 0);
    if (m_addr == (void *) -1)
      return 0;

    m_isAttached = true;
    return static_cast<char *> (m_addr);
  }

  int shm::size ()
  {
    shmid_ds info;
    if (::shmctl (m_id, IPC_STAT, &info) == -1)
      return -1;
    return info.shm_segsz;
  }

  void shm::fill (unsigned char c /* = 0x00 */)
  {
    if (!m_isAttached)
      (void) buffer ();
    if (m_addr != (void *) -1)
      ::memset (m_addr, c, size ());
  }
}

#ifdef __TEST__

#include <cstdio>

using namespace netmon;

void readshm ()
{
  try
  {
    shm s (0x0000ceca, 500, IPC_CREAT | 0666);
    char *b = s.buffer ();
    while (*b) ++b;
    ++b;
    printf ("%s", b);
  }
  catch (int e)
  {
    errno = e;
    perror ("shm");
  }
}

int main ()
{
  try
  {
    shm s (0x0000ceca, 500, IPC_EXCL | IPC_CREAT | 0666);
    printf ("%d\n", s.size ());
    char *b = s.buffer ();
    int i = sprintf (b, "%s", "kurac");
    sprintf (b + i + 1, "picka");
    printf ("wrote %d bytes\n", i);
    readshm ();
  }
  catch (int e)
  {
    errno = e;
    perror ("shm");
  }
}

#endif // __TEST__
