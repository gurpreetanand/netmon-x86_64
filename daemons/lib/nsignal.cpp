#include "nsignal.h"

namespace netmon
{
  signal_handler *signal_handler::m_instance = 0;
  event_handler *signal_handler::m_handlers[NSIG];
  event *signal_handler::m_events[NSIG];

  signal_handler::signal_handler ()
  {
  }

  signal_handler *signal_handler::instance ()
  {
    if (m_instance == 0)
      m_instance = new signal_handler;
    return m_instance;
  }

  event_handler *signal_handler::register_handler (int sig, event_handler *handler)
  {
    if (sig < 0 || sig > NSIG)
      return 0;

    event_handler *old = m_handlers[sig];
    m_handlers[sig] = handler;

    struct sigaction sa;
    sa.sa_handler = signal_handler::dispatcher;
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction (sig, &sa, 0);

    return old;
  }

  event_handler *signal_handler::register_handler_ev (int sig, event_handler *handler)
  {
    if (sig < 0 || sig > NSIG)
      return 0;

    event_handler *old = m_handlers[sig];
    m_handlers[sig] = handler;

    m_events[sig] = new event;
    event_set (m_events[sig], SIGTERM, EV_SIGNAL | EV_PERSIST, ev_dispatcher, m_events[sig]);
    event_add (m_events[sig], 0);

    return old;
  }

  void signal_handler::remove_handler (int sig)
  {
    m_handlers[sig] = 0;
  }

  void signal_handler::dispatcher (int sig)
  {
    if (m_handlers[sig] != 0)
      signal_handler::m_handlers[sig]->handle_signal (sig);
  }

  void signal_handler::ev_dispatcher (int, short, void *arg)
  {
    int i = EVENT_SIGNAL ((event *) arg);
    if (m_handlers[i] != 0)
      signal_handler::m_handlers[i]->handle_signal (i);
  }
}

#ifdef __TEST__

#include <cstdio>
#include <unistd.h>

using namespace netmon;

class sigterm_handler : public event_handler
{
  public:
    virtual int handle_signal (int)
    {
      printf ("Signal received\n");
    }
};

int main()
{
  sigterm_handler h;
  signal_handler::instance()->register_handler (SIGTERM, &h);
  for (int i = 0; i < 20; ++i)
    sleep (1);
}

#endif
