#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/if.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "netmon.h"
#include "log.h"

#include <sstream>

void installSigHandler (void (*sighndl)(int))
{
  struct sigaction action;

  action.sa_handler = SIG_IGN;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0;

  sigaction(SIGCHLD, &action, NULL);

  action.sa_handler = sighndl;
        
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGUSR1, &action, NULL);
  sigaction(SIGUSR2, &action, NULL);
  sigaction(SIGHUP,  &action, NULL);
  sigaction(SIGALRM, &action, NULL);

  /* We don't expect these signals, but want to know if they occur */
  sigaction(SIGINT,    &action, NULL);
  sigaction(SIGPIPE,   &action, NULL);
  sigaction(SIGPOLL,   &action, NULL);
  sigaction(SIGPROF,   &action, NULL);
  sigaction(SIGVTALRM, &action, NULL);
  sigaction(SIGSTKFLT, &action, NULL);
  sigaction(SIGPWR,    &action, NULL);
  sigaction(SIGWINCH,  &action, NULL);
  sigaction(SIGURG,    &action, NULL);
  sigaction(SIGTSTP,   &action, NULL);
  sigaction(SIGTTIN,   &action, NULL);
  sigaction(SIGTTOU,   &action, NULL);
  sigaction(SIGSTOP,   &action, NULL);
  sigaction(SIGSTOP,   &action, NULL);
  sigaction(SIGCONT,   &action, NULL);
  sigaction(SIGABRT,   &action, NULL);
  sigaction(SIGFPE,    &action, NULL);
  sigaction(SIGILL,    &action, NULL);
  sigaction(SIGQUIT,   &action, NULL);
  sigaction(SIGSEGV,   &action, NULL);
  sigaction(SIGTRAP,   &action, NULL);
  sigaction(SIGSYS,    &action, NULL);
  sigaction(SIGBUS,    &action, NULL);
  sigaction(SIGXCPU,   &action, NULL);
  sigaction(SIGXFSZ,   &action, NULL);
}

int speedtostr (char *buf, int size, unsigned long long bytes, int tstamp)
{
  unsigned long long speedbps;
  double dspeedbps;

  speedbps = (bytes * 8) / tstamp;

  if (speedbps < 1024)
    return snprintf (buf, size, "%Ld bps\n", speedbps);

  if (speedbps < 1048576)
  {
    speedbps = speedbps / 1024;
    return snprintf (buf, size, "%Ld Kbps\n", speedbps);
  }   

  if (speedbps < 1073741824)
  {
    dspeedbps  = ((double) speedbps) / 1048576;
    return snprintf (buf, size, "%.2f Mbps\n", dspeedbps);
  }   

  dspeedbps = ((double) speedbps) / 1073741824;
  return snprintf (buf, size, "%.2f Gbps\n", dspeedbps);
}

int daemonize ()
{
  pid_t pid;

  if ((pid = fork ()) < 0)
    return -1;
  else 
    if (pid != 0)
      exit (0); // terminate parent

  setsid ();

  chdir ("/");

  umask (0);

  int fd = open ("/dev/null", O_RDWR);

  if (fd < 0)
    return -1;

  close (STDIN_FILENO);
  close (STDOUT_FILENO);
  close (STDERR_FILENO);

  dup2 (fd, STDIN_FILENO);
  dup2 (fd, STDOUT_FILENO);
  dup2 (fd, STDERR_FILENO);

  return 0;
}
