#include "arrayutil.h"

#include <pcrecpp.h>

namespace netmon
{
  namespace util
  {
    std::string createArray (const std::set<unsigned int> &myset) 
    {
      static std::ostringstream ss;
      bool isfirst = true;

      ss.str("");
      ss << "{";
      for (std::set<unsigned int>::iterator it = myset.begin (); it != myset.end (); ++it)
      {
        if (!isfirst)
          ss << ",";
        ss << *it;
        isfirst = false;
      }
      ss << "}";
      return ss.str ();
    }

    /*
     * Parse pg's arrays in format {i1,i2,i3}
     */
    void parseArray (const std::string &str, std::set<unsigned int> &myset)
    {
      if (str.size () < 2)
        return;

      std::string tmp = str;
      replace (tmp.begin () + 1, tmp.end () - 1, ',', ' ');
      std::istringstream s(std::string(tmp, 1, tmp.size() - 2));
      unsigned int i;

      while (!s.eof ())
      {
        s >> i;
        myset.insert (i);
      }
    }

    std::string create2DArray (const d2_list &mylist)
    {
      static std::ostringstream ss;
      bool isfirst = true;

      ss.str("");
      ss << "{";
      for (d2_list::const_iterator it = mylist.begin (); it != mylist.end (); ++it)
      {
        if (!isfirst)
          ss << ",";
        ss << "{" << (*it).first << ", " << (*it).second << "}";
        isfirst = false;
      }
      ss << "}";
      return ss.str ();
    }

    void parse2DArray (const std::string &str, d2_list &mylist)
    {
      static pcrecpp::RE re("\\{?\\s*\\{\\s*(\\d+)\\s*\\,\\s*(\\d+)\\s*\\}\\,?\\s*\\}?");
      pcrecpp::StringPiece input(str);

      int v1, v2;
      while (re.Consume(&input, &v1, &v2))
        mylist.push_back (std::make_pair (v1, v2));
    }
  }
}

#ifdef __TEST__

#include <cstdio>

using namespace std;
using namespace netmon;

void test1d()
{
  set<unsigned int> s;
  s.insert (10);
  s.insert (20);
  s.insert (30);
  s.insert (40);
  string str = createArray (s);
  printf ("%s\n", str.c_str());
  s.clear();
  parseArray (str, s);
  for (set<unsigned int>::iterator i = s.begin(); i != s.end(); ++i)
    printf ("%d\n", *i);
}

int main()
{
//  test1d();
//  return 0;
  d2_list l;
  l.push_back(make_pair(10, 20));
  l.push_back(make_pair(20, 30));
  l.push_back(make_pair(30, 40));
  l.push_back(make_pair(40, 50));
  string s = create2DArray (l);
  printf ("%s\n", s.c_str());

  d2_list j;
  for (int i = 0; i < 10000; ++i)
    parse2DArray (s, j);
//  for (d2_list::iterator it = j.begin(); it != j.end(); ++it)
//    printf ("%d %d\n", (*it).first, (*it).second);
  return 0;
}

#endif // __TEST__
