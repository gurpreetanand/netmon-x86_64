#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdarg>
#include <ctime>
#include <string>
//added by sameer on 12/12/18
#include <fstream>
#include <stdlib.h>
// end of update

#include "log.h"

namespace netmon
{
  log *log::m_instance = 0;

  log::log ()
    : m_init (false)
  {}

  log::~log ()
  {
    if (m_init)
    {
      fflush (m_log);
      fclose (m_log);
    }
  }

  log *log::instance ()
  {
    if (m_instance == 0) {
      m_instance = new log;
     // added by sameer on 14/12/18
      // std::ofstream file("/var/log/netmonlog.log",std::fstream::out | std::fstream::app);
      // system("chmod 777 /var/log/netmonlog.log");
      // m_instance->init("/var/log/netmonlog.log",_eLogDebug);
     // end of update
    }
    return m_instance;
  }

  void log::init (const char *name, loglevel l)
  {
    m_level = l;
    m_log = fopen (name, "a");
    if (m_log == 0)
      throw (errno);
    m_init = true;
  }

  void log::init (FILE *f, loglevel l)
  {
    m_log = f;
    m_level = l;
    m_init = true;
  }

  int log::add (loglevel l, const char *format, ...)
  {
    if (!m_init || l < m_level)
      return -1;

    time_t t = time(0);
    std::string stime = ctime (&t);
    stime.erase (stime.size() - 1);

    fprintf (m_log, "%s [%d] ", stime.c_str(), getpid ());
    printLevel (l);
    va_list v;
    va_start (v, format);
    int ret = vfprintf (m_log, format, v);
    fprintf (m_log, "\n");
    fflush (m_log);
    va_end (v);
    return ret;
  }

  void log::printLevel (loglevel l)
  {
    if (!m_init)
      return;

    switch (l)
    {
      case _eLogDebug:
        fprintf (m_log, "DEBUG: ");
        break;
      case _eLogInfo:
        fprintf (m_log, "INFO: ");
        break;
      case _eLogWarning:
        fprintf (m_log, "WARNING: ");
        break;
      case _eLogError:
        fprintf (m_log, "ERROR: ");
        break;
      case _eLogFatal:
        fprintf (m_log, "FATAL: ");
        break;
      case _eLogUrgent:
        fprintf (m_log, "URGENT: ");
        break;
    }
  }  

  loglevel llevel (int l)
  {
    switch (l)
    {
      case 0:
        return _eLogDebug;
      case 1:
        return _eLogInfo;
      case 2:
        return _eLogWarning;
      case 3:
        return _eLogError;
      case 4:
        return _eLogFatal;
      default:
        return _eLogDebug;
    }
  }
}

#ifdef __TEST__

int main ()
{
  using namespace netmon;
  log::instance()->init("test", _eLogDebug);
  log::instance()->add(_eLogDebug, "Sisaj %d kurca", 2);
  log::instance()->add(_eLogInfo, "Sisaj %d kurca", 3);
  log::instance()->add(_eLogWarning, "Sisaj %d kurca", 4);
  log::instance()->add(_eLogError, "Sisaj %d kuraca", 5);
  log::instance()->add(_eLogFatal, "Sisaj %d kuraca", 6);
}

#endif // __TEST__
