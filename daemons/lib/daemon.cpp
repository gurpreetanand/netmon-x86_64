#include "daemon.h"
#include "netmon.h"
#include "db.h"
#include "dvars.h"
#include "log.h"
#include "serial.h"
#include "hash.h"

#include <cstring>
#include <cstdlib>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>
#include <sys/resource.h>
#include <openssl/md5.h>
#include <iostream>

namespace netmon
{
  sig_atomic_t daemon::m_isTermSignaled = 0;
  daemon::daemon ()
    : m_toDaemon (true), m_init (false), m_argc (0)
  {
  }

  daemon::daemon (int argc, char **argv)
    : m_toDaemon (true), m_init (false), m_argc (argc), m_argv (argv)
  {
  }

  daemon::~daemon ()
  {
    m_db->close();
    delete m_db;
    delete m_vars;
  }

  void daemon::daemonize ()
  {
    if (!m_toDaemon)
      return;
    if (::daemonize() < 0)
      throw daemon_exc ("Cannot sink into background");
  }

  int daemon::start ()
  {
    if (!m_init)
      init();
    return run();
  }

  void daemon::init ()
  {
    std::cout<<"daemon init()"<<std::endl;
    installSignalHandler();
    if (m_init)
      return;
    m_init = true;

    netmon::log::instance()->init(getLogFileName(), _eLogDebug);
    if (m_argc > 0)
      parseOptions (m_argc, m_argv);
    std::cout<<"daemonizing now"<<std::endl;
    daemonize();
    signal_handler::instance()->register_handler (SIGTERM, &m_termHandler);

    setRLimits();

    char dbname[DBNAMELEN];
    char dbuser[DBUSERLEN];

    readConfig (dbname, dbuser);
    m_db = new db;
    m_db->connect (dbname, dbuser);

    m_vars = new dvars (*m_db, getDaemonName());
    int logl = 0; // this was set to 2. changed to 0 so that all logs are generated
    m_vars->get ("loglevel", logl); // no entry of loglevel for netmond in daemonsconfig

    netmon::log::instance()->logLevel() = netmon::llevel (logl);

    m_sn = new serial_number (m_db);
    if (!m_sn->valid())
      exit (1);
    m_devLimit = m_sn->devLimit();
  }

  void daemon::installSignalHandler ()
  {
    struct sigaction action;

    // install signal handler; ignore SIGCHLD

    action.sa_handler = SIG_IGN;
    sigemptyset (&action.sa_mask);
    action.sa_flags = 0;

    action.sa_flags = SA_NOCLDSTOP;
    sigaction (SIGCHLD, &action, NULL);
  }

  void daemon::usage ()
  {
    printf ("Usage: %s [-h] [-f]\n -f: run in foreground\n -h: gives this help\n", getDaemonName());
  }

  void daemon::parseOptions (int argc, char **argv)
  {
    int c;

    while ((c = getopt (argc, argv, getShortOptions())) != -1)
    {
      onOption (c);
    }
  }

  void daemon::setRLimits ()
  {
    struct rlimit r;
    getrlimit (RLIMIT_CORE, &r);
    r.rlim_cur = RLIM_INFINITY;
    setrlimit (RLIMIT_CORE, &r);
  }

  bool daemon::devLimitsValid ()
  {
    if (!checkSPH())
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Error 138");
      return false;
    }

    if (m_devLimit == 0)
      return true;

    try
    {
      PGresult *res = m_db->execQuery ("SELECT COUNT(*) FROM unique_hosts_on_ip");
      if (PQntuples (res) != 0)
      {
        if (atoi (PQgetvalue (res, 0, 0)) > m_devLimit)
        {
          PQclear (res);
          netmon::log::instance()->add(netmon::_eLogFatal, "There are more devices configured than allowed by this activation key!");
          return false;
        }
      }
      PQclear (res);
    }
    catch (db_exception &e)
    {
      netmon::log::instance()->add(netmon::_eLogError, e.what());
      netmon::log::instance()->add(netmon::_eLogUrgent, "list_unique_ips() query failed.");
      return true;
    }
    return true;
  }

  void daemon::onOption (char c)
  {
    switch (c)
    {
      case 'h':
        usage();
        exit (0);
        break;
      case 'f':
        m_toDaemon = false;
        break;
      case '?':
        exit (-1);
    }
  }

  const char *daemon::getShortOptions ()
  {
    return "hf";
  }

  bool daemon::checkSPH ()
  {
    PGresult *res = 0;
    try
    {
      res = m_db->execQuery ("select prosrc from pg_proc where proname = 'list_unique_ips'");
    }
    catch (netmon::db_exception &e)
    {
      fprintf (stderr, "%s\n", e.what());
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
      PQclear (res);
      return false;
    }

    MD5_CTX ctx;
    MD5_Init (&ctx);
    MD5_Update (&ctx, (const void *) PQgetvalue (res, 0, 0), strlen (PQgetvalue (res, 0, 0)));

    unsigned char final[16];
    char hex[32];

    MD5_Final (final, &ctx);

    for (int i = 0; i < 16; ++i)
      sprintf (hex + i * 2, "%02x", final[i]);

    bool ret = false;
    if (memcmp ((void *) hex, (void *) sp_hash, 32) == 0)
      ret = true;

    PQclear (res);
    return ret;
  }

  int daemon::daemon_term_handler::handle_signal (int sig)
  {
    daemon::m_isTermSignaled = 1;
    netmon::log::instance()->add(netmon::_eLogUrgent, "Killed by SIGTERM");

    return 0;
  }
}
