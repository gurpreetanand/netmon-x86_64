#ifndef __CONDITIONAL_MANAGER__
#define __CONDITIONAL_MANAGER__

#include <map>
#include <string>

namespace netmon
{
  class db;

  class conditional_manager
  {
    public:
      conditional_manager (db *, unsigned int = 60);
      ~conditional_manager ();

      void reload (bool = true);
      bool conditionalValid (int);
      bool conditionalValid (const char *);

    private:
      struct cond_data
      {
        std::string m_ip;
        unsigned int m_timeStamp;
      };

      typedef std::map<int, cond_data *> cond_map_t;

    protected:
      bool loadConditional (int);
      void clear ();

      db *m_db;
      unsigned int m_timeout;
      cond_map_t m_map;
  };
}

#endif // __CONDITIONAL_MANAGER__
