#include "alertmanager.h"
#include "alertthrottler.h"
#include "flapdetect.h"
#include "db.h"
#include "alerts.h"
#include "log.h"

#include <cstdlib>
#include <sstream>

namespace netmon
{
  alert_manager::alert_manager (db *mydb, bool useUpDownAlerts /* = true */)
    : m_db (mydb), m_useUpDownAlerts (useUpDownAlerts)
  {
    m_compFactory = new int_compare_factory;
    m_throttler = new alert_throttler;
    m_flapDetector = new flapping_detector;
  }

  alert_manager::~alert_manager ()
  {
    clearAlerts();
    delete m_compFactory;
    delete m_throttler;
    delete m_flapDetector;
  }

  void alert_manager::init ()
  {
    createAlertQuery();
    reload();
  }

  void alert_manager::reload ()
  {
    loadStaticData();
    clearAlerts();
    loadAlerts();
    m_throttler->removeOldEntries();
  }

  unsigned int alert_manager::getAlerts (unsigned int id, long long value, const values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    alerthandler *h;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;
      if (i->second->m_triggered && !i->second->m_comparator->compare(value, i->second->m_threshold))
      {
        m_flapDetector->addState (id, true);
        h = createAlertHandler (i->second, true);
        getReferenceData (id, h->getMap());
        dispatchAlert (h, vmap);
        toggleAlertFlag (i->second->m_triggerID);
        ++ret;
      }
      if (!i->second->m_triggered && i->second->m_comparator->compare(value, i->second->m_threshold))
      {
        unsigned cnt = 0;
        m_flapDetector->addState (id, false);
        if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
        {
          h = createAlertHandler (i->second, false);
          getReferenceData (id, h->getMap());
          if (cnt != 0)
            addThrottleMessage (h, cnt);
          if (m_flapDetector->getStatus (id) == flapping_detector::eStartedToFlap)
            addFlappingMessage (h);
          dispatchAlert (h, vmap);
          toggleAlertFlag (i->second->m_triggerID);
          ++ret;
        }
      }
    }

    return ret;
  }

  unsigned int alert_manager::getAlerts (unsigned int id, const char *value, const values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    alerthandler *h;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;
      if (i->second->m_triggered && match (i->second->m_pattern.c_str(), value))
      {
        m_flapDetector->addState (id, true);
        h = createAlertHandler (i->second, true);
        getReferenceData (id, h->getMap());
        dispatchAlert (h, vmap);
        toggleAlertFlag (i->second->m_triggerID);
        ++ret;
      }
      if (!i->second->m_triggered && !match (i->second->m_pattern.c_str(), value))
      {
        unsigned cnt = 0;
        m_flapDetector->addState (id, false);
        if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
        {
          alerthandler *h = createAlertHandler (i->second, false);
          getReferenceData (id, h->getMap());
          if (cnt != 0)
            addThrottleMessage (h, cnt);
          if (m_flapDetector->getStatus (id) == flapping_detector::eStartedToFlap)
            addFlappingMessage (h);
          dispatchAlert (h, vmap);
          toggleAlertFlag (i->second->m_triggerID);
          ++ret;
        }
      }
    }
    return ret;
  }

  unsigned int alert_manager::getAlerts (unsigned int id, const values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i;

    if (id != (unsigned) -1)
      i = m_alerts.find (id);
    else
      i = m_alerts.begin();

    if (i == m_alerts.end())
      return 0;

    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    unsigned int cnt = 0;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;
      if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
      {
        alerthandler *h = createAlertHandler (i->second, false);
        getReferenceData (id, h->getMap());
        if (cnt != 0)
          addThrottleMessage (h, cnt);
        if (m_flapDetector->getStatus (id) == flapping_detector::eStartedToFlap)
          addFlappingMessage (h);
        dispatchAlert (h, vmap);
        ++ret;
      }
    }
    return ret;
  }

  void alert_manager::dispatchAlert (alerthandler *h, const values_map &vmap)
  {
    for (values_map::const_iterator i = vmap.begin(); i != vmap.end(); ++i)
    {
      if (i->second.m_type == keyword_value::_eNull)
        continue;
      if (i->second.m_type == keyword_value::_eInt)
        h->addKeywordValue (i->first, i->second.m_int);
      else
        h->addKeywordValue (i->first, i->second.m_string.c_str());
    }
    h->insertAlert();
    delete h;
  }

  void alert_manager::toggleAlertFlag (unsigned int id)
  {
    static std::ostringstream sql;

    sql.str("");
    sql <<
    "update alert_triggers set triggered = not triggered where trigger_id ="
    << id;

    m_db->execSQL (sql.str().c_str());
  }

  alerthandler *alert_manager::createAlertHandler (const alert_data *d)
  {
    alerthandler *h = new alerthandler(m_db);
    time_t timestamp = time (0);

    h->setID (d->m_alertID);
    h->addKeywordValue (alerthandler::eLabel, d->m_label.c_str());
    h->addKeywordValue (alerthandler::eThreshold, d->m_threshold);
    h->addKeywordValue (alerthandler::ePattern, d->m_pattern.c_str());
    h->addKeywordValue (alerthandler::eTime, ctime(&timestamp), true);

    return h;
  }

  alerthandler *alert_manager::createAlertHandler (const alert_data *d, bool below)
  {
    alerthandler *h = createAlertHandler (d);
    h->setRecovery (below);
    if (below)
    {
      // doesn't match anymore
      h->setSubject (m_belowSubject.c_str());
      h->setTemplate (m_belowTemplate);
    }
    else
    {
      // matches, trigger an alert
      h->setSubject (m_overSubject.c_str());
      h->setTemplate (m_overTemplate);
    }
    return h;
  }

  void alert_manager::addThrottleMessage (alerthandler *h, int cnt)
  {
    static std::ostringstream buf;

    buf.str("");
    buf << std::endl << std::endl << "(Last message repeated " << cnt << " times)";
    h->getTemplate() += buf.str();
  }

  void alert_manager::addFlappingMessage (alerthandler *h)
  {
    static std::ostringstream buf;

    buf.str("");
    buf << std::endl << std::endl << "(Service flapping detected. Further alerts will be suppressed.)";
    h->getTemplate() += buf.str();
  }

  void alert_manager::loadStaticData ()
  {
    std::string tmpl;
    getOverName (tmpl);
    loadStaticData (tmpl, m_overTemplate, m_overSubject);
    if (m_useUpDownAlerts)
    {
      getBelowName (tmpl);
      loadStaticData (tmpl, m_belowTemplate, m_belowSubject);
    }
  }

  void alert_manager::createAlertQuery ()
  {
    std::string table;
    std::string id;
    std::ostringstream sql;

    getReferenceTable (table);
    getIDName (id);
    sql << "select distinct a.id, c."
        << id
        << ", b.triggered, b.trigger_threshold, b.pattern, "
        << "b.trigger_id, b.comp_exp, b.label, a.conditional_id, "
        << "b.throttle_interval "
        << "from alert_handlers a, alert_triggers b, "
        << table << " c where a.trigger_id = b.trigger_id and "
        << "b.active = 't' and "
        << "b.reference_table_name = '"
        << table << "' and c."
        << id
        << " = b.reference_pkey_val";

    m_getAlertsQuery = sql.str();
  }

  void alert_manager::loadAlerts ()
  {
    unsigned int id;
    PGresult *res = m_db->execQuery (m_getAlertsQuery.c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      alert_data *d = new alert_data;
      d->m_alertID = atoi (PQgetvalue (res, i, 0));
      id = (unsigned) atoi (PQgetvalue (res, i, 1));
      d->m_triggered = *PQgetvalue (res, i, 2) == 't' ? true : false;
      d->m_threshold = atoi (PQgetvalue (res, i, 3));
      d->m_pattern = std::string (PQgetvalue (res, i, 4));
      d->m_triggerID = atoi (PQgetvalue (res, i, 5));
      d->m_comparator = m_compFactory->createComparator (PQgetvalue (res, i, 6));
      d->m_label = std::string (PQgetvalue (res, i, 7));
      d->m_conditionalID = std::string (PQgetvalue (res, i, 8));
      m_throttler->updateAlert (d->m_alertID, atoi (PQgetvalue (res, i, 9)));
      m_alerts.insert (std::pair<unsigned int, alert_data*>(id, d));
    }

    PQclear(res);
  }

  void alert_manager::getReferenceData (unsigned int id, std::map<std::string, std::string> &values)
  {
    std::ostringstream sql;
    std::string table;
    std::string ref_id;

    getReferenceTable (table);
    getIDName (ref_id);

    sql << "select * from " << table << " where " << ref_id << " = " << id;

    PGresult *res = m_db->execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples (res); ++i)
      for (int j = 0; j < PQnfields (res); ++j)
        values[std::string (PQfname (res, j))] = std::string (PQgetvalue (res, i, j));

    PQclear(res);
  }

  void alert_manager::clearAlerts ()
  {
    for (alert_map::iterator i = m_alerts.begin(); i != m_alerts.end(); ++i)
      delete i->second;
    m_alerts.clear ();
  }

  void alert_manager::loadStaticData (const std::string &type, std::string &templ, std::string &subj)
  {
    std::string sql = "select default_template, default_subject from alert_types where name = '";
    sql += type;
    sql += "'";

    PGresult *res = m_db->execQuery (sql.c_str());

    if (PQntuples(res) == 0)
    {
      PQclear(res);
      throw alert_no_template_exception (type);
    }

    templ = std::string (PQgetvalue (res, 0, 0));
    subj = std::string (PQgetvalue (res, 0, 1));

    PQclear(res);
  }

  bool alert_manager::match (const char *pattern, const char *value)
  {
    return strcmp (pattern, value) == 0 ? true : false;
  }
}
