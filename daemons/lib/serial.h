#ifndef __SERIAL_NUMBER__
#define __SERIAL_NUMBER__

#include <dumbnet.h>
#include <stdint.h>

namespace netmon
{
  class db;

  class serial_number
  {
    public:
      serial_number (db *);
      ~serial_number ();

      uint32_t devLimit () const
      {
        return m_devLimit;
      }

      uint32_t timeStamp () const
      {
        return m_timeStamp;
      }

      bool valid ();

    protected:
      void init ();
      void checkInterface (const intf_entry *);

      static int ifCallback (const intf_entry *, void *);

      db *m_db;
      bool m_firstDev;
      uint32_t m_devIndex;
      uint32_t m_devLimit;
      uint32_t m_timeStamp;
      uint8_t m_mac[6];
  };
}

#endif // __SERIAL_NUMBER__
