#ifndef __FLAPPING_DETECTOR__
#define __FLAPPING_DETECTOR__

#include <map>
#include <list>
#include <stdint.h>

namespace netmon
{
  class flapping_detector
  {
    public:
      enum status { eNotFlapping, eStartedToFlap, eFlapping };
      flapping_detector (float upper = 0.35, float lower = 0.3)
        : m_upper (upper), m_lower (lower)
      {}

      void addState (uint32_t, bool);
      status getStatus (uint32_t);
      bool isFlapping (uint32_t);

    protected:
      void checkStatus (uint32_t);

      typedef std::list<bool> bool_list_t;

      struct history_data
      {
        history_data () : m_size (0), m_status (eNotFlapping) {}

        void addState (bool up)
        {
          if (m_size > eMaxHistoryData)
            m_list.pop_front();
          else
            ++m_size;
          m_list.push_back (up);
        }

        bool_list_t m_list;
        uint8_t m_size;
        status m_status;
        enum { eMaxHistoryData = 20 };
      };

      float calculateFlappingRatio (history_data &);

      typedef std::map<uint32_t, history_data> history_map_t;

      float m_upper;
      float m_lower;
      history_map_t m_history;
  };
}

#endif // __FLAPPING_DETECTOR__
