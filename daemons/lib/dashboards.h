#ifndef __DASHBOARDS_H__
#define __DASHBOARDS_H__

#include <jsoncpp/json/value.h>

namespace netmon
{
  class dashboards
  {
    public:
      static Json::Value getDashboards();
  };
}

#endif // __DASHBOARDS_H__
