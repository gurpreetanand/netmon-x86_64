#ifndef __ARRAY_UTIL__
#define __ARRAY_UTIL__

#include <algorithm>
#include <set>
#include <list>
#include <string>
#include <sstream>

namespace netmon
{
  namespace util
  {
    typedef std::list<std::pair<unsigned int, unsigned int> > d2_list;

    std::string createArray (const std::set<unsigned int> &);
    void parseArray (const std::string &, std::set<unsigned int> &);

    template <typename T>
    void parseArray (const std::string &str, T &cont)
    {
      if (str.size () < 2)
        return;

      std::string tmp = str;
      std::replace (tmp.begin () + 1, tmp.end () - 1, ',', ' ');
      std::istringstream s(std::string(tmp, 1, tmp.size() - 2));
      unsigned int i;

      while (!s.eof ())
      {
        s >> i;
        cont.insert (cont.end(), i);
      }
    }

    std::string create2DArray (const d2_list &);
    void parse2DArray (const std::string &, d2_list &);
  }
}

#endif // __ARRAY_UTIL__
