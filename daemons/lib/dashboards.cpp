#include "dashboards.h"

#include <string>
#include <fstream>
#include <jsoncpp/json/reader.h>

#define JSON_CONFIG "/apache/dashboards.json"

namespace netmon
{
  // static
  Json::Value dashboards::getDashboards() {
    Json::Value root;
    Json::Reader reader;

    std::ifstream config(JSON_CONFIG);

    std::string json((std::istreambuf_iterator<char>(config)), std::istreambuf_iterator<char>());

    reader.parse(json, root);

    return root;
  }
}