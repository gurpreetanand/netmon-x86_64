#include "buffer.h"
#include "nfheaders.h"
#include "flows.h"
#include "flowcollector.h"
#include "netmon.h"
#include "clientmanager.h"
#include "cachemanager.h"

#include "../lib/db.h"

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdio>

// global stuff from netmon.lib
char dbname [DBNAMELEN];
char dbuser [DBUSERLEN];
int errvalue;

const char *nf5 = "nf5.packet";

using namespace netmon;

void testcollector ()
{
  readConfig ();
  db *mydb = new db;
  mydb->connect (dbname, dbuser);
  cache_manager *cache = new cache_manager(mydb);
  client_manager_db *cm = new client_manager_db (mydb, cache);
  flow_collector c (3030, cm, cache);
  c.run ();
}

void testheader ()
{
  uint8_t *p = new uint8_t[10];
  uint16_t t = 0x09;

  memcpy ((void *) p, (void *) &t, sizeof (t));
  t = 0x23;
  memcpy ((void *) (p + sizeof (t)), (void *) &t, sizeof (t));

  try
  {
    buffer buf(p, 3);
    nfheader h (buf);
    printf ("version: 0x%04x count: 0x%04x\n", h.getVersion(), h.getCount());
  }
  catch (std::exception &e)
  {
    printf ("%s\n", e.what());
  }
  delete[] p;
}

void testv1header ()
{
  uint8_t *p = new uint8_t[16];
  uint16_t t = 0x01;
  uint32_t s = 0xcafebabe;

  memcpy ((void *) p, (void *) &t, sizeof (t));
  t = 0x11;
  memcpy ((void *) (p + sizeof (t)), (void *) &t, sizeof (t));

  memcpy ((void *) (p + 2 * sizeof (t)), (void *) &s, sizeof (s));
  ++s;
  memcpy ((void *) (p + 2 * sizeof (t) + sizeof (s)), (void *) &s, sizeof (s));
  ++s;
  memcpy ((void *) (p + 2 * sizeof (t) + 2 * sizeof (s)), (void *) &s, sizeof (s));

  try
  {
    buffer buf(p, 16);
    nfheader_v1 h (buf);
    printf ("version: 0x%04x count: 0x%04x\n", h.getVersion(), h.getCount());
    printf ("uptime: %d seconds: %d nseconds: %d\n", h.getUpTime(), h.getSeconds(), h.getNSeconds());
  }
  catch (std::exception &e)
  {
    printf ("%s\n", e.what());
  }
  delete[] p;
}

void testv5flow (buffer &b)
{
  flow_v5 f (b);
  in_addr addr;
  addr.s_addr = f.getSrc();
  printf ("src address: %s\n", inet_ntoa (addr));
  addr.s_addr = f.getDst();
  printf ("dst address: %s\n", inet_ntoa (addr));
  addr.s_addr = f.getNextHop();
  printf ("next hop address: %s\n", inet_ntoa (addr));
  printf ("SNMP input iface: %d SNMP output iface: %d\n", f.getInputInterface(), f.getOutputInterface());
  printf ("packets: %d octets: %d\n", f.getPackets(), f.getOctets());
  printf ("srcport: %d dstport: %d\n", f.getSrcPort(), f.getDstPort());
  printf ("tcp flags: %d protocol: %d tos: %d\n", f.getTCPFlags(), f.getProtocol(), f.getTOS());
  printf ("src AS: %d dst AS: %d\n", f.getSrcAS(), f.getDstAS());
  printf ("src mask: %d dst mask: %d\n", f.getSrcMask(), f.getDstMask());
}

void testv5header ()
{
  struct stat s;
  if (stat (nf5, &s) != 0)
  {
    perror ("stat");
    return;
  }

  if (s.st_size == 0)
  {
    fprintf (stderr, "Illegal size\n");
    return;
  }

  uint8_t *buf = new uint8_t[s.st_size];
  int fd = open (nf5, O_RDONLY);
  if (fd < 0)
  {
    perror ("open");
    delete[] buf;
    return;
  }

  if (read (fd, (void *) buf, s.st_size) != s.st_size)
  {
    perror ("read");
    delete[] buf;
    close (fd);
    return;
  }

  close (fd);

  try
  {
    buffer b(buf, s.st_size);
    nfheader_v5 h (b);
    printf ("version: 0x%04x count: 0x%04x\n", h.getVersion(), h.getCount());
    printf ("uptime: %ud seconds: %d nseconds: %d\n", h.getUpTime(), h.getSeconds(), h.getNSeconds());
    printf ("flow sequence: 0x%08x engine type: 0x%02x engine id: 0x%02x\n", h.getSequence(), h.getEngineType(), h.getEngineID());
    for (int i = 0; i < h.getCount(); ++i)
    {
      printf ("\nflow: %d\n", i);
      testv5flow (b);
    }
  }
  catch (std::exception &e)
  {
    printf ("%s\n", e.what());
  }
  delete[] buf;
}

int main ()
{
  testcollector ();
  testv5header ();
  return 0;

  int i = 10;
  int j = 0xba;
  unsigned char x = 0x2f;
  unsigned char *p = new unsigned char [32];

  memcpy ((void *) p, (void *) &x, sizeof (x));
  memcpy ((void *) (p + 1), (void *) &j, sizeof (j));
  try
  {
    buffer b ((const unsigned char *) &i, sizeof (i));
    int j = 0;
    b >> j;
    printf ("got: %d\n", j);
    buffer b1 (p, 32);
    unsigned char c1;
    int c2;
    b1 >> c1 >> c2;
    printf ("got: 0x%02x 0x%04x\n", c1, c2);
    delete[] p;
  }
  catch (std::exception &e)
  {
    printf ("%s\n", e.what());
  }
}
