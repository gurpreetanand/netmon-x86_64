#include "sflow.h"
#include "buffer.h"
#include "flowtime.h"

#include <arpa/inet.h>

namespace netmon
{
  void sflow_object::ntohl (uint32_t &v)
  {
    v = ::ntohl (v);
  }

  sflow_address::sflow_address (buffer &b)
  {
    deserialize (b);
  }

  void sflow_address::deserialize (buffer &b)
  {
    b >> m_type;
    ntohl (m_type);
    if (m_type == eIPV4)
      b >> m_address.ip_v4;
    else
      b >> m_address.ip_v6;
  }

  sflow_header::sflow_header (buffer &b)
  {
    deserialize (b);
  }

  void sflow_header::deserialize (buffer &b)
  {
    b >> m_version;
    ntohl (m_version);

    m_address = sflow_address (b);

    if (m_version == 5) // sflow v5
    {
      b >> m_subAgentID;
      ntohl (m_subAgentID);
    }

    b >> m_sequence >> m_upTime >> m_records;

    ntohl (m_sequence);
    ntohl (m_upTime);
    ntohl (m_records);

    printf ("subagentid: %d, seq: %d, uptime: %d, records: %d\n", m_subAgentID, m_sequence, m_upTime, m_records);
    printf ("ip: %s\n", inet_ntoa (m_address.m_address.ip_v4));
  }

  sflow_sampled_ipv4::sflow_sampled_ipv4 (buffer &b)
  {
    deserialize (b);
  }

  void sflow_sampled_ipv4::deserialize (buffer &b)
  {
    b >> m_len
      >> m_protocol
      >> m_src
      >> m_dst
      >> m_srcPort
      >> m_dstPort
      >> m_tcpFlags
      >> m_tos;

    ntohl (m_len);
    ntohl (m_protocol);
    ntohl (m_srcPort);
    ntohl (m_dstPort);
    ntohl (m_tcpFlags);
    ntohl (m_tos);
  }

  sflow_sampled_header::sflow_sampled_header (buffer &b, const sflow_header &h)
    : m_sfHeader (h), m_isValid (false)
  {
    deserialize (b);
  }

  void sflow_sampled_header::deserialize (buffer &b)
  {
printf ("\nnew sampled header!!!\n");
    b >> m_protocol
      >> m_len;

    if (m_sfHeader.m_version > 4)
    {
      b >> m_stripped;
      ntohl (m_stripped);
    }

    b >> m_headerLen;
    uint32_t pos = b.pos();

    ntohl (m_protocol);
    ntohl (m_len);
    ntohl (m_headerLen);

    printf ("protocol: %d len: %d headerLen: %d stripped: %d\n", m_protocol, m_len, m_headerLen, m_stripped);

    uint16_t type = 0;

    switch (m_protocol)
    {
      case eEthernet:
        printf ("ethernet protocol.\n");
        type = deserializeLinkLayer (b);
        break;
      case eIPV4:
        type = IPV4;
        break;
      default:
        break;
    }

    if (type == IPV4)
    {
      deserializeIPV4 (b);
    }

    b.pos() = pos + m_headerLen + m_headerLen % 4; // aligned to 4 bytes 
  }

  uint16_t sflow_sampled_header::deserializeLinkLayer (buffer &b)
  {
    uint8_t src[6];
    uint8_t dst[6];

    b >> dst >> src;

    printf ("src: %02x:%02x:%02x:%02x:%02x:%02x ", src[0], src[1], src[2], src[3], src[4], src[5]);
    printf ("dst: %02x:%02x:%02x:%02x:%02x:%02x\n", dst[0], dst[1], dst[2], dst[3], dst[4], dst[5]);

    uint16_t type;
    b >> type;
    type = ntohs (type);

    printf ("type: 0x%04x\n", type);

    if (type == 0x8100) // VLAN
    {
      b.skip (2);
      b >> type;
      type = ntohs (type);
    }

    return type;
  }

  void sflow_sampled_header::deserializeIPV4 (buffer &b)
  {
    uint32_t pos = b.pos();
    b >> m_ip;

    in_addr a, c;
    a.s_addr = m_ip.ip_src;
    c.s_addr = m_ip.ip_dst;

    printf ("tos: %d len: %d, proto: 0x%02x ", m_ip.ip_tos, ntohs (m_ip.ip_len), m_ip.ip_p);
    printf ("src: %s ", inet_ntoa (a));
    printf ("dst: %s\n", inet_ntoa (c));

    pos += m_ip.ip_hl * 4;
    b.pos() = pos;

    switch (m_ip.ip_p)
    {
      case 0x06: // TCP
        {
          tcp_hdr tcph;
          uint32_t pos = b.pos();
          b >> tcph;
          b.pos() = pos + tcph.th_off * 4;
          m_isValid = true;
          m_srcPort = tcph.th_sport;
          m_dstPort = tcph.th_dport;
          printf ("src port: %d dst port: %d flags: %d\n", ntohs (tcph.th_sport), ntohs (tcph.th_dport), tcph.th_flags);
        }
        break;
      case 0x11: // UDP
        {
          udp_hdr udph;
          b >> udph;
          m_isValid = true;
          m_srcPort = udph.uh_sport;
          m_dstPort = udph.uh_dport;
          printf ("src port: %d dst port: %d \n", ntohs (udph.uh_sport), ntohs (udph.uh_dport));
        }
    }
  }

  sflow_sample::sflow_sample (buffer &b, const sflow_header &sfhdr, cache_manager *cache, uint32_t ip, bool isExpanded /* = false */)
    : m_isExpanded (isExpanded), m_sfHeader (sfhdr), m_cache (cache), m_ip (ip)
  {
    deserialize (b);
  }

  void sflow_sample::deserialize (buffer &b)
  {
    b >> m_sampleLen >> m_samples;

    ntohl (m_sampleLen);
    ntohl (m_samples);

    printf ("sampleLen: %d samples: %d\n", m_sampleLen, m_samples);

    deserializeSID (b);
    printf ("class:index %d:%d\n", m_SIDClass, m_SIDIndex);

    b >> m_samplingRate >> m_samplePool >> m_drops;

    ntohl (m_samplingRate);
    ntohl (m_samplePool);
    ntohl (m_drops);

    printf ("sampling rate: %d sample pool: %d drops: %d\n", m_samplingRate, m_samplePool, m_drops);

    deserializePorts (b);
    printf ("in: %d out: %d\n", m_input, m_output);//::ntohl (m_input), ::ntohl (m_output));

    uint32_t num;
    b >> num;
    ntohl (num);

    printf ("numelements: %d\n", num);

    for (uint32_t i = 0; i < num; ++i)
    {
      uint32_t tag, taglen;
      b >> tag;
      b >> taglen;

      ntohl (tag);
      ntohl (taglen);
      printf ("tag: %d taglen: %d\n", tag, taglen);

      switch (tag)
      {
        case eHeader:
          handleSampledHeader (b);
          break;
        case eIPV4:
          handleSampledIPV4Header (b);
          break;
        case eEthernet:
        case eIPV6:
        default:
          b.pos() += taglen;
          printf ("pos after taglen adding: %d\n", b.pos());
          break;
      }
    }
  }

  void sflow_sample::handleSampledHeader (buffer &b)
  {
    sflow flow;
    sflow_sampled_header sh (b, m_sfHeader);
    if (sh.isValid())
    {
      flow.src() = sh.m_ip.ip_src;
      flow.dst() = sh.m_ip.ip_dst;
      flow.srcPort() = sh.m_srcPort;
      flow.dstPort() = sh.m_dstPort;
      flow.in() = htons ((uint16_t) m_input);
      flow.out() = htons ((uint16_t) m_output);
      flow.octets() = htonl ((uint32_t)ntohs (sh.m_ip.ip_len));
      flow.start() = flow.end() = (uint32_t) time (0);
      flow_time ft;
      m_cache->add (m_ip, flow, ft);
    }
  }

  void sflow_sample::handleSampledIPV4Header (buffer &b)
  {
    sflow_sampled_ipv4 ipv4 (b);
  }

  void sflow_sample::deserializeSID (buffer &b)
  {
    if (m_isExpanded)
    {
      b >> m_SIDClass >> m_SIDIndex;
      ntohl (m_SIDClass);
      ntohl (m_SIDIndex);
    }
    else
    {
      uint32_t sourceID;
      b >> sourceID;
      ntohl (sourceID);
      m_SIDClass = sourceID >> 24;
      m_SIDIndex = sourceID & 0x00ffffff;
    }
  }

  void sflow_sample::deserializePorts (buffer &b)
  {
    if (m_isExpanded)
    {
      b >> m_inputFormat >> m_input >> m_outputFormat >> m_output;
      ntohl (m_inputFormat);
      ntohl (m_input);
      ntohl (m_outputFormat);
      ntohl (m_output);
    }
    else
    {
      uint32_t in, out;
      b >> in >> out;
      ntohl (in);
      ntohl (out);
      m_inputFormat = in >> 30;
      m_outputFormat = out >> 30;
      m_input = /*htonl (*/in & 0x3fffffff;
      m_output = /*htonl (*/out & 0x3fffffff;
    }
  }
}
