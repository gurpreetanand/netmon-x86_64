#ifndef __NETFLOW_V9_HANDLER__
#define __NETFLOW_V9_HANDLER__

#include <stdint.h>
#include <list>
#include <map>
#include <set>

#include "flowhandler.h"
#include "flows.h"
#include "flowtime.h"

namespace netmon
{
  class v9_handler : public flow_handler
  {
    public:
      v9_handler (cache_manager *, uint32_t);
      ~v9_handler ();

      virtual void processBuffer (buffer &);

    protected:
      enum { eTemplate = 0, eOption, eReserved = 255 };

      void processData (buffer &);
      void handleTemplateFlow (buffer &);
      void handleOptionsFlow (buffer &);
      void handleDataFlow (buffer &, uint16_t);

      typedef std::map<uint16_t, tlist_t> tmap_t;

      tmap_t m_template;
      flow_time m_flowTime;
  };
}

#endif // __NETFLOW_V9_HANDLER__
