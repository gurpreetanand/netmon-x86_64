#include "flowstorage.h"
#include "flows.h"
#include "flowtime.h"

namespace netmon
{
  flow_data_simple::flow_data_simple (const flow &f, const flow_time &ft)
    : m_nextHop (f.getNextHop()),
      m_iiface (f.getInputInterface()),
      m_oiface (f.getOutputInterface()),
      m_packets (f.getPackets()),
      m_octets (f.getOctets()),
      m_startTime (ft.getStartTime(f)),
      m_endTime (ft.getEndTime(f)),
      m_protocol (f.getProtocol()),
      m_tos (f.getTOS()),
      m_tcpflags (f.getTCPFlags())
  {
  }

  flow_data_simple::flow_data_simple (const flow_data_simple &f)
  {
    m_nextHop = f.m_nextHop;
    m_iiface  = f.m_iiface;
    m_oiface  = f.m_oiface;
    m_packets = f.m_packets;
    m_octets  = f.m_octets;
    m_startTime = f.m_startTime;
    m_endTime = f.m_endTime;
    m_protocol = f.m_protocol;
    m_tos       = f.m_tos;
    m_tcpflags  = f.m_tcpflags;
  }

  flow_storage::flow_storage ()
    : m_total (0)
  {
  }

  flow_storage::~flow_storage ()
  {
  }

  void flow_storage::add (const flow &f, const flow_time &t)
  {
    addConversationDetailed (f, t);

    m_total += f.getOctets();
  }

  uint64_t flow_storage::getTotalBytes () const
  {
    return m_total;
  }

  void flow_storage::clear ()
  {
    m_convDMap.clear();
    m_total = 0;
  }

  void flow_storage::addConversationDetailed (const flow &f, const flow_time &t)
  {
    ip_port_entry e(f.getSrc(), f.getDst(), f.getSrcPort(), f.getDstPort());
    cdmap::iterator i = m_convDMap.find (e);
    if (i == m_convDMap.end())
    {
      flow_data_simple fs(f, t);
      m_convDMap[e] = fs;
      return;
    }
    i->second.m_octets += f.getOctets();
    i->second.m_packets += f.getPackets();
    i->second.m_endTime = t.getEndTime(f);
  }
}
