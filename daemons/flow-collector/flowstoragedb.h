#ifndef __FLOW_STORAGE_DB__
#define __FLOW_STORAGE_DB__

#include "flowstorage.h"
#include "db.h"

#include <mqueue.h>
#include <set>
#include <string>

namespace netmon
{
  class flow_storage_db : public flow_storage
  {
    public:
      flow_storage_db (uint32_t, db *);
      ~flow_storage_db ();

      virtual void store (int);

    protected:
      void store (unsigned int, unsigned int, uint16_t, uint16_t, const flow_data_simple &, int);
      void enqueueIPPair (uint32_t, uint32_t);
      void enqueueIPs ();

      db *m_db;
      std::string m_ip;
      std::set<uint32_t> m_ips;
      mqd_t m_queue;
  };
}

#endif // __FLOW_STORAGE_DB__
