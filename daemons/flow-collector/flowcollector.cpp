#include "flowcollector.h"
#include "clientmanager.h"
#include "cachemanager.h"
#include "nflowcollector.h"
#include "sflowcollector.h"
#include "exception.h"
#include "nsignal.h"
#include "log.h"

namespace netmon
{
  flow_collector::flow_collector (int nport, int sport, cache_manager *cc, client_manager *cm)
    : m_nport (nport), m_sport (sport), m_cache (cc), m_clientManager (cm),
      m_lastCommit ((time(0) / eCommitInterval) * eCommitInterval)
  {
    init();
  }

  flow_collector::~flow_collector ()
  {
    delete m_nfCollector;
    delete m_sfCollector;
  }

  void flow_collector::run ()
  {
    signal_handler::instance()->register_handler_ev (SIGTERM, &m_sigHandler);

    while (!m_sigHandler.isTermSignaled())
      event_loop (EVLOOP_ONCE);
  }

  void flow_collector::init ()
  {
    event_init();

    m_nfCollector = new nflow_collector (m_nport, m_clientManager, m_cache);
    m_sfCollector = new sflow_collector (m_sport, m_clientManager, m_cache);

    setTimer();
  }

  void flow_collector::setTimer ()
  {
    m_timeout.tv_sec = eLiveCommitInterval;
    m_timeout.tv_usec = 0;
    evtimer_set (&m_timerEvent, onTimeout, (void *) this);
    evtimer_add (&m_timerEvent, &m_timeout);
  }

  void flow_collector::onTimeout (int, short, void *me)
  {
    flow_collector *f = reinterpret_cast<flow_collector *> (me);
    f->onTimeout();
  }

  void flow_collector::onTimeout ()
  {
    static unsigned int cnt = 0;

    evtimer_add (&m_timerEvent, &m_timeout);
    m_cache->storeLive (eLiveCommitInterval);
    m_cache->clearLive();

    if (++cnt > 5)
    {
      cnt = 0;
      m_clientManager->reload();
    }

    if ((time(0) / eCommitInterval) > (m_lastCommit / eCommitInterval))
    {
      m_lastCommit = (time(0) / eCommitInterval) * eCommitInterval;
      m_cache->store (m_lastCommit);
    }
  }
}
