#ifndef __TOP_LIST__
#define __TOP_LIST__

#include <list>
#include <algorithm>
#include <functional>

template <typename T, class comp = std::greater<T> > class toplist : public std::list<T>
{
  typedef std::list<T> inherited;
  public:
    toplist (size_t maxsize = 200)
      : std::list<T>(), m_maxsize(maxsize), m_size(0)
    {
    }

    void insert (const T &elem)
    {
      if (m_size == 0)
      {
        inherited::push_back (elem);
        ++m_size;
        return;
      }
      if (m_comp.operator()(inherited::back (), elem) && m_size == m_maxsize)
        return;
      inherited::insert (std::lower_bound (inherited::begin(), inherited::end(), elem, m_comp), elem);
      if (m_size == m_maxsize)
        inherited::pop_back();
      else
        ++m_size;
    }

    void purge ()
    {
      inherited::clear ();
      m_size = 0;
    }

    size_t size () const
    {
      return inherited::size ();
    }

  protected:
    size_t m_maxsize;
    size_t m_size;
    comp m_comp;
};

#endif // __TOP_LIST__
