#ifndef __FLOW_COLLECOTR__
#define __FLOW_COLLECOTR__

#include "udpcollector.h"

#include <map>

namespace netmon
{
  class cache_manager;
  class flow_handler;

  class sflow_collector : public udp_collector
  {
    public:
      sflow_collector (int, client_manager *);
      sflow_collector (int, client_manager *, cache_manager *);

      void setCacheManager (cache_manager *storage)
      {
        m_cache = storage;
      }

    protected:
      virtual void handleData ();

      void handleSFlow ();

      cache_manager *m_cache;

      typedef std::map<uint32_t, flow_handler *> flow_handlers_t;
      flow_handlers_t m_flowHandlers;
  };
}

#endif // __FLOW_COLLECOTR__
