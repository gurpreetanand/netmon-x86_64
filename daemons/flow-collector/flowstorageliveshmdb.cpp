#include "flowstorageliveshmdb.h"
#include "flows.h"
#include "flowtime.h"

#include <sstream>
#include <string>
#include <time.h>

namespace netmon
{
  flow_storage_live_shm_db::flow_storage_live_shm_db (db *mydb, unsigned int interface, unsigned int device, uint32_t basekey, int maxstreams, int maxproto)
    : flow_storage_live_shm (interface, device, basekey, maxstreams, maxproto), m_db (mydb)
  {
    m_now = ::time (0);
    m_round = 0;
  }

  flow_storage_live_shm_db::~flow_storage_live_shm_db ()
  {
  }

  void flow_storage_live_shm_db::store (int interval)
  {
    flow_storage_live_shm::store (interval);

    if (++m_round < 12)
      return;

    m_round = 0;
    unsigned int now = ::time (0);
    std::ostringstream sql;
    std::string ports;
    std::string octets;

    toplist<portitem, comp<portitem> > l(100);
    getTopProtocols (m_Protocols, l);
    createArrays (l, ports, octets);
    sql << "INSERT INTO PROTOCOL_BREAKDOWN (INTERFACE, DEVICE, "
        << "START_TIME, END_TIME, PORTS, OCTETS) VALUES ("
        << m_interface << ", "
        << m_device << ", "
        << m_now << ", "
        << now << ", '"
        << ports << "', '"
        << octets << "')";

    m_db->execSQL (sql.str().c_str());
    m_now = now;
    m_Protocols.clear();
  }

  void flow_storage_live_shm_db::createArrays (const toplist<portitem, comp<portitem> > &tlist, std::string &ports, std::string &octets)
  {
    std::ostringstream p;
    std::ostringstream o;
    bool first = true;

    p << "{";
    o << "{";

    for (toplist<portitem, comp<portitem> >::const_iterator i = tlist.begin(); i != tlist.end(); ++i)
    {
      if ((*i).m_octets == 0)
        continue;
      if (!first)
      {
        p << ", " << (*i).m_port;
        o << ", " << (*i).m_octets;
      }
      else
      {
        first = false;
        p << (*i).m_port;
        o << (*i).m_octets;
      }
    }
    ports = p.str();
    ports += "}";
    octets = o.str();
    octets += "}";
  }
}
