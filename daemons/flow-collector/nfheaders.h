#ifndef __NETFLOW_HEADERS__
#define __NETFLOW_HEADERS__

#include <arpa/inet.h>
#include "buffer.h"

namespace netmon
{
  class nfheader
  {
    public:
      nfheader (buffer &b)
      {
        deserialize (b);
      }

      virtual ~nfheader () {}

      uint16_t getVersion () const
      {
        return ntohs (m_version);
      }

      uint16_t getCount () const
      {
        return ntohs (m_count);
      }

      uint32_t getUpTime () const
      {
        return ntohl (m_uptime);
      }

      uint32_t getSeconds () const
      {
        return ntohl (m_seconds);
      }

    protected:
      nfheader () {}
      virtual void deserialize (buffer &);

      uint16_t m_version;
      uint16_t m_count;
      uint32_t m_uptime;
      uint32_t m_seconds;
  };

  class nfheader_v1 : public nfheader
  {
    public:
      nfheader_v1 (buffer &b)
        : nfheader (b) 
      {
        deserialize (b);
      }

      uint32_t getNSeconds () const
      {
        return ntohl (m_nseconds);
      }

    protected:
      virtual void deserialize (buffer &);

      uint32_t m_nseconds;
  };

  class nfheader_v57 : public nfheader_v1
  {
    public:
      nfheader_v57 (buffer &b)
        : nfheader_v1 (b)
      {
        deserialize (b);
      }

      uint32_t getSequence () const
      {
        return ntohl (m_sequence);
      }

    protected:
      virtual void deserialize (buffer &);

      uint32_t m_sequence;
  };

  class nfheader_v5 : public nfheader_v57
  {
    public:
      nfheader_v5 (buffer &b)
        : nfheader_v57 (b)
      {
        deserialize (b);
      }

      uint8_t getEngineType () const
      {
        return m_engineType;
      }

      uint8_t getEngineID () const
      {
        return m_engineID;
      }

    protected:
      virtual void deserialize (buffer &);

      uint8_t m_engineType;
      uint8_t m_engineID;
  };

  class nfheader_v7 : public nfheader_v57
  {
    public:
      nfheader_v7 (buffer &b)
        : nfheader_v57 (b)
      {
        deserialize (b);
      }

    protected:
      virtual void deserialize (buffer &);
  };

  class nfheader_v9 : public nfheader
  {
    public:
      nfheader_v9 (buffer &b)
        : nfheader (b)
      {
        deserialize (b);
      }

      uint32_t getSequence () const
      {
        return ntohl (m_flowSequence);
      }

      uint32_t getSourceID () const
      {
        return ntohl (m_sourceID);
      }

    protected:
      virtual void deserialize (buffer &);

      uint32_t m_flowSequence;
      uint32_t m_sourceID;
  };
}

#endif // __NETFLOW_HEADERS__
