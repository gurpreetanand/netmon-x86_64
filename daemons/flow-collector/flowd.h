#ifndef __FLOW_DAEMON__
#define __FLOW_DAEMON__

#include "daemon.h"

namespace netmon
{
  class cache_manager;
  class client_manager_db;
  class flow_collector;

  class flow_daemon : public daemon
  {
    public:
      flow_daemon (int, char **);
      ~flow_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "flowd";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/flowd";
      }

      cache_manager *m_cache;
      client_manager_db *m_clientManager;
      flow_collector *m_flowCollector;
  };
}

#endif // __FLOW_DAEMON__
