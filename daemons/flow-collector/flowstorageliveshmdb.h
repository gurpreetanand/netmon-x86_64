#ifndef __FLOW_STORAGE_LIVE_SHM_DB__
#define __FLOW_STORAGE_LIVE_SHM_DB__

#include "db.h"
#include "flowstorageliveshm.h"
#include "toplist.h"

#include <string>

namespace netmon
{
  class flow_storage_live_shm_db : public flow_storage_live_shm
  {
    typedef flow_storage_live_shm inherited;
    public:
      flow_storage_live_shm_db (db *, unsigned int, unsigned int, uint32_t, int = 200, int = 10);
      ~flow_storage_live_shm_db ();

      virtual void store (int);

    protected:
      void createArrays (const toplist<portitem, comp<portitem> > &, std::string &, std::string &);

      db *m_db;
      unsigned int m_now;
      unsigned int m_round;
  };
}

#endif // __FLOW_STORAGE_LIVE_SHM_DB__
