#ifndef __SFLOW_H__
#define __SFLOW_H__

#include <stdint.h>
#include <netinet/in.h>
#include <dumbnet.h>

#include "cachemanager.h"
#include "flows.h"

namespace netmon
{
  class buffer;

  class sflow : public flow
  {
    public:
      sflow ()
        : flow (true)
      {}

      uint32_t &src()
      {
        return m_src;
      }

      uint32_t &dst()
      {
        return m_dst;
      }

      uint16_t &in()
      {
        return m_iiface;
      }

      uint16_t &out()
      {
        return m_oiface;
      }

      uint32_t &octets()
      {
        return m_octets;
      }

      uint32_t &start()
      {
        return m_startTime;
      }

      uint32_t &end()
      {
        return m_endTime;
      }

      uint16_t &srcPort()
      {
        return m_srcPort;
      }

      uint16_t &dstPort()
      {
        return m_dstPort;
      }

      uint8_t &tos()
      {
        return m_tos;
      }

      uint8_t &flags()
      {
        return m_tcpflags;
      }

    protected:
      virtual void deserialize (buffer &)
      {}
  };

  class sflow_object
  {
    public:
      sflow_object () {}
      virtual ~sflow_object () {}

    protected:
      virtual void deserialize (buffer &) = 0;
      void ntohl (uint32_t &);
  };

  struct sflow_address : public sflow_object
  {
    enum { eIPV4 = 1, eIPV6 = 2 };

    sflow_address ()
      : m_type (0)
    {}

    sflow_address (buffer &);
    virtual void deserialize (buffer &);

    union sflow_address_value
    {
      in_addr ip_v4;
      in6_addr ip_v6;
    };

    uint32_t m_type;
    sflow_address_value m_address;
  };

  struct sflow_sampled_ipv4 : public sflow_object
  {
    sflow_sampled_ipv4 (buffer &);
    virtual void deserialize (buffer &);

    uint32_t m_len;
    uint32_t m_protocol;
    in_addr m_src;
    in_addr m_dst;
    uint32_t m_srcPort;
    uint32_t m_dstPort;
    uint32_t m_tcpFlags;
    uint32_t m_tos;
  };

  struct sflow_header : public sflow_object
  {
    sflow_header ()
      : m_version (0) {}
    sflow_header (buffer &);
    virtual void deserialize (buffer &);

    uint32_t m_version;
    sflow_address m_address;
    uint32_t m_subAgentID;
    uint32_t m_sequence;
    uint32_t m_upTime;
    uint32_t m_records;
  };

  struct sflow_sampled_header : public sflow_object
  {
    enum
    {
      eEthernet   = 1,
      eTokenBus   = 2,
      eTokenRing  = 3,
      eFDDI       = 4,
      eFrameRelay = 5,
      eX25        = 6,
      ePPP        = 7,
      eSMDS       = 8,
      eAAL5       = 9,
      eAAL5IP     = 10,
      eIPV4       = 11,
      eIPV6       = 12,
      eMPLS       = 13
    };

    sflow_sampled_header (buffer &, const sflow_header &);
    virtual void deserialize (buffer &);

    uint16_t deserializeLinkLayer (buffer &);
    void deserializeIPV4 (buffer &);
    bool isValid () const
    {
      return m_isValid;
    }

    enum eth_type { IPV4 = 0x0800, IPV6 = 0x86dd };

    uint32_t m_protocol;
    uint32_t m_len;
    uint32_t m_stripped;
    uint32_t m_headerLen;
    ip_hdr m_ip;

    uint16_t m_srcPort;
    uint16_t m_dstPort;

    const sflow_header &m_sfHeader;
    bool m_isValid;
  };

  class cache_manager;

  struct sflow_sample : public sflow_object
  {
    sflow_sample (buffer &, const sflow_header &, cache_manager *, uint32_t, bool = false);
    virtual void deserialize (buffer &);

    void deserializeSID (buffer &);
    void deserializePorts (buffer &);

    void handleSampledHeader (buffer &);
    void handleSampledIPV4Header (buffer &);

    enum
    {
      eHeader   = 1,
      eEthernet = 2,
      eIPV4     = 3,
      eIPV6     = 4,
      eIgnored
    };

    bool m_isExpanded;
    const sflow_header &m_sfHeader;
    cache_manager *m_cache;
    uint32_t m_ip;

    uint32_t m_sampleLen;
    uint32_t m_samples;
    uint32_t m_SIDClass;
    uint32_t m_SIDIndex;
    uint32_t m_samplingRate;
    uint32_t m_samplePool;
    uint32_t m_drops;
    uint32_t m_input;
    uint32_t m_output;
    uint32_t m_inputFormat;
    uint32_t m_outputFormat;
    uint32_t m_elements;
  };
}

#endif // __SFLOW_H__
