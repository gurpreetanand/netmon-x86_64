#ifndef __FLOW_HANDLER__
#define __FLOW_HANDLER__

#include <stdint.h>

namespace netmon
{
  class buffer;
  class cache_manager;

  // Base class for sflow/netflow v9 handling

  class flow_handler
  {
    public:
      flow_handler (cache_manager *cache, uint32_t ip)
        : m_cache (cache), m_ip (ip)
      {}
      virtual ~flow_handler ()
      {}

      virtual void processBuffer (buffer &) = 0;

    protected:
      cache_manager *m_cache;
      uint32_t m_ip;
  };
}

#endif // __FLOW_HANDLER__
