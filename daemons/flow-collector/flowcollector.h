#ifndef __FLOW_COLLECTOR__
#define __FLOW_COLLECTOR__

#include "sigtermhandler.h"

#include <sys/types.h>
#include <event.h>

namespace netmon
{
  class cache_manager;
  class client_manager;
  class nflow_collector;
  class sflow_collector;

  class flow_collector
  {
    public:
      flow_collector (int, int, cache_manager *, client_manager *);
      ~flow_collector ();

      void run ();

    protected:
      static void onTimeout (int, short, void *);
      void onTimeout ();

      void init ();
      void setTimer ();

      int m_nport;
      int m_sport;
      cache_manager *m_cache;
      client_manager *m_clientManager;

      nflow_collector *m_nfCollector;
      sflow_collector *m_sfCollector;

      event m_timerEvent;
      timeval m_timeout;
      int m_lastCommit;

      sig_term_handler m_sigHandler;

      enum { eLiveCommitInterval = 10, eCommitInterval = 120 };
  };
}

#endif // __FLOW_COLLECTOR__
