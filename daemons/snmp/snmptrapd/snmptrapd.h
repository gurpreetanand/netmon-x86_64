#ifndef __SNMP_TRAP_DAEMON__
#define __SNMP_TRAP_DAEMON__

#include "daemon.h"

namespace netmon
{
  class trapmonitor;

  class snmp_trap_daemon : public daemon
  {
    public:
      snmp_trap_daemon (int, char **);
      ~snmp_trap_daemon ();

      virtual void init ();

      bool termSignaled ()
      {
        return isTermSignaled();
      }

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "snmptrapd";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/snmptrapd";
      }

      trapmonitor *m_monitor;
  };
}

#endif // __SNMP_TRAP_DAEMON__
