#ifndef __TRAP_MONITOR__
#define __TRAP_MONITOR__

#include "db.h"
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  class trap_alert_manager;
  class snmp_trap_daemon;

  class trapmonitor
  {
    public:
      trapmonitor (db *, snmp_trap_daemon *);
      ~trapmonitor ();
      void run ();

    protected:
      void init ();
      static void callback (int, Snmp_pp::Snmp *, Snmp_pp::Pdu &, Snmp_pp::SnmpTarget &, void *);

      db *m_db;
      trap_alert_manager *m_alertManager;
      snmp_trap_daemon *m_daemon;
  };
}

#endif // __TRAP_MONITOR__
