#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/time.h>

#include "log.h"
#include "exception.h"
#include "snmptrapd.h"

static int installtimer ();

int reload = 0;

int main (int argc, char **argv)
{
  try
  {
    netmon::snmp_trap_daemon *d = new netmon::snmp_trap_daemon (argc, argv);
    d->init ();
    if (installtimer () == -1)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot install timer");
      return -1;
    }
    d->start ();
  }
  catch (netmon::netmon_exception &e)
  {
    printf ("Error: %s\n", e.what());
    std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
    netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    return -1;
  }
  return 0;
}

void alrmhandler (int /*sig*/)
{
  reload = 1;
}

static int installtimer ()
{
  struct sigaction saction;

  saction.sa_handler = alrmhandler;
  if (sigemptyset (&saction.sa_mask) == -1)
    return -1;

  saction.sa_flags = 0;
  if (sigaction (SIGALRM, &saction, NULL) == -1)
    return -1;

  itimerval timer;
  timer.it_interval.tv_sec = 30;
  timer.it_interval.tv_usec = 0;
  timer.it_value.tv_sec = 30;
  timer.it_value.tv_usec = 0;

  if (setitimer (ITIMER_REAL, &timer, 0) == -1)
    return -1;

  return 0;
}
