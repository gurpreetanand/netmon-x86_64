#include "oidcache.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace netmon
{
  oidcache::oidcache ()
  {
  }

  oidcache::~oidcache ()
  {
    m_newOIDs.clear ();
    m_logOIDs.clear ();
  }

  bool oidcache::toLog (const char *ip, const char *oid, unsigned int &id)
  {
    in_addr addr;
    if (inet_aton (ip, &addr) == 0)
      return false;

    iphash::iterator it = m_logOIDs.find (addr.s_addr);
    if (it == m_logOIDs.end ())
    {
      updateCache (addr.s_addr, oid);
      return false;
    }

    const std::set<oidentry, oidcomp> &myset = it->second;
    std::set<oidentry, oidcomp>::const_iterator i = myset.find (oidentry (oid));
    if (i == myset.end ())
    {
      updateCache (addr.s_addr, oid);
      return false;
    }
    id = (*i).m_id;
    return true;
  }

  void oidcache::addToLogList (const char *ip, unsigned int id, const char *oid)
  {
    in_addr addr;
    if (inet_aton (ip, &addr) == 0)
      return;

    addToLogList (addr.s_addr, id, oid);
  }

  void oidcache::addToLogList (unsigned long ip, unsigned int id, const char *oid)
  {
    m_logOIDs[ip].insert (oidentry (oid, id));
  }

  void oidcache::clearLogList ()
  {
    clearList (m_logOIDs);
  }

  void oidcache::clearNewList ()
  {
    clearList (m_newOIDs);
  }

  void oidcache::clearList (iphash &hash)
  {
    for (iphash::iterator it = hash.begin (); it != hash.end (); ++it)
      it->second.clear ();
    hash.clear ();
  }

  void oidcache::updateCache (unsigned long ip, const char *oid, unsigned int id, bool isnew)
  {
    iphash::iterator it = m_newOIDs.find (ip);
    if (it != m_newOIDs.end ())
    {
      const std::set<oidentry, oidcomp> &myset = it->second;
      if (myset.find (oidentry (oid)) != myset.end ())
        return;
    }
    m_newOIDs[ip].insert (oidentry (oid, id, isnew));
  }
}

#ifdef __TEST__

#include <cstdio>

using namespace netmon;

int main ()
{
  oidcache c;

  if (c.toLog ("127.0.0.1", "1.3.6.1."))
    printf ("Will log\n");

  c.addToLogList ("127.0.0.1", "1.3.6.1.");
  if (c.toLog ("127.0.0.1", "1.3.6.1."))
    printf ("Will log\n");

  c.clearLogList ();
  if (c.toLog ("127.0.0.1", "1.3.6.1."))
    printf ("Will log\n");
}

#endif
