#ifndef __DB_OID_CACHE__
#define __DB_OID_CACHE__

#include "oidcache.h"
#include "db.h"

namespace netmon
{
  class oid_translator;

  class dboidcache : public oidcache
  {
    public:
      static dboidcache *instance ();
      void init (db *);
      void reload ();
      bool getTranslation (const char *, std::string &);

#ifdef __TEST__
      void dump ();
#endif // __TEST__

    protected:
      dboidcache () {}
      virtual ~dboidcache ();
      void storeNewOIDs ();
      void loadOIDs ();
      void insert (unsigned long, const std::string &);

      static dboidcache *m_instance;
      db *m_db;
      oid_translator *m_oidTranslator;
  };
}

#endif // __DB_OID_CACHE__
