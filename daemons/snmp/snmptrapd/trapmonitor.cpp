#include <sstream>

#include "alerts.h"
#include "alertutil.h"
#include "exception.h"
#include "trapmonitor.h"
#include "trapalertmgr.h"
#include "dboidcache.h"
#include "snmptrapd.h"
#include "snmp_pp/snmp_pp.h"
#include "snmp_pp/collect.h"
#include "snmp_pp/notifyqueue.h"

#define TRAPPORT 162

extern int reload;

namespace netmon
{
  trapmonitor::trapmonitor (db *mydb, snmp_trap_daemon *d)
    : m_db (mydb), m_daemon (d)
  {
    init();
  }

  trapmonitor::~trapmonitor ()
  {
    Snmp_pp::Snmp::socket_cleanup ();
    delete m_alertManager;
  }

  void trapmonitor::run ()
  {
    int status; 
    Snmp_pp::Snmp snmp (status, 0);
    if (status != SNMP_CLASS_SUCCESS)
    {
      std::string exc = std::string ("SNMP++ Session Create Fail: ") + snmp.error_msg(status);
      throw netmon_exception (exc);
    }

    Snmp_pp::OidCollection oidc;
    Snmp_pp::TargetCollection targetc;
    snmp.notify_set_listen_port (TRAPPORT);
    status = snmp.notify_register (oidc, targetc, callback, this);
    if (status != SNMP_CLASS_SUCCESS)
    {
      std::ostringstream exc;
      exc << "Error register for notify (" << status << "): "
          << snmp.error_msg(status);
      throw netmon_exception (exc.str());
    }

    while (!m_daemon->termSignaled())
    {
      snmp.eventListHolder->SNMPProcessEvents();
      if (reload)
      {
        reload = 0;
        dboidcache::instance()->reload();
        m_alertManager->reload();
      }
    }
  }

  void trapmonitor::init ()
  {
    Snmp_pp::Snmp::socket_startup();
    dboidcache::instance()->init (m_db);
    m_alertManager = new trap_alert_manager (m_db);
    m_alertManager->init();
  }

  void trapmonitor::callback (int reason, Snmp_pp::Snmp *snmp, Snmp_pp::Pdu &pdu, Snmp_pp::SnmpTarget &target, void *me)
  {
    trapmonitor *tm = reinterpret_cast<trapmonitor *>(me);
    Snmp_pp::Vb nextVb;
    Snmp_pp::GenAddress addr;

    target.get_address (addr);
    Snmp_pp::UdpAddress from (addr);

    Snmp_pp::Oid id;
    pdu.get_notify_id(id);

    std::ostringstream sql;
    std::string ip(from.get_printable());
    std::string port = "0";

    size_t pos = ip.find ('/');
    if (pos != std::string::npos)
    {
      port = std::string (ip, pos + 1, ip.size() - pos);
      ip = std::string (ip, 0, pos);
    }

    unsigned int trapid;
    if (!dboidcache::instance()->toLog (ip.c_str(), id.get_printable(), trapid))
      return;

    sql << "INSERT INTO SNMPTRAP_LOG VALUES (DEFAULT, '" << ip << "', "
        << port << ", '" << id.get_printable() << "', " << (int) time(0)
        << ")";

    printf ("%s\n", sql.str().c_str());
    tm->m_db->execSQL (sql.str().c_str());

    std::ostringstream payload;

    std::string oidtrans;
    if (dboidcache::instance()->getTranslation (id.get_printable(), oidtrans))
    {
      std::ostringstream ids;
      ids << oidtrans << " (" << id.get_printable() << ")";
      oidtrans = ids.str();
    }
    else
      oidtrans = id.get_printable();

    for (int i = 0; i < pdu.get_vb_count(); ++i)
    {
      pdu.get_vb(nextVb, i);

      std::string sql2 = "INSERT INTO SNMPTRAPOIDS VALUES ((SELECT CURRVAL('SNMPTRAP_LOG_ID_SEQ')), '";
      const char *var = nextVb.get_printable_value();
      std::string varval("");
      if (var != 0)
      {
        char *esc = new char[strlen (var) * 2 + 1];
        PQescapeString (esc, var, strlen (var));
        varval = std::string (esc);
        delete[] esc;
      }
      std::string oidtrans;
      if (dboidcache::instance()->getTranslation (nextVb.get_printable_oid(), oidtrans))
        payload << oidtrans << " (" << nextVb.get_printable_oid() << ")";
      else
        payload << nextVb.get_printable_oid();
      payload << ": " << varval << std::endl;

      sql2 += std::string (nextVb.get_printable_oid()) + "', '" + varval + "')";
      printf ("%s\n", sql2.c_str());
      tm->m_db->execSQL (sql2.c_str());
    }
    values_map vals;
    vals[alerthandler::ePayload] = keyword_value (payload.str());
    vals[alerthandler::eHostIP] = keyword_value (ip);
    vals[alerthandler::eOID] = keyword_value (oidtrans);
    tm->m_alertManager->getAlerts (trapid, vals);
  }
}
