#include "snmpproxyd.h"
#include "server.h"
#include "dvars.h"

namespace netmon
{
  static const char *socket_path = "/var/run/.snmp_proxy.s";

  snmp_proxyd::~snmp_proxyd ()
  {
  }

  void snmp_proxyd::init ()
  {
    daemon::init();
    std::string mibs("");
    m_vars->get ("mib_path", mibs);
    m_server = new netmon::server (socket_path, mibs, this);
  }

  int snmp_proxyd::run ()
  {
    m_server->run();
    delete m_server;
    return 0;
  }
}
