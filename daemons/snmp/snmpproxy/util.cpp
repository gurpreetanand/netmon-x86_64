#include "util.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

namespace netmon
{
  namespace util
  {
    void saveOutputFlags (std::map<int, int> &map, int &outputStringFormat, int &outputOIDFormat)
    {
      map[NETSNMP_DS_LIB_DONT_BREAKDOWN_OIDS] = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DONT_BREAKDOWN_OIDS);
      map[NETSNMP_DS_LIB_PRINT_NUMERIC_ENUM]  = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_NUMERIC_ENUM);
      map[NETSNMP_DS_LIB_ESCAPE_QUOTES]       = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_ESCAPE_QUOTES);
      map[NETSNMP_DS_LIB_QUICKE_PRINT]        = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICKE_PRINT);
      map[NETSNMP_DS_LIB_QUICK_PRINT]         = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_QUICK_PRINT);
      map[NETSNMP_DS_LIB_NUMERIC_TIMETICKS]   = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_NUMERIC_TIMETICKS);
      map[NETSNMP_DS_LIB_PRINT_HEX_TEXT]      = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_HEX_TEXT);
      map[NETSNMP_DS_LIB_DONT_PRINT_UNITS]    = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_DONT_PRINT_UNITS);
      map[NETSNMP_DS_LIB_PRINT_BARE_VALUE]    = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_PRINT_BARE_VALUE);
      map[NETSNMP_DS_LIB_EXTENDED_INDEX]      = netsnmp_ds_get_boolean(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_EXTENDED_INDEX);

      outputStringFormat = netsnmp_ds_get_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_STRING_OUTPUT_FORMAT);
      outputOIDFormat = netsnmp_ds_get_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT);
    }

    void restoreOutputFlags (const std::map<int, int> &map, int outputStringFormat, int outputOIDFormat)
    {
      for (std::map<int, int>::const_iterator i = map.begin(); i != map.end(); ++i)
        netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID, i->first, i->second);

      netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_STRING_OUTPUT_FORMAT, outputStringFormat);
      netsnmp_ds_set_int(NETSNMP_DS_LIBRARY_ID, NETSNMP_DS_LIB_OID_OUTPUT_FORMAT, outputOIDFormat);
    }
  }
}
