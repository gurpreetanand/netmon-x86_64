#ifndef __CLIENT__
#define __CLIENT__

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

#include <list>
#include <map>
#include <string>
#include <vector>

namespace netmon
{
  struct OID
  {
    std::string m_name;
    oid m_oid[MAX_OID_LEN];
    size_t m_len;
  };

  class client
  {
    public:
      enum type { _eNULL, _eSNMP_GET, _eSNMP_WALK };
      client (int fd);
      ~client ();

      int getFD () const
      {
        return m_fd;
      }

      snmp_session *getSession () const
      {
        return m_session;
      }

      unsigned int getArgc () const
      {
        return m_args.size();
      }

      char **getArgs () const
      {
        return (char **)&(m_args[0]);
      }

      void addOID (OID *oid)
      {
        bool empty = m_oids.empty();
        m_oids.push_back (oid);
        if (empty)
          m_iter = m_oids.begin();
      }

      OID *getNextOID ()
      {
        OID *o = 0;
        if (m_iter != m_oids.end())
        {
          o = *m_iter;
          ++m_iter;
        }
        return o;
      }

      bool hasMoreOIDs () const
      {
        return m_iter != m_oids.end();
      }

      char *getWriteBuffer ();

      int getWriteBuffSize () const
      {
        return m_writeBuffSize - m_wrote;
      }

      void wroteBytes (int b)
      {
        if (b > 0)
          m_wrote += b;
      }

      type getType () const
      {
        return m_type;
      }

      void setType (type t)
      {
        m_type = t;
      }

      bool isDone () const
      {
        return m_done;
      }

      void done ()
      {
        m_done = true;
      }

      void end ()
      {
        done();
        m_wrote = 0;
      }

      void setRootOID (const OID &o)
      {
        m_rootOID.m_name = o.m_name;
        memcpy (m_rootOID.m_oid, o.m_oid, o.m_len * sizeof (oid));
        m_rootOID.m_len = o.m_len;
      }

      const OID &getRootOID ()
      {
        return m_rootOID;
      }

      size_t getRootSize ()
      {
        return m_rootOID.m_len;
      }

      void rootValid ()
      {
        m_rootValid = true;
      }

      bool isRootValid () const
      {
        return m_rootValid;
      }

      bool writeWillBlock () const
      {
        return m_wantsWrite;
      }

      bool hasPendingData () const
      {
        if (m_wrote == 0 || m_wrote == m_writeP)
          return false;
        return true;
      }

      void saveOutputFlags ();
      void restoreOutputFlags ();

      int read ();
      int write ();

      bool parseInput ();

    protected:
      void init ();
      void addArg ();

      int m_fd;
      bool m_done;
      snmp_session *m_session;
      char *m_readBuff;
      char *m_writeBuff;
      int m_readP;
      int m_writeP;
      int m_wrote;
      int m_parseP;
      std::vector<char *> m_args;
      std::list<OID *> m_oids;
      std::list<OID *>::iterator m_iter;
      OID m_rootOID;
      bool m_rootValid;
      type m_type;
      std::string m_arg;
      int m_writeBuffSize;
      bool m_wantsWrite;
      std::map<int, int> m_outputFlags;
      int m_outputStringFormat;
      int m_outputOIDFormat;
      static const int m_buffSize = 4096;
  };
}

#endif // __CLIENT__
