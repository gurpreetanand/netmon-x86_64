#ifndef __SNMP_AUTO_DAEMON__
#define __SNMP_AUTO_DAEMON__

#include "daemon.h"

namespace netmon
{
  class snmpseeker;

  class snmp_autod : public daemon
  {
    public:
      snmp_autod (int, char **);
      ~snmp_autod ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "snmpautod";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/snmpautod";
      }

      snmpseeker *m_seeker;
  };
}

#endif // __SNMP_AUTO_DAEMON__
