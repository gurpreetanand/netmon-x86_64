#include <netinet/in.h>
#include <arpa/inet.h>
#include <sstream>
#include <tre/regex.h>
#include <libpq-fe.h>
#include <stdlib.h>

#include "seeker.h"
#include "dvars.h"
#include "log.h"
#include "exception.h"
#include "dashboards.h"
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  static const char *daemonname = "snmpautod";

  // default values
  static unsigned short dport = 161;
  static const char *dcommunity = "public";
  static const char *dversion = "1";

  snmpseeker::snmpseeker (db *mydb)
    : m_db (mydb), m_community (dcommunity), m_version (dversion), m_count (0),
      m_port (dport), m_timeout (eTimeout)
  {
    init();
  }

  snmpseeker::~snmpseeker ()
  {
    Snmp_pp::Snmp::socket_cleanup();
  }

  void snmpseeker::run ()
  {
    readConfig();
    seek();
    updateFound();
    //removeOldAgents();
    m_visitedNetworks.clear();
  }

  void snmpseeker::seek ()
  {
    for (netlist::iterator it = m_networks.begin(); it != m_networks.end(); ++it)
    {
      for (std::list<std::string>::iterator i = m_communities.begin(); i != m_communities.end(); ++i)
      {
        m_community = (*i);
        inetaddr addr (it->second.c_str());
        inetaddr bcast = addr.getBroadcast();
        if (!isAddressSeen (inetaddr (it->second.c_str())))
          seekBCast (bcast);
        seekEach (*it);
      }
      m_visitedNetworks.push_back (std::make_pair (inetaddr (it->first.c_str()), inetaddr (it->second.c_str())));
    }
  }

  void snmpseeker::seekBCast (const inetaddr &bcast)
  {
    using namespace Snmp_pp;
    UdpAddress address (bcast.addr());

    snmp_version version = version1;
    if (m_version == "v2c")
      version = version2c;
    int timeout = 100;
    OctetStr community (m_community.c_str());

    int status;
    Snmp snmp (status, 0, false);

    if (status != SNMP_CLASS_SUCCESS)
    {
      throw netmon_exception ("Cannot create session");
    }

    address.set_port (m_port);

    UdpAddressCollection addresses;

    for (int loops = 1; loops <= 2; ++loops)
    {
      status = snmp.broadcast_discovery(addresses, (timeout + 99) / 100,
                                        address, version, &community);
    }

    UdpAddressCollection filtered_addrs;
    int dummy_pos;

    for (int n = 0; n < addresses.size(); ++n)
      if (filtered_addrs.find(addresses[n], dummy_pos) == FALSE)
        filtered_addrs += addresses[n];

    for (int m = 0; m < filtered_addrs.size(); ++m)
    {
      char agent[32];
      strncpy (agent, filtered_addrs[m].get_printable(), 31);
      char *pos = strchr (agent, '/');
      if (pos != 0)
        *pos = '\0';
      in_addr ina;
      if (inet_aton (agent, &ina) != 0)
      {
        ipmap::iterator i = m_agents.find (ina.s_addr);
        time_t now = time (0);
        if (i == m_agents.end())
        {
          m_agents[ina.s_addr] = now;
          std::string sysdescr("");
          (void) getSysDescr ((const char *) agent, sysdescr);
          notify (agent, sysdescr);
        }
        else
          i->second = now;
      }
    }
  }

  void snmpseeker::seekEach (const localnet &lnet)
  {
    inetaddr from (lnet.first.c_str());
    inetaddr to (lnet.second.c_str());

    if (from == to)
      return;

    while (from <= to)
    {
      if (!from.isValid() || isAddressSeen (from)) // skip invalid and already seen addresses
      {
        ++from;
        continue;
      }
      ipmap::iterator i = m_agents.find (from.iaddr());
      std::string sysdescr("");

      if (getSysDescr (from.addr(), sysdescr))
      {
        time_t now = time (0);
        if (i != m_agents.end())
          i->second = now;
        else
        {
          m_agents[from.iaddr()] = now;
          notify (from.addr(), sysdescr);
        }
      }
      ++from;
    }
  }

  bool snmpseeker::getSysDescr (const char *ip, std::string &desc)
  {
    using namespace Snmp_pp;
    UdpAddress address(ip);
    Snmp_pp::Oid oid ("1.3.6.1.2.1.1.1.0");
    OctetStr community(m_community.c_str());
    int status;
    Snmp snmp (status, 0, false);

    if (status != SNMP_CLASS_SUCCESS)
      return false;

    Pdu pdu;
    Vb vb;
    vb.set_oid (oid);
    pdu += vb;

    address.set_port (m_port);
    CTarget ctarget (address);

    if (m_version == "2c")
      ctarget.set_version (version2c);
    else
      ctarget.set_version (version1);

    ctarget.set_retry (1);
    ctarget.set_timeout (100);
    ctarget.set_readcommunity (community);
    SnmpTarget *target;
    target = &ctarget;

    status = snmp.get (pdu, *target);

    if (status == SNMP_CLASS_SUCCESS)
    {
      pdu.get_vb (vb,0);
      desc = std::string (vb.get_printable_value());
      return true;
    }

    return false;
  }

  void snmpseeker::notify (const char *ip, const std::string &sysdescr)
  {
    try
    {
      std::ostringstream sql;
      std::string profile = getProfile(sysdescr);

      sql << "INSERT INTO DEVICES (IP_ADDRESS, LABEL, SNMP_COMMUNITY, SYSDESCR, PROFILE) "
          << "VALUES ('" << ip << "', '" << ip << "', '" << m_community << "', '" << sysdescr << "', '" << profile << "') "
          << "RETURNING id";
      printf ("%s\n", sql.str().c_str());
      PGresult* res = m_db->execQuery(sql.str().c_str());
      ++m_count;

      sql.str("");
      sql << "INSERT INTO servers (ip, name, protocol, interval, timeout, pending, log_timeout) "
      << "VALUES ('" << ip << "', '" << ip << "', 'ICMP', 60, 1, 'Y', 0)";

      m_db->execSQL (sql.str().c_str());

      if (PQntuples(res) == 1) {
        int device_id = atoi(PQgetvalue(res, 0, 0));
        // Clear PG
        PQclear(res);

        if (
          !m_dashboards.isObject() ||
          !m_dashboards[profile].isObject() ||
          !m_dashboards[profile]["oids"].isArray()
        ) {
          printf("No match on %s->oids[] in dashboard file\n", profile.c_str());
          return;
        }

        Json::Value oids = m_dashboards[profile]["oids"];
        for (Json::ValueIterator oid = oids.begin(); oid != oids.end(); ++oid) {
          if ((*oid).isObject() && (*oid)["addTracker"].isObject()) {
            sql.str("");
            sql << "INSERT INTO oids (device_id, oid, enable_logging, label, homedisplay) "
            << "VALUES (" << device_id << ", '" << (*oid)["oid"].asString() << "', 't', '" << (*oid)["name"].asString() << "', 'f')";
            m_db->execSQL (sql.str().c_str());
          }
        }
      }
    }
    catch (db_exception &e)
    {
      printf ("Query failed: %s", e.what());
      log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  void snmpseeker::reload ()
  {
    m_networks.clear();
    getNetworks();
  }

  void snmpseeker::init ()
  {
    m_dashboards = dashboards::getDashboards();

    Snmp_pp::Snmp::socket_startup();

    getNetworks();
    populate();
  }

  void snmpseeker::readConfig ()
  {
    dvars vars (*m_db, daemonname);

    m_port = 161;
    int port;
    if (vars.get ("port", port))
      m_port = port;
    vars.get ("community", m_community);
    vars.get ("version", m_version);
    vars.get ("found", m_found);
    vars.get ("timeout", m_timeout);
    parseCommunityList (m_community);
  }

  void snmpseeker::parseCommunityList (const std::string &list)
  {
    unsigned int i = 0;
    std::string com;

    while (i < list.size())
    {
      if (list[i] == '\\')
      {
        ++i;
        if (i >= list.size())
        {
          if (com.size() > 0)
          {
            com.push_back ('\\');
            m_communities.push_back (com);
          }
          return;
        }
        if (list[i] == ',')
          com.push_back (',');
        else
        {
          com.push_back ('\\');
          com.push_back (list[i]);
        }
      }
      else if (list[i] == ',' && com.size() > 0)
      {
        m_communities.push_back (com);
        com = "";
      }
      else
        com.push_back (list[i]);
      ++i;
    }
    if (com.size() > 0)
      m_communities.push_back (com);
  }

  void snmpseeker::getNetworks ()
  {
    PGresult *res = m_db->execQuery ("SELECT NETWORK, BROADCAST FROM LOCALNETS WHERE ENABLE_SNMP_DISCOVERY = 't'");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      m_networks.push_back (std::make_pair (std::string (PQgetvalue (res, i, 0)),
                                            std::string (PQgetvalue (res, i, 1))));
    }
    PQclear (res);
  }

  void snmpseeker::populate ()
  {
    time_t now = time (0);
    PGresult *res = m_db->execQuery ("SELECT IP_ADDRESS FROM DEVICES");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      in_addr addr;
      if (inet_aton (PQgetvalue (res, i, 0), &addr) == 0)
        continue;
      m_agents[addr.s_addr] = now;
    }
    PQclear (res);
  }

  void snmpseeker::removeOldAgents ()
  {
    in_addr addr;
    std::list<unsigned int> oldips;
    time_t now = time (0);

    for (ipmap::iterator i = m_agents.begin(); i != m_agents.end(); ++i)
    {
      if ((now - i->second) < m_timeout)
        continue;
      addr.s_addr = i->first;
      oldips.push_back (i->first);
      try
      {
        std::ostringstream sql;
        sql << "DELETE FROM DEVICES WHERE IP_ADDRESS = '"
            << inet_ntoa (addr) << "' AND ENABLE_SNMP = 'f' AND ENABLE_NETFLOW = 'f'";
        printf ("%s\n", sql.str().c_str());
        int ret = m_db->execSQL (sql.str().c_str());
        if (ret < 1)
          i->second = true;
      }
      catch (db_exception &e)
      {
        printf ("Query failed: %s", e.what());
        log::instance()->add(netmon::_eLogError, e.what());
      }
    }
    for (std::list<unsigned int>::iterator i = oldips.begin(); i != oldips.end(); ++i)
      m_agents.erase (*i);
  }

  bool snmpseeker::isAddressSeen (const inetaddr &a)
  {
    for (inet_list::iterator i = m_visitedNetworks.begin(); i != m_visitedNetworks.end(); ++i)
      if (i->first <= a && a < i->second)
        return true;
    return false;
  }

  void snmpseeker::updateFound ()
  {
    if (m_found != 0)
      return;

    std::ostringstream sql;
    sql << "update daemonsconfig set value = '" << m_count << "' where var = '"
        << "found' and daemon_id = (select id from daemons where name = 'snmpautod')";

    try
    {
      m_db->execSQL (sql.str().c_str());
    }
    catch (db_exception &e)
    {
      printf ("Query failed: %s", e.what());
      log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  std::string snmpseeker::getProfile(std::string sysdescr) {
    printf("Trying to match dashboard: [%s]", sysdescr.c_str());
    for (Json::ValueIterator dash = m_dashboards.begin(); dash != m_dashboards.end(); ++dash) {
      printf("On dash %s\n", dash.key().asString().c_str());
      if ((*dash).isObject()) {
        Json::Value regexList = (*dash).get("regex", Json::Value(Json::arrayValue));
        printf("Past\n");
        if (regexList.isArray()) {
          for (unsigned int i = 0; i < regexList.size(); i++) {
            printf("Trying dashboard: [%s] Regex: [%s] I: [%d]\n",
              dash.key().asString().c_str(), regexList[i].asString().c_str(), i
            );
            regex_t rt;
            if (regcomp(&rt, regexList[i].asString().c_str(), REG_ICASE|REG_EXTENDED|REG_NOSUB) == 0) {
              if (regexec(&rt, sysdescr.c_str(), 0, NULL, 0) == 0) {
                // Found a match, free memory and get out of here.
                regfree(&rt);

                printf("Found match: %s\n", dash.key().asString().c_str());

                return dash.key().asString();

              } else {
                regfree(&rt);
              }
            }
          }
        }
      }
    }
    printf("Returning default\n");
    return "snmp";
  }
}
