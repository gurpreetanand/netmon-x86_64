#ifndef __SNMP_SEEKER__
#define __SNMP_SEEKER__

#include <string>
#include <list>
#include <map>
#include <jsoncpp/json/value.h>

#include "../lib/db.h"
#include "../lib/inetaddr.h"
#include "snmp_pp/target.h"

namespace netmon
{
  class snmpseeker
  {
    public:
      snmpseeker (db *);
      ~snmpseeker ();

      void run ();
      void reload ();

    protected:
      typedef std::pair <std::string, std::string> localnet;
      typedef std::list <localnet> netlist;
      typedef std::map <uint32_t, time_t> ipmap; // IP -> timestamp

      void init ();
      void readConfig ();
      void getNetworks ();
      void populate ();
      void seek ();
      void seekBCast (const inetaddr &);
      void seekEach (const localnet &);
      void notify (const char *, const std::string &);
      void removeOldAgents ();
      bool getSysDescr (const char *, std::string &);
      bool isAddressSeen (const inetaddr &);
      void parseCommunityList (const std::string &);
      void updateFound ();
      std::string getProfile(std::string sysdescr);

      netlist m_networks;
      ipmap m_agents;
      db *m_db;
      std::string m_community;
      std::string m_version;
      int m_found;
      int m_count;
      unsigned short m_port;
      std::list<std::string> m_communities;
      int m_timeout;

      enum { eTimeout = 3 * 24 * 60 * 60 };

      typedef std::pair <inetaddr, inetaddr> inet_pair;
      typedef std::list <inet_pair> inet_list;
      inet_list m_visitedNetworks;

      Json::Value m_dashboards;
  };
}

#endif // __SNMP_SEEKER__
