#include "snmpautod.h"
#include "seeker.h"

namespace netmon
{
  snmp_autod::snmp_autod (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  snmp_autod::~snmp_autod ()
  {
    delete m_seeker;
  }

  void snmp_autod::init ()
  {
    daemon::init();
    m_seeker = new snmpseeker (m_db);
  }

  int snmp_autod::run ()
  {
    while (!isTermSignaled())
    {
      m_seeker->run();
      sleep (5 * 60); // slep 5 minutes
      printf ("\n\n New scan\n=========\n");
      m_seeker->reload();
    }
    return 0; // never reached
  }
}
