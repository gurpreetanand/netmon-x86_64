#include "dbcache.h"

#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>
#include <sstream>

#include "log.h"

namespace netmon
{
  dbsnmpcache::dbsnmpcache (db *mydb)
    : snmpcache (), m_db (mydb)
  {
    load();
  }

  dbsnmpcache::~dbsnmpcache ()
  {
  }

  void dbsnmpcache::update (uint32_t ip, uint32_t iface, uint32_t id, const snmpifacedata &data, uint32_t speed)
  {
    snmpcache::update (ip, iface, id, data, speed);

    std::ostringstream sql;
    sql << "UPDATE INTERFACES SET INRESETS = " << getInResets (ip, iface)
        << ", OUTRESETS = " << getOutResets (ip, iface)
        << ", LAST_INBOUND = " << getInbound (ip, iface)
        << ", LAST_OUTBOUND = " << getOutbound (ip, iface)
        << ", LAST_INBOUND_THROUGHPUT = " << getThroughput (ip, iface, true)
        << ", LAST_OUTBOUND_THROUGHPUT  = " << getThroughput (ip, iface, false)
        << " WHERE INTERFACE = " << iface << " AND ID = " << id;

    try
    {
      m_db->execSQL (sql.str().c_str());
      printf ("%s\n", sql.str().c_str());
    }
    catch (db_exception &e)
    {
     std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }

  void dbsnmpcache::load ()
  {
    PGresult *res;
    res = m_db->execQuery (
    "SELECT a.id, b.interface, b.inresets, b.outresets, b.speed, b.id \
    from devices a, interfaces b where b.device_id = a.id");

    for (int i = 0; i < PQntuples (res); ++i)
    {
      unsigned int id = atoi (PQgetvalue (res, i, 0));
      uint32_t iface = atoi (PQgetvalue (res, i, 1));
      uint32_t inr = atoi (PQgetvalue (res, i, 2));
      uint32_t outr = atoi (PQgetvalue (res, i, 3));
      uint32_t speed = atoi (PQgetvalue (res, i, 4));
      uint32_t ifaceid = atoi (PQgetvalue (res, i, 5));

      snmpifaceaggdata tmp;
      tmp.m_inResets = inr;
      tmp.m_outResets = outr;
      tmp.m_speed = speed;
      tmp.m_id = ifaceid;

      m_map[id][iface] = tmp;
    }
    PQclear (res);
  }
}
