#include "snmpcheck.h"

#include <cctype>
#include <set>
#include <sstream>
#include <list>
#include <jsoncpp/json/writer.h>

#include "alerts.h"
#include "alertmngr.h"
#include "alertutil.h"
#include "dbcache.h"
#include "log.h"
#include "snmpmond.h"
#include "dashboards.h"

#define MINUTE 60
#define HOUR   3600

namespace netmon
{
  static u_short SNMPport = 161;

  Snmp_pp::Oid snmpcheck::m_sysDescr ("1.3.6.1.2.1.1.1.0");
  Snmp_pp::Oid snmpcheck::m_iFaces ("1.3.6.1.2.1.2.2.1.1");

  static void formatMAC (const std::string &from, std::string &to)
  {
    bool start = true;
    int cnt = 0;

    for (unsigned int i = 0; i < from.size (); ++i)
    {
      if (cnt == 12)
        return;
      if (isspace (from[i]) && (start || cnt > 11))
        continue;
      if (from[i] == ' ')
        to.push_back (':');
      if (isxdigit (from[i]))
      {
        start = false;
        to.push_back (from[i]);
        ++cnt;
      }
    }
  }

  static bool isMACValid (const std::string &mac)
  {
    int i;
    if (sscanf (mac.c_str(), "%02X:%02X:%02X:%02X:%02X:%02X", &i, &i, &i, &i, &i, &i) == 6)
      return true;
    return false;
  }

  snmpcheck::snmpcheck (db *mydb, snmp_mond *d)
    : m_db (mydb), m_daemon (d), m_round (0)
  {
    Snmp_pp::OctetStr::set_hex_output_type (Snmp_pp::OctetStr::OutputClear);
    init();
  }

  snmpcheck::~snmpcheck ()
  {
    delete m_cache;
    delete m_snmp;
    delete m_ifaceAlertManager;
  }

  void snmpcheck::run () throw (netmon_exception)
  {
    if (m_round % 12 == 0) // send our keep alive msg every minute
    {
      if (!m_daemon->devLimitsValid())
        exit (1);
    }

    m_ifaceAlertManager->reload();
    checkAgents();
    if (++m_round > 120)
      m_round = 0;
  }

  void snmpcheck::init ()
  {
    m_dashboards = dashboards::getDashboards();
    setPendingFlag();
    m_cache = new dbsnmpcache (m_db);
    m_snmp = new snmp;
    m_ifaceAlertManager = new iface_alert_manager (m_db);
    m_ifaceAlertManager->init();
    m_ignores = SnmpIgnores();
  }

  void snmpcheck::checkAgents ()
  {
    m_ignores.cleanup();
    static std::ostringstream sql;

    sql.str("");
    sql << "SELECT ID, IP_ADDRESS, EXAMINED, SNMP_COMMUNITY, INTERVAL, "
        << "SNMP_PORT FROM DEVICES WHERE PENDING != 't' "
        << "AND ENABLE_SNMP = 't' "
        << "AND (TIMESTAMP + INTERVAL) < " << (int) time (0);

    printf ("%s\n", sql.str().c_str());

    PGresult *res = NULL;
    try {
      res = m_db->execQuery (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }  

    for (int i = 0; i < PQntuples (res); ++i)
    {
      if (strlen (PQgetvalue (res, i, 5)) < 1)
        m_currport = SNMPport;
      else
        m_currport = atoi (PQgetvalue (res, i, 5));

      m_snmp->setPort (m_currport);

      if (*PQgetvalue (res, i, 2) == 'f')
      {
        bool ret = findSNMPInterfaces (PQgetvalue (res, i, 1), PQgetvalue (res, i, 3), PQgetvalue (res, i, 0));
        if (!ret)
        {
          setExaminedFlag (PQgetvalue (res, i, 1), false, false);
          continue;
        }
      }
      else if (m_round == 0)
        checkInterfaces (PQgetvalue (res, i, 1), PQgetvalue (res, i, 3), PQgetvalue (res, i, 0));
      unsigned int interval = atoi (PQgetvalue (res, i, 4));
      checkAgent (atoi (PQgetvalue (res, i, 0)), PQgetvalue (res, i, 1), PQgetvalue (res, i, 3), interval);
    }
    PQclear (res);
//    checkIPAlerts ();
  }

  void snmpcheck::findConnectedMACs (const char *ip, const char *community)
  {
    static const Snmp_pp::Oid vlOid("1.3.6.1.4.1.9.9.46.1.3.1.1.2");
    static const Snmp_pp::Oid bPort("1.3.6.1.2.1.17.4.3.1.2");
    static const Snmp_pp::Oid bIndx("1.3.6.1.2.1.17.1.4.1.2");

    m_cam.clear();

    m_snmp->walkSNMP (ip, community, vlOid, m_cam);

    std::string vlan;
    m_cam.setMode (camfunctor::_eBridge);

    if (!m_cam.hasVLANs()) // this is not Cisco device
    {
      m_snmp->walkSNMP (ip, community, bPort, m_cam);
    }
    else
    {
      while (m_cam.getNextVLAN (vlan))
      {
        std::ostringstream csi;
        csi << community << "@" << vlan;
        m_snmp->walkSNMP (ip, csi.str().c_str(), bPort, m_cam);
      }
    }

    m_cam.setMode (camfunctor::_ePort);
    m_snmp->walkSNMP (ip, community, bIndx, m_cam);
  }

  bool snmpcheck::findSNMPInterfaces (const char *ip, const char *community, const char *id)
  {
    printf("Begin findSNMPInterfaces()\n");
    ifacesfunctor ifaces;

    if (m_ignores.isOidValid(ip, "1.3.6.1.2.1.2.2.1.1")) {
      int status = m_snmp->walkSNMP (ip, community, m_iFaces, ifaces);
      if (status != 1) {
        m_ignores.addSkip(ip, "1.3.6.1.2.1.2.2.1.1", status);
      }
      findConnectedMACs (ip, community);

      for (std::list<std::string>::iterator it = ifaces.m_ifaces.begin(); it != ifaces.m_ifaces.end(); ++it)
        insertInterface (ip, community, id, *it);

    } else {
      printf("Skipped polling interfaces for %s\n", ip);
    }

    printf("End findSNMPInterfaces()\n");
    return true;
  }

  bool snmpcheck::getInterfaceData (const char *ip, const char *community, const std::string &iface, iface_data &data)
  {
    std::string ifName = "N/A";
    std::string ifDescr = "N/A";
    std::string ifSpeed;
    std::string ifMAC;
    std::string ifAlias;
    std::string oid = "1.3.6.1.2.1.31.1.1.1.1." + iface;

    (void) m_snmp->snmpGet (ip, community, oid.c_str(), ifName);

    oid = "1.3.6.1.2.1.2.2.1.2." + iface;
    if (!m_snmp->snmpGet (ip, community, oid.c_str(), ifDescr))
      return false;

    oid = "1.3.6.1.2.1.31.1.1.1.18." + iface;
    if (m_snmp->snmpGet (ip, community, oid.c_str(), ifAlias))
    {
      if (ifDescr.compare(ifName) == 0 && ifAlias.size() > 0)
      {
	ifDescr = ifAlias;
      }
    }

    if (ifDescr.size() == 0)
      ifDescr = "Interface " + iface;

    oid = "1.3.6.1.2.1.2.2.1.5." + iface;
    if (!m_snmp->snmpGet (ip, community, oid.c_str(), ifSpeed))
      return false;

    std::map<unsigned int, unsigned int>::iterator i = m_cam.m_ports.find (atoi (iface.c_str()));
    if (i != m_cam.m_ports.end())
    {
      std::list<std::string> &l = m_cam.m_bridge[i->second];
      if (l.size() > 0)
        ifMAC = l.front();
    }

    std::string escifMAC;
    if (!isMACValid (ifMAC))
    {
      oid = "1.3.6.1.2.1.2.2.1.6." + iface;
      Snmp_pp::OctetStr::set_hex_output_type (Snmp_pp::OctetStr::OutputHex);
      bool ret = m_snmp->snmpGet (ip, community, oid.c_str(), ifMAC);
      Snmp_pp::OctetStr::set_hex_output_type (Snmp_pp::OctetStr::OutputClear);
      if (!ret)
        return false;
      formatMAC (ifMAC, escifMAC);
      if (!isMACValid (escifMAC))
        escifMAC = std::string ("");
    }
    else
      escifMAC = ifMAC;

    char *escifDescr = m_db->escapeString (ifDescr.c_str(), ifDescr.size());
    if (escifDescr == 0)
    {
      netmon::log::instance()->add(netmon::_eLogError, "Cannot escape string");
      return false;
    }

    char *escifName = m_db->escapeString (ifName.c_str(), ifName.size());
    if (escifName == 0)
    {
      delete[] escifDescr;
      netmon::log::instance()->add(netmon::_eLogError, "Cannot escape string");
      return false;
    }

    data.m_name = escifName;
    data.m_descr = escifDescr;
    data.m_speed = ifSpeed;
    data.m_mac = escifMAC;

    delete[] escifDescr;
    delete[] escifName;
    
    cacheInterfaceErrors(ip, community, iface);

    return true;
  }
  
  void snmpcheck::cacheInterfaceErrors(const char *ip, const char *community, const std::string &iface)
  {


    if (m_dashboards["networkCachePanel"].isNull()) {
      return;
    }

    for (
      Json::ValueIterator dash = m_dashboards["networkCachePanel"].begin();
      dash != m_dashboards["networkCachePanel"].end();
      ++dash
    ) {
      if (
          (*dash).isObject() &&
          (*dash)["oid"].isString() &&
          (*dash)["oidPretty"].isString() &&
          (*dash)["name"].isString()
      ) {
        std::string oidName = (*dash)["oidPretty"].asString() + iface;
        std::string oid = (*dash)["oid"].asString() + iface;

        std::string value;

        if (!m_snmp->snmpGet(ip, community, oid.c_str(), value)) {
          printf("Stopping Setting immedatley on ip: %s oid: %s\n", ip, oid.c_str());
          return; // Stop immediately, we don't support Etherlike-MIB
        }

        (*dash)["timestamp"] = Json::Value((int)time(NULL));
        (*dash)["value"] = Json::Value(value);

        Json::FastWriter writer;
        std::string json_log = writer.write((*dash));

        // Set memcache key IP:OID
        std::string memcacheKey = ip + std::string(KEY_SEPARATOR) + oid;
        printf("Setting %s : %s\n", memcacheKey.c_str(), json_log.c_str());
        m_memcache.set(memcacheKey, json_log);


      }
    }
  }
  
  void snmpcheck::insertInterface (const char *ip, const char *community, const char *id, const std::string &iface)
  {
    iface_data data;
    if (!getInterfaceData (ip, community, iface, data))
      return;

    std::ostringstream sql;
    sql << "INSERT INTO INTERFACES (DEVICE_ID, INTERFACE, DESCRIPTION, NAME, SPEED, MAC) VALUES ("
        << id << ", " << iface << ", '" << data.m_descr << "', '" << data.m_name << "', " << data.m_speed << ", '" << data.m_mac << "')";
    printf ("%s\n", sql.str().c_str());

    try {
      m_db->execSQL (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }

  }

  void snmpcheck::updateInterface (const char *ip, const char *community, const char *id, const std::string &iface, const iface_data &old_data)
  {
    iface_data data;
    if (!getInterfaceData (ip, community, iface, data))
      return;

    if (
//        data.m_name != old_data.m_name ||
//        data.m_descr != old_data.m_descr ||
        data.m_speed != old_data.m_speed ||
        data.m_mac != old_data.m_mac)
    {
      static std::ostringstream sql;
      sql.str("");
      sql << "UPDATE INTERFACES SET SPEED = " << data.m_speed << ", "
          << "MAC = '" << data.m_mac << "' "
//        << "NAME = '" << data.m_name << "', "
//        << "DESCRIPTION = '" << data.m_descr << "' "
          << "WHERE DEVICE_ID = " << id << " AND INTERFACE = " << iface;
      
      printf ("%s\n", sql.str().c_str());
      try {
	m_db->execSQL (sql.str().c_str());
      } catch (db_exception &e) {
	printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
	netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      }
    }
  }

  void snmpcheck::checkAgent (unsigned int id, const char *ip, const char *community, unsigned int interval)
  {
    clearPendingFlag (ip);
    static std::ostringstream tmp;
    static std::ostringstream sql;

    sql.str("");
    sql << "SELECT INTERFACE, SPEED, ENABLE_LOGGING, ID FROM INTERFACES WHERE DEVICE_ID = " << id;
    printf ("%s\n", sql.str().c_str());

    PGresult *res = NULL;
    try {
      res = m_db->execQuery (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }

    bool isup = false;
    for (int i = 0; i < PQntuples (res); ++i)
    {
      bool tolog = *PQgetvalue (res, i, 2) == 't' ? true : false;
      std::string ifInOctets = std::string ("1.3.6.1.2.1.2.2.1.10.") + PQgetvalue (res, i, 0);
      std::string ifOutOctets = std::string ("1.3.6.1.2.1.2.2.1.16.") + PQgetvalue (res, i, 0);
      std::string inOctets, outOctets;

      if (!m_snmp->snmpGet (ip, community, ifInOctets.c_str(), inOctets))
        continue;
      if (!m_snmp->snmpGet (ip, community, ifOutOctets.c_str(), outOctets))
        continue;

      isup = true;
      tmp.str("");
      tmp << "Host " << ip << ", interface " << PQgetvalue (res, i, 0) << ", inoctets="
          << inOctets << ", outoctets=" << outOctets;
      netmon::log::instance()->add(netmon::_eLogDebug, tmp.str().c_str());

      uint32_t speed = atol (PQgetvalue (res, i, 1));
      updateCache (id, PQgetvalue (res, i, 0), PQgetvalue (res, i, 3), inOctets, outOctets, speed);

      checkIfaceAlerts (id, atoi (PQgetvalue (res, i, 0)), atoi (PQgetvalue (res, i, 3)), ip);

      uint32_t inresets, outresets;
      getCounters (id, PQgetvalue (res, i, 0), inresets, outresets);

      if (tolog)
      {
        tmp.str("");
        tmp << "INSERT INTO SNMP_LOG (IP, INTERFACE, INOCTETS, OUTOCTETS, INRESETS"
            << ", OUTRESETS, TIMESTAMP) VALUES ('" << ip << "', " << PQgetvalue (res, i, 0)
            << ", " << inOctets << ", " << outOctets << ", " << inresets << ", "
            << outresets << ", " << (int) time (0) << ")";
        printf ("%s\n", tmp.str().c_str());
	try {
	  m_db->execSQL (tmp.str().c_str());
	} catch (db_exception &e) {
	  printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), tmp.str().c_str());
	  netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), tmp.str().c_str());
	}
        if ((unsigned int) (time(NULL) % HOUR) < interval)
        {
          tmp.str("");
          tmp << "INSERT INTO AGG_SNMP_LOG (IP, INTERFACE, INOCTETS, OUTOCTETS, INRESETS"
              << ", OUTRESETS, TIMESTAMP) VALUES('" << ip << "', " << PQgetvalue (res, i, 0)
              << ", " << inOctets << ", " << outOctets << ", " << inresets << ", "
              << outresets << ", " << (int) time (0) << ")";
	  try {
	    m_db->execSQL (tmp.str().c_str());
	  } catch (db_exception &e) {
	    printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), tmp.str().c_str());
	    netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), tmp.str().c_str());
	  }
        }
      }
    }
    PQclear (res);
    setExaminedFlag (ip, isup);
  }

  void snmpcheck::checkInterfaces (const char *ip, const char *community, const char *id)
  {
    printf("Begin checkInterfaces()\n");
    iface_map oldifaces;
    ifacesfunctor ifaces;

    getInterfaces (id, oldifaces);
    m_snmp->walkSNMP (ip, community, m_iFaces, ifaces);
    findConnectedMACs (ip, community);

    if (ifaces.m_ifaces.size() == 0)
      return;

    std::set<unsigned int> oldi;
    std::set<unsigned int> newi;

    for (iface_map::iterator it = oldifaces.begin(); it != oldifaces.end(); ++it)
      oldi.insert (it->first);
    for (std::list<std::string>::iterator it = ifaces.m_ifaces.begin (); it != ifaces.m_ifaces.end (); ++it)
      newi.insert (atoi((*it).c_str()));

    std::set<unsigned int> res;
    std::insert_iterator<std::set<unsigned int> > it1 (res, res.begin());
    std::set_difference (oldi.begin(), oldi.end(), newi.begin(), newi.end(), it1);

    std::ostringstream sql;
    m_db->execSQL ("BEGIN");
    
    // delete dead interfaces
    for (std::set<unsigned int>::iterator it = res.begin(); it != res.end(); ++it)
    {
      sql.str("");
      sql << "DELETE FROM INTERFACES WHERE ID = " << oldifaces.find(*it)->second.m_id;
      printf("%s\n", sql.str().c_str());
      try {
	m_db->execSQL (sql.str().c_str());
      } catch (db_exception &e) {
	printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
	netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      }
    }

    res.clear();
    it1 = std::insert_iterator<std::set<unsigned int> > (res, res.begin());
    std::set_intersection (newi.begin(), newi.end(), oldi.begin(), oldi.end(), it1);

    // update all interfaces if needed
    for (std::set<unsigned int>::iterator it = res.begin(); it != res.end(); ++it)
    {
      sql.str("");
      sql << *it;
      updateInterface (ip, community, id, sql.str(), oldifaces.find(*it)->second);
    }

    res.clear();
    it1 = std::insert_iterator<std::set<unsigned int> > (res, res.begin());
    std::set_difference (newi.begin(), newi.end(), oldi.begin(), oldi.end(), it1);
    
    // insert new interfaces
    for (std::set<unsigned int>::iterator it = res.begin(); it != res.end(); ++it)
    {
      sql.str("");
      sql << *it;
      printf("%d\n", *it);
      insertInterface (ip, community, id, sql.str());
    }

    m_db->execSQL ("COMMIT");
    printf("End checkInterfaces()\n");
  }

  void snmpcheck::getInterfaces (const char *id, iface_map &ifaces)
  {
    static std::ostringstream sql;

    sql.str("");
    sql << "SELECT INTERFACE, ID, NAME, DESCRIPTION, SPEED, MAC FROM INTERFACES WHERE DEVICE_ID = " << id;
    printf ("%s\n", sql.str().c_str());

    PGresult *res = NULL;
    try {
      res = m_db->execQuery (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }

    iface_data data;
    for (int i = 0; i < PQntuples (res); ++i)
    {
      data.m_id = atoi (PQgetvalue (res, i, 1));
      data.m_name = PQgetvalue (res, i, 2);
      data.m_descr = PQgetvalue (res, i, 3);
      data.m_speed = PQgetvalue (res, i, 4);
      data.m_mac = PQgetvalue (res, i, 5);
      ifaces[atoi (PQgetvalue (res, i, 0))] = data;
    }
    PQclear (res);
  }

  void snmpcheck::updateCache (unsigned int id, const char *iface, const char *ifaceid, const std::string &in, const std::string &out, uint32_t speed)
  {
    uint32_t iif = atol (iface);
    uint32_t iid = atol (ifaceid);

    snmpifacedata data (atoll (in.c_str()), atoll (out.c_str()), (int) time(0));
    m_cache->update (id, iif, iid, data, speed);
  }

  void snmpcheck::getCounters (unsigned int id, const char *iface, uint32_t &inresets, uint32_t &outresets)
  {
    uint32_t iif = atol (iface);

    inresets = m_cache->getInResets (id, iif);
    outresets = m_cache->getOutResets (id, iif);
  }

  void snmpcheck::checkIPAlerts ()
  {
    snmpcache::snmpmap::iterator it;

    for (it = m_cache->m_map.begin (); it != m_cache->m_map.end (); ++it)
    {
      in_addr adr;
      adr.s_addr = it->first;
      std::string ip = std::string (inet_ntoa (adr));
      std::ostringstream sql;
      sql <<
      "select distinct a.id, a.alert_template_id, b.triggered, b.trigger_threshold, b.trigger_id, b.label \
       from alert_handlers a, alert_triggers b, devices c \
       where a.trigger_id = b.trigger_id \
       and b.reference_table_name = 'devices' \
       and b.reference_pkey_val = c.id \
       and c.ip_address = '" << ip << "'";

//      printf ("%s\n", sql.str().c_str());
      PGresult *res = NULL;
      try {
	res = m_db->execQuery (sql.str().c_str());
      } catch (db_exception &e) {
	printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
	netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      }
      for (int i = 0; i < PQntuples (res); ++i)
      {
        bool triggered = (*PQgetvalue (res, i, 2) == 't');
        int threshold = atoi (PQgetvalue (res, i, 3));
        snmpcache::ifacemap::iterator it2;
        if (!triggered) // find any interface with threshold above given alert threshold
        {
          for (it2 = it->second.begin (); it2 != it->second.end (); ++it2)
          {
            double ifacethr = m_cache->getThroughputPercentage (it->first, it2->first, true);
            if (ifacethr > (double) threshold)
            {
              std::ostringstream sql2;
              sql2 << "select default_template, default_subject from alert_types where name = 'SNMP_IFACE_ABOVE_THRESHOLD'";
              PGresult *res2 = NULL;
	      try {
		res2 = m_db->execQuery (sql2.str().c_str());
	      } catch (db_exception &e) {
		printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
		netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	      }
              if (PQntuples (res2) > 0)
              {
                alerthandler ah (m_db);
                ah.addKeywordValue (alerthandler::eHostIP, ip.c_str());
                ah.addKeywordValue (alerthandler::eInterface, it2->first);
                ah.addKeywordValue (alerthandler::eUtilization, (int) ifacethr);
                ah.addKeywordValue (alerthandler::eLabel, PQgetvalue(res, i, 5));

                ah.setSubject (PQgetvalue(res2, 0, 1));
                int aid = atoi (PQgetvalue(res, i, 0));
                ah.insertAlert (aid, PQgetvalue(res2, 0, 0));
              }
              PQclear (res2);
              sql2.str("");
              sql2 << "update alert_triggers set triggered = 't' where trigger_id = "
                   << PQgetvalue (res, i, 4);
	      try {
		m_db->execSQL (sql2.str().c_str());
	      } catch (db_exception &e) {
		printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
		netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	      }
	      break;
            }
          }
        }
        else
        {
          // if all interfaces are below threshold we notify user that usage is below threshold
          for (it2 = it->second.begin (); it2 != it->second.end (); ++it2)
          {
            double ifacethr = m_cache->getThroughputPercentage (it->first, it2->first, true);
            if (ifacethr > (double) threshold)
              break; // we still have a iface with threshold above given value

            std::ostringstream sql2;
            sql2 << "select default_template, default_subject from alert_types where name = 'SNMP_IFACE_BELOW_THRESHOLD'";
            PGresult *res2 = NULL;
	    try {
	      res2 = m_db->execQuery (sql2.str().c_str());
	    } catch (db_exception &e) {
	      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	    }
            if (PQntuples (res2) > 0)
            {
              alerthandler ah (m_db);
              ah.addKeywordValue (alerthandler::eHostIP, ip.c_str());
              ah.addKeywordValue (alerthandler::eLabel, PQgetvalue(res, i, 5));

              ah.setSubject (PQgetvalue(res2, 0, 1));
              int aid = atoi (PQgetvalue(res, i, 0));
              ah.insertAlert (aid, PQgetvalue(res2, 0, 0));
            }
            PQclear (res2);
            sql2.str("");
            sql2 << "update alert_triggers set triggered = 'f' where trigger_id = "
                 << PQgetvalue (res, i, 4);
	    try {
	      m_db->execSQL (sql2.str().c_str());
	    } catch (db_exception &e) {
	      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql2.str().c_str());
	    }
	  }
        }
      }
      PQclear (res);
    }
  }

  void snmpcheck::checkIfaceAlerts (unsigned int id, unsigned int iface, unsigned int ifaceid, const char *ip)
  {
    double inthr = m_cache->getThroughputPercentage (id, iface, true);
    double outthr = m_cache->getThroughputPercentage (id, iface, false);

    if (inthr == -1 or outthr == -1)
      return;

    std::list<alerthandler *> alerts;
    values_map vals;
    vals[alerthandler::eHostIP] = keyword_value (ip);
    vals[alerthandler::eInterface] = keyword_value (iface);

    m_ifaceAlertManager->getAlerts (ifaceid, (unsigned int) inthr, (unsigned int) outthr, vals);
  }

  void snmpcheck::setPendingFlag ()
  {
    std::ostringstream sql;
    sql.str("");
    sql << "UPDATE DEVICES SET PENDING = 'f', TIMESTAMP = 0 WHERE ENABLE_SNMP = 't'";
    try {
      m_db->execSQL (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }
  }

  void snmpcheck::clearPendingFlag (const char *ip)
  {
    static std::ostringstream sql;
    sql.str("");
    sql << "UPDATE DEVICES SET PENDING = 't', TIMESTAMP = " << (int) time (0)
        << " WHERE IP_ADDRESS = '" << ip << "'";
    try {
      m_db->execSQL (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }
  }

  void snmpcheck::setExaminedFlag (const char *ip, bool up, bool flag /* = true */)
  {
    static std::ostringstream sql;
    sql.str("");

    sql << "UPDATE DEVICES SET PENDING = 'f', EXAMINED = '";
    if (flag)
      sql << "t";
    else
      sql << "f";

    sql << "', TIMESTAMP = " << (int) time (0) << ", STATUS = '";

    if (up)
      sql << "up'";
    else
      sql << "down'";

    sql << " WHERE IP_ADDRESS = '" << ip << "'";
    try {
      m_db->execSQL (sql.str().c_str());
    } catch (db_exception &e) {
      printf("++++++++++++++++++++++++++Exception PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, "PostgreSQL Error: [%s] SQL String: [%s]", e.what(), sql.str().c_str());
    }
  }
}
