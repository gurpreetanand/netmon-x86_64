#ifndef __DB_SNMP_CACHE__
#define __DB_SNMP_CACHE__

#include "db.h"
#include "cache.h"

namespace netmon
{
  class dbsnmpcache : public snmpcache
  {
    public:
      dbsnmpcache (db *);
      virtual ~dbsnmpcache ();

      virtual void update (uint32_t, uint32_t, uint32_t, const snmpifacedata &, uint32_t);

    protected:
      void load ();
      db *m_db;
  };
}

#endif // __DB_SNMP_CACHE__
