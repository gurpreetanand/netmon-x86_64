#include "oidd.h"
#include "db.h"
#include "dashboards.h"
#include "log.h"
#include "alerts.h"
#include "alertutil.h"
#include "oidalertmgr.h"
#include "snmp_pp/config_snmp_pp.h"
#include "common/snmp.h"
#include "common/oidtranslator.h"

#include <list>
#include <sstream>
#include <map>
#include <jsoncpp/json/writer.h>


namespace netmon
{
  oid_daemon::oid_daemon ()
    : daemon ()
  {
    root = dashboards::getDashboards();
    m_memcache = Memcacher();
    m_snmp = new snmp;
    m_ignores = SnmpIgnores();
  }

  oid_daemon::oid_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
    root = dashboards::getDashboards();
    m_memcache = Memcacher();
    m_snmp = new snmp;
    m_ignores = SnmpIgnores();
  }

  oid_daemon::~oid_daemon ()
  {
    delete m_snmp;
    delete m_alertMgr;
    delete m_oidTranslator;
  }
  
  int oid_daemon::run ()
  {
    while (!isTermSignaled())
    {
      checkOIDs ();
      sleep (5);
    }
    return 0; // keep the compiler happy
  }
  
  std::string oid_daemon::getKey(oid_daemon::oid_data* oid){
    std::ostringstream key("");
    key << 
    oid->m_ip << KEY_SEPARATOR <<
    oid->m_port<< KEY_SEPARATOR <<
    oid->m_community << KEY_SEPARATOR <<
    oid->m_oid;
    return key.str();
  }
  
  void oid_daemon::getDashboardOIDs(oidmap& m_toAdd) {
    printf("Get dashboard OIDs\n");
    static std::ostringstream sql("");
    sql << "select ip_address, snmp_community, snmp_port, profile "
        << "from devices where enable_snmp='t';";

    PGresult *res = m_db->execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      if (!root[PQgetvalue(res, i, 3)].isNull() && !root[PQgetvalue(res, i, 3)]["oids"].isNull()) {
        Json::Value dashboard = root[PQgetvalue(res, i, 3)]["oids"];
        for (unsigned int j = 0; j < dashboard.size(); j++) {
          printf("Adding: %s\n", dashboard[j]["oid"].asString().c_str());
          oid_data *d = new oid_data;
          d->m_ip = std::string (PQgetvalue(res, i, 0));
          d->m_community = std::string (PQgetvalue(res, i, 1));
          d->m_port = atoi (PQgetvalue(res, i, 2));
          
          d->m_oid = dashboard[j]["oid"].asString();
          d->m_log = false;
          d->m_id = 0;
          d->m_msg = "";
          d->m_isNumeric = dashboard[j]["numeric"].asBool();

          m_toAdd[getKey(d)] = d;
        }
      }
    }

    PQclear (res);
  }
  
  void oid_daemon::setDashboardWalks() {
    printf("Get dashboard walkss\n");
    static std::ostringstream sql("");
    sql << "select ip_address, snmp_community, snmp_port, profile "
        << "from devices where enable_snmp='t';";

    PGresult *res = m_db->execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      if (!root[PQgetvalue(res, i, 3)].isNull() && !root[PQgetvalue(res, i, 3)]["walks"].isNull()) {
        Json::Value dashboard = root[PQgetvalue(res, i, 3)]["walks"];
        for (unsigned int j = 0; j < dashboard.size(); j++) {
          printf("Adding Walk: %s\n", dashboard[j]["oid"].asString().c_str());
          oid_data *d = new oid_data;
          d->m_ip = std::string (PQgetvalue(res, i, 0));
          d->m_community = std::string (PQgetvalue(res, i, 1));
          d->m_port = atoi (PQgetvalue(res, i, 2));
            
          d->m_oid = dashboard[j]["oid"].asString();
          d->m_log = false;
          d->m_id = 0;
          d->m_msg = "";
          d->m_isNumeric = dashboard[j]["numeric"].asBool();
          d->m_isWalk = true;
          m_walks.push_back(d);
        }
      }
    }

    PQclear (res);
  }

  void oid_daemon::getTrackedOIDs(oidmap& m_toAdd) {
    static std::ostringstream sql("");
    sql << "select ip_address, snmp_community, snmp_port, oid, enable_logging, "
        << "oids.id, message, datatype from devices, oids where oids.device_id = devices.id and "
        << "(oids.timestamp + oids.interval) < " << (int) time(0) << ";";

    PGresult *res = m_db->execQuery (sql.str().c_str());

    for (int i = 0; i < PQntuples(res); ++i)
    {
      oid_data *d = new oid_data;
      d->m_ip = std::string (PQgetvalue(res, i, 0));
      d->m_community = std::string (PQgetvalue(res, i, 1));
      d->m_port = atoi (PQgetvalue(res, i, 2));
      d->m_oid = std::string (PQgetvalue(res, i, 3));
      d->m_log = *PQgetvalue(res, i, 4) == 't' ? true : false;
      d->m_id = atoi (PQgetvalue(res, i, 5));
      d->m_msg = std::string (PQgetvalue(res, i, 6));
      if (*PQgetvalue(res, i, 7) == 'S' || *PQgetvalue(res, i, 7) == 's')
        d->m_isNumeric = false;
      else
        d->m_isNumeric = true;
      
      oid_data* remove = m_toAdd[getKey(d)];

      // Cleanup memory
      if (remove != NULL) {
        delete remove;
      }
      
      m_toAdd[getKey(d)] = d;
    }

    PQclear (res);
  }

  void oid_daemon::checkOIDs()
  {
    oidmap m_toAdd;
    
    getDashboardOIDs(m_toAdd);
    getTrackedOIDs(m_toAdd);
    setDashboardWalks();
    
    for (oidmap::iterator it = m_toAdd.begin(); it != m_toAdd.end(); ++it) {
      m_oids.push_back(it->second);
    }

    m_alertMgr->reload();

    for (oid_list::iterator it = m_oids.begin(); it != m_oids.end(); ++it)
      getOID (*it);
    
    for (oid_list::iterator it = m_walks.begin(); it != m_walks.end(); ++it)
      walkOID (*it);

    clearLists ();
  }

  void oid_daemon::getOID (oid_data *d)
  {
    static std::string value;
    unsigned long syntax = 0;
    int status = 0;

    bool skippedPoll = false;
    bool validPoll = true;

    if (!m_ignores.isOidValid(d->m_ip, d->m_oid)) {
      // Skip any further OID polls for this IP
      skippedPoll = true;
      validPoll = false;
      printf("Skipping blocked IP: %s\n", d->m_ip.c_str());
    } else {

      m_snmp->setPort (d->m_port);
      m_snmp->setAddress (d->m_ip.c_str());
      m_snmp->setOID (d->m_oid.c_str());
      m_snmp->setCommunity (d->m_community.c_str());

      status = m_snmp->snmpGet(value, syntax);

      if (status != 1) {
        // Block this
        m_ignores.addSkip(d->m_ip, d->m_oid, status);
      }
    }

    if (status != 1) {
      if (!skippedPoll) {
        log::instance()->add(_eLogError, "SNMPGet failed, IP: %s, OID: %s", d->m_ip.c_str(), d->m_oid.c_str());
      }

      if (d->m_isNumeric)
        value = "-1";
      else
        value = "NETMON_OID_ERROR";
    }

    logOID (d, value);

    values_map vmap;
    insertValues (vmap, d, value);

    // Only run alerts if it wasn't skipped.
    if (validPoll) {
      if (d->m_isNumeric) {
        m_alertMgr->getAlerts (d->m_id, atoll (value.c_str()), vmap);
      } else {
        m_alertMgr->getAlerts (d->m_id, value.c_str(), vmap);
      }
    }
  }
  
  void oid_daemon::walkOID (oid_data *d)
  {
    if (!m_ignores.isOidValid(d->m_ip, d->m_oid)) {
      const Snmp_pp::Oid oid(d->m_oid.c_str());
      snmp_walk_functor functor;
      snmp* walker = new snmp;

      walker->setPort(161);

      printf("Walking [%s] on [%s]\n", d->m_oid.c_str(), d->m_ip.c_str());

      int status = walker->walkSNMP (
                           d->m_ip.c_str(),
                           d->m_community.c_str(),
                           oid,
                           functor);
      if (status == 1) {
        logOID (d, functor.toString());
      } else {
        m_ignores.addSkip(d->m_ip, d->m_oid, status);
      }

      delete walker;
    }
  }

  void oid_daemon::clearLists ()
  {
    for (oid_list::iterator it = m_oids.begin(); it != m_oids.end(); ++it)
      delete *it;
    
    for (oid_list::iterator it = m_walks.begin(); it != m_walks.end(); ++it)
      delete *it;
    
    m_oids.clear();
    m_walks.clear();
    m_ignores.cleanup();
  }

  void oid_daemon::logOID (oid_data *d, const std::string &value)
  {
    printf("logoid %s\n", value.c_str());
    static std::ostringstream sql("");
    unsigned int now = (unsigned int) time(0);
    char *m1 = new char[value.size() * 2 + 1];
    char *m2 = new char[d->m_msg.size() * 2 + 1];

    PQescapeString (m1, value.c_str(), value.size());
    PQescapeString (m2, d->m_msg.c_str(), d->m_msg.size());

    std::string esc1(m1);
    std::string esc2(m2);

    delete[] m1;
    delete[] m2;

    // DB field is max 255
    esc1 = esc1.substr(0, 250);
    
    // Insert into database if logged.
    if (d->m_log)
    {
      sql.str("");
      sql << "INSERT INTO OID_LOG (ID, OID_ID, MESSAGE, TIMESTAMP) VALUES "
          << "(DEFAULT, " << d->m_id << ", '" << esc1 << "', " << now
          << ");";
      printf("trying sql %s\n", sql.str().c_str());
      try {
        m_db->execSQL (sql.str().c_str());
      } catch (int e) {
        printf("Failed sql %s\n", sql.str().c_str());
      }

    }
    
    // Insert into cache either way
    Json::Value root;
    Json::FastWriter writer;
    printf("The val is: %s the msg is: %s value: %s\n", esc1.c_str(), esc2.c_str(), value.c_str());
    root["value"] = Json::Value(esc1);
    root["message"] = Json::Value(esc2);
    root["timestamp"] = Json::Value((int)now);
    std::string json_log = writer.write(root);

    // Set memcache key IP:OID
    std::string memcacheKey = d->m_ip + KEY_SEPARATOR + d->m_oid;
    printf("Setting %s : %s\n", memcacheKey.c_str(), json_log.c_str());
    bool rc = m_memcache.set(memcacheKey, json_log);
    if (rc) {
      printf("Success\n");
    } else {
      printf("Failure\n");
    }

    if (d->m_log) {
      sql.str("");
      sql << "UPDATE OIDS SET TIMESTAMP = " << now << ", MESSAGE = '" << esc1
          << "', PREV_MESSAGE = '" << esc2 << "' WHERE ID = " << d->m_id << ";";
      m_db->execSQL (sql.str().c_str());
    }
  }

  void oid_daemon::insertValues (values_map &vmap, oid_data *d, const std::string &value)
  {
    static std::string trOID;
    static std::ostringstream oid;

    if (m_oidTranslator->getTranslation (d->m_oid.c_str(), trOID))
    {
      oid.str("");
      oid << trOID << " (" << d->m_oid << ")";
      vmap[alerthandler::eOID] = keyword_value (oid.str());
    }
    else
      vmap[alerthandler::eOID] = keyword_value (d->m_oid);
    vmap[alerthandler::eHostIP] = keyword_value (d->m_ip);
    vmap[alerthandler::ePayload] = keyword_value (value);
  }

  void oid_daemon::init ()
  {
    daemon::init ();
    m_db->execSQL ("UPDATE OIDS SET TIMESTAMP = 0");
    m_alertMgr = new oid_alert_manager (m_db);
    m_alertMgr->init();
    m_oidTranslator = new oid_translator (m_db);
  }
}
