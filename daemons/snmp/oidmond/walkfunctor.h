#ifndef WALK_FUNCTOR_H
#define WALK_FUNCTOR_H

#include <list>
#include <map>
#include <string>
#include <jsoncpp/json/writer.h>

#include "common/snmp.h"
#include "snmp_pp/snmp_pp.h"

namespace netmon {
  class snmp_walk_functor : public snmpvbfunctor {
    public:
      snmp_walk_functor ()
      :snmpvbfunctor()
      {}
      virtual void add (const Snmp_pp::Vb &v) {
        Json::Value value;
        unsigned int now = (unsigned int) time(0);
        value["timestamp"] = Json::Value((int)now);
        value["value"] = Json::Value(v.get_printable_value());
        value["oid"] = Json::Value(v.get_printable_oid());
        root[v.get_printable_oid()] = Json::Value(value);
      }

      void clear () {
        root = Json::Value();
      }

      void dump () {
      }
      
      std::string toString() {
        return writer.write(root); 
      }

    protected:
      Json::Value root;
      Json::FastWriter writer;
  };
}

#endif