#ifndef __SNMP_ASYNC_QUERY__
#define __SNMP_ASYNC_QUERY__

#include <map>
#include <string>
#include <stdint.h>
#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  struct payload
  {
    virtual ~payload () {}
  };

  struct oid_value
  {
    oid_value ()
      : m_valid (false), m_payload (0) {}

    bool m_valid;
    std::string m_value;
    payload *m_payload;
  };

  class snmp_async_query
  {
    friend class snmp_async;

    protected:
      enum {snmp_port = 161};

    public:
      snmp_async_query ();
      snmp_async_query (const char *);
      snmp_async_query (const char *, const char *, Snmp_pp::snmp_version = Snmp_pp::version1, uint16_t = snmp_port);

      virtual ~snmp_async_query ();

      void addOID (const char *, payload * /* = 0 */);

      const uint16_t &port () const
      {
        return m_port;
      }

      uint16_t &port ()
      {
        return m_port;
      }

      const Snmp_pp::UdpAddress &address () const
      {
        return m_address;
      }

      Snmp_pp::UdpAddress &address ()
      {
        return m_address;
      }

      const Snmp_pp::OctetStr &community () const
      {
        return m_community;
      }

      Snmp_pp::OctetStr &community ()
      {
        return m_community;
      }

      const Snmp_pp::snmp_version &version () const
      {
        return m_version;
      }

      Snmp_pp::snmp_version &version ()
      {
        return m_version;
      }

      typedef std::map<Snmp_pp::Oid, oid_value *> oid_map_t;

      const oid_map_t &getOids () const
      {
        return m_oids;
      }

      oid_map_t &getOids ()
      {
        return m_oids;
      }

    protected:
      uint16_t m_port;
      Snmp_pp::UdpAddress m_address;
      Snmp_pp::OctetStr m_community;
      Snmp_pp::snmp_version m_version;

      oid_map_t m_oids;
  };
}

#endif // __SNMP_ASYNC_QUERY__
