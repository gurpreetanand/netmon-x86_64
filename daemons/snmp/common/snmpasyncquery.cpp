#include "snmpasyncquery.h"

namespace netmon
{
  static const char *pcommunity = "public";

  snmp_async_query::snmp_async_query ()
    : m_port (snmp_port), m_version (Snmp_pp::version1)
  {
    Snmp_pp::Snmp::socket_startup();
  }

  snmp_async_query::snmp_async_query (const char *ip)
    : m_port (snmp_port), m_address (ip), m_community (pcommunity), m_version (Snmp_pp::version1)
  {
    Snmp_pp::Snmp::socket_startup();
  }

  snmp_async_query::snmp_async_query (const char *ip, const char *community, Snmp_pp::snmp_version version, uint16_t port)
    : m_port (port), m_address (ip), m_community (community), m_version (version)
  {
    Snmp_pp::Snmp::socket_startup();
  }

  snmp_async_query::~snmp_async_query ()
  {
    for (snmp_async_query::oid_map_t::const_iterator i = m_oids.begin(); i != m_oids.end(); ++i)
      delete i->second;
    m_oids.clear();
    Snmp_pp::Snmp::socket_cleanup();
  }

  void snmp_async_query::addOID (const char *oid, payload *pl)
  {
    oid_value *ov = new oid_value;
    ov->m_payload = pl;
    m_oids[Snmp_pp::Oid (oid)] = ov;
  }
}

#ifdef __TEST2__

#include <cstdio>

using namespace netmon;

struct mypayload : public payload
{
  mypayload (int i)
    : m_id (i) {}
  int m_id;
};

void t1 ()
{
  printf ("t1()\n");
  snmp_async_query a ("127.0.0.1");
  printf ("%s\n", a.address().get_printable());
  printf ("%s\n", a.community().get_printable());
  printf ("%d\n", a.port());
  printf ("%d\n", a.version());
}

void t2 ()
{
  printf ("t2()\n");
  snmp_async_query a;
  a.port() = 161;
  printf ("%d\n", a.port());
  a.community() = "public";
  printf ("%s\n", a.community().get_printable());
//  a.address() = "10.0.2.202";
  a.address() = "192.168.0.1";
  printf ("%s\n", a.address().get_printable());
  a.version() = Snmp_pp::version1;
  printf ("%d\n", a.version());
  a.addOID ("1.3.6.1.2.1.1.1.0", new mypayload (0));
  a.addOID ("1.3.6.1.2.1.1.5.0", new mypayload (1));
  const snmp_async_query::oid_map_t oids = a.getOids();
  for (snmp_async_query::oid_map_t::const_iterator i = oids.begin(); i != oids.end(); ++i)
  {
    printf ("%s\n", i->first.get_printable());
  }
}

int main ()
{
  t1();
  t2();
}

#endif // __TEST__
