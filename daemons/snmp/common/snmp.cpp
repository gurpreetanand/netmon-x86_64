#include "snmp.h"

namespace netmon
{
  snmp::snmp ()
  {
    Snmp_pp::Snmp::socket_startup ();
  }

  snmp::~snmp ()
  {
    Snmp_pp::Snmp::socket_cleanup ();
  }

  int snmp::snmpGet (const char *ip, const char *community, const char *oid, std::string &value)
  {
    static unsigned long dumb;
    m_address = Snmp_pp::UdpAddress (ip);
    m_oid = Snmp_pp::Oid (oid);
    m_community = Snmp_pp::OctetStr (community);

    return snmpGet (value, dumb);
  }

  int snmp::snmpGet (std::string &value, unsigned long &syntax)
  {
    int status;
    Snmp_pp::Snmp mysnmp (status, 0, false);

    if (status != SNMP_CLASS_SUCCESS)
      return false;

    Snmp_pp::Pdu pdu;
    Snmp_pp::Vb vb;
    vb.set_oid (m_oid);
    pdu += vb;

    m_address.set_port (m_port);
    Snmp_pp::CTarget ctarget (m_address);

    ctarget.set_version (Snmp_pp::version2c);
    ctarget.set_retry (1);
    ctarget.set_timeout (250);
    ctarget.set_readcommunity (m_community);

    Snmp_pp::SnmpTarget *target;
    target = &ctarget;

    status = mysnmp.get (pdu, *target);

    if (status == SNMP_CLASS_SUCCESS)
    {
      pdu.get_vb (vb, 0);
      value = std::string (vb.get_printable_value());
      syntax = vb.get_syntax();
      return 1;
    } else {
      return status;
    }
  }

  int snmp::walkSNMP (const char *ip, const char *community, const Snmp_pp::Oid &oid, snmpvbfunctor &func)
  {
    Snmp_pp::UdpAddress address (ip);
    if (!address.valid ())
      throw std::string ("Invalid Address");
    Snmp_pp::snmp_version version = Snmp_pp::version1;
    Snmp_pp::OctetStr comm (community);
    int status;

    Snmp_pp::Snmp mysnmp (status, 0, false);

    if (status != SNMP_CLASS_SUCCESS)
      throw std::string ("Cannot create SNMP++ session");

    Snmp_pp::Pdu pdu;
    Snmp_pp::Vb vb;
    vb.set_oid (oid);
    pdu += vb;

    address.set_port (m_port);
    Snmp_pp::CTarget ctarget (address);
    ctarget.set_version (Snmp_pp::version2c);
    ctarget.set_retry (2);
    ctarget.set_timeout (250);
    ctarget.set_readcommunity (comm);

    Snmp_pp::SnmpTarget *target;
    target = &ctarget;

    Snmp_pp::Oid myoid = oid;
    unsigned int cnt = 0;
    while ((status = mysnmp.get_bulk (pdu, *target, 0, 10)) == SNMP_CLASS_SUCCESS) {
      ++cnt;
      for (int z = 0; z < pdu.get_vb_count(); ++z) {
        pdu.get_vb (vb,z);
        Snmp_pp::Oid tmp;
        vb.get_oid (tmp);
        if ((myoid.nCompare(myoid.len(), tmp) != 0))
          return 1;

        // look for var bind exception, applies to v2 only
        if (vb.get_syntax() != sNMP_SYNTAX_ENDOFMIBVIEW)
          func.add (vb);
      }
      // last vb becomes seed of next rquest
      pdu.set_vblist (&vb, 1);
    }
    if (status != SNMP_CLASS_SUCCESS && cnt == 0) { // no data retrieved, get failed
      return status;
    }
    return 1;
  }
}
