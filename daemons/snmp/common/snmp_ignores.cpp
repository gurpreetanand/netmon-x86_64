#include "snmp_ignores.h"
#include "snmp_pp/snmp_pp.h"

#include <stdio.h>
#include <ctime>
#include <cstdlib>

#define DEFAULT_TIMEOUT 3 * 60 * 60 // 3 hours

namespace netmon {

  void SnmpIgnores::addSkip(std::string ip, std::string oid, int status, int timeout) {
    printf("Adding skip %s : %d\n", getKey(ip,oid).c_str(), status);

    // Replace default timeout if timeout isn't specified.
    if (timeout == 0) {
      timeout = DEFAULT_TIMEOUT;
    }

    uint32_t time = (uint32_t) std::time(0);
    time += timeout;

    if (status == SNMP_ERROR_NO_SUCH_NAME) {
      printf("Skip will be OID\n");
      time += rand() % DEFAULT_TIMEOUT;
      ipOidSkips[getKey(ip, oid)] = time;
    }
  }

  bool SnmpIgnores::isOidValid(std::string ip, std::string oid) {
    printf("Is ip/oid valid or skipped? %s: %d/%d\n", getKey(ip, oid).c_str(), ipSkips[ip], ipOidSkips[getKey(ip, oid)]);
    return ipSkips[ip] < 1 && ipOidSkips[getKey(ip, oid)] < 1;
  }

  const std::string SnmpIgnores::getKey(std::string ip, std::string oid) const {
    return ip + ":" + oid;
  }

  void SnmpIgnores::cleanup() {
    for (blockMap::iterator it = ipOidSkips.begin(); it != ipOidSkips.end(); ++it) {
      if ((int)std::time(0) > it->second) {
        printf("REMOVED BLOCK ON %s\n", it->first.c_str());
        ipOidSkips.erase(it);
      }
    }
    for (blockMap::iterator it = ipSkips.begin(); it != ipSkips.end(); ++it) {
      if ((int)std::time(0) > it->second) {
        printf("REMOVED IP BLOCK ON %s\n", it->first.c_str());
        ipSkips.erase(it);
      }
    }
  }

}