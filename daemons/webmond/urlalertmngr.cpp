#include "urlalertmngr.h"
#include "alertthrottler.h"
#include "condmanager.h"

#include <sstream>

namespace netmon
{
  url_alert_manager::url_alert_manager (db *mydb)
   : alert_manager (mydb)
  {
    m_condManager = new conditional_manager (mydb);
  }

  url_alert_manager::~url_alert_manager ()
  {
    delete m_condManager;
  }

  unsigned int url_alert_manager::getAlerts (unsigned int id, bool isOK, const values_map &vmap)
  {
    static std::ostringstream buf;
    alert_map::iterator i = m_alerts.find (id);
    if (i == m_alerts.end())
      return 0;
    alert_map::iterator end = m_alerts.upper_bound (id);
    unsigned int ret = 0;
    alerthandler *h;

    for (; i != end; ++i)
    {
      if (!isAlertValid (i->second))
        continue;
      if (i->second->m_triggered && isOK)
      {
        h = createAlertHandler (i->second, false);
        getReferenceData (id, h->getMap());
        dispatchAlert (h, vmap);
        toggleAlertFlag (i->second->m_triggerID);
        ++ret;
        continue;
      }
      if (!i->second->m_triggered && !isOK)
      {
        unsigned cnt = 0;
        if (m_throttler->toTrigger (i->second->m_alertID, vmap, cnt))
        {
          alerthandler *h = createAlertHandler (i->second, true);
          if (cnt != 0)
          {
            buf.str("");
            buf << std::endl << std::endl << "(Last message repeated " << cnt << " times)";
            h->getTemplate() += buf.str();
          }
          getReferenceData (id, h->getMap());
          dispatchAlert (h, vmap);
          toggleAlertFlag (i->second->m_triggerID);
          ++ret;
        }
      }
    }
    return ret;
  }

  void url_alert_manager::reload ()
  {
    alert_manager::reload();
    m_condManager->reload();
  }

  bool url_alert_manager::isAlertValid (alert_data *d)
  {
    return m_condManager->conditionalValid (d->m_conditionalID.c_str());
  }
}
