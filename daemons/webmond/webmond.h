#ifndef __WEB_DAEMON__
#define __WEB_DAEMON__

#include "daemon.h"

namespace netmon
{
  class webmonitor;

  class webmon_daemon : public daemon
  {
    public:
      webmon_daemon (int, char **);
      ~webmon_daemon ();

      virtual void init ();

      bool termSignaled ()
      {
        return isTermSignaled();
      }

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "webmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/webmond";
      }

      webmonitor *m_webMonitor;
  };
}

#endif //__WEB_DAEMON__
