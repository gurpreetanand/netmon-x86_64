#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "cache.h"

namespace netmon
{
  cache::cache ()
  {
  }

  cache::~cache ()
  {
    m_hash.clear();
  }

  bool cache::needsUpdate (uint32_t ip)
  {
    mutex_lock l (m_mutex);

    ipnameshash::iterator i = m_hash.find (ip);
    if (i == m_hash.end())
    {
      m_hash[ip] = ipnames (ip, (int) time (0));
      return true;
    }
    return (((int) time (0)) - i->second.m_timestamp > 3600);
  }

  void cache::update (uint32_t ip)
  {
    mutex_lock l (m_mutex);

    ipnameshash::iterator it = m_hash.find (ip);
    if (it == m_hash.end()) // we don't have this IP?
      return;

    it->second.m_timestamp = (int) time (0);
  }

  bool cache::update (uint32_t ip, const char *name, bool isDNS, bool isDirty /* = true */)
  {
    mutex_lock l (m_mutex);

    ipnameshash::iterator it = m_hash.find (ip);
    if (it == m_hash.end()) // we don't have this IP?
      return false;

    it->second.m_timestamp = (int) time (0);

    if (isDNS)
    {
      if (it->second.m_DNSNames.find (name) != it->second.m_DNSNames.end())
        return false; // no update needed
      it->second.m_DNSNames.insert (name);
      if (isDirty)
        m_dirtyList.push_back (new_name (ip, name, true));
      return true;
    }
    if (it->second.m_NBName == name)
      return false;
    it->second.m_NBName = name;
    if (isDirty)
      m_dirtyList.push_back (new_name (ip, name, false));
    return true;
  }

  void cache::dump ()
  {
    name_list_t::iterator end = m_dirtyList.end();
    for (name_list_t::iterator i = m_dirtyList.begin(); i != end; ++i)
      dumpName (*i);
    m_dirtyList.clear();
  }

  void cache::dumpName (const new_name &n)
  {
    in_addr a;
    a.s_addr = n.m_ip;

    printf ("IP: %s -> %s (%s)\n", inet_ntoa (a), n.m_name.c_str(), n.m_isDNS ? "DNS" : "NetBIOS");
  }
}
