#ifndef __DNS__
#define __DNS__

#include <resolv.h>
#include <arpa/nameser.h>

#include <list>
#include <string>

namespace netmon
{
  class reversedns
  {
    public:
      reversedns ();
      ~reversedns ();

      bool query (const char *, std::list<std::string> &);
      const char *getError () const;

    protected:
      union _response
      {
        HEADER hdr;              /* defined in resolv.h */
        u_char buf[NS_PACKETSZ]; /* defined in arpa/nameser.h */
      };

      bool parseResponse (ns_msg, std::list<std::string> &);
      std::string makeInArpaAddr (const char *);
      static const char *arpstr;
      int m_error;
  };
}

#endif // __DNS__
