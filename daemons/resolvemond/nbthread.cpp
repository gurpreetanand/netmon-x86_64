#include "nbthread.h"
#include "cache.h"
#include "getname.h"

#include <netinet/in.h>
#include <arpa/inet.h>

namespace netmon
{
  nb_thread::nb_thread (ip_queue *q, cache *mycache)
    : m_queue (q), m_cache (mycache)
  {
  }

  void nb_thread::run ()
  {
    uint32_t ip;
    in_addr inaddr;
    char inname[1024];
    char indomain[1024];
    char error_msg[1024];

    while (true)
    {
      m_queue->pop (ip);
      inaddr.s_addr = ip;

      int ret = ::getname (::inet_ntoa (inaddr), 445, (char *) "NETMON", (char *) "ND", inname,
                           indomain, error_msg);
      if (ret != 0)
        ret = ::getname (::inet_ntoa (inaddr), 139, (char *) "NETMON", (char *) "ND", inname,
                           indomain, error_msg);
      if (ret != 0)
        continue;

      m_cache->update (ip, inname, false);
    }
  }
}
