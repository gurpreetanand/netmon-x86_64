#ifndef __NB_THREAD_H__
#define __NB_THREAD_H__

#include "../netmond/thread.h"
#include "../netmond/queue.h"

#include <stdint.h>

namespace netmon
{
  typedef queue<uint32_t> ip_queue;

  class cache;

  class nb_thread : public thread
  {
    public:
      nb_thread (ip_queue *, cache *);

    protected:
      virtual void run ();

      ip_queue *m_queue;
      cache *m_cache;
  };
}

#endif // __NB_THREAD_H__
