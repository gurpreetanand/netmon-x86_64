#include "dbcache.h"
#include "db.h"

#include <cstring>
#include <sstream>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace netmon
{
  db_cache::db_cache (db *mydb)
    : m_db (mydb)
  {}

  void db_cache::init ()
  {
    int ts = (int) time (0);

    PGresult *res = m_db->execQuery ("SELECT IP, HOSTNAME, HOST_NAME_TYPE FROM HOSTS");

    bool isDNS = false;
    bool isNetBIOS = false;

    for (int i = 0; i < PQntuples (res); ++i)
    {
      isDNS = (strcmp ("DNS", PQgetvalue (res, i, 2)) == 0);
      isNetBIOS = (strcmp ("NetBIOS", PQgetvalue (res, i, 2)) == 0);
      if (isDNS || isNetBIOS)
      {
        in_addr ip;
        if (inet_aton (PQgetvalue (res, i, 0), &ip) == 0) // illegal IP address
          continue;

        ipnameshash::iterator j = m_hash.find (ip.s_addr);
        if (j == m_hash.end())
          m_hash[ip.s_addr] = ipnames (ip.s_addr, ts);

        if (isDNS)
          update (ip.s_addr, PQgetvalue (res, i, 1), true, false);
        if (isNetBIOS)
          update (ip.s_addr, PQgetvalue (res, i, 1), false, false);
      }
    }
    PQclear (res);
  }

  void db_cache::dump ()
  {
    mutex_lock l (m_mutex);

    m_db->execSQL ("BEGIN");
    m_db->beginCopy (
    "COPY HOSTS (IP, HOSTNAME, HOST_NAME_TYPE, TIMESTAMP) FROM STDIN");
    cache::dump();
    m_db->endCopy (0);
    m_db->execSQL ("COMMIT");
  }

  void db_cache::dumpName (const new_name &n)
  {
    cache::dumpName(n);
    static in_addr a;
    static std::ostringstream sql;

    sql.str("");
    a.s_addr = n.m_ip;

    sql << inet_ntoa (a) << "\t"
        << n.m_name << "\t"
        << (n.m_isDNS ? "DNS" : "NetBIOS") << "\t"
        << (int) time (0) << "\n";

    m_db->copyData (sql.str().c_str(), sql.str().size());
  }
}
