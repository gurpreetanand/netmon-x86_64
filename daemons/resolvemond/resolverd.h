#ifndef __RESOLVER_DAEMON__
#define __RESOLVER_DAEMON__

#include "daemon.h"

namespace netmon
{
  class async_resolver;
  class cache;

  class resolver_daemon : public daemon
  {
    public:
      resolver_daemon (int, char **);
      ~resolver_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "resolvemond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/resolvemond";
      }

      async_resolver *m_resolver;
      cache *m_cache;
  };
}

#endif // __RESOLVER_DAEMON__
