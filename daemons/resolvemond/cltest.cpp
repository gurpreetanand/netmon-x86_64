#include <cstdio>
#include <cstdlib>
#include <errno.h>

#include "msgqueue.h"

using namespace netmon;

struct mymsg : public msg
{
  int m_no;
};

int main ()
{
  msgqueue m;
  
  try
  {
    m.access (0xff00ceca);
    mymsg mym;
    size_t rsize = 0;
    while (true)
    {
      m.recv (mym, rsize, sizeof (mym), 0);
      printf ("Got %d\n", mym.m_no);
    }
  }
  catch (int e)
  {
    errno = e;
    perror ("test");
    exit (-1);
  }
}
