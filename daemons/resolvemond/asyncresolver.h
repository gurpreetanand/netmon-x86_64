#ifndef __ASYNC_RESOLVER_H__
#define __ASYNC_RESOLVER_H__

#include <sys/types.h>
#include <event.h>
#include <mqueue.h>
#include <stdint.h>

#include <list>

#include "inetaddr.h"
#include "nbthread.h"
#include "sigtermhandler.h"

struct dns_ctx;
struct dns_parse;
struct dns_rr;

namespace netmon
{
  class cache;
  class db;

  class async_resolver
  {
    private:
      struct query;

    public:
      async_resolver (cache *, db *);
      ~async_resolver ();

      void run ();

    protected:
      static void onTimeout (int, short, void *);
      void onTimeout ();

      static void onRead (int, short, void *);
      void onRead (int);

      static void onDNSData (dns_ctx *, void *, void *);
      void onDNSData (dns_ctx *, void *, query *);
      void handleRR (dns_parse *, dns_rr *, uint32_t);

      void init ();
      void initEvents ();
      void initUDNS ();
      void initLocalNets ();

      void openQueue ();
      void drainQueue ();

      bool sendQuery (uint32_t, bool = true);
      void sendQueuedQueries ();

      bool isLocalAddress (uint32_t);

      int m_fd;
      mqd_t m_queue;
      mq_attr m_qAttr;

      event m_qEvent;
      event m_sockEvent;
      event m_timer;

      sig_term_handler m_sigHandler;

      cache *m_cache;
      db *m_db;
      ip_queue *m_ipqueue;

      unsigned short m_runningQueries;
      unsigned int m_enqueuedIPs;

      enum { maxRunningQueries = 5 };
      enum { maxQueueSize = 16384 };

      typedef std::list<uint32_t> ip_list_t;
      ip_list_t m_queuedIPs;

      typedef std::pair<inetaddr, inetaddr> in_addr_pair_t;
      typedef std::list<in_addr_pair_t> in_addr_list_t;
      in_addr_list_t m_localNets;

    private:
      struct query
      {
        query (uint32_t, const unsigned char *);
        ~query ();

        uint32_t m_ip;
        unsigned char *m_dn;
      };

      struct wrapper
      {
        query *m_query;
        async_resolver *m_resolver;
      };
  };
}

#endif // __ASYNC_RESOLVER_H__
