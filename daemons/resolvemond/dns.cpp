#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <cstring>

#include "dns.h"
#include "exception.h"

extern int h_errno;  /* for resolver errors */
extern int errno;    /* general system errors */

namespace netmon
{
  const char *reversedns::arpstr = ".in-addr.arpa";

  reversedns::reversedns ()
    : m_error (0)
  {
    if (res_init() == -1)
      throw netmon_exception (strerror (errno));
  }

  reversedns::~reversedns ()
  {
  }

  bool reversedns::query (const char *ip, std::list<std::string> &names)
  {
    _response response;
    int responseLen;
    ns_msg handle;

    try
    {
      std::string rip = makeInArpaAddr (ip);
      if ((responseLen = res_search (rip.c_str (), 1, ns_t_ptr, (u_char *) &response, sizeof(response))) < 0)
      {
        m_error = h_errno;
        return false;
      }

      if (responseLen > PACKETSZ)
        return false;

      if (ns_initparse (response.buf, responseLen, &handle) < 0)
      {
        fprintf(stderr, "ns_initparse: %s\n", strerror(errno));
        return false;
      }

      parseResponse (handle, names);
    }
    catch (int e)
    {
      perror ("dns");
      return false;
    }

    return true;
  }

  bool reversedns::parseResponse (ns_msg handle, std::list<std::string> &names)
  {
    ns_rr rr;

    for (int rrnum = 0; rrnum < ns_msg_count (handle, ns_s_an); ++rrnum)
    {
      if (ns_parserr (&handle, ns_s_an, rrnum, &rr))
      {
        fprintf(stderr, "ns_parserr: %s\n", strerror(errno));
        return false;
      }

      if (ns_rr_type (rr) == ns_t_ptr)
      {
        char name[MAXDNAME];

        if (ns_name_uncompress (ns_msg_base (handle),
                                ns_msg_end (handle),
                                ns_rr_rdata (rr),
                                name,
                                MAXDNAME)
                                < 0)
        {
          (void) fprintf(stderr, "ns_name_uncompress failed\n");
          return false;
        }
        names.push_back (std::string (name));
      }
    }

    return true;
  }

  std::string reversedns::makeInArpaAddr (const char *ip)
  {
    std::string tmp;
    union
    {
      in_addr addr;
      unsigned char b[4];
    } myaddr;
    
    if (::inet_aton (ip, &myaddr.addr) == 0)
      throw netmon_exception (strerror (errno));

    unsigned char c = myaddr.b[0];
    myaddr.b[0] = myaddr.b[3];
    myaddr.b[3] = c;

    c = myaddr.b[1];
    myaddr.b[1] = myaddr.b[2];
    myaddr.b[2] = c;

    tmp = std::string (::inet_ntoa (myaddr.addr));
    tmp += arpstr;

printf ("%s\n", tmp.c_str ());
    return tmp;
  }

  const char *reversedns::getError () const
  {
    switch (m_error)
    {
      case HOST_NOT_FOUND:
        return "Unknown domain";
      case NO_DATA:
        return "No NS records for";
      case TRY_AGAIN:
        return "No response for NS query";
      default:
        return "Unexpected error";
    }
  }
}

#ifdef __TEST__

int main (int argc, char **argv)
{
  if (argc < 2)
  {
    printf ("%s ip\n", argv[0]);
    return -1;
  }

  std::list<std::string> mylist;
  netmon::reversedns r;
  if (r.query (argv[1], mylist))
  {
    for (std::list<std::string>::iterator it = mylist.begin(); it != mylist.end(); ++it)
      printf ("%s\n", (*it).c_str());
  }
  else
  {
    printf ("error: %s\n", r.getError ());
  }
  
  return 0;
}

#endif
