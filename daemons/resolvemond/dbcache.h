#ifndef __DB_CACHE_H__
#define __DB_CACHE_H__

#include "cache.h"

namespace netmon
{
  class db;

  class db_cache : public cache
  {
    public:
      db_cache (db *);

      virtual void init ();
      virtual void dump ();

    protected:
      virtual void dumpName (const new_name &);

      db *m_db;
  };
}

#endif // __DB_CACHE_H__
