#include "pagermond.h"
#include "pagersender.h"
#include "log.h"
#include "dvars.h"
#include "dbus.h"

#include <sys/time.h>

#include <string>

namespace netmon
{
  sig_atomic_t pagermon_daemon::m_timerFired = 1;
  DBus::BusDispatcher pagermon_daemon::m_dispatcher;

  pagermon_daemon::pagermon_daemon (int argc, char **argv)
    : daemon (argc, argv)
  {
  }

  pagermon_daemon::~pagermon_daemon ()
  {
    delete m_pSender;
  }

  void pagermon_daemon::init ()
  {
    daemon::init();

    installTimer();

    std::string sid, token, number;

    m_vars->get ("twilio_sid", sid);
    m_vars->get ("twilio_token", token);
    m_vars->get ("twilio_number", number);

    m_pSender = new pager_sender (m_db, sid, token, number);

    DBus::default_dispatcher = &m_dispatcher;
  }

  int pagermon_daemon::run ()
  {
    DBus::Connection conn = DBus::Connection::SystemBus();
    conn.request_name ("ca.netmon.DBus.pagermond");

    dbus_pagermond_server server (conn, m_pSender);

    while (!isTermSignaled())
    {
      if (m_timerFired == 1)
      {
        printf ("on timer\n");
        m_pSender->run();
        m_timerFired = 0;
        netmon::log::instance()->add(netmon::_eLogInfo, "New iteration");
        installTimer();
      }
      m_dispatcher.enter();
    }
    return 0; // never reached
  }

  int pagermon_daemon::installTimer ()
  {
    struct sigaction saction;

    saction.sa_handler = onTimer;
    if (sigemptyset (&saction.sa_mask) == -1)
      return -1;

    saction.sa_flags = 0;
    if (sigaction (SIGALRM, &saction, NULL) == -1)
      return -1;
    if (sigaction (SIGHUP, &saction, NULL) == -1)
      return -1;

    itimerval timer;
    timer.it_interval.tv_sec = timer_interval;
    timer.it_interval.tv_usec = 0;
    timer.it_value.tv_sec = timer_interval;
    timer.it_value.tv_usec = 0;

    if (setitimer (ITIMER_REAL, &timer, 0) == -1)
      return -1;

    return 0;
  }

  void pagermon_daemon::onTimer (int sig)
  {
    if (sig == SIGALRM)
    {
      m_dispatcher.leave();
      m_timerFired = 1;
    }
  }
}
