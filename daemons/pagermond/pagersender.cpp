#include <sstream>
#include <cstring>
#include <vector>

#include "log.h"
#include "pagersender.h"
#include "arrayutil.h"
#include "db.h"

#define TWILIO_API_VERSION "2010-04-01"

namespace netmon
{
  pager_sender::pager_sender (db *mydb, const std::string &sid, const std::string &token, const std::string &number)
    : m_db (mydb), m_twilioSid(sid), m_twilioToken(token), m_twilioNumber(number), m_rest(sid, token)
  {
    getMediaID ();
    if (m_twilioSid.size() > 0 && m_twilioToken.size() > 0 && m_twilioNumber.size() > 0) {
      m_validConfig = true;
    }
  }

  void pager_sender::run ()
  {
    clear ();
    if (m_validConfig) {
      getPendingPages ();
      sendAlerts ();
    }
  }

  void pager_sender::getPendingPages ()
  {
    static std::ostringstream sql;
    sql.str("");
    sql <<
    "select b.id, b.parsed_alert_message, d.pager_terminal, d.pager_number, \
     (a.required_retries - b.retries_processed) as c1 \
     from alert_handlers a, alert_pending b, alert_medias c, users d \
     where b.handler_id = a.id \
     and d.id = a.user_id \
     and c.id = a.media_id \
     and c.name = 'pager' \
     and (a.required_retries - b.retries_processed) > 0 \
     and b.sent = '0' \
     order by b.trigger_timestamp asc";

    PGresult *res;
    res = m_db->execQuery (sql.str().c_str());

    int count = PQntuples (res);
    std::ostringstream countmsg;
    countmsg <<"Pending pages count: " << count;
    netmon::log::instance()->add(netmon::_eLogDebug, countmsg.str().c_str());

     for (int i = 0; i < PQntuples(res); ++i)
     {
       pager_data *d = new pager_data;
       d->m_id = atoi (PQgetvalue (res, i, 0));
       fixMessage (PQgetvalue (res, i, 1), d->m_message);
       d->m_terminal = std::string (PQgetvalue (res, i, 2));
       d->m_pager = std::string (PQgetvalue (res, i, 3));
       m_pagers.push_back(d);
     }

     PQclear(res);
  }

  void pager_sender::sendAlerts ()
  {
    for (pagers_t::iterator i = m_pagers.begin(); i != m_pagers.end(); ++i)
    {
      // Send page via twilio, update alert.
      updateAlert ((*i)->m_id, sendPage((*i)->m_pager, (*i)->m_message));
    }
  }

  void pager_sender::updateAlert (unsigned int id, bool ok)
  {
    static std::ostringstream sql;
    sql.str("");
    if (ok)
    {
      sql << "UPDATE ALERT_PENDING SET SENT = '1', DISPATCH_TIMESTAMP = "
          << (int) time(0)
          << ", RETRIES_PROCESSED = RETRIES_PROCESSED+1 WHERE ID = "
          << id;
    }
    else
    {
      sql << "UPDATE ALERT_PENDING SET RETRIES_PROCESSED = RETRIES_PROCESSED+1"
          << ", DISPATCH_TIMESTAMP = "
          << (int) time(0)
          << " WHERE ID = "
          << id;
    }
    m_db->execSQL (sql.str().c_str());
  }

  void pager_sender::clear ()
  {
    for (pagers_t::iterator i = m_pagers.begin(); i != m_pagers.end(); ++i)
      delete *i;
    m_pagers.clear ();
  }

  void pager_sender::getMediaID ()
  {
    PGresult *res = m_db->execQuery ("select id from alert_medias where name = 'pager'");
    if (PQntuples(res) == 0)
      m_mediaID = 0;
    else
      m_mediaID = atoi (PQgetvalue (res, 0, 0));
    PQclear(res);
  }

  void pager_sender::fixMessage (char *msg, std::string &fixed)
  {
    unsigned int i = strlen (msg);
    if (i > 250)
      i = 250;

    fixed.reserve (i);

    i = 0;
    for (char *p = msg; *p != '\0' && i < 250; ++p, ++i)
      if (*p == '\r' || *p == '\n')
        fixed.push_back (' ');
      else
        fixed.push_back (*p);
  }

  bool pager_sender::sendPage(std::string number, std::string message) {
    static std::string url = "/" + std::string(TWILIO_API_VERSION) + "/Accounts/" + m_twilioSid + "/SMS/Messages";

    std::vector<twilio::Var> vars;

    vars.push_back(twilio::Var("To", number));
    vars.push_back(twilio::Var("From", m_twilioNumber));
    vars.push_back(twilio::Var("Body", message));

    std::ostringstream pagermsg;
    pagermsg << "-----PagerAlert-----\nTo: " << number << "\nFrom: "<<m_twilioNumber<<"\nBody: "<<message;
    netmon::log::instance()->add(netmon::_eLogDebug, pagermsg.str().c_str());

    std::string response = m_rest.request(url, "POST", vars);

    // TODO: If people use twilio, actually parse the response XML.
    if (response.find("RestException") == std::string::npos) {
      return true;
    } else {
      return false;
    }
  }
}
