#ifndef __EMAIL_MOND_DBUS__
#define __EMAIL_MOND_DBUS__

#include <dbus-c++/dbus.h>

#include "pagermond-glue.h"

namespace netmon
{
  class pager_sender;

  class dbus_pagermond_server
    : public DBus::IntrospectableAdaptor,
      public DBus::ObjectAdaptor
  {
    public:
      dbus_pagermond_server (DBus::Connection &, pager_sender *);

      virtual void alert();

    protected:
      pager_sender *m_pSender;
  };
}

#endif // __EMAIL_MOND_DBUS__
