#include "dbus.h"
#include "pagersender.h"
#include <stdio.h>

namespace netmon
{
  static const char *SERVER_PATH = "/ca/netmon/DBus/pagermond";

  dbus_pagermond_server::dbus_pagermond_server (DBus::Connection &conn, pager_sender *sender)
    : DBus::ObjectAdaptor(conn, SERVER_PATH), m_pSender (sender)
  {
  }

  void dbus_pagermond_server::alert ()
  {
    printf ("alert() called\n");
    m_pSender->run();
  }
}
