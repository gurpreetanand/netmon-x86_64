#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "../lib/netmon.h"
#include "bigint/BigIntegerLibrary.hh"

#define MAXLINE          1024

int getdiskusage(char *inputline, const char *partition, char *error_message, BigInteger &, BigInteger &);

/******************************************************************************
* 
* Name: getspace
* Description: gets disk attributes on remote Unix server
* 
* Arguments: host IP, port number, partition
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int getspace(const char *host, int port, const char *partition, int *percentage,
             char *error_message, BigInteger &total, BigInteger &available)
{
  int                  sockfd;
  struct sockaddr_in   serv_addr;
  char                 line[MAXLINE];
      
  /* Fill the structure serv_addr with the actuall server address */
  
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(host);
  serv_addr.sin_port        = htons(port);
          
  /* Open a TCP socket */
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    strcpy(error_message, "Could not allocate socket.");
    return errno;
  }

  // set timeout value to 2 second
  timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(tv));

  /* Connect to the server */
  if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    strcpy(error_message, "Cannot connect to server");
    
    /* Close socket */
    close(sockfd);

    return errno;
  }

  memset(line, '\0', MAXLINE);
  if (read(sockfd, line, MAXLINE) < 0)
  {
    strcpy(error_message, "Server not responding");
    
    /* Close socket */
    close(sockfd);
    
    return errno;
  }

  /* Disconnect */
  /* Close socket */
  
  close(sockfd);
  printf("\n\n\nLINE: %s\n\n\n",line);
  if ((*percentage = getdiskusage(line, partition, error_message, total, available)) < 0)
    return -1;
    
  /* Everything went fine if we came to this point, return 0 */
  
  return 0;
}

int getdiskusage(char *inputline, const char *partition, char *error_message, BigInteger &tot, BigInteger &ava)
{
  char *pointer;
  char nullstring[128];
  char total[128];
  char available[128];
  int percentage = 0;
  
  if ((pointer = strstr(inputline, partition)) == NULL)
  {
    strcpy(error_message, "Partition not found");
    return -1;
  }
  printf("\n\n\nPOINTER: %s\n\n\n",pointer);
  if (sscanf(pointer, "%s%s%s%s%d%%",
          nullstring, 
          total,
          nullstring,
          available,
          &percentage) < 5)
  {
    strcpy(error_message, "Unknown df format");
    return -1;
  }

  tot = stringToBigInteger(total);
  ava = stringToBigInteger(available);

  return percentage;
}
