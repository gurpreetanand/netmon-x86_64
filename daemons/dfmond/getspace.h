#ifndef __GETSPACE__
#define __GETSPACE__

#include "bigint/BigInteger.hh"

int getspace(const char *host, int port, const char *partition, int *percentage,
             char *error_message, BigInteger &, BigInteger &);

#endif // __GETSPACE__
