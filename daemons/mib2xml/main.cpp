#include <cstdio>
#include <getopt.h>
#include <smi.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <string>
#include <sstream>

#include "netmon.h"
#include "exception.h"
#include "dumpxml.h"
#include "dboidstore.h"

const char version[] = "0.2.2";
static bool force = false;

char dbname [DBNAMELEN];
char dbuser [DBUSERLEN];

static struct option const long_options[] =
{
  {"help",     no_argument, 0, 'h'},
  {"version",  no_argument, 0, 'v'},
  {"force",    no_argument, 0, 'f'},
  {"xml",      no_argument, 0, 'x'},
  {"mib",      required_argument, 0, 'm'},
  {"output",   required_argument, 0, 'o'},
  {0, 0, 0, 0}
};

static int printversion ()
{
  printf ("mib2xml v%s\n", version);
  return 0;
}

static int usage ()
{
  printf ("Usage: mib2xml -m mib [-o output] [-f] [-h] [-v] \n");
  printf ("\t-h: gives this help\n");
  printf ("\t-v: prints version information\n");
  printf ("\t-f: force parsing (will ignore warnings and errors) - dangerous!\n");
  printf ("\t-x: output XML to the filesystem instead of the DB\n");
  printf ("\t-m mib: parse this mib\n");
  printf ("\t-o output: output XML to this file (default: use module name)\n");

  return 0;
}

static void errorHandler (char *path, int line, int severity, char *msg, char *tag)
{
  if (severity <= 1)
    fprintf (stderr, "Error: ");
  else
    fprintf (stderr, "Warning: ");

  if (path)
    fprintf (stderr, "%s:%d: ", path, line);

  fprintf (stderr, "%s\n", msg);

  if (severity <= 1 && !force)
    exit (1);
}

int main (int argc, char **argv)
{
  int c, longind;
  const char *mib = 0;
  const char *out = 0;
  bool tofs = false;

  while ((c = getopt_long (argc, argv, "hvfxm:o:", long_options, &longind)) != EOF)
  {
    switch (c)
    {
      case 'h':
        return usage ();
      case 'v':
        return printversion ();
      case 'f':
        force = true;
        break;
      case 'm':
        mib = optarg;
        break;
      case 'x':
        tofs = true;
        break;
      case 'o':
        out = optarg;
        break;
      default:
        printf ("Illegal argument, use --help for help\n");
        exit (-1);
    }
  }

  if (mib == 0)
  {
    printf ("No mib file given. Use mib2xml -h for help\n");
    exit (-1);
  }

  readConfig (dbname, dbuser);

  smiInit ("mib2xml");
  smiSetErrorHandler (errorHandler);
  int flags = smiGetFlags ();
  flags |= SMI_FLAG_ERRORS;
  smiSetFlags (flags);
  std::string oname;

  char *modulename = smiLoadModule (mib);
  if (modulename != 0)
  {
    SmiModule *smiModule = smiGetModule (modulename);
    if (smiModule)
    {
      if (out == 0)
      {
        out = smiModule->name;
        if (out != 0)
        {
          oname = std::string (out);
          oname += ".xml";
          out = oname.c_str();
        }
      }
      if (!tofs)
      {
        std::ostringstream t;
        t << "/tmp/smitmp.xml" << getpid();
        oname = t.str();
        out = oname.c_str();
      }

      dumpXml (smiModule, flags, out);

      try
      {
        netmon::dboidstore oidstore (dbname, dbuser);
        netmon::oidstore::instance()->store (oidstore);
        if (!tofs)
        {
          netmon::db db;
          db.connect (dbname, dbuser);
//          storeXML (out, db);
          unlink (out);
        }
      }
      catch (netmon::netmon_exception &e)
      {
        fprintf (stderr, "Error: %s\n", e.what());
        exit (1);
      }
    }
  }

  smiExit ();
  return 0;
}
