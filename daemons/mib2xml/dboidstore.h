#ifndef __DB_STORE__
#define __DB_STORE__

#include <string>

#include "db.h"
#include "oidstore.h"

namespace netmon
{
  class dboidstore : public storefunctor
  {
    public:
      dboidstore (const std::string &, const std::string &);
      ~dboidstore ();

      virtual void store (const std::string &, const oid &);

    protected:
      void init ();

      db m_db;
      std::string m_dbname;
      std::string m_dbuser;
  };
}

#endif // __DB_STORE__
