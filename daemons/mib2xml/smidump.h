/*
 * smidump.h --
 *
 *      This header contains the entry points into the modules
 *	which dump MIB modules in various output format.
 *
 * Copyright (c) 1999 Frank Strauss, Technical University of Braunschweig.
 * Copyright (c) 1999 J. Schoenwaelder, Technical University of Braunschweig.
 *
 * See the file "COPYING" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 * @(#) $Id$
 */

#ifndef _SMIDUMP_H
#define _SMIDUMP_H

/*
 * The following flags can be passed to output drivers in the flags
 * member of the struct above.
 */

#define SMIDUMP_FLAG_SILENT	0x01	/* suppress comments */
#define SMIDUMP_FLAG_UNITE	0x02	/* generated united output */

#define SMIDUMP_FLAG_CURRENT	0x04	/* not yet used */
#define SMIDUMP_FLAG_DEPRECATED	0x08	/* not yet used */
#define SMIDUMP_FLAG_OBSOLETE	0x08	/* not yet used */
#define SMIDUMP_FLAG_COMPACT	0x10	/* not yet used */

/*
 * Driver capability flags which are used to warn about options not
 * understood by a particular output driver.
 */

#define SMIDUMP_DRIVER_CANT_UNITE	0x02
#define SMIDUMP_DRIVER_CANT_OUTPUT	0x04

/*
 * The functions are wrappers for the malloc functions which handle
 * memory allocation errors by terminating the program.
 */

extern void *xmalloc(size_t size);
extern void *xrealloc(void *ptr, size_t size);
extern char *xstrdup(const char *s);
extern void xfree(void *ptr);

#endif /* _SMIDUMP_H */
