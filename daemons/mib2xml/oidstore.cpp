#include "oidstore.h"

namespace netmon
{
  oidstore *oidstore::m_instance = 0;

  oidstore::~oidstore ()
  {
    for (hash_map<std::string, oid *>::iterator it = m_oids.begin(); it != m_oids.end(); ++it)
      delete it->second;
    m_oids.clear ();
  }

  oidstore *oidstore::instance ()
  {
    if (m_instance == 0)
      m_instance = new oidstore;
    return m_instance;
  }

  void oidstore::add (const char *oidstr, const char *name, const char *desc)
  {
    oid *t = new oid;
    t->m_name = std::string (name);
    if (desc != 0)
      t->m_description = std::string (desc);

    m_oids[std::string(oidstr)] = t;
  }

  void oidstore::store (storefunctor &f)
  {
    for (hash_map<std::string, oid *>::iterator i = m_oids.begin(); i != m_oids.end(); ++i)
      f.store (i->first, *(i->second));
  }
}
