#include "trapoidstore.h"

#include <cstring>
#include <sstream>

namespace netmon
{
  trapoidstore::trapoidstore (const std::string &dbname, const std::string &dbuser)
    : m_mibInserted (false), m_dbname (dbname), m_dbuser (dbuser), m_id (-1)
  {
    init();
  }

  trapoidstore::~trapoidstore ()
  {
    m_db.execSQL ("COMMIT");
    m_db.close();
  }

  void trapoidstore::store (const std::string &stroid, const oid &myoid)
  {
    static std::ostringstream sql;

    if (!m_mibInserted && m_id == -1)
    {
      insertMIBFile();
      m_mibID = getMIBID();
    }

    char *esc = 0;
    if (myoid.m_description.size() > 0)
    {
      esc = new char[(myoid.m_description.size() * 2 + 1)];
      PQescapeString (esc, myoid.m_description.c_str(), myoid.m_description.size());
    }

    trap_data_hash_t::iterator i = m_data.find (stroid);
    if (i != m_data.end())
    {
      if (i->second->m_name != myoid.m_name ||
          strcmp (i->second->m_desc.c_str(), esc) != 0)
      {
        sql.str("");
        sql << "update snmp_traps_trans set trap_name = '" << myoid.m_name
            << "', trap_description = '" << esc << "' where id = "
            << i->second->m_id;

        delete[] esc;

        m_db.execSQL (sql.str().c_str());
      }
    }
    else
    {
      sql.str("");
      sql << 
      "insert into snmp_traps_trans (id, trap_oid, trap_name, trap_description, \
       mib_id) values (default, '" << stroid << "', '" << myoid.m_name << "', '";

      if (esc != 0)
        sql << esc;

      delete[] esc;

      sql << "', "
          << m_mibID
          << ")";

      m_db.execSQL (sql.str().c_str());
    }
  }

  void trapoidstore::setFile (const std::string &file, const std::string &path)
  {
    m_file = file;
    m_path = path;

    std::string sql ("select id from snmp_mib_files where mib_file = '");
    sql += file;
    sql += "'";

    PGresult *res = m_db.execQuery (sql.c_str());
    if (PQntuples (res) == 0)
      return;

    m_id = atoi (PQgetvalue (res, 0, 0));
    PQclear (res);

    if (m_id != -1)
      loadOIDs();
  }

  void trapoidstore::init ()
  {
    m_db.connect (m_dbname.c_str(), m_dbuser.c_str());
    m_db.execSQL ("BEGIN");
  }

  void trapoidstore::loadOIDs ()
  {
    std::ostringstream sql;
    sql << "select id, trap_oid, trap_name, trap_description from "
        << "snmp_traps_trans where mib_id = " << m_id;

    PGresult *res = m_db.execQuery (sql.str().c_str());
    for (int i = 0; i < PQntuples (res); ++i)
    {
      trap_data *data = new trap_data;
      data->m_id = atoi (PQgetvalue (res, i, 0));
      data->m_name = std::string (PQgetvalue (res, i, 2));
      data->m_desc = std::string (PQgetvalue (res, i, 3));
      m_data[std::string (PQgetvalue (res, i, 1))] = data;
    }
    PQclear (res);
  }

  void trapoidstore::insertMIBFile ()
  {
    std::ostringstream sql;
    sql << "insert into snmp_mib_files values (default, '"
        << m_file
        << "', '"
        << m_path
        << "')";
    m_db.execSQL (sql.str().c_str());
    m_mibInserted = true;
  }

  unsigned int trapoidstore::getMIBID ()
  {
    PGresult *res = m_db.execQuery ("select currval('snmp_mib_files_id_seq')");
    if (PQntuples (res) == 0)
      return 0;

    unsigned int ret = atoi (PQgetvalue (res, 0, 0));
    PQclear (res);

    return ret;
  }
}
