CREATE TABLE SMB_SERVERS (
  srv_id    bigserial		not null,
  ip           inet		not null,
  share        varchar(32),
  username     varchar(32),
  password     varchar(32),
  servername   varchar(32),
  domain       varchar(32),
  timeout      int              not null,
  interval     int		not null,
  threshold    int		not null,
  status       int,
  total        int,
  available    int,
  blocksize    int,
  timestamp    int,
  message      varchar(255),
  pending      varchar(1),
  primary key (srv_id)
);

CREATE INDEX smb_servers_ip_indx ON SMB_SERVERS(ip);
CREATE INDEX smb_servers_timestamp_indx ON SMB_SERVERS(timestamp);

CREATE TABLE SMB_SERVER_LOG (
  log_id       bigserial    not null,
  srv_id       bigint       references SMB_SERVERS,
  total        int,
  available    int,
  blocksize    int,
  timestamp    int,
  primary key (log_id));

CREATE INDEX smb_server_srv_id_indx ON SMB_SERVER_LOG(srv_id);
CREATE INDEX smb_server_log_timestamp_indx ON SMB_SERVER_LOG(timestamp);

CREATE TABLE SMB_EMAILALERTS (
  srv_id         bigint       references SMB_SERVERS,
  email_id       bigint       references EMAILS,
  primary key    (srv_id, email_id));

CREATE TABLE SMB_PAGERALERTS (
  srv_id          bigint       references SMB_SERVERS,
  pager_id        bigint       references PAGERS,
  retries         int          not null,
  primary key     (srv_id, pager_id));

ANALYZE VERBOSE;
VACUUM ANALYSE;

