#ifndef __SMB_ALERT_MANAGER__
#define __SMB_ALERT_MANAGER__

#include "srvalertmanager.h"
#include "alertutil.h"

namespace netmon
{
  class smb_alert_manager : public service_alert_manager
  {
    public:
      using alert_manager::getAlerts;
      smb_alert_manager (db *);
      void sendUp (unsigned int id, values_map vmap);

    protected:
      virtual void getReferenceTable (std::string &s)
      {
        s = "smb_servers";
      }

      virtual void getOverName (std::string &s)
      {
        s = "SMB_DISK_OVER_THRESHOLD";
      }

      virtual void getBelowName (std::string &s)
      {
        s = "SMB_DISK_BELOW_THRESHOLD";
      }

      virtual void getUpName (std::string &s)
      {
        s = "SMB_SERVICE_UP";
      }

      virtual void getDownName (std::string &s)
      {
        s = "SMB_SERVICE_DOWN";
      }
  };
}

#endif // __SMB_ALERT_MANAGER__
