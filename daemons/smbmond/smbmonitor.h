#ifndef __SMB_MONITOR__
#define __SMB_MONITOR__

#include <string>
#include <list>

namespace netmon
{
  class db;
  class smb_alert_manager;

  class smbmonitor
  {
    public:
      smbmonitor (db *);
      ~smbmonitor ();

      void run ();

    protected:
      struct server_data
      {
        unsigned int m_id;
        int m_status;
        std::string m_ip;
        std::string m_share;
        std::string m_user;
        std::string m_pass;
        std::string m_serverName;
        std::string m_domain;
      };

      typedef std::list<server_data *> server_list;

      void getServers ();
      void checkServer (server_data *);
      void clearServerList ();
      void resetFlags ();
      void setPendingFlag (unsigned int);
      void updateDBFailure (unsigned int, char *);
      void updateDBSuccess (int, int, int, int, int);
      void createShareName (const std::string &, const std::string &, std::string &);

      server_list m_list;
      db *m_db;
      smb_alert_manager *m_alertManager;
  };
}

#endif // __SMB_MONITOR__
