#ifndef __GETSPACE__
#define __GETSPACE__

#include <list>
#include <string>
#include <stdint.h>

int prepare_114(char *line);
int unicode_copy(char *dest, char *src);
unsigned short lendian(unsigned char first, unsigned char second);
int int_lendian(unsigned char first, unsigned char second, unsigned third, unsigned forth);

int getspace (const char *smbhost, int port, const char *share, 
             const char *username, const char *password, 
             const char *servername, const char *domain,
             int *total, int *available, int *blocksize,
             char *error_message);

int getshares (const char *smbhost, int port, 
             const char *username, const char *password, 
             const char *servername, const char *domain,
             std::list<std::string> &,
             char *error_message);

class shareInfo {
  public:
    uint32_t share_rid;
    uint32_t comment_rid;
    uint32_t type;
    std::string name;
    std::string comment;
    uint32_t share_length;
    uint32_t comment_length;
};
             
#endif // __GETSPACE__
