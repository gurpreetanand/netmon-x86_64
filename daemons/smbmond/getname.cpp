#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/tcp.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <syslog.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <netdb.h>
#include <setjmp.h>

#include "../lib/netmon.h"
#include "getspace.h"

#define MAXLINE          1024

static int check_error(char *line);
static int analyze_114(char *line, char *inname, char *indomain);

extern char    uid_l, uid_h, tid_l, tid_h;
int     timed_out = 0;     
jmp_buf env;

/******************************************************************************
* 
* Name: check_error_nbt
* Description: checks if received packet is a nbt response we expect
* 
* Arguments: Buffer that contains an NetBIOS packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/

static int check_error_nbt(char *line, char *error_message)
{
  /* Positive session response */

  if ((unsigned char)*(line) == 0x82)
    return 0;
  /* No error */

  /* Negative session response */
  if ((unsigned char)*(line) == 0x83)
  {
    sprintf(syslogbuff, "Negative session response, error code = %d\n", (unsigned char)*(line+4)); 
    strcat(error_message, syslogbuff);
    return -5;
  } 
  
  /* Retarget session response */
  if ((unsigned char)*(line) == 0x84)
  {
    strcat(error_message, "Error: Netmon does not support retargeting.");
    return -6;
  }
  
  sprintf(syslogbuff, "Error: Unknown session request response: %d.", (unsigned char)*(line));  
  strcat(error_message, syslogbuff);
  
  return -7;
}

/******************************************************************************
* 
* Name: NB_Encode
* Description: encodes string to NetBIOS preffered shape
* 
* Arguments: destination string, source string
* Returns: pointer to destination string
*
*****************************************************************************/
 
static unsigned char *NB_Encode( unsigned char *dst, unsigned char *name )
  {
  int i = 0;
  int j = 0;

  /* Encode the name. */
  while( ('\0' != name[i]) && (i < 16) )
    {
    dst[j++] = 'A' + ((name[i] & 0xF0) >> 4);
    dst[j++] = 'A' + (name[i++] & 0x0F);
    }

  /* Encode the padding bytes. */
  while( j < 32 )
    {
    dst[j++] = 'C';
    dst[j++] = 'A';
    }

  /* Terminate the string. */
  dst[32] = '\0';
  return( dst );

} /* NB_Encode */

/******************************************************************************
* 
* Name: prepare_nbt_129
* Description: assembles Netbios packet 129 (command = 0x81)
* 
* Arguments: Buffer to store NetBIOS packet
* Returns:   size of packet
*
*****************************************************************************/

static int prepare_nbt_129(char *line, char *server_name)
{
  char *pointer;
  char called_name[128], calling_name[128];

  pointer = line;

  /* Type */
  *pointer++ = 129;
  
  /* Flags */
  *pointer++ = 0;

  /* Length - to be determined later */
  *pointer++ = 0;
  pointer++;
  
  NB_Encode((unsigned char *) called_name, (unsigned char *) server_name);
  NB_Encode((unsigned char *) calling_name, (unsigned char *) "NETMON");

  *pointer++ = 0x20;
  
  strcpy(pointer, called_name);
  pointer += strlen(called_name) + 1;
  
  *pointer++ = 0x20;
  
  strcpy(pointer, calling_name);
  pointer += strlen(calling_name) + 1;

  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  *pointer++ = 0x00;
  
  *(line+3) = pointer - line - 4;

  return pointer - line;  
}

/******************************************************************************
* 
* Name: getspace
* Description: gets machine netbios name
* 
* Arguments: SMB host, port number,
*            server name, domain name
* Returns:   0 if succesfull, non-zero otherwise
*
*****************************************************************************/

int getname(char *smbhost, int port, char *servername, char *domain,
            char *inname, char *indomain, char *error_message)
{
  int                  sockfd, byteswritten, smb_len;
  struct sockaddr_in   serv_addr;
  char                 line[MAXLINE];
  int                  connect_reply = -1; 

  /* Fill the structure serv_addr with the actuall server address */
  
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = inet_addr(smbhost);
  serv_addr.sin_port        = htons(port);
          
  /* Open a TCP socket */
  if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    sprintf(syslogbuff, "%d: Could not allocate socket. ", port);
    strcat(error_message, syslogbuff);
    return -1;
  }

  timed_out = 0;

  /* This is where we return if connect times out */
  sigsetjmp(env, 1);

  if (timed_out == 1)
  {
    sprintf(syslogbuff, "%d: Cannot connect to SMB server. ", port);
    strcat(error_message, syslogbuff);
   
    /* Close socket */
    close(sockfd);

    return -2;
  }

  /* Set timeout to 5 seconds */
  alarm(5);   

  /* Connect to the SMB server */
  connect_reply = connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

  /* Turn off alarm */
  alarm(0);

  if (connect_reply < 0)
  {
    sprintf(syslogbuff, "%d: Cannot connect to SMB server. ", port);
    strcat(error_message, syslogbuff);
    
    /* Close socket */
    close(sockfd);

    return -2;
  }

  /* If SMB is going over NetBIOS, establish session */
  if (port == 139)
  {
    int nbt_len;

    memset(line, '\0', MAXLINE);
    
    /* 0x81 = SESSION REQUEST */
    nbt_len = prepare_nbt_129(line, servername);
    
    if ((byteswritten = write(sockfd, line, nbt_len)) != nbt_len)
    {
      sprintf(syslogbuff, "%d: Error while writing to SMB server (while sending NBT_129).\
                         Number of bytes sent: %d", port, byteswritten);
    
      strcat(error_message, syslogbuff);
      
      /* Close socket */
      close(sockfd);
    
      return 3;
    }

    memset(line, '\0', MAXLINE);  
    if (read(sockfd, line, MAXLINE) < 0)
    {
      sprintf(syslogbuff, "%d: SMB server not responding. ", port);
      strcat(error_message, syslogbuff);

      /* Close socket */
      close(sockfd);
    
      return 4;
    }

    if (check_error_nbt(line, error_message) != 0)
    {
      sprintf(syslogbuff, "%d: Could not establish netbios session. ", port);
      strcat(error_message, syslogbuff);
      
      /* Close socket */
      close(sockfd);
      
      return 5;
    }
    
  } /* NetBIOS session */


  memset(line, '\0', MAXLINE);
  smb_len = prepare_114(line);

  if ((byteswritten = write(sockfd, line, smb_len)) != smb_len)
  {
    sprintf(syslogbuff, "%d: Error while writing to SMB server (while sending 114).\
                         Number of bytes sent: %d", port, byteswritten);
    
    strcat(error_message, syslogbuff);
    
    /* Close socket */
    close(sockfd);
    
    return 6;
  }

  /* Hey, I was friendly, let's see what you have to say */

  memset(line, '\0', MAXLINE);  
  if (read(sockfd, line, MAXLINE) < 0)
  {
    sprintf(syslogbuff, "%d: SMB server not responding. ", port);
    strcat(error_message, syslogbuff);

    /* Close socket */
    close(sockfd);
    
    return 7;
  }

  if (check_error(line) != 0)
  {
    sprintf(syslogbuff, "%d, SMB Protocol negotiation failed. ", port);
    strcat(error_message, syslogbuff);

    /* Close socket */
    close(sockfd);

    return 8;
  } 
      
  /* This is convenient for debugging, but we don't really need it */
  analyze_114(line, inname, indomain);

  /* Check if we actually got the name */
  if (strlen(inname) == 0)
    return 9;

  /* Disconnect */
  /* Close socket */
  
  close(sockfd);
  
  /* Everything went fine if we came to this point, return 0 */
  
  return 0;
}

/******************************************************************************
* 
* Name: analyze_114
* Description: gets various information from received SMB block 114
* 
* Arguments: Buffer that contains received SMB block
* Returns: 0
*
*****************************************************************************/

static int analyze_114(char *line, char *inname, char *indomain)
{
  /* Word count is always 17 for 114 SMB command */
  /* Challenge/response key is always 8 */
  /* So, our position is 81 */

  char *start, *pointer;
  char a1, a2;
  char name[128], domain[128];
  
  memset(name,   0, 128);
  memset(domain, 0, 128);
  
// *********** DEBUG SECTION **************  
//  /* Get the lower byte of flags2 */
//  flags2 = *(line+14);
//  
//  if (flags2 & 0x01)
//  {
//    // DEBUG
//    printf("Unicode supported by server.\n");
//    unicode = 1; 
//  }
//  else
//  {
//    // DEBUG
//    printf("Unicode not supported by server.\n");
//    unicode = 0;
//  }
//
//  if (flags2 & 0x01) printf ("14: 0x01\n");
//  if (flags2 & 0x02) printf ("14: 0x02\n");
//  if (flags2 & 0x04) printf ("14: 0x04\n");
//  if (flags2 & 0x08) printf ("14: 0x08\n");
//  if (flags2 & 0x10) printf ("14: 0x10\n");
//  if (flags2 & 0x20) printf ("14: 0x20\n");
//  if (flags2 & 0x40) printf ("14: 0x40\n");
//  if (flags2 & 0x80) printf ("14: 0x80\n");
//
//  /* Get the upper byte of flags2 */
//  flags2 = *(line+15);
//
//  if (flags2 & 0x01) printf ("15: 0x01\n");
//  if (flags2 & 0x02) printf ("15: 0x02\n");
//  if (flags2 & 0x04) printf ("15: 0x04\n");
//  if (flags2 & 0x08) printf ("15: 0x08\n");
//  if (flags2 & 0x10) printf ("15: 0x10\n");
//  if (flags2 & 0x20) printf ("15: 0x20\n");
//  if (flags2 & 0x40) printf ("15: 0x40\n");
//  if (flags2 & 0x80) printf ("15: 0x80\n");
// ********** END OF DEBUG SECTION **********

  /* Go to start of Buffer area */
  start   = line + 81;

  pointer = start;

  a1 = *pointer;
  a2 = *(pointer+1);

  while ((a1 != 0) || (a2 != 0))
  {
    strncat(domain, pointer, 1);
  
    pointer += 2;
    a1 = *pointer;
    a2 = *(pointer+1);
  }
  strncat(domain, "\0", 1);
  
  pointer += 2;
  a1 = *pointer;
  a2 = *(pointer+1);

  while ((a1 != 0) || (a2 != 0))
  {
    strncat(name, pointer, 1);
    
    pointer += 2;
    a1 = *pointer;
    a2 = *(pointer+1);
  }
  strncat(name, "\0", 1);


  strcpy(inname, name);
  strcpy(indomain, domain);
  // printf("Computer name: %s\n", name);
  // printf("Domain   name: %s\n", domain);
    
  return 0;
}

/******************************************************************************
* 
* Name: check_error
* Description: checks if received packet is an SMB packet, and if it's 
*              status is OK
* 
* Arguments: Buffer that contains an SMB packet
* Returns: 0 if no error, -1 otherwise
*
*****************************************************************************/
            
static int check_error(char *line)
{
  int status;
  unsigned char err_1, err_2, err_3, err_4;
  unsigned char s, m, b;
  
  s     = *(line+5);
  m     = *(line+6);
  b     = *(line+7);
  
  if ((s != 'S') || (m != 'M') || (b != 'B'))
  {
    // DEBUG
    // syslog(LOG_ERR, "Error, non-SMB packet received.\n", SYSLOGLINELEN);
    return -3;
  }
  
  err_1 = *(line+9);
  err_2 = *(line+10);
  err_3 = *(line+11);
  err_4 = *(line+12);
  
  status = int_lendian(err_1, err_2, err_3, err_4);

  if (status != 0)
  {
    // DEBUG
    // sprintf(syslogbuff, "SMB Error, status = %d\n", (unsigned int)status);
    // syslog(LOG_ERR, syslogbuff, SYSLOGLINELEN);
    return -4;
  }

  return 0;
}

