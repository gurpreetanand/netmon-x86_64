#include <cstdio>
#include <string>
#include <cstdlib>
#include <sstream>
#include <getopt.h>
#include <sys/time.h>

#include "netmon.h"
#include "db.h"
#include "dvars.h"
#include "email.h"

static struct option const long_options[] =
{
  {"help",              no_argument, 0, 'h'},
  {"userid",            required_argument, 0, 'u'},
  {"message",           required_argument, 0, 'm'},
  {"subject",           required_argument, 0, 's'},
  {0, 0, 0, 0}
};

int usage ()
{
  printf ("Usage: alerttest [-h] -u user_id -s subject -m message\n");
  return 0;
}

int send_mail (int, const std::string &, const std::string &);

int main (int argc, char **argv)
{
  if (argc < 6)
    return usage ();

  int c, longind;
  std::string msg, subj;
  unsigned int id = 0;

  while ((c = getopt_long (argc, argv, "hs:u:m:", long_options, &longind)) != EOF)
  {
    switch (c)
    {
      case 'h':
        return usage ();
      case 'm':
        msg = std::string (optarg);
        break;
      case 's':
        subj = std::string (optarg);
        break;
      case 'u':
        id = atoi (optarg);
        break;
      default:
        printf ("Illegal argument, use --help for help\n");
        exit (-1);
    }
  }

  return send_mail (id, msg, subj);
}

bool getMailAddress (netmon::db *mydb, int id, std::string &addr)
{
  std::ostringstream sql;
  PGresult *res = 0;
  bool ret = false;

  sql << "select email from users where id = " << id;

  try
  {
    res = mydb->execQuery (sql.str().c_str());
    if (PQntuples (res) != 0)
    {
      addr = std::string (PQgetvalue (res, 0, 0));
      ret = true;
    }
  }
  catch (std::string &e)
  {
    printf ("Error: %s\n", e.c_str());
  }

  PQclear (res);
  return ret;
}

int send_mail (int id, const std::string &msg, const std::string &subj)
{
  char dbname [DBNAMELEN];
  char dbuser [DBUSERLEN];

  readConfig (dbname, dbuser);

  try
  {
    netmon::db *mydb = new netmon::db;
    mydb->connect (dbname, dbuser);
    netmon::dvars vars (*mydb, "emailmond");
    std::string smtp, acc;
    int port = 25, timeout;

    vars.get ("smtpserver", smtp);
    vars.get ("smtpaccount", acc);
    vars.get ("smtpport", port);
    vars.get ("smtp_timeout", timeout);
    
    timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 10; // this doesn't really matter

    std::string email;
    if (!getMailAddress (mydb, id, email))
    {
      delete mydb;
      return -1;
    }

    sendnotice ((char *) smtp.c_str(), port, (char *) acc.c_str(), (char *) email.c_str(), (char *) subj.c_str(), (char *) msg.c_str(), tv);
    delete mydb;
  }
  catch (std::string &e)
  {
    printf ("Fatal error: %s\n", e.c_str());
    return -1;
  }

  return 0;
}
