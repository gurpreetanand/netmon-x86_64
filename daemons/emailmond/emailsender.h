#ifndef __EMAIL_SENDER__
#define __EMAIL_SENDER__

#include <list>
#include <string>
#include <sys/time.h>

namespace netmon
{
  class db;

  class email_sender
  {
    public:
      email_sender (db *, const std::string &, const std::string &, unsigned short, int);

      void run ();
      void sendSetupNotification (int, int);

    protected:
      struct email_data
      {
        unsigned int m_id;
        std::string m_message;
        std::string m_subject;
        std::string m_email;
      };

      typedef std::list<email_data *> emails_t;

      void clear ();
      void getPendingEMails ();
      void sendAlerts ();
      void updateAlert (unsigned int, bool);
      void updateNotificationFlag (bool);
      bool getAdminEmail (std::string &);

      db *m_db;
      emails_t m_emails;
      std::string m_server;
      std::string m_user;
      unsigned short m_port;
      timeval tv;
  };
}

#endif // __EMAIL_SENDER__
