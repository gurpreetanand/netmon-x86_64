#ifndef __EMAIL_MOND_DBUS__
#define __EMAIL_MOND_DBUS__

#include <dbus-c++/dbus.h>

#include "emailmond-glue.h"

namespace netmon
{
  class email_sender;

  class dbus_emailmond_server : public DBus::ObjectAdaptor, public DBus::IntrospectableAdaptor
  {
    public:
      dbus_emailmond_server (DBus::Connection &, email_sender *);

      virtual void alert();

    protected:
      email_sender *m_eSender;
  };
}

#endif // __EMAIL_MOND_DBUS__
