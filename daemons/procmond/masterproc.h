#ifndef __MASTER_PROC__
#define __MASTER_PROC__

#include <sys/types.h>
#include <dumbnet.h>
#include <list>
#include <string>
#include <set>
#include <map>

#include "daemondata.h"
#include "../netmond/mutex.h"

namespace netmon
{
  class db;
  class serializer;
  class daemon_container;

  class master_proc
  {
    public:
      master_proc (db *, daemon_container *, const char *);
      ~master_proc ();

      int startDaemon (const char *, const char * = 0);
      int stopDaemon (const char *, const char * = 0);
      void stopAllDaemons ();
      int getDaemonStatus (const char *, const char * = 0);

      bool isDaemon (const char *);
      void updateProc (const char* daemon);

      void serializeStatus (serializer &);

    protected:
      std::map <std::string, pid_t> procMap;
      void updateProcs ();

      void init ();
      void getInterfaces ();
      void getDaemons ();
      void startDaemons ();

      static int ifCallback (const intf_entry *, void *);
      static int spawn (char **);

      struct iface
      {
        iface (const std::string name, bool up)
          : m_name (name), m_up (up)
        {}
        std::string m_name;
        bool m_up;
      };

      struct plugin
      {
        plugin (const char *name, const char *desc, const char *start, const char *devs, const char *run_devs)
          : m_name (name), m_desc (desc), m_devs (devs), m_rundevs (run_devs)
        {
          if (start == 0 || strlen (start) == 0)
            m_start = false;
          else
            m_start = (*start == 't' ? true : false);
        }
        std::string m_name;
        std::string m_desc;
        std::string m_devs;
        std::string m_rundevs;
        bool m_start;
      };

      void getPlugins (const std::string &, std::list<plugin> &);

      db *m_db;
      daemon_container *m_container;

      typedef std::list<iface> iface_list_t;

      iface_list_t m_ifaces;
      daemon_list_t m_daemons;

      std::string m_path;

    private:
      void addInterface (const std::string &, bool);
      void parse_devs (const std::string &, std::set<std::string> &);

      static char *getProcName (pid_t);
      static char *getProcArgs (pid_t);
      pid_t getDaemonPID (const char *, const char * = 0);
      mutex m_mutex;

  };
}

#endif // __MASTER_PROC__
