#ifndef __DAEMON_DATA__
#define __DAEMON_DATA__

#include <list>
#include <string>
#include <cstring>

namespace netmon
{
  struct daemon_data
  {
    daemon_data (const char *name, const char *desc, const char *start)
      : m_name (name), m_desc (desc)
    {
      if (start == 0 || strlen (start) == 0)
        m_start = false;
      else
        m_start = (*start == 't' ? true : false);
    }

    explicit daemon_data (const std::string &name, const std::string &desc, bool start)
      : m_name (name), m_desc (desc), m_start (start)
    {
    }
    std::string m_name;
    std::string m_desc;
    bool m_start;
  };

  struct daemon_data_order
  {
    bool operator() (const daemon_data &a, const daemon_data &b) const
    {
      return a.m_desc < b.m_desc;
    };
  };

  typedef std::list<daemon_data> daemon_list_t;
}

#endif // __DAEMON_DATA__
