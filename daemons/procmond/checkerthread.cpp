#include "checkerthread.h"
#include "db.h"
#include "log.h"
#include "netmon.h"
#include "procmond.h"
#include "masterproc.h"
#include "daemoncontainer.h"

#include <dirent.h>
#include <sys/vfs.h>
#include <math.h>
#include <fstream>
#include <string>
#include <cctype>
#include <limits>
#include <iostream>

#include <unistd.h>

#define DB_POLL_TIME 5
#define POLL_TIME 20
#define MEMORY_SHUTDOWN 0.8


namespace netmon
{
  checker_thread::checker_thread (master_proc *master, daemon_container *container, procmon_daemon *daemon)
    : m_masterProc (master), m_container (container), m_daemon (daemon)
  {
    init();
    m_container->getDaemonList (m_daemons);
  }

  checker_thread::~checker_thread ()
  {
    m_daemons.clear();
  }

  void checker_thread::run ()
  {
    bool restartNeeded = false;
    while (true) {
      while (!isDBConnectionAlive()) {
        netmon::log::instance()->add(netmon::_eLogFatal, "DB connection is dead! Trying to reconnect.\n");
        usleep(DB_POLL_TIME);
        restartNeeded = true;
      }

      if (restartNeeded) {
        if (!daemonsStopped) {  // if we killed the daemons for memory saving, leave them stopped when the DB
                    // is restored and until we have enough (<75%) free memory.
          restartDaemons();
        }
        restartNeeded = false;
      }

      usleep (POLL_TIME);
      checkMemUsage();
      checkDBPartition();
      checkFreezes();
      checkCrashes();
    }
  }

  void checker_thread::checkLimitsValid ()
  {
    if (!m_daemon->devLimitsValid())
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Device limits have been exceeded!");
      m_masterProc->stopAllDaemons();
      exit (1);
    }
  }


  bool checker_thread::isDBConnectionAlive ()
  {
    try
    {
      PGresult *res = m_db->execQuery ("SELECT 1");
      PQclear (res);
      return true;
    }
    catch (db_exception &e)
    {
      return false;
    }
  }

  void checker_thread::checkDBPartition ()
  {
    bool varIsFull = dbPartitionFull();
    if (varIsFull)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Var partition is 90%% full");
      m_masterProc->stopAllDaemons();
      exit (1);
    }
  }

  void checker_thread::checkCrashes (bool sendSnapshot /* = true */)
  {
    const int32_t twelveHours = 12 * 60 * 60;
    struct timeval tv;
    gettimeofday(&tv,0);
    for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i) {
      if (!m_container->shouldBeRunning ((*i).c_str()))
        continue;

      // make sure we don't stop them if we killed them due to memory
      if (m_masterProc->getDaemonStatus ((*i).c_str()) == 0 && !daemonsStopped) {

        m_masterProc->updateProc((*i).c_str());
        usleep(5);

        if (m_masterProc->getDaemonStatus((*i).c_str()) != 0)
          continue;

        if (m_masterProc->startDaemon ((*i).c_str()) == 0)
          continue;

        printf("Restarting daemon [%s], which should be running\n", (*i).c_str());
        netmon::log::instance()->add(netmon::_eLogFatal, "Restarting daemon [%s], which should be running", (*i).c_str());
        if (sendSnapshot && (tv.tv_sec - lastSnap.tv_sec >= twelveHours)) {
          //this has been disabled until the python script plays nicely with out system()
          //system ("python /apache/cli/snapshot.py --crash");
          netmon::log::instance()->add(netmon::_eLogFatal, "Sent snapshot because of service failure.");
          gettimeofday(&lastSnap, 0);
        }
      }
    }
  }

  void checker_thread::checkFreezes ()
  {
    size_t size;
    daemon_msg msg;

    while (true)
    {
      size = 0;
      try
      {
        if (!m_queue.recv (msg, size, sizeof (msg), 1, IPC_NOWAIT))
          return; // no messages in the queue, return
      } catch (int e) {
        return;
      }

      std::string daemon (msg.mdata);
      dmap_t::iterator it = m_daemonTimeStamp.find (daemon);
      if (it == m_daemonTimeStamp.end())
        m_daemonTimeStamp[daemon] = time(0); // first time we see this daemon
      else
        checkFreeze (it);
    }
  }

 /*
  * This method will check the memory+swap use of the system.  If the total memory+swap usage is
  * greater than 75% of the total available memory+swap, we will halt all the daemons.  Also,
  * we stop all checks of these daemons in checkCrashes() if the daemonsStopped flag is set to true
  * as this prevents that method from simply restarting these services blindly
  *
  */
 
  void checker_thread::checkMemUsage ()
  {

    bool overLimit = memOverLimit();

    if (overLimit && !daemonsStopped) { // we're using too much memory and the daemons are going, kill the daemons
      

      m_masterProc->stopAllDaemons();
      daemonsStopped = true;
      
    } else if (!overLimit && daemonsStopped) { // we killed the daemons but now we have room, start the daemons

      
      for (daemon_list_t::iterator i = m_daemons.begin(); i != m_daemons.end(); ++i) {
        if (!m_container->shouldBeRunning ((*i).c_str()))
          continue;
        if (m_masterProc->getDaemonStatus ((*i).c_str()) == 0)
        {
          m_masterProc->startDaemon ((*i).c_str());
        }
      }
      daemonsStopped = false;
    }
  
  }

  bool checker_thread::dbPartitionFull ()
  {
    struct statfs s;
    statfs ("/var", &s);
    float p = (float) s.f_bavail / (float) s.f_blocks;
    if (p * 100 < 10.0)
      return true;
    return false;
  }

  void checker_thread::restartDaemons ()
  {
    checkCrashes (false);
  }

  void checker_thread::init ()
  {
    connectToDB();
    createQueue();
    daemonsStopped = false;
    lastSnap.tv_sec = 0;
    lastSnap.tv_usec = 0;
  }

  void checker_thread::connectToDB ()
  {
    char dbname[DBNAMELEN];
    char dbuser[DBUSERLEN];

    readConfig (dbname, dbuser);
    m_db = new db;
    m_db->connect (dbname, dbuser);
  }

  void checker_thread::createQueue ()
  {
    m_queue.create (queue_id, 0600);
    msqid_ds qstat;
    m_queue.stat (qstat);
    qstat.msg_qbytes = 65536;
    m_queue.set (qstat);
  }

  void checker_thread::checkFreeze (dmap_t::iterator &it)
  {
    unsigned int now = time (0);

    if ((now - it->second) > m_timeOut && m_container->shouldBeRunning (it->first.c_str()))
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Restarting daemon [%s], which has timed out", it->first.c_str());
      m_masterProc->stopDaemon (it->first.c_str());
      m_masterProc->startDaemon (it->first.c_str());
    }
    it->second = now;
  }
  
  /*
   *
   * This method uses /proc/meminfo which is available on all Linux systems
   * to report the amount of memory that is free/not free 
   *
   */
   bool checker_thread::memOverLimit() {
    std::string token;
    uint32_t memTotal = 0, memFree = 0, buffers = 0, cache = 0, swapFree = 0;
    std::ifstream file("/proc/meminfo");
    while(file >> token) {
	if (token == "MemFree:" ) {
	  file >> memFree;
    } else if (token == "MemTotal:") {
      file >> memTotal;
	} else if (token == "Buffers:") {
	  file >> buffers;
	} else if (token == "Cached:") {
	  file >> cache;
	} else if (token == "SwapFree:") {
	  file >> swapFree;
	}
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    double percentage = (memTotal - (memFree + cache - buffers)) / (double)memTotal;

    #ifdef DEBUG
      std::cout.precision(10);
      std::cout << "Total memory usage: " << (memTotal - memFree - cache + buffers) << "Kb, Total memory: " << memTotal << "Kb, percentage " << std::fixed << percentage << std::endl;
    #endif

    if (percentage >= MEMORY_SHUTDOWN && !daemonsStopped) {

      netmon::log::instance()->add(netmon::_eLogFatal, "Total memory usage %dKb is over threshold %dKb, stopping all daemons",
				   memTotal - (memFree + cache - buffers), memTotal);

    } else if (percentage <= MEMORY_SHUTDOWN && daemonsStopped) {

      netmon::log::instance()->add(netmon::_eLogFatal, "Total memory usage %dKb is now under threshold %dKb, restarting all daemons",
				   memTotal - (memFree + cache - buffers), memTotal);

    }

    return percentage >= MEMORY_SHUTDOWN;
    
    }
  
}
