#ifndef __PROC_MON_DAEMON__
#define __PROC_MON_DAEMON__

#include "daemon.h"

#include <pcre.h>
#include <string>

namespace netmon
{
  class master_proc;
  class daemon_container;

  class procmon_daemon : public daemon
  {
    public:
      procmon_daemon (int, char **);
      ~procmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "procmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/procmond";
      }

      virtual void usage ();
      virtual void onOption (char);
      virtual const char *getShortOptions ();

      void createSocket ();
      bool createLockFile ();
      void handleClient (int);
      void handleSigInt ();

      bool runCmd (char *);
      bool parseCmd (char *, std::string &, std::string &, std::string &);
      bool execCmd (const std::string &, const std::string &, const std::string &);

      master_proc *m_master;
      daemon_container *m_container;
      int m_fd;
      int m_lockfd;
      std::string m_msg;
      std::string m_opts;
      std::string m_path;
      std::string m_myName;
      pcre *m_re;
  };
}

#endif // __PROC_MON_DAEMON__
