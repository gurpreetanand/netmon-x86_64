#include "serializer.h"

#include <cstdio>

namespace netmon
{
  void serializer::serializeIFace (const std::string &iface)
  {
    printf ("iface: %s\n", iface.c_str());
  }

  void serializer::serializeProc (const std::string &proc, const std::string &desc, bool up, bool auto_start, bool /* hasPlugins = false */)
  {
    printf ("daemon: %s (%s): %s auto: %s\n", proc.c_str(), desc.c_str(), up ? "up" : "down", auto_start ? "yes" : "no");
  }

  void serializer::serializePlugin (const std::string &plugin, const std::string &desc, const ifaces_simple_t &devs, const std::set<std::string> & /* */, const std::set<std::string> & /* */)
  {
    printf ("plugin: %s\n", plugin.c_str());
  }

  void xml_serializer::serializeIFace (const std::string &iface)
  {
    m_buffer << "<interface>" << iface << "</interface>" << std::endl;
  }

  void xml_serializer::serializeProc (const std::string &proc, const std::string &desc, bool up, bool auto_start, bool hasPlugins /* = false */)
  {
    m_buffer << "<service name=\"" << proc << "\" description=\"" << desc
             << "\" status=\"";
    if (up)
      m_buffer << "up\"";
    else
      m_buffer << "down\"";

    m_buffer << " start_auto=\"";
    if (auto_start)
      m_buffer << "yes";
    else
      m_buffer << "no";
    m_buffer << "\"";

    if (!hasPlugins)
      m_buffer << "/>";
    else
      m_buffer << ">";
    m_buffer << std::endl;
  }

  void xml_serializer::serializePlugin (const std::string &plugin, const std::string &desc, const ifaces_simple_t &devs, const std::set<std::string> &setr, const std::set<std::string> &sets)
  {
    if (devs.empty())
      return;
    m_buffer << "<plugin name=\"" << plugin << "\" description=\"" << desc
             << "\">" << std::endl << "<interfaces>" << std::endl;

    for (ifaces_simple_t::const_iterator i = devs.begin(); i != devs.end(); ++i)
    {
      m_buffer << "<interface iface=\"" << (*i).first << "\" status=\"";
      if ((*i).second && setr.find((*i).first) != setr.end())
        m_buffer << "up";
      else
        m_buffer << "down";
      m_buffer << "\" start_auto=\"";

      if (sets.find((*i).first) != sets.end())
        m_buffer << "yes";
      else
        m_buffer << "no";

      m_buffer << "\"/>" << std::endl;
    }
    m_buffer << "</interfaces>" << std::endl << "</plugin>" << std::endl;
  }
}
