#ifndef __MESSAGE_QUEUE__
#define __MESSAGE_QUEUE__

#include <list>

#include "mutex.h"
#include "condition.h"
#include "lock.h"

namespace netmon
{
  const size_t MAX_SIZE = 1024;

  template <typename T>
  class queue
  {
    public:
      queue (size_t max_items = MAX_SIZE)
        : m_size (0), m_maxsize (max_items), m_notfull (&m_mutex), m_notempty (&m_mutex)
      {
      }

      void push (T msg)
      {
        mutex_lock l(m_mutex);

        while (this->full_i ())
        {
          m_notfull.wait ();
        }

        this->push_i (msg);
        m_notempty.wake_all ();
      }
      
      void pushUrgent (T msg)
      {
        mutex_lock l(m_mutex);

        while (this->full_i ())
        {
          m_notfull.wait ();
        }

        this->pushUrgent_i (msg);
        m_notempty.wake_all ();
      }
      
      void pop (T &msg)
      {
        mutex_lock l(m_mutex);

        while (this->empty_i ())
        {
          m_notempty.wait ();
        }

        this->pop_i (msg);
        m_notfull.wake_all ();
      }

      bool empty () const
      {
        mutex_lock l(m_mutex);

        return empty_i ();
      }

      bool full () const
      {
        mutex_lock l(m_mutex);

        return full_i ();
      }

      size_t size() const
      {
        return m_size;
      }

    private:
      void push_i (T msg)
      {
        m_queue.push_back (msg);
        ++m_size;
      }

      void pushUrgent_i (T msg)
      {
        m_queue.push_front (msg);
        ++m_size;
      }

      void pop_i (T &msg)
      {
        msg = m_queue.front ();
        --m_size;
        m_queue.erase (m_queue.begin ());
      }

      bool empty_i () const
      {
        return m_size == 0;
      }

      bool full_i () const
      {
        return m_size == m_maxsize;
      }

      size_t m_size;
      size_t m_maxsize;

      mutable mutex m_mutex;

      condition m_notfull;
      condition m_notempty;

      std::list<T> m_queue;
  };
}

#endif //__MESSAGE_QUEUE__
