#include <cstdio>
#include <unistd.h>
#include "thread.h"

using namespace netmon;

class mythread : public thread
{
  public:
    mythread ()
      : thread (), m_run(true)
    {}

    void stop ()
    {
      m_run = false;
    }

  protected:
    virtual void run ()
    {
      while (m_run)
      {
        printf ("Hello world!\n");
        sleep (1);
      }
    }

    mutable bool m_run;
};

int main()
{
  mythread t;
  t.start();
  sleep (2);
  t.stop();
  t.wait();
  return 0;
}
