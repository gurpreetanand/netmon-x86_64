#ifndef __THREAD__
#define __THREAD__

#include <pthread.h>

extern "C"
{
  namespace netmon
  {
    static void *starter (void *);
  }
}

namespace netmon
{
  class thread
  {
    public:
      thread ()
        : m_isrunning (false), m_isfinished (false)
      {
      }

      virtual ~thread () {}

      void start ()
      {
        pthread_attr_t attr;

        pthread_attr_init (&attr);
        pthread_attr_setinheritsched (&attr, PTHREAD_INHERIT_SCHED);
        pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);
        pthread_create (&m_thread, NULL, starter, (void *) this);
        pthread_attr_destroy (&attr);
        m_isrunning = true;
      }

      void kill () const
      {
        pthread_exit (NULL);
      }

      void wait () const
      {
        pthread_join (m_thread, NULL);
      }

      bool isrunning () const
      {
        return m_isrunning;
      }

      bool isfinished () const
      {
        return m_isfinished;
      }

    protected:
      virtual void run () = 0;
      friend void *starter (void *);

      static void start (thread *t)
      {
        t->run ();
        t->m_isfinished = true;
        t->m_isrunning = false;
      }

    private:
      thread (const thread &);
      thread &operator= (const thread &);

      pthread_t m_thread;
      bool m_isrunning, m_isfinished;
  };
}

extern "C"
{
  namespace netmon
  {
    static void *starter (void *t)
    {
      thread *thr = reinterpret_cast<thread *> (t);
      thread::start (thr);

      return 0;
    }
  }
}

#endif // __THREAD__
