#include "connmanager.h"

#include <dumbnet.h>
#include <stdio.h>

namespace netmon
{
  connection_manager::connection_manager (uint16_t iddleTime)
    : m_iddleTime (iddleTime)
  {}

  connection_manager::~connection_manager ()
  {
    for (conn_map_t::iterator i = m_connections.begin(); i != m_connections.end(); ++i)
      delete i->second;
    m_connections.clear();
  }

  connection_manager::status connection_manager::getStatus (uint32_t src, uint32_t dst, uint16_t sport, uint16_t dport, uint8_t flags)
  {
    prepareCurrentEntry (src, dst, sport, dport);
//printf ("flags: 0x%02x\n", flags);
    conn_map_t::iterator i = m_connections.find (m_current);
    if (i == m_connections.end())
    {
//printf ("Connection not found\n");
      if (!(flags & TH_SYN))
      {
//        printf ("No SYN flag set\n");
        return eNotTracking;
      }
      //printf ("New connection\n");
      connection_data *d = new connection_data;
      d->m_ts = time (0);
      d->m_data = 0;
      d->m_pluginID = -1;
      d->m_state = connection_data::eSyn;
      m_connections[m_current] = d;
      return eNew; // this is a new connection
    }
    // we've seen this connection before
    if (flags & (TH_FIN | TH_RST)) // connection closing
    {
    //printf ("Connection closed\n");
      delete i->second;
      m_connections.erase (i);
      return eNotTracking;
    }

    if (i->second->m_pluginID != -1) // we track this conversation
    {
      return eTracking;
    }

    switch (i->second->m_state)
    {
      case connection_data::eSyn:
        if (flags & (TH_PUSH | TH_ACK))
        {
        //printf ("First packet seen\n");
          i->second->m_state = connection_data::eFirstPacket;
          return eReady;
        }
        break;
      case connection_data::eFirstPacket:
        return eReady;
      case connection_data::eMoreData:
        break;
    }
    return eReady;
  }

  void connection_manager::setPluginID (uint32_t src, uint32_t dst, uint16_t sport, uint16_t dport, uint8_t plugin)
  {
    prepareCurrentEntry (src, dst, sport, dport);

    conn_map_t::iterator i = m_connections.find (m_current);
    if (i == m_connections.end())
      return;
    i->second->m_pluginID = plugin;
  }

  int connection_manager::getPluginID (uint32_t src, uint32_t dst, uint16_t sport, uint16_t dport)
  {
    prepareCurrentEntry (src, dst, sport, dport);

    conn_map_t::iterator i = m_connections.find (m_current);
    if (i == m_connections.end())
      return -2;
    return i->second->m_pluginID;
  }

  void connection_manager::removeIddleConnections ()
  {
    time_t now = time (0);

    for (conn_map_t::iterator i = m_connections.begin(); i != m_connections.end(); )
    {
      if ((now - i->second->m_ts) > m_iddleTime)
      {
        delete i->second;
        m_connections.erase (i++);
        continue;
      }
      else
        ++i;
    }
  }

  void connection_manager::prepareCurrentEntry (uint32_t src, uint32_t dst, uint16_t sport, uint16_t dport)
  {
    if (src < dst)
    {
      m_current.m_src = src;
      m_current.m_dst = dst;
    }
    else
    {
      m_current.m_src = dst;
      m_current.m_dst = src;
    }
    if (sport < dport)
    {
      m_current.m_srcPort = sport;
      m_current.m_dstPort = dport;
    }
    else
    {
      m_current.m_srcPort = dport;
      m_current.m_dstPort = sport;
    }
  }
}
