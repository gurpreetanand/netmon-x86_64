#ifndef __DUMMY_PLUGIN__
#define __DUMMY_PLUGIN__

#include "plugin.h"
#include "timer.h"

#include <stdint.h>
#include <cstdio>
#include <map>

namespace netmon
{
  struct stat_data
  {
    stat_data ()
      : m_packets (0), m_octets (0)
    {}
    uint64_t m_packets;
    uint64_t m_octets;
  };

  class stats_plugin : public plugin
  {
    public:
      stats_plugin (unsigned int id)
        : plugin (id)
      {
        for (unsigned int i = 0; i < 4; ++i)
          m_map[i] = stat_data();
      }

      ~stats_plugin ()
      {
        printf ("plugin dtor() called\n");
      }

      virtual void init (db *)
      {
        printf ("init() called\n");
      }

      virtual unsigned int getWantedDevice ()
      {
        return (unsigned int) (_eETH0 | _eETH1 | _eETH2 | _eETH3);
      }

      virtual unsigned int getWantedProtocol ()
      {
        return _eETH;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new timer (10, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);

      virtual void onTimer (const timer *t, db *);

      virtual const char *getName ()
      {
        return "mod_stats";
      }

    protected:
      typedef std::map<uint16_t, stat_data> stat_map;
      stat_map m_map;
  };
}

#endif // __DUMMY_PLUGIN__
