#include "dummyplugin.h"

extern "C" netmon::plugin *create (int id)
{
  return new netmon::dummy_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
