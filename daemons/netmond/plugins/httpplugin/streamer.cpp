#include "streamer.h"
#include "netmon.h"
#include "log.h"

#include <cstring>
#include <sys/shm.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace netmon
{
  static const unsigned int shmwebs = 0xf4000001;
  static const unsigned int shmwebc = 0xf4000002;

  shm_streamer::shm_streamer (int maxwebs /* = 10 */, int maxwebc /* = 10 */)
    : streamer (maxwebs, maxwebc)
  {
    initshm();
  }

  shm_streamer::~shm_streamer ()
  {
    delete m_shmwebs;
    delete m_shmwebc;
  }

  void shm_streamer::initshm ()
  {
    int size;
    size = m_maxWebS * 322 + 4096; // at least 322 bytes for web server data
    m_shmwebs = new shm (shmwebs, size, IPC_CREAT | 0666, true);
    size = m_maxWebC * 64 + 4096; // at least 64 bytes for ipitem
    m_shmwebc = new shm (shmwebc, size, IPC_CREAT | 0666, true);
  }

  void shm_streamer::serialize (const storage::top_servers_t &t)
  {
    in_addr ip;

    try
    {
      shm *s;
      s = m_shmwebs;
      int size = s->size();
      char *buf = s->buffer();
      s->fill (0xff);
      if (m_octets == 0)
        return;
      int i = 0;
      for (storage::top_servers_t::const_iterator it = t.begin (); it != t.end (); ++it)
      {
        ip.s_addr = (*it).m_data.m_ip;
        i += snprintf (buf + i, size - i, "%s\n%s\n%" u64prefix "u\n", inet_ntoa (ip), (*it).m_host.c_str(), (*it).m_data.m_octets);
        i += speedtostr (buf + i, size - i, (*it).m_data.m_octets, m_interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_data.m_octets * 100 / m_octets)) + 1;
      }
/*
FILE *f = fopen ("/tmp/out.dump", "w");
fwrite ((void *) buf, size, 1, f);
fclose (f);
*/
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment %s", strerror (e));
    }
  }

  void shm_streamer::serialize (const storage::top_clients_t &t)
  {
    in_addr ip;

    try
    {
      shm *s;
      s = m_shmwebc;
      int size = s->size();
      char *buf = s->buffer();
      s->fill (0xff);
      if (m_octets == 0)
        return;
      int i = 0;
      for (storage::top_clients_t::const_iterator it = t.begin (); it != t.end (); ++it)
      {
        ip.s_addr = (*it).m_ip;
        i += snprintf (buf + i, size - i, "%s\n%" u64prefix "u\n", inet_ntoa (ip), (*it).m_octets);
        i += speedtostr (buf + i, size - i, (*it).m_octets, m_interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_octets * 100 / m_octets)) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment %s", strerror (e));
    }
  }
}
