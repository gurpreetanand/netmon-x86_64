#ifndef __LINE_PARSER__
#define __LINE_PARSER__

#include <string>
#include <list>
#include <utility>
#include <map>
#include <limits.h>
#include <stdint.h>

#include "log.h"

//#define DEBUG

namespace netmon
{

  struct parser_state
  {
    typedef std::pair<int, bool> intbool;
    ~parser_state() {
    }

    void newChunk (const uint8_t *data, uint16_t size)
    {

      if (size == 0)
        return;
      m_len = 0;
      m_pos = 0;
      std::string temp = std::string((char*)data, 0, size);
      m_data = (uint8_t*)temp.c_str();
      m_sent = false;

      std::list<int> breakLocation;
      std::map<int, bool> breakComp;

      #ifdef DEBUG
      std::string supdebug;
      for (int i=0; i < (int)temp.size(); i++)
      {
        if (temp[i] == '\r')
          supdebug += "\\r";
        else if (temp[i] == '\n')
          supdebug += "\\n";
        else if (temp[i] == '\0')
          supdebug += "\\0";
        else 
          supdebug += temp[i];
        }

      netmon::log::instance()->add(netmon::_eLogWarning, "RAW LINE: %s\n", supdebug.c_str());
      #endif


      size_t location = 0;

      while ((location = temp.find("\r", location)) != std::string::npos) {
        if (temp[location+1] == '\n')
        { 
          location++;
          continue;
        }
        if ((int)location <= size) {
          #ifdef DEBUG
          netmon::log::instance()->add(netmon::_eLogWarning, "Found \\r Location %d Next: %c", (int) location, temp[location+1]);
          #endif
          breakLocation.push_back(location);
          breakComp[location] = false;
          location++;
        }
      }

      location = 0;

      while ((location = temp.find("\n", location)) != std::string::npos) {
        if (location > 0 && temp[location-1] == '\r')
        { 
          location++;
          continue;
        }
        if ((int)location <= size) {
          #ifdef DEBUG
          netmon::log::instance()->add(netmon::_eLogWarning, "Found \\n Location %d Next: %c", (int) location, temp[location-1]);
          #endif
          breakLocation.push_back(location);
          breakComp[location] = false;
          location++;
        }
      }

      location = 0;

      while ((location = temp.find("\r\n", location)) != std::string::npos) {
        if ((int)location <= size) {
          #ifdef DEBUG
          netmon::log::instance()->add(netmon::_eLogWarning, "Found \\r\\n");
          #endif
          breakLocation.push_back(++location);
          breakComp[location] = true;
        }
      }

      size_t tSize = temp.size();
      if (temp[tSize-1] != '\r' || temp[tSize-1] != '\n')
        breakLocation.push_back(INT_MAX); //handle half-line packets

      breakLocation.sort();

      #ifdef DEBUG
      std::string bpoints="";
      for (std::list<int>::iterator it = breakLocation.begin(); it != breakLocation.end(); ++it) {
        netmon::log::instance()->add(netmon::_eLogWarning, "Breakpoint: %d B Size %d Line Size: %d", *it, breakLocation.size(), temp.size());
      }
      #endif


      int start = 0, end;
      while (!breakLocation.empty()) {
         end = breakLocation.front()-1;
         breakLocation.pop_front();

         std::string final;

         if (breakComp[end+1]) {
           final = std::string(temp, start, end-start);
           start = end+2;
         } else {
           final = std::string(temp, start, end-start+1);
           start = end+1;
         }

         if (end != INT_MAX-1)
	 {
           final += '\r';
           final += '\n'; // add a CRLF
         }

         std::string last = "";

         if (m_lines.empty()) {
           #ifdef DEBUG
           netmon::log::instance()->add(netmon::_eLogWarning, "New line, pushing new string");
           #endif
           m_lines.push_back(final);
         } else {
           last = m_lines.back();
            if (final.size() != 2 /* \r\n only */ && last.rfind("\r\n") == std::string::npos) {
             #ifdef DEBUG
             netmon::log::instance()->add(netmon::_eLogWarning, "Last line has no \\r\\n, [%s] to string RFIND VALUE [%d] SIZE [%d]", last.c_str(), last.rfind("\r\n"), last.size());
             #endif
             m_lines.back() += final;
           } else {
           #ifdef DEBUG
             netmon::log::instance()->add(netmon::_eLogWarning, "Adding New line because last line ended in \\r\\n");
             #endif
             m_lines.push_back(final);
           }
         }

        #ifdef DEBUG
        std::string supdebug;
        for (int i=0; i < (int)final.size(); i++)
        {
          if (final[i] == '\r')
            supdebug += "\\r";
          else if (final[i] == '\n')
            supdebug += "\\n";
          else if (final[i] == '\0')
            supdebug += "\\0";
          else
            supdebug += final[i];
          }

        netmon::log::instance()->add(netmon::_eLogWarning, "Pushing back String: %s", supdebug.c_str());
        #endif
        m_len += end-start+2;
      }


    }

    const uint8_t *m_data;
    uint16_t m_size;
    uint16_t m_pos;
    uint16_t m_len;
    bool m_sent;
    bool m_fsent;
    std::string m_line;
    std::string m_lastline;
    std::list<std::string> m_lines;
  };

  class line_parser
  {
    public:
      line_parser ();
      ~line_parser ();

      std::string getLine (parser_state *);
      std::string getPrev (parser_state *);
      std::string getFullLine (parser_state *);

  };
}

#endif // __LINE_PARSER__
