#ifndef __TEST__
#define __TEST__

#include "smtpplugin.h"

namespace netmon
{
  class test : public smtp_plugin
  {
    public:
      test (const char *);
      ~test ();
      void run ();

    protected:
      void newData (const uint8_t *, uint16_t);
      void dump ();
      void dumpToDB ();

      smtp_data *m_data;
      int m_fd;
  };
}

#endif // __TEST__
