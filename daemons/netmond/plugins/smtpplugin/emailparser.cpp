#include "emailparser.h"
#include "strings.h"

#include <strings.h>
#include <cstring>

//#define DEBUG

namespace netmon
{
  static const char sFrom[] = "From:";
  static const char sTo[]   = "To:";
  static const char sCC[]   = "CC:";
  static const char sID[]   = "Message-ID:";
  static const char sSubject[] = "Subject:";
  static const char sCntType[] = "Content-Type:";
  static const char sMixed[]  = "multipart/mixed";

  void email_parser::handleHeaders (parser_state *ps, email_data *smtp)
  {
    std::string line;

    line = m_parser.getLine(ps);
    while (line.size() > 0)
    {
      std::string temp;

      #ifdef DEBUG
      netmon::log::instance()->add(netmon::_eLogWarning, "Checking Header: [%s]", line.c_str());
      #endif

      if (line.compare("\r\n") == 0)
      {
        smtp->m_eState = email_data::sBody;
        return;
      }
      std::string header = std::string(line);
      if (header[0] == ' ')
      {
        smtp->m_headers.push_back (header);
        trim (header);
        handleMultiLineHeader (smtp, header);
      }
      else
      {
        if (header.find(":") != std::string::npos) {
          smtp->m_headers.push_back (header);
          handleHeader (smtp, header);
	}
      }
      smtp->m_size += line.size() - 2;
      line = m_parser.getLine(ps);
    }
  }

  void email_parser::handleBody (parser_state *ps, email_data *smtp, bool imap)
  {
    std::string getLine (parser_state *);
    static const char *name = "name=";
    static const char *fname = "filename=";
    static const char *ctd = "Content-Disposition: attachment;";
    static const uint8_t lenct = strlen (sCntType);
    static const uint8_t lenctd = strlen (ctd);
    static const uint8_t lenname = strlen (name);
    static const uint8_t lenfname = strlen (fname);
    std::string line, last;
    bool attachSeen = false;

    line = m_parser.getLine(ps);
    while (!line.empty()) 
    {
      if (imap)
        if (line.find("OK FETCH completed") != std::string::npos) //IMAP
	{
	  smtp->m_eState = email_data::sDone;
	  return;
	}
      if (line.find(".\r\n", 0, 3) == 0) //POP3/SMTP
      {
        smtp->m_eState = email_data::sDone;
        return;
      }

      smtp->m_size += line.size() - 2;

      if (!smtp->m_hasAttach)
      {
        line = m_parser.getLine(ps);
        continue;
      }

      if (line.size() == 2 /* \r\n */ && smtp->m_eState == email_data::sAttachHeader && smtp->m_currAttach.m_isRealAttach)
      {
        smtp->m_eState = email_data::sAttach;
	line = m_parser.getLine(ps);
        continue;
      }
      if (line.size() == 2 /* \r\n */ && smtp->m_eState == email_data::sAttachHeader && !smtp->m_currAttach.m_isRealAttach)
      {
        smtp->m_eState = email_data::sBody;
	line = m_parser.getLine(ps);
        continue;
      }
      if (line.size() != 0 && smtp->m_eState == email_data::sAttach)
      {
        smtp->m_currAttach.m_size += line.size() - 2;
      }

      if ((line.size() == smtp->m_boundary.size() + 2 ||  /* #\r\n */ 
         line.size() == smtp->m_boundary.size() + 4 || /* --#\r\n */ 
	 line.size() == smtp->m_boundary.size() + 6) /* --#--\r\n */ &&
         (line[0] == '-') && (line[1] == '-') &&
	  line.find(smtp->m_boundary) != std::string::npos 
         )
      {
        if (smtp->m_eState == email_data::sAttach)
        {
          smtp->m_eState = email_data::sAttachHeader;
          if (smtp->m_currAttach.m_isRealAttach)
          {
            smtp->m_currAttach.m_size -= line.size();
            smtp->m_attachments.push_back (smtp->m_currAttach);
            smtp->m_currAttach.m_size = 0;
            smtp->m_currAttach.m_isRealAttach = false;
          }
	  line = m_parser.getLine(ps);
          continue;
        }
        if (smtp->m_eState == email_data::sBody)
        {
          smtp->m_eState = email_data::sAttachHeader;
        }
	line = m_parser.getLine(ps);
        continue;
      }

      if (line.size() >= lenct && strncasecmp (line.c_str(), sCntType, lenct) == 0 && smtp->m_eState == email_data::sAttachHeader)
      {
        std::string sline = std::string (line.c_str(), line.size());
        smtp->m_currAttach.m_contentType = getHeaderValue (sline);
        int s = smtp->m_currAttach.m_contentType.size();
        if (s > 1 && smtp->m_currAttach.m_contentType[s - 1] == ';')
          smtp->m_currAttach.m_contentType.erase (s - 1, s);
        attachSeen = true;
      }

      if (line.size() >= lenctd && strncasecmp (line.c_str(), ctd, lenctd) == 0 && smtp->m_eState == email_data::sAttachHeader)
      {
        smtp->m_currAttach.m_isRealAttach = true;
        smtp->m_currAttach.m_size = 0;
        attachSeen = true;
      }
      if (attachSeen)
      {
        size_t aname, bname;
	if ((aname = (line.find(name))) != std::string::npos || (bname = (line.find(fname))) != std::string::npos)
        {
          attachSeen = false;

	  if (aname != std::string::npos)
            smtp->m_currAttach.m_name = std::string (line, aname+lenname, line.size()-aname-lenname);
	  else
	    smtp->m_currAttach.m_name = std::string (line, aname+lenfname, line.size()-aname-lenfname);
          trim (smtp->m_currAttach.m_name);
          trimQuotes (smtp->m_currAttach.m_name);
        }
      }
      line = m_parser.getLine(ps);
    }
  }

  void email_parser::handleHeader (email_data *smtp, const std::string &header)
  {
    static const uint8_t lensub = strlen (sSubject);
    static const uint8_t lenfrm = strlen (sFrom);
    static const uint8_t lento = strlen (sTo);
    static const uint8_t lencc = strlen (sCC);
    static const uint8_t lenct = strlen (sCntType);
    static const uint8_t lencm = strlen (sMixed);
    static const uint8_t lenid = strlen (sID);

    std::string tmp;
    std::list<std::string> list;

    if (header.size() >= lensub && strncasecmp (header.c_str(), sSubject, lensub) == 0)
    {
      smtp->m_subject = getHeaderValue (header);
      #ifdef DEBUG
      netmon::log::instance()->add(netmon::_eLogWarning, "Got subject: %s", smtp->m_subject.c_str());
      #endif
      smtp->m_header = email_data::hSubject;
      return;
    }

    if (header.size() >= lenfrm && strncasecmp (header.c_str(), sFrom, lenfrm) == 0)
    {

      if (header.find("@") != std::string::npos)
      {
        parseAddress (getHeaderValue (header), smtp->m_from);
        #ifdef DEBUG
        netmon::log::instance()->add(netmon::_eLogWarning, "Parsing FROM on HEADER");
        netmon::log::instance()->add(netmon::_eLogWarning, "Got From %s@%s %s", smtp->m_from.m_user.c_str(), smtp->m_from.m_domain.c_str(), smtp->m_from.m_label.c_str());
	#endif
      }
      smtp->m_header = email_data::hFrom;
      return;
    }

    if (header.size() >= lenid && strncasecmp (header.c_str(), sID, lenid) == 0)
    {
      smtp->m_id = getHeaderValue (header);
      int s = smtp->m_id.size();
      if (s > 1 && smtp->m_id[0] == '<' && smtp->m_id[s - 1] == '>')
      {
        smtp->m_id.erase (s - 1, s);
        smtp->m_id.erase (0, 1);
      }
      smtp->m_header = email_data::hID;
      return;
    }

    if (header.size() >= lento && strncasecmp (header.c_str(), sTo, lento) == 0)
    {
      tmp = getHeaderValue (header);
      if (tmp.find("@") != std::string::npos) {
        tokenize (tmp, list);
        trim (list);
        parseAddressList (list, smtp->m_to);
      }
      smtp->m_header = email_data::hTo;
      return;
    }

    if (header.size() >= lencc && strncasecmp (header.c_str(), sCC, lencc) == 0)
    {
      tmp = getHeaderValue (header);
      if (tmp.find("@") != std::string::npos) {
        tokenize (tmp, list);
        trim (list);
        parseAddressList (list, smtp->m_cc);
      }
      smtp->m_header = email_data::hCC;
      return;
    }

    if (header.size() >= lenct && strncasecmp (header.c_str(), sCntType, lenct) == 0)
    {
      smtp->m_contentType = getHeaderValue (header);
      int s = smtp->m_contentType.size();
      if (s > 1 && smtp->m_contentType[s - 1] == ';')
        smtp->m_contentType.erase (s - 1, s);
      if (smtp->m_contentType.size() >= lencm && strncasecmp (smtp->m_contentType.c_str(), sMixed, lencm) == 0)
      {
        smtp->m_hasAttach = true;
      }
      smtp->m_header = email_data::hCT;
      return;
    }

    smtp->m_header = email_data::hUnknown;
  }

  void email_parser::handleMultiLineHeader (email_data *smtp, const std::string &line)
  {
    static const char *boundary = "boundary=";
    static const uint8_t len = strlen (boundary);

    if (smtp->m_header == email_data::hUnknown)
      return;

    if (smtp->m_header == email_data::hCT) // content-type multi line
    {
      if (line.size() > len && strncasecmp (line.c_str(), boundary, len) == 0)
      {
        std::string tmp = std::string (line, len, line.size() - len);
        if (tmp[0] == '"')
          tmp.erase (0, 1);
        if (tmp[tmp.size() - 1] == '"')
          tmp.erase (tmp.size() - 1, 1);
        smtp->m_boundary = tmp;
      }
      return;
    }

    if (smtp->m_header == email_data::hFrom) // Multi-Line From
    {
      if (line.find("@") != std::string::npos)
      {
	parseAddress (line.c_str(), smtp->m_from);
	#ifdef DEBUG
        netmon::log::instance()->add(netmon::_eLogWarning, "Got ML From %s@%s %s", smtp->m_from.m_user.c_str(), smtp->m_from.m_domain.c_str(), smtp->m_from.m_label.c_str());
	#endif
      }
      return;
    }

    if (smtp->m_header == email_data::hTo || // To: or CC: multi line
        smtp->m_header == email_data::hCC)
    {
      if (line.find("@") != std::string::npos) {
        std::list<std::string> list;
        tokenize (line, list);
        trim (list);
        if (smtp->m_header == email_data::hTo)
          parseAddressList (list, smtp->m_to);
        else
          parseAddressList (list, smtp->m_cc);
      }
      return;
    }
  }

  std::string email_parser::getHeaderValue (const std::string &header)
  {
    size_t pos = header.find (':');
    if (pos == std::string::npos || pos == header.size())
      return "";
    std::string tmp = std::string (header, pos + 1, header.size() - pos - 1);
    trim (tmp);
    return tmp;
  }

  void email_parser::parseAddress (const std::string &str, email_address &adr)
  {
    size_t pos = str.rfind ('@');
    if (pos == std::string::npos || pos == 0 || pos == (str.size() - 1))
      return;

    std::string tmp;
    size_t i = pos + 1;

    while (i < str.size() && str[i] != '>' && str[i] != ' ' && str[i] != '\r')
    {
      tmp.push_back (str[i]);
      ++i;
    }

    adr.m_domain = tmp;
    trim (adr.m_domain);
    tmp.clear();
    i = pos - 1;

    while (i > 0 && str[i] != '<')
    {
      tmp.push_back (str[i]);
      --i;
    }
    if (i == 0 && str[i] != '<')
      tmp.push_back (str[i]);

    adr.m_user = std::string (tmp.rbegin(), tmp.rend());
    trim (adr.m_user);
    tmp.clear();

    if (i > 0)
      --i;
    else
      return;

    while (i > 0)
    {
      tmp.push_back (str[i]);
      --i;
    }

    tmp.push_back (str[i]);
    adr.m_label = std::string (tmp.rbegin(), tmp.rend());
    trim (adr.m_label);
    trimQuotes (adr.m_label);
  }

  void email_parser::parseAddressList (const std::list<std::string> &slist, std::list<email_address> &alist)
  {
    for (std::list<std::string>::const_iterator i = slist.begin(); i != slist.end(); ++i)
    {
      email_address tmp;
      parseAddress (*i, tmp);
      alist.push_back (tmp);
    }
  }
}
