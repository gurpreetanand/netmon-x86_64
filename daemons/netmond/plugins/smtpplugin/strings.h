#ifndef __STRINGS__
#define __STRINGS__

#include <list>
#include <string>

namespace netmon
{
  class db;

  void tokenize (const std::string &, std::list<std::string> &);
  void trim (std::string &);
  void trim (std::list<std::string> &);
  void trimQuotes (std::string &);
  std::string createPGArray (const std::list<std::string> &, db *);
  std::string createText (const std::list<std::string> &);
}

#endif // __STRINGS__
