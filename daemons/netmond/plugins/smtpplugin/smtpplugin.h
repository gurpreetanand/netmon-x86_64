#ifndef __SMTP_PLUGIN__
#define __SMTP_PLUGIN__

#include <list>
#include <set>
#include <string>
#include <stdint.h>

#include "inetaddr.h"
#include "plugin.h"
#include "timer.h"
#include "mutex.h"
#include "emailparser.h"
#include "emailplugin.h"
#include "lineparser.h"
#include "../common/iphashmap.h"

namespace netmon
{
  class db;

  class smtp_plugin : public plugin, public email_plugin
  {
    public:
      smtp_plugin (unsigned int id)
        : plugin (id)
      {
      }
      ~smtp_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual const bool wantsHeaders () const
      {
        return false;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new full_span_timer (m_commitInt, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_smtp";
      }

    protected:
      struct smtp_data : public email_data
      {
        enum { eNew, eMailFrom, eRcpt, eData, eDone };

        smtp_data (uint32_t srv, uint32_t cln)
          : email_data(), m_serverIP (srv), m_clientIP (cln), m_state (eNew)
        {}

        uint32_t m_serverIP;
        uint32_t m_clientIP;
        uint8_t m_state;
        parser_state m_parserState;
      };

      void parseData (smtp_data *, const uint8_t *, uint16_t, bool);
      void handleStartEnd (smtp_data *, const uint8_t *, uint16_t);
      void handlePrologue (smtp_data *, const uint8_t *, uint16_t);

      void prepareIPPortPair (const ip_port_entry &, ip_port_entry &);

      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;

      typedef hash_map<ip_port_entry, smtp_data *, ip_port_hash, ip_port_eq> conn_map_t;
      conn_map_t m_connections;
      std::list<smtp_data *> m_completedMessages;

      line_parser m_parser;
      email_parser *m_eParser;
      mutex m_mutex;
      static const int m_commitInt = 120;

    private:
      ip_port_entry m_IPPortPair;
  };
}

#endif // __SMTP_PLUGIN__
