#include "smtpplugin.h"
#include "strings.h"
#include "dvars.h"
#include "lock.h"
#include "log.h"

#include <dumbnet.h>
#include <strings.h>
#include <cstring>
#include <string>
#include <sstream>

namespace netmon
{
  static const char *sMailFrom = "MAIL FROM:";
  static const char *sRcptTo = "RCPT TO:";
  static const char *sData = "DATA";

  smtp_plugin::~smtp_plugin ()
  {
  }

  void smtp_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    int ll = 2;

    vars.get ("loglevel", ll);

    m_wantedProtocol = _eTCP;
    m_wantedDevs = getWantedDevices (mydb);

    m_eParser = new email_parser (m_parser);
  }

  void smtp_plugin::onTimer (const timer *, db *mydb)
  {
    std::list<smtp_data *> tmp;
    mutex_lock lock (m_mutex);

    tmp.splice (tmp.end(), m_completedMessages);
    uint32_t now = time (0);

    for (conn_map_t::iterator i = m_connections.begin(); i != m_connections.end(); )
    {
      // insert connections with time out
      if ((now - i->second->m_timestamp) > 600 && i->second->m_state != smtp_data::eDone)
      {
        tmp.push_back (i->second);
        m_connections.erase (i++);
        continue;
      }
      if (i->second->m_state == smtp_data::eDone)
      {
	// WHY?!
        //delete i->second;
        m_connections.erase (i++);
        continue;
      }
      ++i;
    }

    lock.unlock();

    std::list<smtp_data *>::iterator end = tmp.end();
    for (std::list<smtp_data *>::iterator i = tmp.begin(); i != end; ++i)
    {
      if ((*i)->m_size != 0)
      {
        if (!(*i)->m_from.m_user.empty() && !(*i)->m_from.m_domain.empty() > 0 && !(*i)->m_to.empty()) {
          mydb->execSQL("BEGIN");
          std::string empty("");
          commit ((*i)->m_serverIP, (*i)->m_clientIP, empty, *i, "smtp", mydb);
          if ((*i)->m_hasAttach)
            commitAttach (*i, mydb);
          mydb->execSQL("COMMIT");
	}
      }
      delete *i;
    }
  }

  void smtp_plugin::onData (const uint8_t *data, uint16_t size, uint8_t, ip_port_entry *e)
  {
    std::string line = std::string((char*) data, size);
    prepareIPPortPair (*e, m_IPPortPair);

    mutex_lock lock (m_mutex);

    conn_map_t::iterator i = m_connections.find (m_IPPortPair);
    if (i == m_connections.end())
    {
      m_connections[m_IPPortPair] = new smtp_data (e->m_src, e->m_dst);
      return; // nothing else to do since this is server's greeting
    }
    parseData (i->second, data, size, e->m_src == i->second->m_clientIP);
  }

  void smtp_plugin::parseData (smtp_data *smtp, const uint8_t *data, uint16_t size, bool fromClient)
  {
    smtp->m_parserState.newChunk (data, size);


    if (fromClient)
    {
      smtp->m_timestamp = time (0);
      switch (smtp->m_state)
      {
        case smtp_data::eNew:
        case smtp_data::eDone:
          handleStartEnd (smtp, data, size);
          return;
        case smtp_data::eMailFrom:
        case smtp_data::eRcpt:
          handlePrologue (smtp, data, size);
          return;
        case smtp_data::eData:
          if (smtp->m_eState == email_data::sHeaders) {
            m_eParser->handleHeaders (&(smtp->m_parserState), smtp);
          } else {
            m_eParser->handleBody (&(smtp->m_parserState), smtp);
            if (smtp->m_eState == email_data::sDone)
            {
              smtp->m_state = smtp_data::eDone;
              m_completedMessages.push_back (smtp);
            }
          }
          return;
//        case smtp_data::eBody:
//          smtp->m_size += size;
//          handleBody (smtp);
//          return;
        default:
          return;
      }
    }
  }

  void smtp_plugin::handleStartEnd (smtp_data *smtp, const uint8_t *data, uint16_t size)
  {
    static const char *sQuit = "QUIT";
    static uint8_t lenq = strlen (sQuit);
    static const uint8_t len = strlen (sMailFrom);
    parser_state *ps = &(smtp->m_parserState);
    std::string line;

    line = m_parser.getLine(ps);
    while (!line.empty()) {
      if (strncasecmp (line.c_str(), sQuit, lenq) == 0)
      {
        return; // client sent QUIT
      }
      if (strncasecmp (line.c_str(), sMailFrom, len) == 0)
      {
        if (smtp->m_state == smtp_data::eDone)
        {
          smtp_data *tmp = new smtp_data (smtp->m_serverIP, smtp->m_clientIP);
          m_connections[m_IPPortPair] = tmp;
          smtp = tmp;
        }
        smtp->m_state = smtp_data::eMailFrom;
      }
      line = m_parser.getLine(ps);
    }
  }

  void smtp_plugin::handlePrologue (smtp_data *smtp, const uint8_t *data, uint16_t size)
  {
    static const uint8_t len = strlen (sRcptTo);
    static const uint8_t lend = strlen (sData);
    parser_state *ps = &(smtp->m_parserState);
    std::string line;

    line = m_parser.getLine(ps);
    while (line.size() != 0)
    {
      if (smtp->m_state == smtp_data::eMailFrom && (line.size() <= len))
      {
        return;
      }
      if (smtp->m_state == smtp_data::eRcpt && (line.size() < lend))
      {
        return;
      }
      if (strncasecmp (line.c_str(), sRcptTo, len) == 0)
      {
        smtp->m_rcpts.push_back (std::string (line, len, line.size()-len));
        trim (smtp->m_rcpts.back());
        smtp->m_state = smtp_data::eRcpt;
        return;
      }
      if (strncasecmp (line.c_str(), sData, lend) == 0)
      {
        smtp->m_state = smtp_data::eData;
        return;
      }
      line = m_parser.getLine(ps);
    }
  }

  void smtp_plugin::prepareIPPortPair (const ip_port_entry &in, ip_port_entry &out)
  {
    if (in.m_src < in.m_dst)
    {
      out.m_src = in.m_src;
      out.m_dst = in.m_dst;
    }
    else
    {
      out.m_src = in.m_dst;
      out.m_dst = in.m_src;
    }
    if (in.m_srcPort < in.m_dstPort)
    {
      out.m_srcPort = in.m_srcPort;
      out.m_dstPort = in.m_dstPort;
    }
    else
    {
      out.m_srcPort = in.m_dstPort;
      out.m_dstPort = in.m_srcPort;
    }
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::smtp_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}


