#include "lineparser.h"

#include <cstring>

namespace netmon
{
  line_parser::line_parser ()
  {
  }

  line_parser::~line_parser ()
  {
  }

  std::string line_parser::getFullLine (parser_state *state)
  {
    if (! state->m_sent) {
      state->m_sent = true;
      char* data = (char*)state->m_data;
      data[state->m_size] = '\0';
      strcat(data, "\r\n");
      return std::string(data);
    } else {
      return std::string("");
    }
  }


  std::string line_parser::getLine (parser_state *state)
  {
    if (state->m_lines.empty())
      return "";
    std::string temp = state->m_lines.front();
    if (temp.rfind("\r\n") == std::string::npos)
    {
      return "";
    }

    if (!state->m_line.empty())
      state->m_lastline = std::string(state->m_line);

    state->m_line = std::string(state->m_lines.front());
    state->m_lines.pop_front();

    return std::string(state->m_line);
  }

    std::string line_parser::getPrev (parser_state *state)
    {
      return state->m_lastline;
    }


}

#ifdef __TEST__

#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
  using namespace netmon;
  if (argc != 2)
  {
    printf ("Usage: test <file_name>\n");
    return -1;
  }
  uint8_t buff[32];
  int fd = open (argv[1], O_RDONLY);

  if (fd == 0)
  {
    printf ("Cannot open '%s'\n", argv[1]);
    return -1;
  }

  line_parser p;
  parser_state s;
  ssize_t r = 0;

  while (true)
  {
    if ((r = read (fd, (void *) buff, sizeof (buff))) == 0)
      break;

    s.newChunk (buff, r);

    while (true)
    {
      const uint8_t *l = p.getLine (&s);
      if (l != 0)
      {
        printf ("line size %d: '", s.m_len);
        fwrite ((void *) l, s.m_len, 1, stdout);
        printf ("'\n");
      }
      else
        break;
    }
  }
  close (fd);
}

#endif // __TEST__
