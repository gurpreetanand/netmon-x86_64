#include "netmonstorage.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>

#include <sstream>

#include "exception.h"
#include "netmon.h"
#include "protocolalertmgr.h"
#include "lock.h"
#include "log.h"
#include "db.h"

namespace netmon
{
  static const unsigned int semkey  = 0xff00ceca;
  static const unsigned int shmconv = 0xf4000000;
  static const unsigned int shmstrm = 0xf4000003;
  static const unsigned int shmprot = 0xf4000004;

  netmonstorage::netmonstorage (int maxconv, int maxstrm, int maxproto)
    : storage (), m_sem (semkey, IPC_CREAT | S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH)
  {
    if (maxconv < 1 || maxstrm < 1 || maxproto < 1)
      throw (netmon_exception ("Illegal parameter ( < 1)"));
    m_maxConv = maxconv;
    m_maxStrm = maxstrm;
    m_maxProt = maxproto;

    mq_attr attr;
    attr.mq_maxmsg = 4; //16384;
    attr.mq_msgsize = sizeof (uint32_t);
    m_queue = mq_open ("/ipqueue", O_CREAT | O_NONBLOCK | O_WRONLY, 0777, &attr);
    if (m_queue == (mqd_t) -1)
    {
      perror ("mq_open");
      std::string message = "error in 123 " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      log::instance()->add(_eLogFatal, strerror (errno));
    }
  }

  netmonstorage::~netmonstorage ()
  {
    delete m_shmconv;
    delete m_shmstrm;
    delete m_shmProtocols;
  }

  void netmonstorage::init (db *mydb)
  {
    m_alertManager = new protocol_alert_manager (mydb);
    m_alertManager->init();
    initshm();
  }

  void netmonstorage::initshm ()
  {
    int size = m_maxConv * 80 + 4096; // we need 80 bytes for convitem serialization
    m_shmconv = new shm (shmconv, size, IPC_CREAT | 0666, true);
    size = m_maxStrm * 96 + 4096; // 96 bytes
    m_shmstrm = new shm (shmstrm, size, IPC_CREAT | 0666, true);
    size = m_maxProt * 64 + 4096; // 96 bytes
    m_shmProtocols = new shm (shmprot, size, IPC_CREAT | 0666, true);
  }

  void netmonstorage::store (int tstamp, db *mydb)
  {
    int now = ::time(0);
    cdmap::iterator i;

    m_alertManager->reload();
    m_ips.clear();
    printf("***BEGIN COPY: %d\n", now);
    mydb->execSQL ("BEGIN");
    mydb->beginCopy (
    "COPY NETFLOW (src_ip, src_port, dst_ip, dst_port, protocol, \
    octets, packets, start_time, end_time, timestamp) FROM STDIN");

    std::list<connitem_detail> items;
    mutex_lock lock (m_mutex);

    for (i = m_convDMap.begin(); i != m_convDMap.end(); )
    {
      if (i->second.m_done || ((now - i->second.m_startTime) > 600))
      {
        if (i->second.m_octets == 0)
        {
          m_convDMap.erase (i++);
          continue;
        }
        connitem_detail item;
        item.m_proto = i->second.m_proto;
        item.m_octets = i->second.m_octets;
        item.m_srcIP = i->first.m_src;
        item.m_dstIP = i->first.m_dst;
        item.m_srcport = i->first.m_srcPort;
        item.m_dstport = i->first.m_dstPort;
        item.m_packets = i->second.m_packets;
        item.m_startTime = i->second.m_startTime;
        item.m_endTime = i->second.m_endTime;
        item.m_flags = 0;

        items.push_back (item);

        m_convDMap.erase (i++);
      }
      else
        ++i;
    }

    lock.unlock();

    std::list<connitem_detail>::iterator end = items.end();
    for (std::list<connitem_detail>::iterator i = items.begin(); i != end; ++i)
      store (*i, tstamp, mydb);

    mydb->endCopy (0);
    storeProtocols (mydb);
    mydb->execSQL ("COMMIT");

    #ifdef DEBUG
      printf("***END COPY: %d\n", (int) ::time(0));
    #endif

    m_alertManager->dispatchAlerts();
    enqueueIPs();

    #ifdef DEBUG
      printf("***END ALERT: %d\n", ::time(0));
    #endif
  }

  void netmonstorage::store (const connitem_detail &item, int tstamp, db *mydb)
  {
    in_addr addr;
    static std::ostringstream sql;

    m_ips.insert (item.m_srcIP);
    m_alertManager->checkItem (item);

    sql.str("");

    addr.s_addr = item.m_srcIP;
    sql << inet_ntoa (addr) << "\t" << item.m_srcport << "\t";
    addr.s_addr = item.m_dstIP;
    sql << inet_ntoa (addr) << "\t" << item.m_dstport << "\t"
        << (int) item.m_proto << "\t"
        << item.m_octets << "\t"
        << item.m_packets << "\t"
        << item.m_startTime << "\t"
        << item.m_endTime << "\t"
        << tstamp << "\n";

    try
    {
      mydb->copyData (sql.str().c_str(), sql.str().size());
    }
    catch (db_exception &e)
    {
      printf ("Error: %s\n", e.what());
      netmon::log::instance()->add(netmon::_eLogError, e.what());
    }
  }

  void netmonstorage::storeProtocols (db *mydb)
  {
    static std::ostringstream sql;
    unsigned int now = ::time (0);
    std::string ports;
    std::string octets;

    toplist<portitem, comp<portitem> > l(100);
    mutex_lock lock (m_mutex);
    getTopProtocols (l);
    lock.unlock();

    createArrays (l, ports, octets);

    sql.str("");
    sql << "INSERT INTO PROTOCOL_BREAKDOWN (INTERFACE, DEVICE, "
        << "START_TIME, END_TIME, PORTS, OCTETS) VALUES (-1, -1, "
        << now - 60 << ", "
        << now << ", '"
        << ports << "', '"
        << octets << "')";

    try
    {
      mydb->execSQL (sql.str().c_str());
    }
    catch (db_exception &e)
    {
      std::string message = "error in " + std::string(__FILE__);
      netmon::log::instance()->add(netmon::_eLogFatal, message.c_str());
      netmon::log::instance()->add(netmon::_eLogFatal, e.what());
    }
  }

  void netmonstorage::createArrays (const toplist<portitem, comp<portitem> > &tlist, std::string &ports, std::string &octets)
  {
    std::ostringstream p;
    std::ostringstream o;
    bool first = true;

    p << "{";
    o << "{";

    for (toplist<portitem, comp<portitem> >::const_iterator i = tlist.begin(); i != tlist.end(); ++i)
    {
      if ((*i).m_octets == 0)
        continue;
      if (!first)
      {
        p << ", " << (*i).m_port;
        o << ", " << (*i).m_octets;
      }
      else
      {
        first = false;
        p << (*i).m_port;
        o << (*i).m_octets;
      }
    }
    ports = p.str();
    ports += "}";
    octets = o.str();
    octets += "}";
  }

  void netmonstorage::enqueueIPs ()
  {
    uint32_t ip;
    for (std::set<uint32_t>::iterator i = m_ips.begin(); i != m_ips.end(); ++i)
    {
      ip = *i;
      mq_send (m_queue, (const char *) &ip, sizeof (ip), 0);
    }
  }

  void netmonstorage::storeLive (int interval)
  {
    m_sem.wait();
    mutex_lock lock (m_mutex);
    storeConv (interval);
    storeStrm (interval);
    storeProt (interval);
    m_sem.post();
  }

  void netmonstorage::storeConv (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      int size = m_shmconv->size();
      char *buf = m_shmconv->buffer();
      m_shmconv->fill (0xff);
      if (total == 0)
        return;
      toplist<convitem, comp<convitem> > l(m_maxConv);
      getTopConversations (l);
      int i = 0;
      for (toplist<convitem, comp<convitem> >::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += (*it).serialize (buf + i, size - i);
        i += speedtostr (buf + i, size - i, (*it).m_octets, interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_octets * 100 / total)) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment");
    }
  }

  void netmonstorage::storeStrm (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      int size = m_shmstrm->size();
      char *buf = m_shmstrm->buffer();
      m_shmstrm->fill (0xff);
      if (total == 0)
        return;
      toplist<connitem, comp<connitem> > l(m_maxStrm);
      getTopDetailConversations (l);
      int i = 0;
      for (toplist<connitem, comp<connitem> >::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += (*it).serialize (buf + i, size - i);
        i += speedtostr (buf + i, size - i, (*it).m_octets, interval);
        i += snprintf (buf + i, size - i, "%.2f", (double)((*it).m_octets * 100 / total)) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment %s", strerror (e));
    }
  }

  void netmonstorage::storeProt (int interval)
  {
    uint64_t total = getTotalBytes();

    try
    {
      int size = m_shmProtocols->size();
      char *buf = m_shmProtocols->buffer();
      m_shmProtocols->fill (0xff);
      if (total == 0)
        return;
      toplist<portitem, comp<portitem> > l(m_maxProt);
      getTopLiveProtocols (l);
      int i = 0;
      for (toplist<portitem, comp<portitem> >::iterator it = l.begin(); it != l.end(); ++it)
      {
        i += (*it).serialize (buf + i, size - i) + 1;
      }
    }
    catch (int e)
    {
      netmon::log::instance()->add(netmon::_eLogFatal, "Cannot create shared memory segment %s", strerror (e));
    }
  }
}
