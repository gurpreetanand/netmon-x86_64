#include "netflowwriter.h"
#include "udpsender.h"
#include "storage.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#include <cstdlib>
#include <cstdio>

namespace netmon
{
  netflow_writer::netflow_writer (udp_sender *sender)
    : m_sender (sender), m_buffer (write_buffer (maxFlowPacketSize)),
      m_counter (0)
  {
    m_fd = open ("/proc/uptime", O_RDONLY);
  }

  void netflow_writer::beginWrite (uint32_t flowsSeen)
  {
    m_flowsSeen = flowsSeen;
    m_counter = 0;
    m_buffer.reset();
  }

  void netflow_writer::endWrite ()
  {
    if (m_counter > 0)
    {
      writeHeader();
      sendPacket();
    }
  }

  void netflow_writer::addFlow (const ip_port_entry &ipp, const flow_data &f)
  {
    if (m_counter == 0)
      m_buffer.skip (headerSize);
    if (m_counter < maxFlows)
    {
      ++m_counter;
      writeFlow (ipp, f);
    }
    else
    {
      writeHeader();
      sendPacket();
      m_buffer.reset();
      m_counter = 0;
    }
  }

  void netflow_writer::writeHeader ()
  {
    static const uint16_t V5 = htons (5);
    timeval tv;

    m_buffer.rewind();
    gettimeofday (&tv, 0);

    m_buffer << V5 << htons (m_counter) << htonl (getSysUpTime())
             << (uint32_t) htonl (tv.tv_sec) << (uint32_t) htonl (tv.tv_usec)
             << htonl (m_flowsSeen) << (uint8_t) 1 << (uint8_t) 1
             << (uint16_t) 0;
  }

  void netflow_writer::writeFlow (const ip_port_entry &ipp, const flow_data &flow)
  {
    m_buffer << ipp.m_src
             << ipp.m_dst
             << (uint32_t) htonl (0) // next hop
             << (uint16_t) htons (flow.m_ifIndex) // inbound SNMP index
             << (uint16_t) htons (flow.m_ifIndex) // outbound SNMP index
             << htonl (flow.m_packets)
             << htonl (flow.m_octets)
             << htonl (flow.m_startTime)
             << htonl (flow.m_endTime)
             << ipp.m_srcPort
             << ipp.m_dstPort
             << (uint8_t) 0
             << flow.m_flags
             << flow.m_proto
             << (uint8_t) 0 // TOS
             << (uint16_t) htons (0) // source AS
             << (uint16_t) htons (0) // dest AS
             << (uint8_t) 0 // source mask 
             << (uint8_t) 0 // dest mask 
             << (uint16_t) 0;
printf ("size: %d\n", m_buffer.size());
  }

  void netflow_writer::sendPacket ()
  {
    m_sender->sendData (m_buffer.data(), m_buffer.size());
  }

  uint32_t netflow_writer::getSysUpTime ()
  {
    static char c[32];
    double f = .0;

    if (m_fd == -1)
      return ::time (0) * 1000;

    lseek (m_fd, 0, SEEK_SET);

    if (read (m_fd, (void *) c, sizeof (c)) == -1)
      return ::time (0) * 1000;
    f = strtod (c, NULL);
    f *= 1000;

    return (uint32_t) f;
  }
}

#ifdef __TEST__

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace netmon;

void prepareIPPort (ip_port_entry &i)
{
  in_addr tmp;
  inet_aton ("127.0.0.1", &tmp);
  i.m_src = tmp.s_addr;
  inet_aton ("192.168.0.2", &tmp);
  i.m_dst = tmp.s_addr;
  i.m_srcPort = htons (10);
  i.m_dstPort = htons (20);
}

void prepareFlow (flow_data &f)
{
  f.m_octets = 1200;
  f.m_proto = 17;
  f.m_flags = 27;
  f.m_startTime = ::time (0);
  f.m_endTime = f.m_startTime + 12;
  f.m_packets = 22;
}

int main()
{
  udp_sender *s = new udp_sender ("192.168.0.3:9996");
  netflow_writer *w = new netflow_writer (s);
  flow_data f;
  ip_port_entry i;

  w->beginWrite (10);
  prepareFlow (f);
  prepareIPPort (i);
  w->addFlow (i, f);

  f.m_octets = 925;
  f.m_proto = 11;
  i.m_srcPort = htons (22);
  i.m_dstPort = htons (12435);

  w->addFlow (i, f);

  w->endWrite();

  delete s;
  delete w;

  return 0;
}

#endif // __TEST__
