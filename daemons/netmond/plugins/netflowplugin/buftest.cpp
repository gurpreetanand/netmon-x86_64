#include "writebuffer.h"
#include <cstdio>

int main()
{
  using namespace netmon;
  write_buffer bu (100);
  char a = 'a';
  short b = 0xcafe;
  int c = 0xcecafafa;
  bu << a << b << c;
  printf ("buffer size: %d \n", bu.size());
  FILE *f = fopen ("out.bin", "w");
  fwrite ((void *) bu.data(), bu.size(), 1, f);
  fclose (f);
}
