#include "storage.h"
#include "lock.h"
#include "udpsender.h"
#include "netflowwriter.h"
#include "snmpifaces.h"

namespace netmon
{
  storage::storage (udp_sender *sender, int aggTime)
    : m_aggTime (aggTime), m_total (0)
  {
    m_writer = new netflow_writer (sender);
    m_snmp = new snmp_interfaces;
    m_snmp->createSNMPMapping();
  }

  storage::~storage ()
  {
    clear();
    delete m_writer;
    delete m_snmp;
  }

  void storage::add (const flow &f)
  {
    mutex_lock lock (m_mutex);

    addFlow (f);

    ++m_total;
  }

  void storage::clear ()
  {
    mutex_lock lock (m_mutex);
    m_flowMap.clear();
  }

  void storage::store ()
  {
    mutex_lock lock (m_mutex);
    time_t now = ::time (0);

    m_writer->beginWrite (m_total);

    for (flow_map_t::iterator i = m_flowMap.begin(); i != m_flowMap.end(); )
    {
      if ((int) (now - i->second.m_lastSentTime) > m_aggTime || i->second.m_done)
      {
        m_writer->addFlow (i->first, i->second);
        if (i->second.m_done)
          m_flowMap.erase (i++);
        else
        {
          i->second.m_octets = 0;
          i->second.m_packets = 0;
          i->second.m_lastSentTime = now;
        }
        continue;
      }
      else
        ++i;
    }

    m_writer->endWrite();
  }

  void storage::addFlow (const flow &f)
  {
    ip_port_entry e (f.m_srcIP, f.m_dstIP, f.m_srcport, f.m_dstport);
    flow_map_t::iterator i = m_flowMap.find (e);
    if (i == m_flowMap.end())
    {
      m_flowMap[e] = flow_data (f);
      m_flowMap[e].m_ifIndex = m_snmp->getSNMPIfIndex (f.m_devID);
      return;
    }
    i->second.m_octets += f.m_octets;
    i->second.m_packets += 1;
    i->second.m_endTime = ::time(0);
    i->second.m_flags |= f.m_flags;
    i->second.checkFlags();
  }
}
