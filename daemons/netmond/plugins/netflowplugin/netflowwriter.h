#ifndef __NETFLOW_WRITER_H__
#define __NETFLOW_WRITER_H__

#include "writebuffer.h"

namespace netmon
{
  class udp_sender;
  struct ip_port_entry;
  struct flow_data;

  class netflow_writer
  {
    public:
      netflow_writer (udp_sender *);

      void beginWrite (uint32_t);
      void endWrite ();

      void addFlow (const ip_port_entry &, const flow_data &);

    protected:
      void writeHeader ();
      void writeFlow (const ip_port_entry &, const flow_data &);
      void sendPacket ();
      uint32_t getSysUpTime ();
      void openUpTime ();

      enum { headerSize = 24, maxFlows = 30, maxFlowPacketSize = 1464 };
      udp_sender *m_sender;
      write_buffer m_buffer;
      uint32_t m_flowsSeen;
      uint8_t m_counter;
      int m_fd;
  };
}

#endif // __NETFLOW_WRITER_H__
