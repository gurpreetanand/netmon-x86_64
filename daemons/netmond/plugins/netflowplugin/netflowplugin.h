#ifndef __NETFLOW_PLUGIN__
#define __NETFLOW_PLUGIN__

#include "plugin.h"
#include "timer.h"

namespace netmon
{
  class storage;
  class udp_sender;

  class netflow_plugin : public plugin
  {
    public:
      netflow_plugin (unsigned int id)
        : plugin (id), m_valid (false), m_wantedDevs (0)
      {}
      ~netflow_plugin ();

      virtual void init (db *);
      virtual unsigned int getWantedDevice ()
      {
        return m_wantedDevs;
      }

      virtual unsigned int getWantedProtocol ()
      {
        return m_wantedProtocol;
      }

      virtual void getTimers (std::vector<timer *> &vec)
      {
        vec.push_back (new timer (m_sendInterval, 0, m_id));
      }

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0);
      virtual void onTimer (const timer *, db *);

      virtual const char *getName ()
      {
        return "mod_netflow";
      }

    protected:
      storage *m_cache;
      udp_sender *m_sender;
      bool m_valid;
      unsigned int m_wantedDevs;
      unsigned int m_wantedProtocol;
      int m_sendInterval;
  };
}

#endif // __NETFLOW_PLUGIN__
