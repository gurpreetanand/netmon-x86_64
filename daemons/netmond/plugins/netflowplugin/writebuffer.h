#ifndef __WRITE_BUFFER_H__
#define __WRITE_BUFFER_H__

#include <string>
#include <cstring>
#include <stdint.h>

#include "exception.h"

namespace netmon
{
  class write_buffer_exception : public netmon_exception
  {
    public:
      write_buffer_exception ()
        : netmon_exception ("Write pass the end of buffer")
      {}
  };

  class write_buffer
  {
    public:
      explicit write_buffer (uint32_t size)
        : m_size (0), m_maxSize (size), m_pos (0)
      {
        if (size == 0)
          throw write_buffer_exception();
        m_buf = new uint8_t[size];
      }

      ~write_buffer ()
      {
        delete[] m_buf;
      }

      template <typename T>
      write_buffer &operator<< (T t)
      {
        if (m_pos + sizeof (t) > m_maxSize)
          throw write_buffer_exception();
        memcpy ((void *) (m_buf + m_pos), (void *)&t, sizeof (t));
        m_pos += sizeof (t);
        if (m_pos > m_size)
          m_size = m_pos;
        return *this;
      }

      const uint32_t &size() const
      {
        return m_size;
      }

      const uint32_t &maxSize() const
      {
        return m_maxSize;
      }

      bool eof() const
      {
        return (m_pos - m_maxSize) == 0;
      }

      const uint32_t &pos() const
      {
        return m_pos;
      }

      uint32_t &pos()
      {
        return m_pos;
      }

      void rewind ()
      {
        m_pos = 0;
      }

      void reset ()
      {
        m_pos = m_size = 0;
      }

      void skip (uint32_t size)
      {
        if (m_pos + size > m_maxSize)
          throw write_buffer_exception();
        m_pos += size;
        if (m_pos > m_size)
          m_size = m_pos;
      }

      uint8_t *data ()
      {
        return m_buf;
      }

    protected:
      uint8_t *m_buf;
      uint32_t m_size;
      uint32_t m_maxSize;
      uint32_t m_pos;
  };
}

#endif // __WRITE_BUFFER_H__
