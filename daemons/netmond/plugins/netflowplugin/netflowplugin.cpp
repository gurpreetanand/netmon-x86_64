#include "netflowplugin.h"
#include "dvars.h"
#include "log.h"
#include "storage.h"
#include "udpsender.h"

#include <dumbnet.h>
#include <string>

namespace netmon
{
  netflow_plugin::~netflow_plugin ()
  {
    delete m_cache;
    delete m_sender;
  }

  void netflow_plugin::init (db *mydb)
  {
    dvars vars (*mydb, getName());
    std::string IPList;
    m_sendInterval = 30;
    int ll = 2;

    vars.get ("period", m_sendInterval);
    vars.get ("loglevel", ll);
    vars.get ("clients", IPList);

    try
    {
      m_wantedProtocol = _eIP;
      m_wantedDevs = getWantedDevices (mydb);
      m_sender = new udp_sender (IPList.c_str());
      m_cache = new storage (m_sender, m_sendInterval);

      m_valid = true;
    }
    catch (netmon_exception &e)
    {
      log::instance()->add (_eLogError, e.what());
    }
  }

  void netflow_plugin::onTimer (const timer *t, db *mydb)
  {
    if (!m_valid)
      return;
    if (t->getTimerID() == 0)
      m_cache->store();
  }

  void netflow_plugin::onData (const uint8_t *data, uint16_t size, uint8_t devID, ip_port_entry *)
  {
    if (!m_valid)
      return;

    ip_hdr *iph = (ip_hdr *) data;
    tcp_hdr *tcp = (tcp_hdr *) (data + iph->ip_hl * 4);

    if (iph->ip_v != 4) // IPv4?
      return;
    if (iph->ip_p != 6 && iph->ip_p != 17) // TCP or UDP?
      return;

    flow item;
    item.m_srcIP = iph->ip_src;
    item.m_dstIP = iph->ip_dst;
    item.m_srcport = tcp->th_sport;
    item.m_dstport = tcp->th_dport;
    item.m_proto = iph->ip_p;
    item.m_octets = size;
    item.m_devID = devID;
    item.m_flags = (iph->ip_p == 6 ? tcp->th_flags : 0x01);

    m_cache->add (item);
  }
}

extern "C" netmon::plugin *create (int id)
{
  return new netmon::netflow_plugin(id);
}

extern "C" void destroy (netmon::plugin *p)
{
  delete p;
}
