#ifndef __SNMP_H__
#define __SNMP_H__

#include <map>
#include <string>

#include "snmp_pp/snmp_pp.h"

namespace netmon
{
  class snmp;

  class snmp_interfaces
  {
    public:
      snmp_interfaces ();
      ~snmp_interfaces ();

      void createSNMPMapping ();

      typedef std::map<std::string, int> iface_map_t;

      const iface_map_t &getSNMPMapping () const
      {
        return m_map;
      }

      int getSNMPIfIndex (int);

    protected:
      enum { SNMPPort = 161 };
      void getName (const std::string &);

      snmp *m_snmp;
      iface_map_t m_map;
      static Snmp_pp::Oid m_iFaces;
  };
}

#endif // __SNMP_H__
