#include "executor.h"
#include "pluginmanager.h"
#include "timer.h"
#include <dumbnet.h>

namespace netmon
{
  void executor::run ()
  {
    timer *t;

    while (m_run)
    {
      m_queue->pop (t);
      if (t == 0)
        break;
      handleTimer (t);
    }
  }

  void executor::handleTimer (timer *t)
  {
    plugin_data *p = m_pluginManager->getPlugin (t->getPluginID());
    if (p == 0)
      return;

    p->m_plugin->onTimer (t, m_db);
  }
}
