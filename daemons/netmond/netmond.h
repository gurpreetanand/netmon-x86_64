#ifndef __NETMON_DAEMON__
#define __NETMON_DAEMON__

#include "daemon.h"

#include <list>
#include <string>

namespace netmon
{
  class netmon_sniffer;

  class netmon_daemon : public daemon
  {
    public:
      netmon_daemon (int, char **);
      ~netmon_daemon ();

      virtual void init ();

    protected:
      virtual int run ();

      virtual const char *getDaemonName ()
      {
        return "netmond";
      }

      virtual const char *getLogFileName ()
      {
        return "/var/log/netmon/netmond";
      }

      virtual void usage ();
      virtual void onOption (char);
      virtual const char *getShortOptions ();

      netmon_sniffer *m_sniffer;
      std::string m_opts;
      std::string m_path;

    private:
      struct pdata
      {
        std::string m_name;
        int m_id;
        std::string m_ports;
        std::string m_pattern;
      };

      void getPlugins (std::list<pdata *> &);
  };
}

#endif //__NETMON_DAEMON__
