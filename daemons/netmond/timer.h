#ifndef __TIMER__
#define __TIMER__

#include <ctime>

namespace netmon
{
  class timer
  {
    public:
      timer (unsigned int sec, unsigned int id, unsigned int plugin)
        : m_sec (sec), m_id (id), m_pluginID (plugin)
      {}

      virtual ~timer ()
      {}

      virtual unsigned int getSeconds ()
      {
        return m_sec;
      }

      unsigned int getTimerID () const
      {
        return m_id;
      }

      unsigned int getPluginID () const
      {
        return m_pluginID;
      }

    protected:
      unsigned int m_sec;
      unsigned int m_id;
      unsigned int m_pluginID;
  };

  class full_span_timer : public timer
  {
    public:
      full_span_timer (unsigned int sec, unsigned int id, unsigned int plugin)
        : timer (sec, id, plugin), m_span (sec)
      {
        setTimer ();
      }

      virtual unsigned int getSeconds ()
      {
        setTimer ();
        return m_sec;
      }

    protected:
      void setTimer ()
      {
        time_t now = time(0);
        m_sec = (int)(now / m_span) * m_span + m_span - now;
      }

      unsigned int m_span;
  };
}

#endif // __TIMER__
