#ifndef __CONNECTION_MANAGER__
#define __CONNECTION_MANAGER__

#include "plugins/common/iphashmap.h"

namespace netmon
{
  class connection_manager
  {
    public:
      enum status { eNew, eMoreData, eReady, eTracking, eNotTracking };
      connection_manager (uint16_t = 600);
      ~connection_manager ();

      status getStatus (uint32_t, uint32_t, uint16_t, uint16_t, uint8_t);
      void setPluginID (uint32_t, uint32_t, uint16_t, uint16_t, uint8_t);
      int getPluginID (uint32_t src, uint32_t dst, uint16_t sport, uint16_t dport);
      void removeIddleConnections ();

    private:
      struct connection_data
      {
        enum { eSyn, eFirstPacket, eMoreData };
        time_t m_ts;
        uint8_t *m_data;
        uint8_t m_state;
        int m_pluginID;
      };

    protected:
      void prepareCurrentEntry (uint32_t, uint32_t, uint16_t, uint16_t);

      typedef hash_map<ip_port_entry, connection_data *, ip_port_hash, ip_port_eq> conn_map_t;
      conn_map_t m_connections;
      uint16_t m_iddleTime;
      ip_port_entry m_current;
  };
}

#endif // __CONNECTION_MANAGER__
