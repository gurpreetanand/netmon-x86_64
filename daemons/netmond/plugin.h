#ifndef __PLUGIN__
#define __PLUGIN__

#include <vector>
#include <string>
#include <stdint.h>

#include "util.h"

namespace netmon
{
  class db;
  class timer;

  enum protocol { _eETH = 1, _eIP = 2, _eTCP = 4, _eUDP = 8 };

  struct ip_port_entry;

  class plugin
  {
    public:
      explicit plugin (unsigned int id)
        : m_id (id)
      {}
      virtual ~plugin ()
      {}

      virtual void init (db *) = 0;

      virtual unsigned int getWantedDevice () = 0;
      virtual unsigned int getWantedProtocol () = 0;
      virtual const bool wantsHeaders () const
      {
        return true;
      }
      virtual void getTimers (std::vector<timer *> &) = 0;

      virtual void onData (const uint8_t *, uint16_t, uint8_t, ip_port_entry * = 0) = 0;
      virtual void onTimer (const timer *, db *) = 0;

      virtual const char *getName () = 0;

      unsigned int getID () const
      {
        return m_id;
      }

      unsigned int getWantedDevices (db *);

      static unsigned int parseDevice (std::string &);

    protected:
      unsigned int m_id;
  };

  typedef plugin *create_t(unsigned int);
  typedef void destroy_t (plugin *);
}

#endif // __PLUGIN__
