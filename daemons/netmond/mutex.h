#ifndef __MUTEX__
#define __MUTEX__

#include <pthread.h>
#include <cstdio>

namespace netmon
{
  class mutex
  {
    public:
      mutex ()
      {
        pthread_mutex_init (&m_mutex, NULL);
        m_locked = false;
      }

      ~mutex ()
      {
        if (m_locked)
          release ();
        pthread_mutex_destroy (&m_mutex);
      }

      void acquire ()
      {
        pthread_mutex_lock (&m_mutex);
        m_locked = true;
      }

      void release ()
      {
        pthread_mutex_unlock (&m_mutex);
        m_locked = false;
      }

    private:
      mutex (const mutex &);
      const mutex &operator= (const mutex &);

      mutable pthread_mutex_t m_mutex;
      bool m_locked;
      friend class condition;
  };
}

#endif //__MUTEX__
