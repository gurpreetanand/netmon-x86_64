/**
 * maintenance_scheduler.js - $Id$
 * Author: Xavier Spriet <xavier@netmon.ca>
 * Sept 25, 2009 - Netmon 5.1
 * Provides a UI object built on top of Ext.js
 * to allow the user to schedule recurring maintenance
 * against an alert.
 * 
 **/


var maintenance_scheduler = function(config) {
	this.config = config
	this.trigger_id = this.config.trigger_id;
	
	/**
	 * Generates and renders the scheduler builder
	 */
	this.render_scheduler = function() {
		this.scheduler = new Ext.form.FormPanel({
			frame : true,
			width : '100%',
			method : 'POST',
			titleCollapse : false,
			autoHeight : true,
			id : 'scheduler-panel',
			title : "Maintenance Scheduler Builder",
			labelWidth : 110,
			items : {
				xtype: 'fieldset',
				title: 'Maintenance Parameters',
				collapsed: false,
				layout: 'form',
				anchor: '95%',
				autoHeight: false,
				//autoShow: false,
				height: '100%',
				width: '100%',
				items: [
					{
					xtype: 'timefield',
					fieldLabel: 'Pause this alert at',
					id: 'cmpHour',
					hiddenName: 'schedule_hour',
					width: 160,
					listwidth: 160,
					name: 'schedule_hour',
					allowBlank: false,
					lazyInit: false,
					format: 'H:i',
					mode: 'local',
					editable: false,
					minValue: '0:00am',
					maxValue: '11:59pm',
					increment: 60
				},  {
					xtype: 'numberfield',
					minValue: 1,
					maxValue: 48,
					allowBlank: false,
					name: 'maintenance_duration',
					id: 'duration',
					value: 1,
					fieldLabel: 'Duration (hours)'
				},{
					xtype: "fieldset",
					title: "Scheduling Options",
					layout: 'form',
					autoHeight: true,
					//height: '100%',
					width: '100%',
					collapsible: false,
					id: 'schedColumnContainer',
					//anchor: '95%',
					hidden: false,
					items: [{
						//columnWidth: 0.5,
						id: 'col_freq',
						autoHeight: true,
						layout: 'form',
						items: [{
								xtype: 'combo',
								id: 'cmbRecurrence',
								hiddenName: 'recurrence_unit',
								lazyInit: false,
								name: 'recurrence_unit',
								fieldLabel: "Every",
								editable: false,
								triggerAction: 'all',
								typeAhead: false,
								mode: 'local',
								autoShow: true,
								width:160,
								listWidth: 160,
								store: [['day', 'Day'], ['week', 'Week'], ['month', 'Month'],
								['year', 'Year']],
								readOnly: true,
								allowBlank: true,
								hidden: false,
								listeners: {
									'select': function(c,r,i) {
										Ext.ComponentMgr.get('pnlWeek').hide();
										Ext.ComponentMgr.get('pnlWeek').allowBlank = true;
										Ext.ComponentMgr.get('pnlMonth').hide();
										Ext.ComponentMgr.get('pnlMonth').allowBlank = true;
										Ext.ComponentMgr.get('pnlYear').hide();
										Ext.ComponentMgr.get('pnlYear').allowBlank = true;
																	
										if ('week' == r.get('field1')) {
											Ext.ComponentMgr.get('pnlWeek').show();
											Ext.ComponentMgr.get('pnlWeek').allowBlank = false;
										} else if ('month' == r.get('field1')) {
											Ext.ComponentMgr.get('pnlMonth').show();
											Ext.ComponentMgr.get('pnlMonth').allowBlank = false;
										} else if ('year' == r.get('field1')) {
											Ext.ComponentMgr.get('pnlYear').show();
											Ext.ComponentMgr.get('pnlYear').allowBlank = false;
										}
										
										Ext.ComponentMgr.get('schedSelection').doLayout();
									}
								}
						}]
					}, {
						//columnWidth: 0.5,
						autoHeight: true,
						layout: 'form',
						id: 'schedSelection',
						items: [{
							xtype: 'panel',
							border: false,
							frame: false,
							layout: 'form',
							id: 'pnlWeek',
							items: [{
								xtype: 'combo',
								id: 'cmpWeek',
								hiddenName: 'schedule_week',
								fieldLabel: 'Day of the Week',
								lazyInit: false,
								name: 'schedule_week_txt',
								fieldLabel: "On",
								editable: false,
								triggerAction: 'all',
								typeAhead: false,
								mode: 'local',
								autoShow: true,
								width:160,
								listWidth: 160,
								store: [['0', 'Sunday'], ['1', 'Monday'], ['2', 'Tuesday'],
								['3', 'Wednesday'], ['4', 'Thursday'], ['5', 'Friday'], ['6', 'Saturday']],
								readOnly: true
								}
							],
							hidden: true
						}, {
							xtype: 'panel',
							id: 'pnlMonth',
							frame: false,
							border: false,
							layout: 'form',
							items: [{
								xtype: 'numberfield',
								name: 'schedule_day',
								id: 'cmpMonth',
								maxValue: '31',
								minValue: '0',
								fieldLabel: "Day of the Month"
							}
							],
							hidden: true
						}, {
							xtype: 'panel',
							id: 'pnlYear',
							layout: 'form',
							frame: false,
							items: [{
								xtype: 'datefield',
								name: 'schedule_year',
								id: 'cmpYear',
								fieldLabel: "Day of the Year",
								format: 'd/m'
							}
							],
							hidden: true
						}]
					}]}
				]
			},
			buttons : [{
				text : 'Schedule Maintenance',
				type : 'submit',
				scope: this,
				handler : function() {
					url = '/?module=mod_alerts&action=schedule_maintenance&root_tpl=blank&trigger_id='+this.trigger_id;
					this.callback = function(act) {
						console.log('Resetting form...');
						parent.parent.pnl_middle.location.reload();
					}
					actionTxt = 'Scheduling Maintenance.. please wait.';
					
					this.scheduler.form.submit({
								waitMsg : actionTxt,
								timeout : 300000,
								url : url,
								scope: this,
								method : 'POST',
								scripts : true,
								success : function(frm, act) {
									console.log("Submission successful. Launching callback action");
									this.callback(act);
								}, failure: function(frm, act) {
									console.log("Submission failed!!");
									console.log(act);
									console.log(frm);
								}
							});
				}
			}, {
				text : 'Reset Scheduler',
				type : 'reset',
				scope: this,
				handler : function() {
					this.scheduler.form.reset();
				}
			}]
		});

		console.log("Loading maintenance scheduler");
		this.scheduler.render(this.config.renderTo);
	}
};

