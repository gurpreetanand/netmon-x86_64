/*
Copyright (c) 2007, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/


// Constructor
GraphManager = function() {};


// Object Prototype
GraphManager.prototype = {
	
	// Device identifier
	_graphs: [],
	
	// Base timeout
	_timeout_base: 60000,
	
	register_graph: function(span_id) {
		console.log("Registering graph " + span_id)
		this._graphs.push(span_id);
	},

	init: function() {
		// Main loop
		for (i = 0; i < this._graphs.length; i++) {
			// Ensure we can still handle 60+ graphs
			if (this._graphs.length >= 60) {
				timeout = this._timeout_base + (2000*i);
			} else {
				timeout = this._timeout_base;
			}
			console.log("Setting up this.refresh_graph("+i+","+timeout+") in "+ (parseInt(this._timeout_base)+parseInt(i*2000)) +"ms");
			
			myObj = this;
			setTimeout("myObj.refresh_graph("+i+","+timeout+")", (parseInt(this._timeout_base) + parseInt(i*2000)));
		}
	},
	
	refresh_graph: function(index, timeout) {
		console.log("Refreshing graph "+index+" ("+this._graphs[index]+") and again in "+timeout+"ms");
		span_id = this._graphs[index];
		document.getElementById(span_id).innerHTML = document.getElementById(span_id).innerHTML;
		myObj = this;
		setTimeout("myObj.refresh_graph("+index+","+timeout+")", timeout);
	}
}