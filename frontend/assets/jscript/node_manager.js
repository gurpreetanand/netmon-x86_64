/*
 * Copyright (c) 2008, Netmon Inc. All Rights Reserved Author: $Author$ ($Id$)
 */


Node_Manager = function() { };


Node_Manager.prototype = {
	// Nodes collection
	_nodes: [],
	
	// Sync frequency
	_sync_rate: 10000,
	
	// Schedules all node refreshes (no, this is not a constructor)
	init: function() {
		for (var i = 0; i < this._nodes.length; i++) {
			if (this._nodes.length >= 25) {
				timeout = this._sync_rate + (1000*this._nodes.length); 
			} else {
				timeout = this._sync_rate;
			}
			
			
			when = (parseInt(this._sync_rate) + parseInt(i*1000));
			scopeObj = this;
			setTimeout("scopeObj.refresh_node("+i+","+timeout+")", when);
		}
	},
	
	refresh_node: function(index, timeout) {
		node = this._nodes[index];
		node.ajax_refresh(index, timeout);
	},
	
	register_node: function(node) {
		this._nodes.push(node);
	}
	
	
	
	
	
	
}