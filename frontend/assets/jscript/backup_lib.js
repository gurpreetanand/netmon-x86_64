/*
Copyright (c) 2006, Netmon Inc. All Rights Reserved
Author: $Author$
($Id$)
*/

var _db_tree    = null;
var nodes       = new Array();
var connections = [];

/** ============================================================================== **/
/** DESTINATION HELPERS BEGIN **/

toggle_destination = function(dest_id) {
	local = document.getElementById('dest_div_local');
	smb   = document.getElementById('dest_div_smb');
	
	local.style.display = (dest_id == 'dest_div_local') ? 'block' : 'none';
	smb.style.display   = (dest_id == 'dest_div_smb')   ? 'block' : 'none';
}

ajax_get_smb_shares = function() {
	showProgressIndicator('smb_progress');
	dest=document.getElementById('smb_share_dropdown');
	for (var i = 0; i < dest.length; i++) {
		dest.options[i] = null;
	}
	
	ip       = document.getElementById('smb_ip_address').value;
	username = document.getElementById("smb_username").value;
	password = document.getElementById('smb_password').value;
	domain   = document.getElementById('smb_domain').value;
	
	uri = "?module=mod_settings&action=ajax_get_smb_shares_xml" + 
		"&username=" + escape(username) +
		"&password=" + escape(password) +
		"&domain=" + escape(domain) +
		"&ip_address=" + escape(ip) +
		"&root_tpl=blank";
	YAHOO.util.Connect.asyncRequest('GET', uri, {success:populate_shares_dropdown, failure:populate_shares_dropdown}, null);
}


populate_shares_dropdown = function(o) {
	xml_doc = o.responseXML;
	
	if (xml_doc) {
		drop_down = document.getElementById('smb_share_dropdown');
		shares = xml_doc.documentElement.getElementsByTagName('share');
	
		if (shares.length > 0) {
			for (i = 0; i < shares.length; i++) {
				name = shares[i].getAttribute('name');
				size = shares[i].getAttribute('size');
				drop_down.options[i] = new Option(name + " (" + size + " free)", name)
			}
			smb_success();
		}
	} else {
		smb_failure();
	}
	
	hideProgressIndicator('smb_progress');
}

smb_success = function() {
	document.getElementById('smb_success_div').style.visibility = 'visible';
	document.getElementById('smb_success_div').style.display = 'block';
	
	document.getElementById('smb_failure_div').style.visibility = 'hidden';
	document.getElementById('smb_failure_div').style.display = 'none';
}

smb_failure = function() {
	if (document.forms['backup_form']['smb_share'].length > 0)
		document.forms['backup_form']['smb_share'].options[document.forms['backup_form']['smb_share'].selectedIndex].value = "";
	
		
	document.getElementById('smb_success_div').style.visibility = 'hidden';
	document.getElementById('smb_success_div').style.display = 'none';
	
	
	document.getElementById('smb_failure_div').style.visibility = 'visible';
	document.getElementById('smb_failure_div').style.display = 'block';
}

update_smb_button = function() {
	if (document.forms['backup_form']['smb_ip_address'].value != '') { 
		document.getElementById('smb_lookup').style.display = "inline"; 
		document.getElementById('smb_lookup').style.visibility = "visible";
	} else {
		document.getElementById('smb_lookup').style.display = "none"; 
		document.getElementById('smb_lookup').style.visibility = "hidden";
	}
}

/** DESTINATION HELPERS END **/
/** ============================================================================== **/


/** ============================================================================== **/
/** DB COMPONENTS HELPERS BEGIN **/



ajax_get_db_components = function() {
	uri = "?module=mod_settings&action=ajax_get_db_tables&root_tpl=blank";
	YAHOO.util.Connect.asyncRequest('GET', uri, {success: build_db_tree, failure: chop_db_tree}, null);
}

chop_db_tree = function(o) {
	document.getElementById('db_tree_div').innerHTML = "<b>Error. Unable to retrieve the database XML description</b><br />";
}

build_db_tree = function(o) {
	xml_doc = o.responseXML;
	
	if (xml_doc) {
		components = xml_doc.documentElement.getElementsByTagName('component');
		
		if (components.length > 0) {
			// Prepare our tree
			_db_tree = new YAHOO.widget.TreeView('db_tree_div');
			var root = _db_tree.getRoot();
			
			for (i = 0; i < components.length; i++) {
				// Add each component to the root of the tree
				idx = nodes.push(new YAHOO.widget.TaskNode("<span>" + components[i].getAttribute("name") + " Module</span>", root, false, true)) -1;
				
				tables = components[i].getElementsByTagName('table');
				if (tables.length > 0) {
					for (x = 0; x < tables.length; x++) {
						label = tables[x].getAttribute('label');
						type = tables[x].getAttribute('type');
						
							if(type == "settings"){
							label_img = "<img src=\"/assets/icons/database_gear.png\" class=\"icon\" alt=\"Settings & Configuration Data\" title=\"Settings & Configuration Data\">";
						} else {
							label_img = "<img src=\"/assets/icons/database_data.gif\" class=\"icon\" alt=\"Historical Data\" title=\"Historical Data\">";
						}
						
						table_idx = nodes.push(new YAHOO.widget.TaskNode(label_img + " " + label + "", nodes[idx], false, true)) -1;
						nodes[table_idx]['tablename'] = tables[x].getAttribute('name');
						
					}
				}
				
				
			}
			_db_tree.draw();

			for (i = 0; i < nodes.length; i++) {
				if (nodes[i]['tablename']) {
					ajax_get_table_size(i);
					break;
				}
			}

		}
	}
}


get_checked_tables = function() {
	selected_nodes = new Array()

	for (var node in nodes) {
		if ((nodes[node]['checked'] > 0) && (nodes[node].hasChildren(false) == false)) {
			selected_nodes.push(nodes[node]['tablename']);
		}
	}
	
	if (selected_nodes.length < 1) {
		alert("You did not select any components to back up.");
		return false;
	}
	
	document.forms['backup_form']['tables'].value = selected_nodes.join(',');
	return true;
}


ajax_get_table_size = function(table_idx) {
	// Retrieves the size of a table through an AJAX hook.
	node = nodes[table_idx];
	if (node['tablename']) {
		tablename = node['tablename'];
		console.log("Requesting table-size for " + tablename)
		uri = "?module=mod_settings&action=ajax_get_table_size&tablename=" + tablename + "&root_tpl=blank";
		connections[table_idx] = YAHOO.util.Connect.asyncRequest('GET', uri, {success:set_table_size, failure:table_size_error, argument:table_idx}, null);
	} else {
		if (table_idx <= nodes.length) {
			ajax_get_table_size(table_idx+1);
		}
	}
}

set_table_size = function(o) {
	if (o.responseText != '') {
		table_idx = o.argument;
		nodes[table_idx].label = "<span style=\"color: #333333;\">" + nodes[table_idx].label + " Table [" + o.responseText + "]</span>";
		nodes[table_idx].parent.refresh();
		if (table_idx <= nodes.length) {
			ajax_get_table_size(table_idx+i);
		}
	}
}

table_size_error = function(o) {
	table_idx = o.argument;
}

cancel_ajax_requests = function() {
	for (conn in connections) {
		console.log("Cancelling AJAX connection...");
		if (YAHOO.util.Connect.isCallInProgress(conn)) {
			YAHOO.util.Connect.abort(conn);
		}
	}
}

/** DB COMPONENTS HELPERS END **/
/** ============================================================================== **/


/** ============================================================================== **/
/** VALIDATION HELPERS BEGIN **/
validate_destination = function() {
	destination = document.forms['backup_form']['destination'].value;
	
	if (destination == "dest_div_local") {
		if (document.forms['backup_form']['label'].value.length < 1) {
			alert("You must specify a label for your destination file");
			return false;
		}
	} else if (destination == "dest_div_smb") {
		if (document.forms['backup_form']['smb_share'].value.length < 1) {
			alert("You must specify a valid SMB destination share");
			return false;
		}
	} else if (destination == "") {
		alert("You must specify a destination for your backup files.");
		return false;
	}
	
	return true;
}

validate_backup_form = function() {
	if ((get_checked_tables() == true) && (validate_destination() == true) && (validate_date_range() == true)) {
		showProgressDialog();
		cancel_ajax_requests();
		return true;
	}
	return false;
}

validate_date_range = function() {
	
	/*
	date_from = cal_from.getSelectedDates()[0];
	date_to   = cal_to.getSelectedDates()[0];
	
	if ((cal_from.getSelectedDates().length < 1) || (cal_to.getSelectedDates().length < 1)) {
		alert("You must select a start/end date");
		return false;
	}
	
	if (YAHOO.widget.DateMath.before(date_to, date_from)) {
		alert("The end date must be before the start date.");
		return false;
	}
	
	document.forms['backup_form']['date_to'].value = dateToStr(date_to);
	document.forms['backup_form']['date_from'].value = dateToStr(date_from);
	
	if ((document.forms['backup_form']['date_to'].value.length < 8)
		|| (document.forms['backup_form']['date_from'].value.length < 8)) {
		return false;
	}
	*/
	return true;
}

/** VALIDATION HELPERS END **/
/** ============================================================================== **/



/** ============================================================================== **/
/** MISC HELPERS BEGIN **/
debug = function(str) {
	document.getElementById('debug').value += str + "\n";
}

dateToStr = function(date) {
	return date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
}

/** MISC HELPERS END **/

/** EVENT TRACKING HELPERS BEGIN **/

fetch_backup_events = function(backup_id, container, last_timestamp) {
	params = {"backup_id": backup_id, "container": container, "last_timestamp": last_timestamp}
	uri = "?module=mod_settings&action=ajax_get_backup_events&root_tpl=blank&id=" + backup_id + "&last_timestamp=" + last_timestamp;
	YAHOO.util.Connect.asyncRequest('GET', uri, {success:add_backup_events, failure:backup_events_error, argument:params}, null);
}

add_backup_events = function(o) {
	arg = o.argument;
	tableObj = document.getElementById(arg['container']).tBodies[0];
	
	xml = o.responseXML.documentElement;
	status = xml.getAttribute('status');
	events = xml.getElementsByTagName('event');
	
	htmlStr = "";
	
	last_timestamp = arg['last_timestamp'];
	if (events.length > 0) {
		for (i = 0; i < events.length; i++) {
			ev = events[i];
			
			newRow = document.createElement("TR");
			tableObj.appendChild(newRow);
			
			timeCell = document.createElement("TD");
			timeCell.innerHTML = ev.getAttribute('time');
			
			payloadCell = document.createElement("TD");
			payloadCell.innerHTML = ev.getAttribute('payload');
			
			newRow.appendChild(timeCell);
			newRow.appendChild(payloadCell);
			
			//htmlStr += "\n<tr><td>" + ev.getAttribute('time') + "</td><td>" + ev.getAttribute('payload') + "</td></tr>\n";
			last_timestamp = ev.getAttribute('timestamp');
		}
	}
	
	//tableObj.innerHTML += htmlStr;
	
	document.getElementById('status').innerHTML = status;
	
	
	
	if (status == "pending") {
		setTimeout('fetch_backup_events('+arg['backup_id']+',"'+arg['container']+'", '+last_timestamp+')', 1000);
	}
	
}

backup_events_error = function(o) {
	return;
}





