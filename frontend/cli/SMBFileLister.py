#!/usr/bin/env python
import os
import time
import signal
# CLI Arguments Parser
from optparse import OptionParser
signal.signal(signal.SIGCHLD, signal.SIG_DFL)

class SMBFileLister:
	def __init__(self, options):
		self.path=options.path
		self.size=options.size
		self.user=options.user
		self.passw=options.passw
        
	def run(self, cmd):
                p=os.popen(cmd)
                s=p.read()
		if s!="" and s!="\n":
			print(s)

	def getFiles(self):
		mountpoint = "/mnt/smb_%d" % int(time.time())
		self.run("mkdir %s" % mountpoint)
		self.run("mount -t smbfs -o username=%s,password=%s,uid=root %s %s" % (self.user, self.passw, self.path, mountpoint))
		os.chdir(mountpoint)
		self.run("find . -size +%sc -xtype f -print0|xargs -0 ls -Q -d -1 -S | xargs du -h --apparent-size -S " % (self.size));
		os.chdir("/root")
		time.sleep(5)
		self.run("umount %s" % mountpoint)
		self.run("rmdir %s" % mountpoint)


if __name__=='__main__':    
	# Prepare the CLI options-parser
	usage       = "Usage: %prog -p PATH -z SIZE -u USER -s PASSWORD"
	version     = "%prog (Netmon Inc.)"
	description = "Find a list of large files on a remote file system."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	# General Options
	parser.add_option("-p", action="store", dest="path", default='', help="The path of the SMB shared folder, ie //10.10.1.23/share")
	parser.add_option("-z", action="store", dest="size", default=5, help="The minimum size of the file to list (in bytes).")
	parser.add_option("-u", action="store", dest="user", default='', help="The username for accessing the share.")
	parser.add_option("-s", action="store", dest="passw", default='', help="The password for accessing the share.")


	(options, parser) = parser.parse_args()
	
	if options.path=='' or options.size =='' or options.user =='' or options.passw =='':
		parser.print_help()
		os._exit(os.EX_OK)
	
	s=SMBFileLister(options)
	s.getFiles()
