#! /usr/bin/python
import commands

# checkDaemons.py
# A script to verify the Netmon daemons are running.  No inputs, outputs 0 or 1

daemons=['/usr/local/sbin/arpmond', '/usr/local/sbin/dfmond', '/usr/local/sbin/emailmond', '/usr/local/sbin/netmond', '/usr/local/sbin/oidmond', '/usr/local/sbin/pagermond', 'usr/local/sbin/procmond', '/usr/local/sbin/resolvemond', '/usr/local/sbin/smbmond', '/usr/local/sbin/snmpmond', '/usr/local/sbin/srvmond', '/usr/local/sbin/syslogmon', '/usr/local/sbin/webmond']


output = commands.getoutput('ps -ef | awk \{\'print $8\'\} | grep mond | grep -v grep')

running_daemons = output.split('\n')
running_daemons.sort()

if running_daemons == daemons:
        print 1
else:
        print 0

