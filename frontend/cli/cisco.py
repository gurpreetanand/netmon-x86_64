#!/usr/bin/env python
# Author:	Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Cisco device management class 
# Created:   15/03/07 03:35:28 PM
# $Id$

"""

This library uses the pexpect python library to emulate a telnet
dialog between Cisco devices and a human-being, parsing the output
of the device and interacting with it.

TODO:
- Use tftp to copy files from the device
- Add support for Cisco routers (only PIX/ASA appliances supported ATM)
- Add error control (ciscoAuthException, ciscoParserException, ciscoEnableAuthException)
- Manipulate access-lists (create data-structures based on ACLs, discover ACLs, etc..)
- Interface discovery
- Testing
- Determine feature support based on IOS/CatOS version

"""


# Imports
import os
import re
import sys
import pexpect
import os.path

# To enable debug output, set this to 1, or 0 otherwise.
DEBUG = 0

class PIX:
	"""pexpect-based Cisco device management class """
	
	# pexpect Telnet handler
	telnet   = None
	# IP address of the Cisco device
	ip	   = ""
	# Telnet password for the device
	password = ""
	# Enable password for the device
	enable   = ""

	def __init__(self, ip, password, enable=""):
		""" Constructor """
		self.ip	   = ip
		self.password = password
		self.enable   = enable
		#sys.stdout = open("/dev/null", "w")
		
		self._login()
		
		# Only enter enable mode if an enable password was provided.
		if "" != enable:
			self._enable()
		
	def _login(self):
		self.debug("Attempting to authenticate into PIX %s using password %s" % (self.ip, self.password))
		sout = sys.stdout
		sys.stdout = open("/dev/null", "w")
		
		
		# Initialize the connection
		self.telnet = pexpect.spawn("telnet %s" % self.ip, maxread=50000, searchwindowsize=50000, logfile=None)
		self.debug("Connected")
		
		# Wait for the password prompt and send the actual password
		self.telnet.expect("(?i)password: ")
		self.debug("Sending password")
		self.telnet.sendline("%s" % self.password)
		self.telnet.expect([r"[a-zA-Z-_]+> ", r"[a-zA-Z-_]+#"])
		sys.stdout = sout
		
	def logoff(self):
		self.debug("Logging off")
		# Send the 'exit' command, then close the pipe
		self.telnet.sendline("exit")
		self.telnet.close()
		
		if self.telnet.isalive():
			self.telnet.kill(9)
		
	def __del__(self):
		""" If the user forgot to log off, do it on their behalf """
		if self.telnet.isalive():
			self.debug("You forgot to logoff() !!!")
			self.logoff()
		
	def getoutput(self, command):
		self.debug("Executing command '%s'" % command)
		sout = sys.stdout
		sys.stdout = open("/dev/null", "w")
		
		# Send the actual command.
		self.telnet.sendline(command)
		
		# Flag used to detect "<!--- More --->" in output and press a key.
		i	= 0
		# Output buffer
		out  = ""
		
		# Perform the "<--- More --->" detection and aggregate all encountered
		# output into out output buffer.
		while i == 0:
			i = self.telnet.expect(['<--- More --->', r"[a-zA-Z-_]+> ", r"[a-zA-Z-_]+#"])
			out += "\n".join(self.telnet.before.splitlines()[1:])
			if i == 0:
				# Send a space to get the device to send us more data
				self.telnet.send(' ')
		out += self.telnet.after
			
		sys.stdout = sout
		# Return the contents of our output buffer
		return "\n".join(out.splitlines()[:-1])
	
	def _enable(self):
		self.debug("Attempting to 'enable' using password %s" % self.enable)
		sout = sys.stdout
		sys.stdout = open("/dev/null", "w")
		
		self.telnet.sendline("enable")
		self.telnet.expect("(?i)password: ")
		self.debug("Sending enable password")
		self.telnet.sendline("%s" % self.enable)
		
		self.telnet.expect([r"[a-zA-Z-_]+> ", r"[a-zA-Z-_]+#"])
		sys.stdout = sout
		
	def debug(self, str):
		if 1 == DEBUG:
			print "==DEBUG== %s" % str