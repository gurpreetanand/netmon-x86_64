#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Report Helper Class 
# Created:   06/02/07 01:56:54 PM

# Imports
import os
import sys
import time
import math
import signal
import os.path
import json
import datetime
from time import mktime
from xml.dom import minidom
from decimal import Decimal

sys.path.append("/apache/cli")
from madnet_db import *


# Constant declarations
TIME_MINUTE = 60
TIME_HOUR   = 3600
TIME_DAY    = 86400
TIME_WEEK   = 604800
TIME_MONTH  = 2592000
TIME_YEAR   = 31536000

# IP Address cache
global IP_CACHE
IP_CACHE= {}

# Ports translation cache
global PORTS_CACHE
PORTS_CACHE={}

# Connect to the database
try:
  db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
  db.set_autocommit(0)
except Exception, e:
  print "ERROR: %s" % e
  sys.exit(1)

# Most reports need to register these arguments into OptParse, so it makes sense
# to do it from here.
def register_timestamp_arguments(parser):
  """ Registers date/time-related CLI arguments to an OptionParser object """
  parser.add_option("--report_period", action="store", dest="report_period", default="today", help="Reporting Period (today,yesterday,lastweek,lastmonth,custom)")
  parser.add_option("--start_date", action="store", dest="start_date", help="Start Date (MM/DD/YYYY)")
  parser.add_option("--end_date", action="store", dest="end_date", help="End Date (MM/DD/YYYY)")
  parser.add_option("--start_time", action="store", dest="start_time", help="Start Time (HH:MM)")
  parser.add_option("--end_time", action="store", dest="end_time", help="End Time (HH:MM)")

# Takes in a netmon-formatted representation of a time-range and converts it
# into a list of pairs of timestamps.
def generate_timestamps(report_period, start_date, end_date, start_time, end_time):
  """ Builds a list of timestamp-pairs for the specified date/time-range """
  timestamps = []
  current_ts = time.localtime()
  
  # Return a timestamp starting at now()-3600 and ending right now
  if report_period == "lasthour":
    start = int(time.mktime(current_ts))-3600
    end = int(time.mktime(current_ts))
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
  
  # Return a timestamp pair starting at 00:00:00 yesterday and ending right now
  elif report_period == "today":
    start = int(time.mktime(time.strptime("%s 00:00:00" % time.strftime("%m/%d/%Y"), \
              "%m/%d/%Y %H:%M:%S")))
    end = int(time.mktime(current_ts))
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
    
  # Return a timestamp pair starting at 00:00:00 yesterday and ending at 23:59:59 yesterday
  elif report_period == "yesterday":
    start = int(time.mktime(time.strptime("%s 00:00:00" % time.strftime("%m/%d/%Y"), \
              "%m/%d/%Y %H:%M:%S")))
    start -= TIME_DAY
    end = int(time.mktime(time.strptime("%s 23:59:59" % time.strftime("%m/%d/%Y"), \
              "%m/%d/%Y %H:%M:%S")))
    end -= TIME_DAY
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
    
  elif report_period == "lastweek":
    start = int(time.mktime(time.strptime("%s 00:00:00" % time.strftime("%m/%d/%Y"), \
              "%m/%d/%Y %H:%M:%S")))
    start_weekday = int(time.strftime("%w"))
    end = start - ((start_weekday-1)*TIME_DAY)
    start = end - TIME_WEEK
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
    
  elif report_period == "thisweek":
    start = int(time.mktime(time.strptime("%s 00:00:00" % time.strftime("%m/%d/%Y"), \
              "%m/%d/%Y %H:%M:%S")))
    end = int(time.mktime(current_ts))
    start_weekday = int(time.strftime("%w"))
    start = start - ((start_weekday-1)*TIME_DAY)
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
    
  elif report_period == "lastmonth":
    # Get timestamp for first day of the month @midnight
    month_start = int(time.mktime(time.strptime("%s 00:00:00" % \
                time.strftime("%m/01/%Y"), \
                "%m/%d/%Y %H:%M:%S")))
    # Find the last day of the previous month (tuple)
    prev_month = time.localtime(month_start-TIME_DAY)
    # Find the first day of the previous month
    start = time.strftime("%m/01/%Y 00:00:00", prev_month)
    # Convert that first day into a timestamp
    start = int(time.mktime(time.strptime(start, "%m/%d/%Y %H:%M:%S")))
    
    # Add 23:59:59 to the prev_month to get the very end of last month
    end = int(time.mktime(prev_month)) + (TIME_DAY-1)
    # Commit the timestamp pair to the list
    timestamps.append({'start': start, 'end': end})
  # Current month
  elif report_period == "thismonth":
    #Get timestamp for first day of the month @midnight
    start = int(time.mktime(time.strptime("%s 00:00:00" % \
                time.strftime("%m/01/%Y"), \
                "%m/%d/%Y %H:%M:%S")))
    
    # Get current timestamp
    end = int(time.mktime(current_ts))
    timestamps.append({'start': start, 'end': end})
    
  # Handle unrecognized time periods (custom)
  else:
    # Custom date/time specifications (PITA)
    custom = True
    if start_time == "all":
      custom = False
      start_time = "00:00"
      end_time = "23:59"
      
    start = int(time.mktime(time.strptime(start_date + " " + start_time + ":00", \
              "%m/%d/%Y %H:%M:%S")))
      
    end = int(time.mktime(time.strptime(end_date + " " + end_time + ":59", \
              "%m/%d/%Y %H:%M:%S")))
    
    #timestamps.append({'start': start, 'end': end})
    if False == custom:
      timestamps.append({'start': start, 'end': end})
    else:
      days = int((end-start)/TIME_DAY)
      for i in range(0, days):
        timestamps.append({'start': start+(i*TIME_DAY), 'end': end+(i*TIME_DAY)})
        
      if 0 == days:
        timestamps.append({'start': start, 'end': end})
      
  return timestamps
    
    
# Template method to display a string in an HTML message bar
def message_bar(str):
  return '<div class="panel" style="background-color: #FFFF80"><img src="assets/icons/info.gif" width="16" height="16" align="absmiddle">&nbsp; '+str+'</div>'

# Fork into the background
def detach():
  """ Fork the process into the background """
  try:
    pid = os.fork()
  except OSError, e:
    return ((e.errno, e.strerror))
  
  if (pid == 0):
    os.setsid()
    # Ignore HUP signal.
    signal.signal(signal.SIGHUP, signal.SIG_IGN)
    
    try:
      # Fork into a new process
      pid = os.fork()
    except OSError, e:
      return ((e.errno, e.strerror))

    if (pid == 0):
      os.umask(0)
    else:
      # Destroy the old process
      os._exit(os.EX_OK)
  else:
    os._exit(os.EX_OK)


# Builds a DOM tree from /etc/netmon-build.xml
# And builds a dictionarry of all components based on that DOM
def _get_filter(path):
  data = {}
  type = ""
  root_dom = minidom.parse(path)
  child = root_dom.firstChild
  
  # Build the dictionary of components
  filters = child.getElementsByTagName("filter")
  for node in filters:
    data[node.getAttribute('name')] = {'params': {'values': []}}
    for param in node.getElementsByTagName('param'):
      try:
        # Host filters have no filter attributes, just a payload
        value = param.childNodes[0].data
        data[node.getAttribute('name')]['params']['values'].append(value)
        type = "host"
        
        
      except Exception, e:
        # Traffic filters have no payload, just attributes
        size = param.attributes.length
        values = {}
        for i in range(size):
          values[param.attributes.item(i).nodeName] = param.attributes.item(i).nodeValue
        data[node.getAttribute('name')]['params']['values'].append(values)
        type = "traffic"
    
  # Return the unserialized data
  return data

# Returns a parsed filter file unserialized in a data-structure
def netmon_filter(name):
  root = "/apache/registry/filters"
  return _get_filter("%s/%s.xml" % (root, name))

# Converts a number of bits into a larger unit
def tpl_capacity(val_str):
  val_num = float(val_str)
  if (val_num >= 1073741824):
    return "%.2f GB" % (val_num / 1073741824)
  elif (val_num >= 1048576):
    return "%.2f MB" % (val_num / 1048576)
  elif (val_num >= 1024):
    return "%.2f KB" % (val_num / 1024)
  else:
    return "%.2f B" % (val_num)
  
# Retrieves a list of local network ranges from the DB
def get_local_nets():
  query = 'SELECT network AS "start", broadcast AS "stop", id, label FROM localnets'
  try:
    res = db.select(query)
  except Exception, e:
    print "Unable to extract local networks entries"
    return []
  
  return res

def get_local_net_by_id(id):
  query = """
  SELECT network AS "start", broadcast AS "stop", id, label
  FROM localnets
  WHERE id = %s""" % (id)
  
  try:
    res = db.getRow(query)
  except Exception, e:
    print "Unable to retrieve specified local network"
    return {}
  return res

# Cache-based IP address lookup routine
def resolve_ip(ip, tstamp=int(time.time())):
  global IP_CACHE
  
  if (ip in IP_CACHE):
    return IP_CACHE[ip]
  
  """
  query = "SELECT hostname, ip, host_name_type FROM hosts WHERE ip = '%s' ORDER BY timestamp DESC" % ip
  try:
    res = db.select(query)
  except Exception, e:
    IP_CACHE[ip] = ip
    return ip
  
  if (len(res) > 0):
    for row in res:
      if row['host_name_type'] == 'CUSTOM':
        IP_CACHE[ip] = row['hostname']
        return row['hostname']
    IP_CACHE[ip] = res[0]['hostname']
    return res[0]['hostname']
  else:
    IP_CACHE[ip] = ip
    return ip
  """
  query = "SELECT hostname FROM resolve('%s', %s)" % (ip, tstamp)
  try:
    res = db.getRow(query)
  except Exception, e:
    IP_CACHE[ip] = ip
    return ip
  
  IP_CACHE[ip] = res['hostname']
  return res['hostname']
  
  
def resolve_port(port):
  global PORTS_CACHE
  
  if (port in PORTS_CACHE):
    return PORTS_CACHE[port]
  
  query = "SELECT name FROM protocols WHERE port = %s LIMIT 1" % port
  try:
    res = db.getRow(query)
  except Exception, e:
    PORTS_CACHE["%s" % port] = port
    return port
  if None == res:
    PORTS_CACHE["%s" % port] = port
    return port
  
  PORTS_CACHE[port] = res['name']
  return res['name']
  
def get_port_colourblock(port):
  port = int(port)  
  if port <= 25:
    return 1
  elif port <= 50:
    return 2
  elif port <= 75:
    return 3
  elif port <= 100:
    return 4
  elif port <= 150:
    return 5
  elif port <= 250:
    return 6
  elif port <= 500:
    return 7
  elif port <= 1000:
    return 8
  elif port <= 5000:
    return 9
  else:
    return 10

# Truncates a string from the beginning instead of the end.
def truncate_reverse(num, val_str):
  if (len(val_str) <= num):
    return val_str
  return "...%s" % val_str[-num:]

# Truncates a string in the middle, replacing the sequence
# of characters between start and end position by '...' if
# necessary
def truncate_middle(start, end, val_str):
  if (len(val_str) <= start):
    return val_str
  return "%s...%s" % (val_str[:start], val_str[-end:])
  
# Recursive function that generates a human-readable representation
# of a time delta expressed in number of seconds.
def format_time(val):
  # 99% of the time, this will eval to True
  if isinstance(val, dict):
    intervals = [{'year': TIME_YEAR}, {'month': TIME_MONTH},
           {'week': TIME_WEEK}, {'day': TIME_DAY},
           {'hour': TIME_HOUR}, {'minute': TIME_MINUTE}]
    for interval in intervals:
      # Find the largest possible interval that the remainder can divide by.
      if (val['remainder'] >= interval.values()[0]):
        res, val['remainder'] = divmod(val['remainder'], interval.values()[0])
        val['intervals'].append("%s %s%s" % (res, interval.keys()[0], (res <> 1 and "s" or "")))
    # If all we have left is smaller than the smallest interval,
    # Store the remainder as seconds, and leave the recursion loop.
    if (val['remainder'] < intervals[-1].values()[0]):
      if (val['remainder'] > 0):
        val['intervals'].append("%s second%s" % (val['remainder'], (val['remainder'] <> 1 and "s" or "")))
      return ", ".join(val['intervals'])
  # The first time this function is called, it must create a data-structure to
  # store its future calculations.
  else:
    if (0 == val):
      return 'None'
    val = {'remainder': int(val), 'intervals': []}
  return format_time(val)

def get_oid_datatype(type):
  type = str(type).lower()
  if type in ['integer', 'counter32', 'gauge32', 'enum']:
    return 'numeric'
  else:
    return 'string'

class NetmonEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))
        if isinstance(obj, Decimal):
            # wanted a simple yield str(o) in the next line,
            # but that would mean a yield on the line with super(...),
            # which wouldn't work (see my comment below), so...
            return (str(o) for o in [obj])
        try:
          return json.JSONEncoder.default(self, obj)
        except TypeError, x:
          try:
            return list(obj)
          except:
            return x