#import time
#from reportHelper import *

<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>

<!-- (\$Id$) -->

<div class="printOnly">
	<table width="100%" border="0" cellpadding="0">
		<tr>
			<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
			<td><div align="right"><h1>Port Scan Report</h1></div></td>
		</tr>
		<tr>
			<td><div align="right">$time.strftime("%b %d, %y %H:%M:%S")</div></td>
		</tr>
	</table>
	<br /><br />
</div>

<div class="noPrint">
	<div class="titlebar">
		Port Scan Report
	</div>
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('report_portscan');">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<input type="button" value="Configure Alerts" class="button" name="config_alerts" onClick="parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=open_ports');">
	</div>
</div>

#if ($varExists('data') and len($data) > 0)

<div class="datagrid">

	<table width="100%" border="0" cellpadding="3" cellspacing="0" id="report_table">
		<thead>
			<tr>
				<td>Host</td>
				<td>Open Ports</td>
				<td>Last Checked</td>
			</tr>
		</thead>
		<tbody>
			#for $row in $data
			<tr>
				<td align="center"><a href="#" onClick="parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$row.srv_ip}&store_request=2'" title="$row.srv_ip">$truncate_reverse(20, $resolve_ip($row.srv_ip))</a><br /></td>
				<td align="left">
				#if (len($row.ports) > 0)
				#for $port in $row.ports
					<a href="javascript:parent.pnl_right.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer=TCP&port_number=$port');"><img src="assets/colorblocks/block$get_port_colourblock($port)#**#.gif" border="0" align="absmiddle"></a>&nbsp;<a href="javascript:parent.pnl_right.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer=TCP&port_number=$port');">$resolve_port($port) ($port)</a>
					<span class="noPrintInline"><a href="javascript:parent.pnl_right.showEditor('?module=mod_services&action=form_create_service&ip=$row.srv_ip&port=$port&name=$resolve_port($port)&type=tcp&norefresh=1');"><img src="/assets/icons/tracker_add.gif" class="icon" width="11" height="18" alt="Monitor This Service" title="Monitor This Service" border="0" /></a></span><br />
				#end for
				#else
					All ports closed.
				#end if
				</td>
				<td>$row.timestamp.strftime("%b %d, %Y %H:%M:%S")</td>
			</tr>
			#end for
		</tbody>
	</table>

<script language="Javascript">
document.getElementById("report_table").className = 'sort-table';
var types = ['CaseInsensitiveString', 'None', 'netmon_date'];
var report = new SortableTable(document.getElementById("report_table"), types);
report.sort(0, true);	
</script>

#end if