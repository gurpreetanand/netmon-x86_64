#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Backup restore utility for the Netmon Database

This script examines the content of the file /backup_db.xml
and then performs a database restore.

"""

import os
import sys
import string
import commands
from xml.dom import minidom
from optparse import OptionParser
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)

class Netmon_Restore:
	
	def set_params(self):
		self.backup_doc = "%s/backup_db.xml" % self.options.path

	
	
	# Builds a DOM tree from backup XML document
	# And builds a dictionarry of all components based on that DOM
	def build_DOM(self):
		data = {}
		root_dom = minidom.parse(self.backup_doc)
		dbdom = root_dom.firstChild
		
		# Database name to restore?
		self.db = dbdom.getAttribute('name')
		
		# Build the dictionary of components
		tables = dbdom.getElementsByTagName("table")
		for node in tables:
			data[node.getAttribute('index')] = {'type': node.getAttribute('type'), 'name': node.getAttribute('name')}
					
		# Move our data-structure to the class scope
		self.tables = data
	
	"""
	This method restores the databases in correct order
	
	"""
	def restore_data(self):

		self.build_DOM()
		# connect to the db
		db = madnet_db("dbname=%s host=127.0.0.1 user=postgres" % self.db)
		db.set_autocommit(1)
		keys = self.tables.keys()
		key_cmp = lambda x, y: cmp(int(x), int(y))
		keys.sort(cmp=key_cmp)
		print self.tables
		# delete data in reverse order first
		for key in reversed(keys):
			tmp_file="%s/%s.bz2" % (self.options.path,self.tables[key]['name'])
			if os.path.exists(tmp_file):
				self.print_verbose ("Beginning restore of %s" % tmp_file)
				# unzip the file
				cmd = "bunzip2 %s" % tmp_file
				(status, out) = commands.getstatusoutput(cmd)
				self.print_verbose("Output of command %s (returning %d): %s" % (cmd,status,out))
				# delete the existing table if this is a settings table
				#if ("settings"==self.tables[key]['type']):
			query = "DELETE FROM %s" % (self.tables[key]['name'])
			self.print_verbose(query)
			try:
				res = db.delete(query)
			except Exception, e:
				print "The following error occured: %s while trying to delete table %s" % (e,self.tables[key]['name'])

		# restore data in original order
		for key in keys:
			# restore the table 
			cmd = "pg_restore -d %s -t %s %s/%s" % (self.db,self.tables[key]['name'],self.options.path,self.tables[key]['name'])
			#print cmd
			(status, out) = commands.getstatusoutput(cmd)
			self.print_verbose("Output of command %s (returning %d): %s" % (cmd,status,out))
	
	def print_verbose(self, str):
		""" Prints a string to stdout if verbose mode is turned on (and quiet turned off) """
		if (self.options.verbose):
			print str
	
	
if __name__ == "__main__":
	# Prepare the CLI options-parser
	usage       = "Usage: %prog [-p pathname][-v]"
	version     = "%prog (Netmon Inc.) v2.0\nCopyright (c) 2006 Netmon Inc."
	description = "Netmon backup restore tool."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	parser.add_option("-p", "--path", action="store", default="/var/backups/", type="string", dest="path", help="Path of backup data.")
	parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Enable extra output. Useful for debugging")
	

	restore = Netmon_Restore()
	(restore.options, args) = parser.parse_args()
	restore.set_params()

	restore.restore_data()
	
