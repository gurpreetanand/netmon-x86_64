from madnet_db import *


class server:
	def __init__(self, id, name, ip, interval, logging, timestamp):
		self.id=id
		self.name=name
		self.ip=ip
		self.interval=interval
		self.logging=logging
		self.timestamp=timestamp
		self.working=True
		
class interface:
	def __init__(self, name, ip, timestamp, interval):
		self.name=name
		self.ip=ip
		self.timestamp=timestamp
		self.interval=interval
		self.working=True
		
class srvmond_log_check:
	def __init__(self, db):
		self.db=db
	
	def getServers(self):
		rows=self.db.select("select srv_id, name, ip, interval, log_timeout, timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed from servers")
		self.servers=[]
		for r in rows:
			self.servers.append(server(r["srv_id"], r["name"], r["ip"], r["interval"], r["log_timeout"], r["timestamp"]))
			if r["elapsed"] > (r["interval"]*2):
				self.servers[len(self.servers)-1].working=False
		return self.servers
			
class snmpmond_log_check:
	def __init__(self, db):
		self.db=db
	
	def getServers(self):
		rows=self.db.select("select name, ip_address, timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed, interval from interfaces inner join devices on device_id=devices.id and enable_logging=true")
		self.servers=[]
		for r in rows:
			self.servers.append(interface(r["name"], r["ip_address"], r["timestamp"], r["interval"]))
			if r["elapsed"] > (r["interval"]*5):
				self.servers[len(self.servers)-1].working=False
		return self.servers
			
class netmond_log_check:
	def __init__(self, db):
		self.db=db
	
	def getLastUpdate(self):
		rows=self.db.select("select timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed from netflow where flow_src is null order by timestamp desc limit 1")
		if len(rows) == 0:
			rows=self.db.select("select timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed from agg_netflow where flow_src is null order by timestamp desc limit 1")
		for r in rows:
			return (r["timestamp"], r["elapsed"])
		return None

class netflowd_log_check:
	def __init__(self, db):
		self.db=db
	
	def getLastUpdate(self):
		rows=self.db.select("select timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed from netflow where flow_src is not null order by timestamp desc limit 1")
		if len(rows)==0:
			rows=self.db.select("select timestamp::int4::abstime as timestamp, extract(epoch from localtimestamp)-timestamp as elapsed from agg_netflow where flow_src is not null order by timestamp desc limit 1")
		for r in rows:
			return (r["timestamp"], r["elapsed"])
		return None

if __name__ == '__main__':
	db=madnet_db("user=root dbname=netmon35 host=127.0.0.1")
	
	x = srvmond_log_check(db)
	print "Service Monitors------------------------------"
	print "Server Name\t\tIP Addr\t\tIntvl\tLast Check\t\tRecording"
	for s in x.getServers():
		print "%s\t%s\t%d\t%s\t%s" % (s.name.ljust(20), s.ip, s.interval, s.timestamp, s.working)
	print ''
	
	x = snmpmond_log_check(db)
	print "SNMP Monitors---------------------------------"
	print "Interface Name\t\tIP Addr\t\tIntvl\tLast Check\t\tRecording"
	for s in x.getServers():
		print "%s\t%s\t%d\t%s\t%s" % (s.name.ljust(20), s.ip, s.interval, s.timestamp, s.working)
	print ''
	
	x = netmond_log_check(db)
	print "Packet Sniffer-------------------------------"
	print "Last Update\t\t\tTime Since"
	res= x.getLastUpdate()
	if res != None:
		print "%s\t\t%02d:%02d:%02d" % (res[0], int(res[1] / 3600), int((res[1]%3600)/60), int((res[1] % 3600) % 60))
		print ''
	
	x = netflowd_log_check(db)
	print "Netflow Sniffer------------------------------"
	print "Last Update\t\t\tTime Since"
	res= x.getLastUpdate()
	if res != None:
		print "%s\t\t%02d:%02d:%02d" % (res[0], int(res[1] / 3600), int((res[1]%3600)/60), int((res[1] % 3600) % 60))
		print ''
	


