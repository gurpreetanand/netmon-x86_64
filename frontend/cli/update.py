#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Patch management system for the Netmon suite

This script examines the content of the file /etc/netmon-build.xml
and then synchronizes the local patch repository located at
/usr/local/netmon_updates from the central Netmon Inc. patch
repository through rsync, only downloading patches that apply to
the currently installed major release of Netmon.

Once the patch registry is up to date, this script will compare the
version of each component with the versions available through the
patch repository and automatically apply any new update in order
and for each component.

Every time a component has been updated, the system will update the
XML file, which will allow it to resume in case of failure.

"""

import os
import sys
import string
from xml.dom import minidom
from optparse import OptionParser

class UnknownProtocolError(Exception):
	def __init__(self, error):
		self.error = error
	def __str__(self):
		return repr(self.error)

class Netmon_Updater:
	
	# Vars declarations and initializations
	components   = {}
	version      = ""
	local_repo   = "/usr/local/netmon_updates"
	registry     = "/etc/netmon-build.xml"
	#repo_host    = "dev.netmon.ca"
	
	#rsync_mirror = "rsync://%s/updates" % repo_host
	#ftp_mirror   = "updates:netmon@%s"  % repo_host
	#http_mirror  = "http://%s/updates/" % repo_host
	
	
	
	# Constructor. Create a data structure based on the XML document.
	def __init__(self):
		self.build_DOM()
		
	def set_params(self):
		self.repo_host    = self.options.host
		self.rsync_mirror = "rsync://%s/updates" % self.repo_host
		self.ftp_mirror   = "updates:netmon@%s"  % self.repo_host
		self.http_mirror  = "http://%s/updates/" % self.repo_host
	
	
	
	# This is a wrapper for the functions that deal with performing upgrades.
	def update_system(self):
		
		if (self.options.summary):
			self.print_summary()
			sys.exit(0)
		
		try:
			self.sync_repository()
		except UnknownProtocolError, e:
			print "Please specify a supported protocol and try again\n%s" % e
			sys.exit(1)
			
		self.apply_patches()
		self.print_summary()
	
	
	
	
	
	# Synchronize the patch repository with the Netmon Inc.'s official
	# Patch repository for this version of the product.
	def sync_repository(self):
		
		if (self.options.protocol == "rsync"):		
			# will build "rsync -crvz rsync://mirror/ver/ /local_repo/ver/"
			command = "rsync --bwlimit=10 -crvz --delete %s/%s/ %s/%s/" % (self.rsync_mirror, self.version, self.local_repo, self.version)
		elif (self.options.protocol == "ftp"):
			command = "lftp -c 'set ftp:list-options -a; open -e \"mirror %s %s\" %s'"% (self.version, self.local_repo, self.ftp_mirror)
		elif (self.options.protocol == "http"):
			command = "lftp -c 'set ftp:list-options -a; open -e \"mget %s.tbz2 -O %s/%s\" %s' && cd / && tar xvjf %s/%s/%s.tbz2 > /dev/null && rm -rf %s/%s/%s.tbz2" % (self.version, self.local_repo, self.version, self.http_mirror, self.local_repo, self.version, self.version, self.local_repo, self.version, self.version)
			
		else:
			raise UnknownProtocolError, "The protocol '%s' is not supported" % self.options.protocol
		
		self.print_verbose("Synchronizing with Netmon Update Repositories")
		self.print_verbose(command)
		os.system(command)
	
	
	
	
	
	# Builds a DOM tree from /etc/netmon-build.xml
	# And builds a dictionarry of all components based on that DOM
	def build_DOM(self):
		data = {}
		root_dom = minidom.parse(self.registry)
		netmon_dom = root_dom.firstChild
		
		# Find out which major release is currently installed
		self.version = netmon_dom.getAttribute('version')
		
		# Build the dictionarry of components
		components = netmon_dom.getElementsByTagName("component")
		for node in components:
			data[node.getAttribute('name')] = {'version': node.getAttribute('version'), 'desc': node.getAttribute('desc')}
			
		# Move our data-structure to the class scope
		self.components = data
	
	"""
	
	This method creates a list of directories in each component
	container, then orders it numerically, which will effectively
	provide it with a list of all version numbers available for
	each component.
	
	It then determines which patches need to be applied by comparing
	current version number for each component with the version numbers
	available and will process them in numeric order if any are
	available. 
	
	To apply a patch, the method looks for the file 'patch.py' in the
	directory named after the version number, and dynamically loads
	that file. The patch.py file must contain a class called Netmon_Updater.
	
	The method will then instanciate a Netmon_Updater object, and call
	the object's apply_patch() method.
	
	Once the apply_patch() method returns, the method will update the
	version number in the class-scope data-structure that holds metadata
	about each component, and then call the update_DOM method, which will
	create a new XML file with the new version number.
	
	Once a patch has been applied and the XML document has been updated,
	this method will proceed to the next patch to apply for this component
	(if any), and will then proceed to the next component (if any).
	
	Once all components have been fully patched, the function will simply
	return.
	
	"""
	def apply_patches(self):
		for component in self.components.keys():
			# (re)Initialize version dictionaries
			int_versions = []
			str_versions = []
			
			# Retrieves a list of patch versions
			str_versions = os.listdir("/usr/local/netmon_updates/%s/%s/" % (self.version, component))
			
			# We need to typecast version numbers (if any) from unicode str to int
			# And then sort them.
			if len(str_versions) == 0:
				continue
				
			# Perform the typecast and get rid of the old structure
			for ver in str_versions:
				int_versions.append(int(ver))
			del(str_versions)
			
			# Do a numeric sort to apply the patches in the proper order
			int_versions.sort(cmp)
			
			# Apply patches for each of the incremental patches available.
			for v in int_versions:
				if v > int(self.components[component]['version']):
					self.print_verbose("Applying patch: %s %s => %s" % (component, self.components[component]['version'], v))
					patchfile = "%s/%s/%s/%s/patch.py" % (self.local_repo, self.version, component, v)
					
					if (os.path.exists(patchfile)):
						os.chdir("%s/%s/%s/%s" % (self.local_repo, self.version, component, v))
						execfile(patchfile)
					
						exec("patch = Netmon_Patch()")
						#dir(patch)
						patch.apply_patch()
						del(patch)
						exec("del(Netmon_Patch)")
					
						self.components[component]['version'] = "%s" % v
						self.update_DOM()
					else:
						print "Patch-file not found."
			
			
	
	
	
	# Update the DOM structure and dump it back in the XML file.
	def update_DOM(self):
		components_str = ""
		
		# Build a new set of components to include in the XML payload
		for component in self.components.keys():
			components_str = """%s\n\t<component name="%s" desc="%s" version="%s" />""" % \
						   (components_str, \
							component, \
							self.components[component]['desc'], \
							self.components[component]['version'])
			
		xml_str = "<netmon version=\"%s\">%s\n</netmon>\n\n" % (self.version, components_str)
		
		fp = open(self.registry, "w")
		fp.write(xml_str)
		fp.close()
		
		self.print_verbose("XML file updated successfully")
		
	
	
	# Displays the component status in a pretty table
	def print_summary(self):
		print "\n\nThis system is currently running Netmon v%s" % self.version
		print "The following components are currently installed:"
		
		
		# Calculate string sizes to format the summary table
		max_desc = max(map(lambda x: len(self.components[x]['desc'])+2, self.components.keys()))
		max_comp = max(map(lambda x: len(x)+2, self.components.keys()))
		max_vers = max(max(map(lambda x: len(self.components[x]['version'])+2, self.components.keys())), len("Version"))
		
		print "Name".center(max_comp),
		print "Description".center(max_desc),
		print "Version".center(max_vers)
		print "=" * (max_comp+max_vers+max_desc+2)
		
		for component in self.components.keys():
			print string.ljust(component, max_comp),
			print string.ljust(self.components[component]['desc'], max_desc),
			print string.ljust(self.components[component]['version'].__str__(), max_vers)
			

	def print_verbose(self, str):
		""" Prints a string to stdout if verbose mode is turned on (and quiet turned off) """
		if (self.options.verbose):
			print str
	
if __name__ == "__main__":
	# Prepare the CLI options-parser
	usage       = "Usage: %prog [-p protocol] [-o hostname] [-s] [-v]"
	version     = "%prog (Netmon Inc.) v2.0\nCopyright (c) 2006 Netmon Inc."
	description = "Netmon automatic update system."
	parser = OptionParser(usage=usage, version=version, description=description)
	
	parser.add_option("-p", "--protocol", action="store", default="rsync", type="string", dest="protocol", help="Protocol to use to locate/retrieve patches (rsync,http,ftp)")
	parser.add_option("-s", "--summary", action="store_true", default=False, dest="summary", help="Run in summary mode (do not actually retrieve or apply any patch)")
#	parser.add_option("-x", "--proxy", action="store", type="string", dest="proxy", help="Use the specified proxy server to retrieve patches")
	parser.add_option("-o", "--host", action="store", dest="host", default="dev.netmon.ca", help="Host where the patch repository resides")
	parser.add_option("-v", action="store_true", dest="verbose", default=False, help="Enable extra output. Useful for debugging")
	

	updater = Netmon_Updater()
	(updater.options, args) = parser.parse_args()
	from react import Reactor
        try:
                r=Reactor(updater.options, parser)
        except Exception, e:
                print "There was a problem reactivating your device, please contact support@netmon.ca"
		print e
                sys.exit(0)
	updater.set_params()
	
	
	updater.update_system()
	
