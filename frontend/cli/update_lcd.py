#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Update Netmon Enterprise LCD Display 
# Created:   28/05/07 01:18:24 PM
# SVN:       $Id$

# Imports
import os
import sys
import time
import signal
import string
import os.path

# CLI Arguments Parser
from optparse import OptionParser

# LCD Update command path & base arguments
CMD = "/usr/local/lcd/test633 /dev/ttyS1 19200 noclear"

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)
	
# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e

class LCD_Manager:
	"""Update Netmon Enterprise LCD Display """
	
	# Current PID
	_pid = os.getpid()
	
	# PID File
	pid_file = '/var/run/netmon_lcd.pid'

	def __init__(self, options, parser):
		""" Constructor """
		self.options = options
		self.parser  = parser
		
		# Install SIGINT handler for graceful shutdown
		signal.signal(signal.SIGINT, self.sigint_handler)
		
		# Check for other running LCD updaters
		self.resolve_pid_conflicts()
			
		while(True):
			try:
				self.print_verbose("Scheduling next alert display round...")
				self.updateDisplay()
			except Exception, e:
				sys.exit(os.EX_OK)
			
	def sigint_handler(self, sig, frame):
		try:
			cmd = "%s \"%s\" \"%s\" > /dev/null" % (CMD, string.center("Netmon Status", 16), string.ljust("Stopping Netmon", 16))
			self.print_verbose(cmd)
			os.system(cmd)
		except Exception, e:
			pass
		self.cleanup()
		
	def cleanup(self):
		print "Cleanup() called"
		os.unlink(self.pid_file)
		sys.exit(os.EX_OK)
		
	def resolve_pid_conflicts(self):
		if os.path.isfile(self.pid_file):
			self.print_verbose("PID file already in use. Killing old process")
			pid = open(self.pid_file, 'r').readline().strip()
			self.print_verbose("Sending SIG_INT to process %s" % pid)
			os.kill(int(pid), 2)
			time.sleep(2)
		self.print_verbose("Locking PID file")
		# Lock the PID file for future instances
		open(self.pid_file, 'w').write("%s" % self._pid)
	
	
	def updateDisplay(self):
		alerts = self.getAlerts()
		
		if len(alerts) > 0:
			for alert in alerts:
				for msg in alert:
					cmd = "%s \"%s\" \"%s\" > /dev/null" % (CMD, string.center(msg['field'], 16), string.ljust(msg['message'][:16], 16))
					os.system(cmd)
					self.print_verbose(cmd)
					time.sleep(4)
				time.sleep(8)
		else:
			self.print_verbose("No alerts")
			cmd = "%s \"%s\" \"%s\" > /dev/null" % (CMD, string.center(self.options.header, 16), string.ljust("No Alerts", 16))
			self.print_verbose(cmd)
			os.system(cmd)
			time.sleep(8)
		return
	
	def getAlerts(self):
		alerts = []
		
		# Get a list of ICMP and TCP services that are down
		query = "SELECT * FROM servers WHERE status = 'DOWN' ORDER BY srv_id ASC"
		try:
			res = db.select(query)
		except Exception, e:
			self.print_verbose(e)
			pass
		
		for row in res:
			alert = []
			alert.append({'field': "IP Address", 'message': row['ip']})
			alert.append({'field': "Service Name", 'message': row['name']})
			alert.append({'field': "Service Status", 'message': "SERVICE DOWN"})			
			alerts.append(alert)
			
		# Get a list of URLs unavailable
		query = "SELECT * FROM urls WHERE status ILIKE 'NO MATCH' OR status ILIKE 'DOWN' ORDER BY id ASC"
		try:
			res = db.select(query)
		except Exception, e:
			pass
		
		for row in res:
			alert = []
			if len(row['url']) > 16:
				alert.append({'field': "URL", 'message': row['url'][:13] + "..."})
				alert.append({'field': "URL", 'message': "..." + row['url'][-13:]})
			else:
				alert.append({'field': "URL", 'message': row['url']})
			alert.append({'field': "Pattern", 'message': row['pattern']})
			alert.append({'field': "Status", 'message': row['status']})
			alert.append({'field': "Server Message", 'message': row['message']})
			alerts.append(alert)
			
		query = """
		SELECT message, ip, partition AS \"share\", 'DF' AS \"type\", status AS percent 
		FROM df_servers WHERE status >= threshold
		
		UNION
		
		SELECT message, ip, share, 'SMB' as \"type\", status AS percent
		FROM smb_servers WHERE status >= threshold
		"""
		
		try:
			res = db.select(query)
		except Exception, e:
			pass
		
		for row in res:
			alert = []
			alert.append({'field': 'Partition', 'message': row['share']})
			alert.append({'field': 'Share IP', 'message': row['ip']})
			alert.append({'field': 'Utilization', 'message': "%s%% Used" % row['status']})
			alert.append({'field': 'Server Message', 'message': row['message']})
			alerts.append(alert)
			
		return alerts
			

	def print_verbose(self, str):
		if (self.options.verbose):
			print str

def is64():
	exec 'import re'
        exec 'import commands'
        regex = ".*amd64.*"
        out = commands.getoutput('uname -a')
        res= re.search(regex, out)
        return res!=None;


if __name__=='__main__':
	# Jump to the script's root
	# 20181217 - Fixed Error of No file or directory '' - Gurpreet Anand
  	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
	
	if (is64()==False):
		#print "This device is not equipped with an LCD display. Skipping..."
		sys.exit(0)

	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Update Netmon Enterprise LCD Display "
	parser = OptionParser(usage=usage, version=version, description=description)
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	parser.add_option("-t", "--header_text", action="store", dest="header", default="Network Status", help="Append Text")

	(options, args) = parser.parse_args()
	obj = LCD_Manager(options, parser)
