#!/usr/bin/env python
# Author:    Xavier Spriet <xavier@netmon.ca>
# Copyright: Netmon Inc.
# Purpose:   Madnet Timing Module 
# Created:   05/04/07 01:54:57 PM
# SVN:       $Id$

# Imports
import os
import time

class madnetTimer:
	"""Madnet Timing Module """

	# CPU clock start-time
	load_start = os.getloadavg()
	# Actual start-time
	user_start = time.time()

	def __init__(self):
		""" Constructor """
		pass
		
	def profile(self):
		# Calculate load deltas
		deltas = []
		load = os.getloadavg()
		for i in range(len(load)):
			deltas.append(load[i] - self.load_start[i])
			
		return {'load_delta': deltas, \
				'user': "%.4f" % (time.time() - self.user_start)}

