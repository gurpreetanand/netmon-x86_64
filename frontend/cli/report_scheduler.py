#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author:   Xavier Spriet <xavier@netmon.ca>
# Purpose:  Report Scheduler
# Created:  01/02/2007

# Imports
import os
import sys
import os.path
import smtplib

# CLI Arguments Parser
from optparse import OptionParser

# XML DOM Parser
from xml.dom import minidom

# Rely on the snapshot class to retrieve SMTP settings
from snapshot import Log_Snapshot

# Try to load the DB driver or abort
try:
	from madnet_db import *
except Exception, e:
	print e
	sys.exit(1)

# Connect to the database
try:
	db = madnet_db("dbname=netmon35 host=127.0.0.1 user=postgres")
	db.set_autocommit(0)
except Exception, e:
	print "ERROR: %s" % e
	#signal_handler(0,0)
	sys.exit(1)
	
class Report_Scheduler:
	def __init__(self, options, parser):
		self.options = options
		
	def get_events_list(self):
		# Retrieves a list of pending events using a SQL query and some timing info
		pass
	
	def instanciate_report(self, report, options):
		# Fork a new process and run the report managers.
		pass
		
if ('__main__' == __name__):
	usage       = "Usage: %prog [-v --verbose]"
	version     = "%prog (Netmon Inc.) v1.0\nCopyright (c) 2007 Netmon Inc."
	description = "Netmon Report Scheduling System"
	parser = OptionParser(usage=usage, version=version, description=description)
	parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Enable extra output")
	
	
	(options, parser) = parser.parse_args()
	Scheduler = Report_Scheduler(options, parser)