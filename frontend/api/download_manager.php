<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @package    MADNET
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */



/**
  * @author Xavier Spriet
  * Loads core configuration file
  */
require_once("config.inc.php");



/**
  * @author Xavier Spriet
  * Load the Controler Facade
  */
require_once(ROOT . "/modules/core/classes/controler_facade.class.php");

/**
  * @package    MADNET
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * Dispatches a query and gives full control of the output and headers to the module action.
  */
class Download_Manager
{

	/*** Attributes: ***/

	/**
	 * Dummy buffer.
	 * @access public
	 * @var    string $output
	 */
	var $output;


	/**
	 * Aggregated controler facade object.
	 * @access private
	 * @var    object $controler
	 */
	var $controler;


	/**
	 * Constructor and main request handler
	 *
	 * @return Index
	 * @access public
	 */
	function Download_Manager( )
	{
		/**
		  * Initialization of aggregate objects
		  *
		  */
		$this->controler = new Controler_Facade;

		$registry = Registry::get_registry();



		/**
		  * Main Query Processing
		  * The controler will return the TITLE while the module will access $index_content.
		  */
		$this->controler->process_query($this->output);


	} // end of member function Index




} // end of Index

/* Instanciate the index object */
$dm = new Download_Manager();



?>
