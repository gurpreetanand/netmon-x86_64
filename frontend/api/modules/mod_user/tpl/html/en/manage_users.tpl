<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("users").className = 'sort-table';
	var types = [null, 'CaseInsensitiveString', 'CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("users"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">Manage User Accounts</div>
<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="document.location.reload();" title="Refresh">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	{input type="button" class="button" value="Add New User" onClick="parent.pnl_right.showEditor('?module=mod_user&action=form_create_user&root_tpl=blank_panel');"}
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_help.gif" class="icon" onClick="parent.pnl_right.showHelp('manage_users');" title="Show Help"> 
</div>

{if $ids}
<div class="datagrid center">
<table width="100%" cellpadding="3" cellspacing="0" border="0" id="users">
	<thead>
	<tr>
		<td width="22">&nbsp;</td>
		<td>Name</td>
		<td>Email</td>
		<td>Actions</td>
	</tr>
	</thead>

{foreach from=$ids item="id"}
	{if $acm->pop($id.id)}
	{assign var="acm_id" value=$acm->get("id")}
	<tr>
		<td width="22"><a href="#" onclick="parent.pnl_right.showEditor('?module=mod_user&action=account_details&id={$acm_id}&root_tpl=blank_panel');"><img src="/assets/icons/users.gif" width="16" height="16" class="icon"></a></td>
		<td>{$acm->get("last_name")|capitalize|cat:", "|cat:$acm->get("first_name")|capitalize}</td>
		<td>{mailto address=$acm->get("email") encode="javascript" text=$acm->get("email")|truncate:20:"...":true}</td>
		<td>
		<a href="#" onclick="parent.pnl_right.showEditor('?module=mod_user&action=account_details&id={$acm_id}&root_tpl=blank_panel');">Details</a> |
		
		<a href="#" onClick="parent.pnl_right.showEditor('?module=mod_user&action=edit_account&id={$acm_id}&root_tpl=blank_panel')">Edit</a> |
		
		{if $acm->get("active")}
			{assign var="action" value="suspend"}
		{else}
			{assign var="action" value="reactivate"}
		{/if}
		
		
		<a href="#" onClick="parent.pnl_right.showEditor('?module=mod_user&action={$action}_account&id={$acm_id}&root_tpl=blank_panel');">{$action|capitalize}</a> |
		<a href="#" onclick="if (confirm('Are you sure you wish to delete {$acm->get("first_name")|cat:" "|cat:$acm->get("last_name")|capitalize|escape:"javascript"|escape:"html"}\'s user account?')) {ldelim} parent.pnl_right.showEditor('?module=mod_user&action=delete_account&id={$acm_id}'); {rdelim}">Delete</a></td>
	</tr>
	{/if}
{/foreach}
</table>
{else}
{"There are currently no users in the database."|message_bar}
{/if}

<!-- end of {$smarty.template} -->

