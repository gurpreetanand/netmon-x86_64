<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

// mod_user.class.php

/**
  * User Module Control Object (MCO)
  *
  * Performs all high-level operation on user accounts.
  * This class controls all Element and SubElement objects in the module
  * It is also the model facade for the controler
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_user extends MadnetModule {

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    require_class($this->module, "account_manager");
    require_class($this->module, "group_manager");
  }

        /**
          * Performs authentication against POST data (through Account_Manager)
          *
          */
        function auth() {
                $payload = json_decode(file_get_contents("php://input"), true);

                $acm = new Account_Manager;

                $loggedIn = $acm->login($payload["username"], $payload["password"]);

                if ($loggedIn) {
                        print "{}";
                } else {
                        header('HTTP/1.1 401 Unauthorized', true, 401);
                        header('Content-Type: application/json');
                        print "{}";
                }
                return;
        }

        /**
    * Returns a simple "Welcome Home, first_name" message.
    *
    * We will use this later for dashboards, framesets, etc...
    *
    * @param reference &$index_content
    * return string
    */
  function home(&$index_content) {
    $_GET['root_tpl'] = "blank";
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("frameset.tpl");
    # $index_content .= "Welcome home, <strong>" . $_SESSION['first_name'] . "</strong><br>";
    # return $_SESSION['first_name'] . "'s Home";
  }

  /**
    * Destroys the current session and confirm successful logout
    *
    * @param reference &$index_content
    * @return string
    */
  function logout(&$index_content) {
    session_unset();
    session_destroy();
    return "Logout successful";
  }

  /**
    * Displays the new user creation form
    *
    * First, ensure that the current user has permission to perform this type
    * of operation, then initialize the parser and an Account_Manager element,
    * pass the Account_Manager's params_manager instance to the parser so it
    * can use it to render the input fields properly.
    *
    * Once this is all set, display the form.
    *
    * @param reference &$index_content
    * @return String
    */
  function form_create_user(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);

    $acm = new Account_Manager;

    $parser->set_params($acm->params);


    $index_content .= $parser->fetch("form_create_user.tpl");

    return "Create new user account";
  }

  /**
    * Processes POST data and creates new user account
    *
    * Interacts with the Account_Manager object to process
    * the data passed to POST from the user creation form.
    *
    * If we cannot create the new user account for any reason,
    * have the parser display the form again. The params_manager
    * bound to it will display the visual error hints, the error_manager
    * will explain every error in detail, and the values will be
    * pre-filled by the templating engine based on POST data.
    *
    * @param reference $index_content
    * @return string
    */
  function process_create_user(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $this->debugger->add_hit("Initializing new account_manager for processor");
    $acm = new Account_Manager();
    
    # Force the password field for new inserts, but not for updates.
    $acm->params->primitives['passwd']['mandatory'] = TRUE;

    if (!$acm->insert()) {
      # Note: This will instanciate a *second* AccountManager object
      # If we want to reduce memory footprint, this is as good a
      # start as any.
      $this->err->err_from_code(400, "Error adding user");
      return "Error";
    } else {
      print '{}';
      return "Success";
    }
  }

  /**
    * Displays the user management dialog
    *
    * The method attempts to obtain security clearance for this area.
    *
    * It then uses an account_manager to act as a data-source for the
    * templating engine which will display all the user accounts in a
    * pretty dialog.
    *
    * @param reference $index_content
    * @return string
    */
  function manage_users(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);

    $acm = new Account_Manager();

    $ids = array();

    if (!$ids = $acm->get_all_ids()) {
      $this->err->err_from_string("The application was not able to retrieve a list of user accounts");
      return "Error";
    } else {
      $this->debugger->add_hit("Retrieved a list of account IDs", NULL, NULL, vdump($ids));
      $parser->assign_by_ref("ids", $ids);
      $parser->assign_by_ref("acm", $acm);
      $index_content .= $parser->fetch("manage_users.tpl");
      return "User Management";
    }

  }
  
  function get_users_groups() {
    require_class($this->module, "sub_group_permissions");
    $sgm = new sub_group_permissions();
    $users = $this->get_users_object();
    
    foreach ($users as &$user) {
      $user['sub_account_group'] = array();
      $ug = $this->db->select("SELECT group_id FROM user2groups WHERE user_id=" . $user['id']);
      foreach ($ug as $group) {
        array_push($user['sub_account_group'], $group['group_id']);
      }
    }
    
    $payload = array(
      "users" => $users,
      "groups" => $this->get_groups_object(),
      "permissions" => $sgm->get_all_perms()
    );
    
    print json_encode($payload, true);
  }
  
  /**
    * Displays the user edit form
    *
    * Works exactly like the form creation method, except that the
    * template is different and we need an account-manager to act
    * as data-source to fill out the fields.
    *
    * @param reference &$index_content
    * @return string
    */
  function edit_account(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);

    $acm = new Account_Manager();
    $parser->set_params($acm->params);

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("An account ID was not passed along with this request.");
      return "Error";
    }

    if (!$acm->pop($id)) {
      $this->err->err_from_string("Unable to fetch account information");
      return "Error";
    }

    $parser->assign_by_ref("acm", $acm);

    $index_content .= $parser->fetch("form_edit_user.tpl");
    return "Account Edit Form";

  }

  /**
    * Updates a user account based on POST data
    *
    * @see $this->processcreate_user
    * @param reference &$index_content
    * @return string
    */
  function process_update_user() {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $acm = new Account_Manager();

    if (!$acm->update()) {
      $this->err->err_from_code(400, "Unable to update user");
      return "Error";
    } else {
      print '{}';
    }
  }

  /**
    * Suspends a user account
    *
    * @param reference &$index_content
    * @return string
    */
  function suspend_account(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $acm = new Account_Manager();

    $id = intval($_GET['id']);

    if ($id == 0) {
      $this->err->err_from_string("An account ID was not passed along with this request.");
      return "Error";
    }

    if (!$acm->suspend($id)) {
      $this->err->err_from_string("Unable to suspend user account #$id");
      return "Error";
    }


    $parser = new Parser($this->tpl_dir);
    $parser->assign("action_taken", "suspended");
    $index_content .= $parser->fetch("account_modified.tpl");

    #return $this->manage_users($index_content);
    return;

  }

  /**
    * Reactivates a user account
    *
    * @param reference &$index_content
    * @return string
    */
  function reactivate_account(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $acm = new Account_Manager();

    $id = intval($_GET['id']);

    if ($id == 0) {
      $this->err->err_from_string("An account ID was not passed along with this request.");
      return "Error";
    }

    if (!$acm->reactivate($id)) {
      $this->err->err_from_string("Unable to re-activate user account #$id");
      return "Error";
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign("action_taken", "reactivated");
    $index_content .= $parser->fetch("account_modified.tpl");

    #$index_content .= message_bar("User account has been re-activated successfully");
    return ;

    #return $this->manage_users($index_content);
  }

  /**
    * Deletes a user account
    *
    * @param reference &$index_content
    * @return string
    */
  function delete_account() {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $acm = new Account_Manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("An account ID was not passed along with this request.");
      $this->err->err_from_code(400, "An account ID was not passed along with this request.");
      return "Error";
    }

    if (!$acm->delete($id)) {
      $this->err->err_from_string("Unable to delete user account #$id");
      $this->err->err_from_code(400, "Unable to delete user account #$id");
      return "Error";
    }


    print '{}';
  }

  /**
    * Displays the account details dialog
    *
    * @param reference &$index_content
    * @return string
    */
  function account_details(&$index_content) {

    if (!has_perm("user_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $acm = new Account_Manager();
    $parser = new Parser($this->tpl_dir);
    $parser->set_params($acm->params);
    $id = intval($_GET['id']);

    if ($id == 0) {
      $this->err->err_from_string("An account ID was not passed along with this request.");
      return "Error";
    }

    if (!$acm->pop($id)) {
      $this->err->err_from_string("Unable to fetch account information");
      return "Error";
    }


    $parser->assign_by_ref("acm", &$acm);


    $index_content .= $parser->fetch("account_details.tpl");
    return "Account Details";

  }



  /* GROUP MANAGEMENT */
  ##################################################################################




  /**
    * Displays the group creation dialog
    *
    * @see $this->form_create_user()
    * @param reference &$index_content
    * @return string
    */
  function form_create_group(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);
    $gm = new Group_Manager;

    $parser->set_params($gm->params);

    $index_content .= $parser->fetch("form_create_group.tpl");
    return "Group Creation Form";
  }



  /**
    * Creates a group based on POST data
    *
    * @see $this->process_create_user()
    * @param reference &$index_content
    * @return string
    */
  function process_create_group(&$index_content) {


    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $this->debugger->add_hit("Initializing new group_manager for processor");
    $gm = new Group_Manager();

    if (!$gm->insert()) {
      $this->err->err_from_code(400, "Unable to create group");
      return "Error";
    } else {
      print '{}';
    }
  }


  /**
    * Displays the group management dialog
    *
    * @see $this->manage_users()
    * @param reference &$index_content
    * @return string
    */
  function manage_groups(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);

    $gm = new Group_Manager();

    $ids = array();

    if (!$ids = $gm->get_all_ids()) {
      $this->err->err_from_string("The application was not able to retrieve a list of account groups");
      return "Error";
    } else {
      $this->debugger->add_hit("Retrieved a list of group IDs", NULL, NULL, vdump($ids));
      $parser->assign_by_ref("ids", $ids);
      $parser->assign_by_ref("gm", $gm);
      $index_content .= $parser->fetch("manage_groups.tpl");
      return "Group Management";
    }

  }


  /**
    * Displays the group edit form
    *
    * @see $this->edit_user
    * @param reference &$index_content
    * @return string
    */
  function edit_group(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $parser = new Parser($this->tpl_dir);

    $gm = new Group_Manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A group ID was not passed along with this request.");
      return "Error";
    }

    if (!$gm->pop($id)) {
      $this->err->err_from_string("Unable to fetch group information");
      return "Error";
    }


    $parser->assign_by_ref("gm", &$gm);
    $parser->set_params($gm->params);

    $index_content .= $parser->fetch("form_edit_group.tpl");

    return "Group Edit Form";

  }

  /**
    * Updates a group based on POST data
    *
    * @see $this->process_update_user()
    * @param reference &$index_content
    * @return string
    */
  function process_update_group(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $gm = new Group_Manager();

    if (!$gm->update()) {
      $this->err->err_from_code(400, "Unable to update group");
      return "Error";
    } else {
      print '{}';
      return "Success";
    }
  }

  /**
    * Deletes a group
    *
    * @see $this->delete_user()
    * @param reference &$index_content
    * @return string
    */

  function delete_group(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
    }

    $gm = new Group_Manager();

    $id = intval($_GET['id']);

    if ($id <= 0) {
      $this->err->err_from_string("A group ID was not passed along with this request.");
      $this->err->err_from_code(400, "A group ID was not passed along with this request.");
      return "Error";
    }

    if (!$gm->delete($id)) {
      $this->err->err_from_string("Unable to delete group #$id");
      $this->err->err_from_code(400, "Unable to delete group #$id");
      return "Error";
    }

    print '{}';
    return;

  }

  /**
    * Displays the group details dialog
    *
    * @param reference &$index_content
    * @return string
    */
  function group_details(&$index_content) {

    if (!has_perm("group_management")) {
      $this->err->err_from_code(403, "You do not have access to this area");
      return "Permission Error";
    }

    $gm = new Group_Manager();
    $parser = new Parser($this->tpl_dir);
    $parser->set_params($gm->params);
    $id = intval($_GET['id']);

    if ($id == 0) {
      $this->err->err_from_string("Agroup ID was not passed along with this request.");
      return "Error";
    }

    if (!$gm->pop($id)) {
      $this->err->err_from_string("Unable to fetch group information");
      return "Error";
    }


    $parser->assign_by_ref("gm", &$gm);


    $index_content .= $parser->fetch("group_details.tpl");
    return "Group Details";

  }

  /**
    * Refreshes the structure of the session stack
    *
    * MADNET uses session versioning to keep track of the expected structure
    * of the session stack. If the version is less than the expected one,
    * this method will rebuild the stack and synchronize it in a way that is
    * completely transparent to the end-user.
    *
    * @param reference &$index_content
    * @return string
    */
  function refresh_session(&$index_content) {
    $acm = new Account_Manager();

    // This key must be consistent accross all versions, just like VERSION
    $id = intval($_SESSION['id']);

    if ($id == 0) {
      $this->err->err_from_string("Incompatible session versions. You will need to re-authenticate");
      return $this->logout(&$index_content);
    }

    $this->debugger->add_hit("Refreshing session to latest structure");
    $acm->sync_session(NULL, $id);
  }
  
  /**
   * Displays an XML document listing all active user accounts
   *
   * @param pointer $index_content
   */
  function xml_get_users(&$index_content) {
    $query = "SELECT id, (COALESCE(last_name, '') || ', '::varchar || COALESCE(first_name, '')) AS \"name\" FROM users ORDER BY name ASC";
    $res = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign("users", $res);
    $index_content .= $parser->fetch("xml_get_users.tpl");
    header("Content-type: application/xml");
  }

  function get_users() {
    print json_encode($this->get_users_object(), true);
  }
  
  function get_users_object() {
    $res = $this->db->select("SELECT id, first_name, last_name, username, email FROM users");
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    return $res;
  }
  
  function get_groups_object() {
    $res = $this->db->select("SELECT * FROM groups");
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    }
    return $res;
  }
}

?>