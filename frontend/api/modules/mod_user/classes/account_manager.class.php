<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Account Manager class
  *
  * The Account manager is a MadnetElement subclass that is used
  * to process user accounts. The MadnetElement structure does
  * all the processing for the insert/delete/update phases but
  * the object also uses the hooks provided by MADNET to process
  * additional operations.
  *
  * Additionally, the class will handle any authentication-related
  * operations.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Account_Manager extends MadnetElement {

  /* Other attributes */

  /**
    * These fields must be declared BEFORE the constructor is called.
    */


  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "users";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_user";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;


  /**
    * Initializes all primitive stack entries (and sub_elements) and all aggregated objects.
    *
    * @return Account_Manager
    * @access public
    */
  function init() {

    $this->params->add_primitive("first_name",     "string",           TRUE ,   "First Name",      "User's First Name"   );
    $this->params->add_primitive("last_name",      "string",           FALSE,   "Last Name",       "User's Last Name"    );
    $this->params->add_primitive("email",          "email",            TRUE ,   "Email Address",   "User's email address");
    $this->params->add_primitive("username",       "string",           TRUE ,   "Username",        "Username this user will use to authenticate to the application");
    $this->params->add_primitive("passwd",         "password",         FALSE ,   "Password",        "The password this user will use to authenticate");
    $this->params->add_primitive("pager_number",   "string",           FALSE,   "Pager Number",    "User's Pager number" );
    $this->params->add_primitive("pager_terminal", "string",           FALSE,   "Pager Terminal",  "Terminal number to use to send the pager messages");

    $this->params->add_sub("mod_user", "account_group", TRUE, "post");

    $this->debugger->add_hit("Instanciating " . __CLASS__);


  }

  /**
    * Process POST credentials against the DB for authentication.
    *
    * This method will attempt to find an active user account with authentication
    * tokens matching the credentials provided in the POST data structure.
    *
    * If it *does* find a matching account, it will flag the user as authenticated
    * by calling the sync_session() method
    *
    * This is an expensive operation since we need to interact with MCrypt for the
    * password encryption, so upon failure, we sleep for 5 seconds in order to prevent
    * DoS. (this isn't perfect as the user can still DoS if they want to, but they
    * will need to send concurrent requests. If that becomes a problem, we will implement
    * an IP ban system in the next release of MADNET.
    *
    * @param string $username
    * @param string password
    * @return mixed
    */
  function login($username, $password) {

    $this->encrypter = $this->registry->get_singleton("core", "encryption_manager");

    $username = $this->db->escape($username);
    $password = $this->db->escape($this->encrypter->encrypt($password));

    $query = "SELECT * FROM " . $this->table . "
    WHERE active = '1'
    AND ((username = $username) OR (email = $username))
    AND passwd = $password";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      $query = "INSERT INTO netmon_auth(ip, timestamp, status, username, medium) VALUES (" .
        $this->db->escape(resolve_ip()) . ", " .
        $this->db->escape(mktime())     . ", " .
        $this->db->escape('Failed')     . ", " .
        $username                       . ", " .
        $this->db->escape("web")        . ")";
        $this->db->insert($query);
      return FALSE;
    } else {
      $this->data = $result;
      $this->data['VERSION'] = SESSION_VERSION;

      $query = "INSERT INTO netmon_auth(ip, timestamp, status, username, medium) VALUES (" .
        $this->db->escape(resolve_ip()) . ", " .
        $this->db->escape(mktime())     . ", " .
        $this->db->escape('Success')     . ", " .
        $username                       . ", " .
        $this->db->escape("web")        . ")";
        $this->db->insert($query);

      return $this->get_account_permissions($result['id']) ? $this->sync_session($this->data) : FALSE;
    }
  }

  /**
    * Initializes  $this->data['perms'] with a data-structure containing all the permissions associated to a user account
    *
    * @param integer $id ID of the user account
    * @return boolean
    */
  function get_account_permissions($id) {

    $id = $this->db->escape($id);

    $query = "SELECT * FROM all_user_perms WHERE user_id = $id";

    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    } else {

      $perms = array();


      foreach($res as $row) {
        $cat = strtolower(str_replace(" ", "_", $row['permission_category']));
        $perm = $row['perm_name'];


        #echo "$cat - $perm<br>";


        if (!in_array($cat, array_keys($perms))) {
          $perms[$cat] = array();
        }
        array_push($perms[$cat], strtolower(str_replace(" ", "_", $row['perm_name'])));
      }
      $this->data['perms'] = $perms;
      return TRUE;
    }
  }

  /**
    * Creates a new data-structure to inject in the SESSION stack.
    *
    * This method will first gather all the data about the specified user
    * account and inject the results in a data-structure.
    *
    * It will then gather the permissions associated witht hat account and push
    * the result to the data-structure.
    *
    * It will then inject an hash-based authentication token to the structure
    * and dump it to the SESSION stack.
    *
    * @param array $data
    * @param integer $uid
    * @return boolean
    */
  function sync_session($data = NULL, $uid = NULL) {

    if ($uid && !$data) {
      $this->pop($uid);
      $this->get_account_permissions($uid);
      $data = $this->data;
    }

    foreach($data as $key => $val) {
      $_SESSION[$key] = $val;
    }
    $_SESSION['AUTHID'] = sha1(APP_AUTHID);
    return TRUE;
  }

  /**
    * Checks for username or email address clashes
    *
    * This method checks the database to ensure that the username and
    * email address provided are not already somewhere in our users
    * database. Since logic is implemented in our pre_insert() hook,
    * we can interrupt the entire insert process if that's the case.
    *
    * If an ID argument is passed, we are doing an update so we are allows
    * exactly ONE collision (that means the user did not change their
    * username or email address), and that collision MUST BE against
    * the id argument. (If we just count the number of collisions, we
    * deserve what's coming)
    *
    * @param integer $id
    * @return boolean
    */
  function pre_insert($id = null) {


    # We must ensure that no other user account with the same username or email exists.
    # We must also report errors accurately so we will need to run a query to
    # check against email addresses and another one to check against usernames.

    $this->debugger->add_hit("Pre-insert phase on " . __CLASS__);


    # Grabs the values for the username and email fields from the params_manager
    #var_dump($this->params);
    $username = $this->db->escape($this->params->getVal('username'));
    $email    = $this->db->escape($this->params->getVal('email'));

    # Builds SELECT queries to find matching elements
    $username_query = "SELECT {$this->pkey} FROM {$this->table} WHERE username = $username";

    $email_query    = "SELECT {$this->pkey} FROM {$this->table} WHERE email    = $email";


    # We use a LIMIT query through get_row because ANY result is BAD
    $username_result = $this->db->get_row($username_query);

    #
    $email_result    = $this->db->get_row($email_query);

    # This will be our return value.
    $success = TRUE;

    if ((is_array($username_result)) && ($username_result[$this->pkey] <> intval($id))) {
      $this->err->err_from_string("A user account with the username " . htmlentities($username) . " already exists");
      $success = FALSE;
    }

    if ((is_array($email_result)) && ($email_result[$this->pkey] <> intval($id))) {
      $this->err->err_from_string("A user account with the email address " . htmlentities($email) . " already exists");
      $success = FALSE;
    }

    $this->debugger->add_hit("Pre-Insert returns " . ($success ? "true" : "false"));
    return $success;

  }

  /**
    * Activates the newly created user account
    *
    * While it would be pretty dumb to add a form field to specify
    * if a user account should be active or not, the params_manager will
    * not process this field if it has no value. Therefore, any newly created
    * account will be suspended upon creation.
    *
    * We use the post_insert() hook to re-activate the account upon creation.
    * An alternative method would be to add a hidden field for that value
    * in the user creation form. I decided to go that route instead.
    *
    * If an ID argument is passed, we are working on an update so we do not need
    * to change anything
    *
    * @param integer $id
    * @return boolean
    */
  function post_insert($id = null) {
    if (!$id) {
      # Activate the newly created user
      return $this->setBit($this->meta['pkey_value'], "active", '1');
    }
    return TRUE;
  }

  /**
    * Calls the username collision detection routine and fixes the password entry
    *
    * The policy in place is that if a password is not specified in the update
    * form, we do not change the password. Therefore, we must trick the params_manager
    * into thinking the user entered the exact same password in POST (otherwise, it will
    * overwrite it with a blank password), so we go look for the password in the DB,
    * decrypt it, and inject it into POST.
    *
    * This is obviously not necessary if the user *did* pass a password in POST.
    *
    * Once this is done, we call the pre_insert hook because we still want to make sure
    * the user account doesn't clash with other accounts (if the user is attempting to
    * change his/her username for example).
    *
    * @param integer $id
    * @return boolean
    */
  function pre_update($id = null) {

    if (strcmp($_POST['passwd'], "") <> 0) {
      ;
    } else {
      $encrypt_mgr = $this->registry->get_singleton("core", "encryption_manager");
      $_POST['passwd'] = $encrypt_mgr->decrypt($this->getBit($id, "passwd"));
    }

    return $this->pre_insert($id);
  }

  /**
    * Kicks a user out of the system before their account is deleted.
    *
    * This method deletes any record in the sessions maintenance table
    * associated with the user ID passed as argument. This effectively
    * kicks the user right out of the system.
    *
    * Since this is a pre_delete hook, the account is deleted immediately after.
    *
    * @param integer $id
    * @return boolean
    */
  function pre_delete($id) {


    $this->db->start_transaction();
    $query = "DELETE FROM user_sessions WHERE session_userid = $id";

    $result = $this->db->delete($query);

    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_string("Unable to kick this user out of the system");
      return FALSE;
    }

    $query = "DELETE FROM alert_pending WHERE handler_id IN (SELECT id FROM alert_handlers WHERE user_id = $id)";
    $result = $this->db->delete($query);

    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_string("Unable to delete user's alerts");
      return FALSE;
    }

    $query = "DELETE FROM alert_handlers WHERE user_id = $id";
    $result = $this->db->delete($query);
    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_string("Unable to delete user's alerts");
      return FALSE;
    }

    $query = "DELETE FROM alert_triggers WHERE trigger_id IN (SELECT trigger_id FROM alert_handlers WHERE user_id = $id)";
    $result = $this->db->delete($query);
    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_string("Unable to delete user's alerts");
      return FALSE;
    }




    $this->db->commit_transaction();
    return TRUE;

  }

  /**
    * Fetches a user account from the DB
    *
    * This will eventually be replated by MadnetElement->fetch()
    * once the API cools down. It is too unstable at the moment.
    * This might see the light of day before Madnet 4.0
    *
    * @param integer id
    * @return boolean
    */
  function pop($id) {
    $query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";
    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    }

    foreach($result as $key => $value) {
      $this->params->setval($key, $value);
    }

    $this->data = $result;
    $this->data['VERSION'] = SESSION_VERSION;


    return TRUE;

  }

  /**
    * Returns an array containing the user ID of every user account in the DB
    *
    * @return mixed
    */
  function get_all_ids() {
    $query = "SELECT {$this->pkey} AS \"id\", last_name, first_name FROM {$this->table} ORDER BY last_name, first_name ASC";
    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      return $result;
    }
  }

  /**
    * Suspends a user account
    *
    * Additionally, this method kicks the user out of the sessions table
    * so any active session they have will be destroyed.
    *
    * @param integer id
    * @return boolean
    */
  function suspend($id) {
    $this->setbit($id, "active", "0");

    $query = "DELETE FROM user_sessions WHERE session_userid = $id";

    $result = $this->db->delete($query);

    if (DB_QUERY_ERROR == $result) {
      return FALSE;
    } else {
      return TRUE;
    }

  }

  /**
    * Reactivates a user account
    *
    * @param integer id
    * @return boolean
    */
  function reactivate($id) {
    return $this->setbit($id, "active", "1");
  }


}


?>
