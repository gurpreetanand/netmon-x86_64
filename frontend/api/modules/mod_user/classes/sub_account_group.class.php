<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * User to Group relationship manager
  *
  * This class handles the process of managing the relationship
  * between user accounts and user groups.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class sub_account_group extends MadnetSubElement {
  
  /**
    * META structure of the MadnetElement object we are bound to
    *
    * @var $element
    * @access private
    */
  var $element;
  
  /**
    * Class constructor method. Instanciates all class attributes.
    *
    * @return sub_account_group
    * @access public
    */
  function sub_account_group() {
    parent::MadnetSubElement();
    
    $this->table = "user2groups";
    $this->fkey = "user_id";
    $this->pkey = "group_id";
    $this->mandatory = TRUE;
    $this->doc = "Groups this user should belong to";
    $this->friendly_name = "Groups association";
    
    $this->debugger->add_hit(__CLASS__ . " instanciated");
  }
  
  /**
    * Validates the data found in the POST stack
    *
    * Creates a structure based on the POST stack filled with all group IDs.
    * It then ensures that none of the values in the structure are null or
    * non-integer, and return TRUE if everything is valid, FALSE otherwise.
    *
    * This provides a fine-grained control over SQL injection
    *
    * @return boolean
    */
  function validate() {
  
    $gids = $GLOBALS['json_post'][__CLASS__];
    
    if ((!is_array($gids)) && ($this->mandatory == TRUE)) {
      $this->err->err_from_string("The group relationship is a mandatory field");
      return FALSE;
    }
    
    #$this->debugger->add_hit("GIDs (before): ", NULL, NULL, vdump($gids));
    for($i = 0; $i < sizeof($gids); $i++) {
      $gids[$i] = (intval($gids[$i]) > 0);
    }
    
    
    $this->debugger->add_hit("GIDs (after): ", NULL, NULL, vdump($gids));
    $success = ((intval($this->element['pkey_value']) > 0) && (!in_array(FALSE, $gids)));
    $this->debugger->add_hit("Validate returns " . ($success ? "true" : "false"));
    return $success;
    
  }
  
  /**
    * Inserts the relationship in the database
    *
    * Builds a hash-table based on the user ID that was just created
    * (since the MadnetElement will update $this->element['pkey_value'] with that data)
    * and autoinserts it in the DB.
    *
    * @return boolean
    */
    
  function insert() {
    
    $success = TRUE;
    
        
    $user_id = intval($this->element['pkey_value']);
      
    $data = $GLOBALS['json_post'][__CLASS__];
    
    if ($user_id == 0) {
      $this->err->err_from_string("User ID has not been passed by reference", NULL, NULL, vdump($this->element));
      return FALSE;
    }
    
    
    # Go through all our parameters (arrays of group => uids) and INSERT the rels in the DB
    foreach($data as $group_id) {
      $auto = array("user_id" => $user_id,
                "group_id" => $group_id);
      if (DB_QUERY_ERROR == $this->db->auto_insert($this->table, $auto)) {
        
        $success = FALSE;
      }  
    }
    
    
    
    
    # The $user_id should be the same accross all elements so the var is already set correctly
    if (!$success) {
      # Undo
      $this->db->delete("DELETE FROM " . $this->table . " WHERE user_id = " . $this->db->escape($user_id));
      return FALSE;
    }
    
    $this->debugger->add_hit(__FUNCTION__ . " returns " . vdump($success));
    
    return $success;
  }
  
  
  /**
    * Deletes all group relationships with the specified user account and re-creates them
    *
    * It is MUCH simpler to start from scratch and call $this->insert than to do a diff of
    * a resultset with POST data.
    *
    * @return boolean
    */
  function update() {
    $query = "DELETE FROM {$this->table} WHERE {$this->fkey} = {$this->element['pkey_value']}";
    
    $result = $this->db->delete($query);
    $this->debugger->add_hit("Result of $query", NULL, NULL, vdump($result));
    
    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_code(DB_QUERY_ERROR, "Unable to delete old records", $result->getMessage());
      return FALSE;
    }
    
    return $this->insert();
    
  }
  
  /**
    * Returns an array containing all the user2group rels for that user.
    *
    * @param integer $fkey_id
    * @return mixed
    */
  function getVal($fkey_id) {
    $query = "SELECT {$this->pkey} FROM {$this->table} WHERE {$this->fkey} = " . $this->db->escape($fkey_id);
    
    $result = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    }
    
    $tmp = array();
    
    
    
    foreach($result as $current) {
      
      array_push($tmp, $current[$this->pkey]);
    }
    return $tmp;
    

    
  }
  
  /**
    * Deletes all relationships pertaining to a user account
    *
    * @return boolean
    */
  function delete() {
    $query = "DELETE FROM {$this->table} WHERE {$this->fkey} = {$this->element['pkey_value']}";
    
    $res = $this->db->delete($query);
    
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_string("Unable to sever user to group relationship");
      return FALSE;
    }
    
    return TRUE;
    
  }
  
  /**
    * Returns an array of all group IDs and names (Smarty format)
    *
    * @return array
    */
  function get_groups() {
    
    $query = "SELECT id, group_name FROM groups ORDER BY group_name ASC";
    $res = $this->db->select_smarty($query, "id", "group_name");
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    } else {
      return $res;
    }
  }
  
  
  
  
}

?>