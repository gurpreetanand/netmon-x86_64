<servers>
{if $services}
{foreach from=$services item="service"}
	<server srvid="{$service.srv_id}" ip="{$service.ip}" srv_name="{$service.name|capitalize}" protocol="{$service.protocol}" port="{$service.port}" />
{/foreach}
{/if}
</servers>

