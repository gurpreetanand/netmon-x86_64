<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib,AC_RunActiveContent"}
{literal}
<script src="/assets/jscript/sortabletable.js"></script>
<script src="/assets/jscript/sort_lib.js"></script>
<script language="Javascript">

	//setTimeout("window.location.reload()", 15000);

	function delete_service(srv_id, name, ip) {
		if (confirm("Are you sure you wish to delete the service " + name + " at " + ip + "?")) {
			parent.parent.pnl_right.showEditor('?module=mod_services&action=delete_service&srv_id=' + srv_id);
		}
	}
init_sort = function() {
	document.getElementById("services").className = 'sort-table';
	var types = ['CaseInsensitiveString', 'netmon_ipv4', 'CaseInsensitiveString', 'netmon_date', 'CaseInsensitiveString', 'latency', 'CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("services"), types);
	report.sort(1, true);
}
//addEvent(window, "load", init_sort);

function ts_getInnerText(el) {
	if (typeof el == "string") return el;
	if (typeof el == "undefined") { return el };
	if (el.innerText) return el.innerText;	//Not needed but it is faster
	var str = "";

	var cs = el.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++) {
		switch (cs[i].nodeType) {
			case 1: //ELEMENT_NODE
				str += ts_getInnerText(cs[i]);
				break;
			case 3:	//TEXT_NODE
				str += cs[i].nodeValue;
				break;
		}
	}
	return str;
}



function getParent(el, pTagName) {
	if (el == null) return null;
	else if (el.nodeType == 1 && el.tagName.toLowerCase() == pTagName.toLowerCase())	// Gecko bug, supposed to be uppercase
		return el;
	else
		return getParent(el.parentNode, pTagName);
}


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}

{if $trackers}

		{foreach item="device" key="ip" name=outer from=$trackers}
			<div class='secondarybar'>{$ip} - Device Trackers
				<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">
				{literal}
				<input name="new_service" onmouseover="if (!is_locked('new_service')) { this.className='button_over'; }" onmouseout="if (!is_locked('new_service')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('new_service');" onblur="this.className='button'; unlock_field('new_service');" title="" class="button" type="button" value="Add TCP/ICMP Tracker" onclick="parent.parent.pnl_right.showEditor('?module=mod_services&amp;action=form_create_service&amp;ip={/literal}{$ip}{literal}&amp;root_tpl=blank_panel&amp;type=icmp');">
				<input name="unknown" onmouseover="if (!is_locked('unknown')) { this.className='button_over'; }" onmouseout="if (!is_locked('unknown')) { this.className='button'; }" onfocus="this.className='button_selected'; lock_field('unknown');" onblur="this.className='button'; unlock_field('unknown');" title="" class="button" type="button" value="Add Disk Tracker" onclick="parent.parent.pnl_right.showEditor('?module=mod_network&amp;action=form_add_disk&amp;ip={/literal}{$ip}{literal}&amp;root_tpl=blank_panel');">
				{/literal}
			</div>

			{if (!$device.tcpicmp) and (!$device.disk)}
                       	{"This device has no trackers."|message_bar} 
			{/if}

			{if $device.tcpicmp}

			<div class="datagrid center">
			<table cellspacing="0" id="services" cellpadding="0" align="center">
				<thead>
				<tr>
					<td>Name</td>
					<td>IP Address</td>
					<td>Protocol</td>
					<td>Last Checked</td>
					<td>Status</td>
					<td>Latency</td>
					<td>Status Msg</td>
					<td>Actions</td>
				</tr>
				</thead>
				{foreach key="key" item="server" from=$device.tcpicmp}
				<tr>
					<td width="20%">{$server.name}</td>
					<td width="15%"><a title="{$server.ip}" href="#" onClick="javascript:parent.parent.location='?module=mod_layout&action=render_section&layout=network&ip={$server.ip}&store_request=2'">{$server.ip}</a></td>
					<td>{$server.protocol}{if $server.port > 1}-{$server.port}{/if}</td>
					<td>{$server.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
					{if $server.status == "UP"}
					<td style="background-color: #009900; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_green.gif'); background-position: top left; background-repeat: repeat-x;">
					{elseif $server.status == "DOWN"}
					<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
					{else}
					<td style="background-color: #444444; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_gray2.gif'); background-position: top left; background-repeat: repeat-x;">
					{/if}
					{$server.status|default:"<strong>Unknown</strong>"}
					</td>
					<td>{$server.latency/1000|default:"?"} ms</td>
					<td><div title="{$server.message}">{$server.message|truncate:30}</div></td>
					<td>
						<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_services&action=form_edit_service&srv_id={$server.srv_id}');">Edit</a> |
						<a href="javascript:delete_service('{$server.srv_id}', '{$server.name|escape:javascript}', '{$server.ip}');">Del</a> |
						<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=srv_service_down&reference_pkey_val={$server.srv_id}&rel=servers')">Alerts</a>

					</td>
				</tr>
				{/foreach}
			</table>
			</div>
			{/if}

			{if $device.urls}
			<div class="datagrid center">
			<table cellspacing="0" id="services" cellpadding="0" align="center" class="sortable">
				<thead>
				<tr>
					<td>URL</td>
					<td>Pattern</td>
					<td>Last Checked</td>
					<td>Status</td>
					<td>Latency</td>
					<td>Status Msg</td>
					<td>Actions</td>
				</tr>
				</thead>
				{foreach from=$device.urls item="url"}
				<tr>
					<td><a title="{$url.url}" target="_blank" href="{$url.url}">{$url.url|truncate:30}</a></td>
					<td><div title="{$url.pattern|escape:html}">{$url.pattern|escape:html|truncate:30}</div></td>
					<td>{$url.timestamp|date_format:"%b %e, %Y %H:%M:%S"|default:"New Service"}</td>
					{if $url.status == "MATCH"}
					<td style="background-color: #009900; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_green.gif'); background-position: top left; background-repeat: repeat-x;">
					{elseif $url.status == "NO MATCH"}
					<td style="background-color: #CC0000; color: #FFFFFF; font-weight: bold; background-image: url('/assets/core/bg_red.gif'); background-position: top left; background-repeat: repeat-x;">
					{else}
					<td style="background-color: #444444; color: #FFFFFF; font-weight: bold;">
					{/if}
					{$url.status|default:"<strong>Unknown</strong>"}
					</td>
					<td>{$url.latency/1000|default:"?"} ms</td>
					<td><div title="{$server.message}">{$url.message|truncate:30}</div></td>
					<td>
						<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_services&action=form_edit_url&id={$url.id}');">Edit</a> |
						<a href="javascript:delete_url('{$url.id}', '{$url.url|escape:javascript}');">Del</a> |
						<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=web_alert_down&url_id={$url.id}&rel=urls')">Alerts</a>

					</td>
				</tr>
				{/foreach}
			</table>
			</div>
			{/if}


			{if $device.disk}
				{foreach from=$device.disk item="disk"}

				<div class="panel">
					<table width="100%" cellpadding="10" cellspacing="0">
						<tr>
							<td width="83"><div align="center">
							
								<script type="text/javascript">
								AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
								'width','63',
								'height','57',
								'src','assets/diskview?currentStatus={$disk.status|default:""}&animation=1&threshold={$disk.threshold|default:"0"}',
								'quality','high',
								'salign','LT',
								'pluginspage','http://www.macromedia.com/go/getflashplayer',
								'movie','assets/diskview?currentStatus={$disk.status|default:""}&animation=1&threshold={$disk.threshold|default:"0"}'
								 ); //end AC code
								</script>
							
								<br>
								{if $disk.total > 0}
								<strong>{$disk.used|capacity}</strong> of <strong>{$disk.total|capacity}</strong> 					{/if}
								</div>
							</td>

							<td>
								<div align="center">
								<strong>{$disk.name}</strong> on <strong>{if $disk.servername}{$disk.servername}{else}{$disk.ip|resolve_ip}{/if} ({$disk.ip})</strong><br /><br />
								Last Checked: {$disk.timestamp|date_format:"%b %e, %Y %H:%M:%S"}<br /><br />
								{if ($disk.message|replace:" ":"") != ""}
									Status Message: <strong>{$disk.message}</strong>
								{/if}
							</td>
							<td>
								<div align="center">
								<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_network&action=form_edit_disk&disk_type={$disk.type|lower}&id={$disk.srv_id}');">Edit</a><br>
								<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type={$disk.type|lower|replace:"smb":"smb_disk"}_over_threshold&disk_type={$disk.type|lower}_servers&disk_id={$disk.srv_id}');">Alerts</a><br>
								<a href="#"onClick="if(confirm('Are you sure you want to remove this disk monitor? Click OK to confirm.')) {ldelim}parent.parent.pnl_right.showEditor('?module=mod_network&action=process_delete_disk&disk_type={$disk.type|lower}&disk_id={$disk.srv_id}');{rdelim}; return false;">Delete</a><br>
								</div>
							</td>
						</tr>
					</table>
				</div>


				{/foreach}
			{/if}

		{/foreach}
	{/if}


	<!-- end of {$smarty.template} -->
