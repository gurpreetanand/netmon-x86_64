<?php


class Services_Manager extends MadnetElement {

  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "servers";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "srv_id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_services";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  function init() {
    $this->params->add_primitive("ip",          "ip_address",        TRUE,   "IP Address", "IP Address");
    $this->params->add_primitive("name",        "string",            TRUE,   "Friendly Name", "Name");
    $this->params->add_primitive("port",        "string",            FALSE,   "Port number", "Port number");
    $this->params->add_primitive("protocol",    "string",            TRUE,   "Protocol", "Protocol");
    $this->params->add_primitive("interval",    "integer",           TRUE,   "Polling Interval", "Polling Interval");
    $this->params->add_primitive("timeout",     "integer",           TRUE,   "Timeout", "Timeout");
    $this->params->add_primitive("log_timeout", "integer",           TRUE,   "Logging Threshold", "Logging Threshold");
  }

  function get_all_services($type = null) {

    if (strcmp($type, "") <> 0) {
      $cmp = " WHERE protocol = " . $this->db->escape($type) . " ";
    }

    $query = "SELECT * FROM {$this->table} {$cmp} ORDER BY status ASC, srv_id ASC";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    }

    return $res;

  }

  function get_device_services($device_id) {
    $device_id = intval($device_id);
    $query = "SELECT srv_id, ip, name, protocol, port
      FROM servers
      WHERE device_id={$device_id}
      ORDER BY name ASC";
    $res = $this->db->select($query);

    if (DB_QUERY_ERROR == $res) {
      return NULL;
    } elseif (DB_NO_RESULT == $res) {
      return NULL;
    }
    foreach ($res as &$row) {
      $row["lastlog"] = $this->get_last_servicelog($row['srv_id']);
    }
    return $res;
  }

  function get_last_servicelog($srv_id) {
    $srv_id = intval($srv_id);
    $query = "SELECT * FROM server_log WHERE srv_id={$srv_id} LIMIT 1";
    $res = $this->db->select($query);

    if (DB_QUERY_ERROR == $res) {
      return NULL;
    } elseif (DB_NO_RESULT == $res) {
      return NULL;
    } else {
      return $res[0];
    }
  }

  function get_down_services() {
    $query = "SELECT * FROM {$this->table} WHERE status = 'DOWN' ORDER BY srv_id ASC";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return;
    }

    return $res;
  }

  function get_by_ip($ip, $type = null) {
    $ip = $this->db->escape($ip);

    $cmp = " WHERE ip=" . $ip . " ";

    if (strcmp($type, "") <> 0) {
            $cmp .= " AND protocol = " . $this->db->escape($type) . " ";
    }

    $query = "SELECT * FROM {$this->table} {$cmp} ORDER BY status ASC, srv_id ASC";
    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
            return array();
    }
    
    foreach ($res as &$row) {
      $row["lastlog"] = $this->get_last_servicelog($row['srv_id']);
    }

    return $res;

  }

  function pre_update($id) {
    /**
      * Determine whether we are allowed to consume this IP slot
      */
    if (NULL != $id) {
      $old_ip = $this->getBit($id, "ip");
    } else {
      $old_ip = NULL;
    }
    
    $core = require_module("core");
    
    if (!$core->check_device_limit($this->params->primitives['ip']['value'], $old_ip)) {
      $this->err->err_from_string("Unable to create tracker: You have exceeded the number of devices allowed in your license.");
      return FALSE;
    }
    /**
      * at this point, we know we can consume an IP slot
      */
    return TRUE;
    
  }

  # Check for dups
  function pre_insert($id = null) {
  
  
    /**
      * Determine whether we are allowed to consume this IP slot
      */
    if (NULL != $id) {
      $old_ip = $this->getBit($id, "ip");
    } else {
      $old_ip = NULL;
    }
    
    $core = require_module("core");
    
    if (!$core->check_device_limit($this->params->primitives['ip']['value'], $old_ip)) {
      $this->err->err_from_string("Unable to create tracker: You have exceeded the number of devices allowed in your license.");
      return FALSE;
    }
    /**
      * at this point, we know we can consume an IP slot
      */  
  
  
  
    $success = TRUE;
    $ip = $this->db->escape($this->params->primitives['ip']['value']);
    $port = $this->db->escape($this->params->primitives['port']['value']);



    $query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip = $ip AND port = $port";

    $res = $this->db->get_row($query);

    if ((is_array($res)) && ($res[$this->pkey] <> intval($id))) {
      $this->err->err_from_string("A device with the IP address " . htmlentities($ip) . " and port " . htmlentities($port) . " already exists.");
      $success = FALSE;
    }

    return $success;
  }

  function post_insert($id = null) {
    if ($id) { $this->meta['pkey_value'] = $id; }
    $query = "UPDATE {$this->table} SET timestamp = ".mktime().", pending = " . $this->db->escape('N') . " WHERE {$this->pkey} = " . $this->db->escape($this->meta['pkey_value']);
    return $this->db->update($query);
  }

  function post_update($id) {
    return $this->post_insert($id);
  }

  function pop($id) {
    $id = $this->db->escape($id);

    $query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->params->setval($key, $value);
      }
      return TRUE;
    }
  }

  
  function pre_delete($id) {
    $this->db->start_transaction();
    $query = "DELETE FROM alert_handlers WHERE trigger_id IN (select trigger_id from alert_triggers WHERE reference_table_name = 'servers' AND reference_pkey_val = $id)";
    $this->db->delete($query);
    $query = "DELETE FROM alert_triggers WHERE reference_table_name = 'servers' AND reference_pkey_val = $id";
    $this->db->delete($query);
    $this->db->delete("DELETE FROM alert_pending WHERE sent = '0' AND handler_id NOT IN (SELECT id FROM alert_handlers)");    
    
    $query = "DELETE FROM server_log WHERE srv_id = " . $this->db->escape($id);
    $this->db->delete($query);
    return $this->db->delete($query);
  }

  function post_delete($id) {
    return $this->db->commit_transaction();
  }

  function get_stats() {
    $query = "SELECT COUNT(srv_id) AS total,
          (SELECT COUNT(srv_id) FROM servers WHERE status = 'UP') AS up,
          (SELECT COUNT(srv_id) FROM servers WHERE status = 'DOWN') as down
          FROM servers";
    $res = $this->db->get_row($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return FALSE;
    }

    $res['unknown'] = $res['total'] - $res['up'] - $res['down'];
    return $res;
  }

	function delete_by_ip($ip) {
	  $query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip='{$ip}'";
	  $results = $this->db->select($query);
	  if ((DB_QUERY_ERROR == $results) || (DB_NO_RESULT == $results)) {
	    $results = array();
	  }
	  foreach ($results as $result) {
	    $this->delete($result[$this->pkey]);
	  }
	}


}
?>
