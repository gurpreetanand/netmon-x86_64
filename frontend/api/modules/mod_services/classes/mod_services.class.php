<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_dashboard.class.php

/**
  * Dashboard MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_services extends MadnetModule {

  /**
    * Initializes all module variables
    *
    * @return void
    */
  function init() {
    require_class($this->module, "services_manager");
  }

  function get_services(&$index_content, $type=NULL) {
    $sm = new Services_Manager();

    if ($type == NULL)
      $type = strtoupper($_GET['type']);

    $servers = $sm->get_all_services($type);
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("servers", &$servers);
    $index_content .= $parser->fetch("get_services.tpl");
    return;
  }

  function add_payload(&$payload, $arr, $type, $ip_field) {
    foreach ($arr as $a) {
      if ($payload[$a[$ip_field]][$type] == NULL) {
        $payload[$a[$ip_field]][$type] = array();
      }
      array_push($payload[$a[$ip_field]][$type], $a);
    }
  }

  function dump_all_services(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $sm = new Services_Manager();

    $payload = array();

                # Handle services from servers table (ICMP, TCP)
                $icmp = $sm->get_all_services("ICMP");
                $tcp = $sm->get_all_services("TCP");

    $this->add_payload($payload, $icmp, "tcpicmp", "ip");
    $this->add_payload($payload, $tcp, "tcpicmp", "ip");

    $this->debugger->add_hit("payload", null, null, vdump($payload));

                # Handle disk trackers
                require_class("mod_network", "composite_disk_manager");
                $cm = new Composite_Disk_Manager();
                $disks = $cm->get_all_ids();
    $this->add_payload($payload, $disks, "disk", "ip");

    ksort($payload);

    $parser->assign("trackers", $payload);

                $index_content .= $parser->fetch("all_services.tpl");
  }



  function form_create_service(&$index_content) {
    $sm = new Services_Manager();
    $parser = new Parser($this->tpl_dir);
    $parser->assign("protocols", $this->_get_protocols_hash());
    $parser->set_params($sm->params);

    $timeouts = array();
    $timeouts[-1] = "Disable Logging";
    $timeouts[0] = "Log Everything";
    for ($i = 100; $i <= 1500; $i+=100) {
      $timeouts[$i] = $i . " ms";
    }
    $this->debugger->add_hit("timeouts", null, null, vdump($timeouts));


    $parser->assign("timeouts", $timeouts);
    $index_content .= $parser->fetch("form_create_service.tpl");
    return;
  }

  function form_edit_service(&$index_content) {
    $sm = new Services_Manager();

    $id = intval($_GET['srv_id']);
    if ($id <= 0) {
      $this->err->err_from_string("Invalid service identifier. Please refresh the service list and try again.");
      return;
    }

    if ($sm->pop($id)) {
      $parser = new Parser($this->tpl_dir);
      $parser->set_params($sm->params);
      $parser->assign_by_ref("service", &$sm);
      $parser->assign("protocols", $this->_get_protocols_hash());

      $timeouts = array();
      $timeouts[-1] = "Disable Logging";
      $timeouts[0] = "Log Everything";
      for ($i = 100; $i <= 1500; $i+=100) {
        $timeouts[$i] = "Latency >= " . $i . " ms";
      }
      $max = $sm->get('timeout')*60000;
      $timeouts[$max] = "Service DOWN (Recommended)";

      $parser->assign("timeouts", $timeouts);

      $index_content .= $parser->fetch("form_edit_service.tpl");
    }

    return;
  }

  function process_create_service() {
    $sm = new Services_Manager();
    if ($sm->insert()) {
      $mod = require_module("mod_alerts");
      print '{}';
    } else {
      $this->err->err_from_code(400, "Could not create alert");
    }
  }

  function process_update_service(&$index_content) {

    $sm = new Services_Manager();

    if ($sm->update()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update service");
    }
  }

  function delete_service() {
    $id = intval($_GET['srv_id']);
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid service identifier. Please refresh the service list and try again.");
    }

    $sm = new Services_Manager();

    if ($sm->delete($id)) {
      print '{}';
    } else { 
      $this->err->err_from_code(400, "Unable to delete service");
    }

  }

  # For Smarty's drop-down handler
  function _get_protocols_hash() {
    return array("ICMP" => "ICMP", "TCP" => "TCP");
  }

  function get_system_status(&$index_content) {
    $sm = new Services_Manager();

    $res = $sm->get_down_services();
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("servers", &$res);

    /**
     * Methods and modifiers assignment
     */
    # The Dashboard module has an IP resolver for the top activity panels so we will let smarty use it.
    require_module("mod_dashboard");
    $dashboard = new mod_dashboard();

    $net = require_module("mod_network");
    $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));
    $parser->register_modifier("resolve_port", array(&$net, "_resolve_port"));

    
    /**
     * Disk Monitoring status
     */
    require_class("mod_network", "df_disk_manager");
    require_class("mod_network", "smb_disk_manager");

    $dmd = new DF_Disk_Manager();
    $smd = new SMB_Disk_Manager();

    $disks = array_merge_recursive($dmd->get_disks_over_threshold(), $smd->get_disks_over_threshold());

    $parser->assign_by_ref("disks", $disks);
    /**
     * End of Disk monitoring Status
     */
    
    
    /**
     * URL Monitors Status
     */
    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    $urls = $um->get_down_urls();
    $parser->assign_by_ref("urls", $urls);
    
    /**
     * End of URL Monitors Status
     */
    
    
    /**
     * Service Uptime Stats
     */
    if ($stats = $sm->get_stats()) {
      $parser->assign_by_ref("stats", &$stats);
    }
    /**
     * End of service stats
     */

    $index_content .= $parser->fetch("system_status.tpl");
    return;
  }

  function get_services_stats(&$index_content) {

    $sm = new Services_Manager();
    $parser = new Parser($this->tpl_dir);

    if ($res = $sm->get_stats()) {
      $parser->assign_by_ref("stats", &$res);
      $index_content .= $parser->fetch("services_stats.tpl");
    }
  }

  function get_xml_services_list(&$index_content) {
    $_GET['root_tpl'] = "blank";

    $query = "SELECT srv_id, ip, name, protocol, port
    FROM servers
    WHERE log_timeout > -1
    ORDER BY name ASC";
    $res = $this->db->select($query);

    if (DB_QUERY_ERROR == $res) {
      return;
    } elseif (DB_NO_RESULT == $res) {
      $res = NULL;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("services", $res);
    $index_content .= $parser->fetch("xml_services.tpl");
    header("Content-type: application/xml");
  }
  
  function get_xml_urls_list(&$index_content) {
    $query = "SELECT id, url, pattern FROM urls WHERE enable_logging = true ORDER BY url ASC, id ASC";
    $res = $this->db->select($query);
    
    if (DB_QUERY_ERROR == $res) {
      return;
    } elseif (DB_NO_RESULT == $res) {
      $res = NULL;
    }
    
    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("urls", $res);
    $index_content .= $parser->fetch("xml_urls.tpl");
    header("Content-type: application/xml");
  }

  /**
   * Displays the URL Management Dialog
   *
   * @param pointer $index_content
   */
  function get_urls() {
    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    $urls = $um->get_all_urls();

    print json_encode($urls, true);
  }

  /**
   * Displays the URL creation form
   *
   * @param pointer $index_content
   */
  function form_create_url(&$index_content) {
    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    $parser = new Parser($this->tpl_dir);
    $parser->set_params($um->params);
    $index_content .= $parser->fetch("form_create_url.tpl");
    return;
  }

  /**
   * Displays the URL editing form
   *
   * @param pointer $index_content
   */
  function form_edit_url(&$index_content) {

    $id = intval($_GET['id']);
    if ($id <= 0) {
      $this->err->err_from_string("Invalid URL identifier. Please refresh the URL list and try again.");
      return;
    }

    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    if (!$um->pop($id)) {
      $this->err->err_from_string("Unable to retrieve the specified URL");
      return;
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("um", $um);
    $parser->set_params($um->params);
    $index_content .= $parser->fetch("form_edit_url.tpl");
    return;
  }

  /**
   * Adds a new URL to the DB
   *
   * @param pointer $index_content
   */
  function process_create_url() {
    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    if ($um->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to create URL tracker");
    }
  }

  /**
   * Updates an URL in the DB
   *
   * @param pointer $index_content
   */
  function process_update_url(&$index_content) {
    $id = intval($_GET['id']);
    if ($id <= 0) {
      $this->err->err_from_string("Invalid URL identifier. Please refresh the URL list and try again.");
      return;
    }

    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    if ($um->update()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update URL");
    }
  }

  /**
   * Deletes a URL from the DB
   *
   * @param pointer $index_content
   */
  function process_delete_url(&$index_content) {
    $id = intval($_GET['id']);
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid URL identifier. Please refresh the URL list and try again.");
      return;
    }

    require_class($this->module, "url_manager");
    $um = new URL_Manager();

    if ($um->delete($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to delete URL.");
    }
  }



}

?>
