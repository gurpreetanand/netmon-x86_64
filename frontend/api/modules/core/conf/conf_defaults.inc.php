<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * Default localization string as per ISO 639 and RFC 1766
  * @name DEFAULT_LOCALE
  */
define("DEFAULT_LOCALE", "en_US");

/**
  * Language code
  * @name LANG
  */
define("LANG", "en");

/**
  * Default template format
  * @name FORMAT
  */
define("FORMAT", "html");


/**
  * This is the default module to load
  * @name DEFAULT_MODULE
  */
define("DEFAULT_MODULE",                     "core");

/**
  * This is the default action to load within that module
  * @name DEFAULT_ACTION
  */
define("DEFAULT_ACTION",                     "login_dialog");

/**
  * This is the default module to load for logged-in users
  * @name DEFAULT_LOGGED_IN_MODULE
  */
define("DEFAULT_LOGGED_IN_MODULE",           "mod_user");

/**
  * Load this action for logged-in users
  * @name DEFAULT_LOGGED_IN_ACTION
  */
define("DEFAULT_LOGGED_IN_ACTION",           "home");



/**
  * Marker for mandatory fields
  * @name MARKER
  */
define("MARKER",                             "<b><font color=\"#FF0000\">*</font></b>");

/**
 * System load threshold. Some things will refuse to run if the load is
 * higher than this value.
 * @name SYSTEM_LOAD_THRESHOLD
 */
define("SYSTEM_LOAD_THRESHOLD",               4);

?>