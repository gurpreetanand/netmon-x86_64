<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
  * SHM Key - Top Protocols
  * @name SHM_TOP_PROTOCOLS
  */
define("SHM_TOP_PROTOCOLS",      0xf5000001);

/**
  * SHM Key - Top 200 IP Streams
  * @name SHM_TOP_IP_STREAMS
  */
define("SHM_TOP_IP_STREAMS",     0xf4000003);

/**
  * SHM Key - Top 9 Conversations
  * @name SHM_TOP_CONVERSATIONS
  */
define("SHM_TOP_CONVERSATIONS",  0xf4000000);


# 
/**
  * SHM Key - Top 9 Web Servers
  * @name SHM_TOP_WEB_SERVERS
  */
define("SHM_TOP_WEB_SERVERS",    0xf4000001);


# 
/**
  * SHM Key - Top 9 Web Clients
  * @name SHM_TOP_WEB_CLIENTS
  */
define("SHM_TOP_WEB_CLIENTS",    0xf4000002);

/**
  * Maximum number of entries to send to the VNE
  * @name SHM_MAX_ENTRIES
  */
define("SHM_MAX_ENTRIES",        64);


define("SHM_LOCAL_BREAKDOWN", 0xf4000004);

#define("SHM_LOCAL_BREAKDOWN_OUT", 0xf4000005);

/**
 * Memcached daemon should be running on this port
 * @name MEMCACHED_PORT
 */
define("MEMCACHED_PORT", 11211);

/**
 * This flag enables/disables cache compression in memcached stores
 * @name MEMCACHED_COMPRESS
 */
define("MEMCACHED_COMPRESS", TRUE);


?>
