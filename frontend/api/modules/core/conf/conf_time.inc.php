<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  *
  * Time constants
  *
  */

/**
  * Number of seconds in one second
  * @name TIME_SECOND
  */
define("TIME_SECOND",                        1);
/**
  * Number of seconds in one minute
  * @name TIME_MINUTE
  */
define("TIME_MINUTE",                        60);
/**
  * Number of seconds in one hour
  * @name TIME_HOUR
  */
define("TIME_HOUR",                          3600);
/**
  * Number of seconds in one day
  * @name TIME_DAY
  */
define("TIME_DAY",                           86400);
/**
  * Number of seconds in one week
  * @name TIME_WEEK
  */
define("TIME_WEEK",                          604800);
/**
  * Number of seconds in one month (typical 30 days)
  * @name TIME_MONTH
  */
define("TIME_MONTH",                         2592000);
/**
  * Number of seconds in one year (365 days)
  * @name TIME_YEAR
  */
define("TIME_YEAR",                          31536000);

?>