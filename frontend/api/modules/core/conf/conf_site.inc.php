<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/* Site information (Change these)                    */

/**
  * Base URI of the application (no trailing slash)
  * @name TOP_DOMAIN
  */
define("TOP_DOMAIN",                         $_SERVER["SERVER_NAME"]);

/**
  * Subdirectory where the app is actually located
  * @version  3.5a
  * @name SUBDIR
  */
#define("SUBDIR",    substr($_SERVER['REQUEST_URI'], 1, sizeof($_SERVER['REQUEST_URI'])-2));
define("SUBDIR", "");

/**
  * Complete base URI (including http:// and training slash
  * @name URI
  */
define("URI",                                "http://" . TOP_DOMAIN . "/" . SUBDIR . ((strcmp(SUBDIR, "") <> 0) ? "/" : ""));

/**
  * Complete URI when we need SSL capabilities
  * @name SSL_URI
  */
define("SSL_URI",                            "https://" . TOP_DOMAIN . "/" . SUBDIR . "/");

/**
  * Name of the website (will appear in headers)
  * @name SITE_NAME
  */
define("SITE_NAME",                          "Netmon");
/**
  * Name of the company/client (for copyrights)
  * @name COMPANY
  */
define("COMPANY",                            "Netmon Inc.");
/**
  * Version number of this website/application instance
  * @name VERSION
  */
define("VERSION",                            " 5.3");
/**
  * Year at which the website/application was launched (for copyrights)
  * @name LAUNCH_YEAR
  */
define("LAUNCH_YEAR",                        "2005");
/**
  * Email address of the application developer
  * @name DEVELOPER
  */
define("ADMIN",                          "support@netmon.ca");

/**
  * Enable XHTML standard compliant mode
  * @name ENABLE_TIDY_EXT
  */
define("ENABLE_TIDY_EXT", FALSE);


/**
 * Shows visual clues of new features in the latest itteration of this application
 * @name HIGHLIGHT_NEW_FEATURES
 *
 */
define("HIGHLIGHT_NEW_FEATURES", TRUE);


?>
