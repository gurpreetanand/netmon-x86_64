<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * Smarty subclass
  *
  * MADNET uses the Smarty templating engine to process and compile templates
  *
  * This class extends the core Smarty engine with an instance of the params_manager
  * class (for input fields and validation of MadnetElement objects) and a couple of
  * rendering functions. It also tells smarty what dirs to use based on the constructor
  * argument.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class Parser extends Smarty {

	/**
	 * Singleton Registry
	 * 
	 * @var Registry registry
	 */
	 var $registry;

	/**
	  * Params_Manager instance to use for $this->params function
	  *
	  * @var $params_manager
	  * @access private
	  */
	var $params_manager;
	
	/**
	 * Cache_Manager instance for JS and CSS file dependency management
	 * 
	 * @var $cache_manager
	 * @access private
	 */
	 var $cache;

	/**
	  * Parser object constructor.
	  *
	  * Tells Smarty where to find all the template handling dirs,
	  * Instanciate the Smarty class, and register any required functions.
	  *
	  * @param string $tpl_dir Where to find the root template dir
	  * @return Parser
	  */
	function Parser($tpl_dir = null) {
		
		
		# Singleton Registry
    	$this->registry = &Registry::get_registry();
    	
    	# Cache Manager
		$this->cache      = &$this->registry->get_singleton("core", "cache_manager");
		
		
		if (!$tpl_dir) { $tpl_dir = MODS_PATH . "core/tpl/html/en/"; }
		$this->Smarty();
		$this->template_dir = $tpl_dir;
		$this->cache_dir    = $this->template_dir . "cache/";
		$this->compile_dir  = $this->template_dir . "compiled/";
		$this->config_dir   = $this->template_dir . "config/";

		$this->register_function("input",         array(&$this, "func_input"),            FALSE, FALSE);
		$this->register_function("param",         array(&$this, "func_param"),            FALSE, FALSE);
		$this->register_function("madnet_action", array(&$this, "madnet_action_handler"), FALSE, FALSE);
		$this->register_function("array_size",    array(&$this, "array_size"),            FALSE, FALSE);
		$this->register_function("has_perm",      array(&$this, "tpl_has_perm"));
		$this->register_function("import_js",     array(&$this, "register_js_dependency"));
		$this->register_function("import_css",    array(&$this, "register_css_dependency"));
		$this->register_function("get_imports",    array(&$this, "get_imports"));
		
		
		$this->register_modifier("message_bar",   "message_bar");
		$this->register_modifier("mktitle",       "mktitle");
		$this->register_modifier("capacity",      array(&$this, "translate_capacity"));
		$this->register_modifier("process_size",      array(&$this, "translate_size"));
		$this->register_modifier("speed",         array(&$this, "translate_speed"));
		$this->register_modifier("reverse", "strrev");
		$this->register_modifier("trim", "trim");
		$this->register_modifier("base64_encode", "base64_encode");
		$this->register_modifier("base64_decode", "base64_decode");
		
		# For JP's device toolbar  :(
		$this->register_modifier("to_id",   array(&$this, "to_id"));
		$this->register_modifier("to_ip",   array(&$this, "to_ip"));
		
	}

	/**
	  * Assigns the argument to $this->params
	  *
	  * This method also assigns the subs stack of the params object passed
	  * to the template.
	  *
	  * @param Params_Manager $params
	  * @return void
	  */
	function set_params($params) {
		$this->params_manager = $params;

		$this->subs = $this->params_manager->get_all_subs();
		$this->assign_by_ref("subs", $this->subs);

	}

	/**
	  * Displays an input field
	  *
	  * This function differs from the func_param function in that the
	  * widgets are not bound to data elements.
	  *
	  * Idea: Replace the name from input to widget. Use the w_type
	  * for the opening tag, and proceed as usual. This would do something
	  * like:
	  * {widget w_type="input" type="text" class="input" name="my_val"}
	  * or:
	  * {widget w_type="td" class="dynamic_cell" align="center"}
	  *
	  * @param array $params
	  * @return string
	  */
	function func_input($params) {
		if (!$params['doc']) { $params['doc'] = ""; }
		if (!$params['name']) { $params['name'] = "unknown"; }
		if (!$params['class']) { $params['class'] = "text"; }

		$ret = <<<EOM
<input
name         ="{$params['name']}"
onMouseOver  ="if (!is_locked('{$params['name']}')) { this.className='{$params['class']}_over'; }"
onMouseOut   ="if (!is_locked('{$params['name']}')) { this.className='{$params['class']}'; }"
onfocus      ="this.className='{$params['class']}_selected'; lock_field('{$params['name']}');"
onblur       ="this.className='{$params['class']}'; unlock_field('{$params['name']}');"
title        ="{$params['doc']}"
class        ="{$params['class']}"
EOM;


		$tag = "";
		foreach($params as $key => $val) {
			if (!in_array($key, array("doc", "name", "class"))) {
				$tag .= " " . $key . "=\"" . $val . "\" ";
			}
		}
		$tag .= " />";

		return $ret . $tag;
		#return $tag;
	}

	/**
	  * Renders an input field based on params_manager data
	  *
	  * This method uses the data associated to a field in the params_manager
	  * object previously set through $this->set_params.
	  *
	  * It will then use that data to build a CSS-based input field of the proper
	  * type for this field. It will include mouse-overs, on-select, tooltips,
	  * validation hints, error management, and will also include client-side
	  * validation starting with MADNET 1.2 (Netmon 4.0)
	  *
	  * @param array $params
	  * @return string
	  */
	function func_param($params) {
		$name = $params['name'];

		if (!is_object($this->params_manager)) {
			return "";
		}


		$field = $this->params_manager->get_param($name);

		if ((!$field) && ($params['type'])) {
			$field = array("name" => $params['name'] ? $params['name'] : "Unknown",
						   "type" => $params['class'] ? $params['class'] : $params['type'],
						   "doc"  => $params['doc']  ? $params['doc']  : "Unknown");
		}

		$field['type'] = $params['class'] ? $params['class'] : $field['type'];

		if (is_a($field, "MadnetSubElement")) {
			$f = array(
						"type"          => "select",
						"doc"           => $field->doc,
						"friendly_name" => $field->friendly_name
						);
			$field = $f;
		}


		$ret = <<<EOM
name         ="$name"
onMouseOver  ="if (!is_locked('{$name}')) { this.className='{$field['type']}_over'; }"
onMouseOut   ="if (!is_locked('{$name}')) { this.className='{$field['type']}'; }"
onfocus      ="this.className='{$field['type']}_selected'; lock_field('{$name}');"
onblur       ="this.className='{$field['type']}'; unlock_field('{$name}');"
title        ="{$field['doc']}"
class        ="{$field['type']}"
EOM;
		return $ret;


	}


	function madnet_action_handler($params) {



		if (!module_exists($params['module'])) {
			return "The module <strong><font color=\"#ff0000\">&quot;" . mktitle($params['module']) . "&quot;</font></strong> does not exist";
		}

		$output = "<!-- Madnet Action " . $params['action'] . " -->\n";

		$module = require_module($params['module']);
		$action = $params['action'];

		#unset($params['smarty']); unset($params['module']); unset($params['action']);
		foreach($params as $key => $val) {
			$_GET[$key] = $val;
		}

		if (!method_exists($module, $action)) {
			return "The method <strong><font color=\"#ff0000\">&quot;" . mktitle($action) . "&quot;</font></strong> does not exist in module " . mktitle($params['module']);
		}

		$module->$action($output);

		$output .= "\n<!-- End of Madnet Action " . $params['action'] . " -->\n";

		return $output;


	}

	function array_size($param) {
		return sizeof($param['array']);
	}

	function translate_capacity($int) {
	    if ($int >= 1073741824)  { return round($int/1073741824, 2) . " Gb"; }
        elseif ($int >= 1048576) { return round($int/1048576, 2) . " Mb"; }
        elseif ($int >= 1024)    { return round($int/1024, 2) . " Kb"; }
        else                     { return round($int, 2) . " b"; }
	}
	
	function translate_size($int) {
		$int *= 1024;
	    if ($int >= 1073741824)  { return round($int/1073741824, 2) . "GB"; }
        elseif ($int >= 1048576) { return round($int/1048576, 2) . " MB"; }
        elseif ($int >= 1024)    { return round($int/1024, 2) . " KB"; }
        else                     { return round($int, 2) . " B"; }
	}
	
	function translate_speed($int) {
		$speeds = array("1000000000" => "Gb", "1000000" => "Mb", "1000" => "Kb", "1" => "b");
		foreach($speeds as $key => $val) {
			if ($int >= intval($key)) {
				return round($int/intval($key), 2) . $val;
			}
		}
	}
	
	function tpl_has_perm($param) {
		return has_perm($param['perm']);
	}

	function to_id($param) {
		require_class("mod_devices", "device_manager");
		$dm = new Device_Manager();
		return $dm->get_id($param);
	}
	
	function to_ip($param) {
		require_class("mod_devices", "device_manager");
		$dm = new Device_Manager();
		return $dm->get_ip($param);
	}
	
	/*
	function to_base64($val) {
		return base64_encode($val);
	}
	
	function from_base64($val) {
		return base64_decode($val);
	}
	*/
	
	
	function register_js_dependency($params) {
		$jsfiles = $params['files'];
		$op = $params['op'] ? $params['op'] : 'push';
		$cached = &$this->cache->get("js_files");
		
		if (FALSE === $cached) {
			$cached = array();
			$this->cache->set("js_files", &$cached);
		}
		
		$jsfiles = explode(",", $jsfiles);
		
		foreach($jsfiles as $jsfile) {
			$parts = explode(".", $jsfile);
			if (sizeof($parts) > 1) {
				if (strcmp('yui', $parts[0]) == 0) {
					$filename = "/assets/jscript/yui/{$parts[1]}/{$parts[1]}.js?_yuiversion=2.3.1";
				} elseif (strcmp('ext', $parts[0]) == 0) {
					$filename = "/assets/jscript/ext/{$parts[1]}.js";
				}
			} else {
				$filename = "/assets/jscript/{$jsfile}.js";
			}
			if (!in_array($filename, array_values($cached))) {
				if ('push' == $op) {
					$cached[] = $filename;
				} else {
					array_unshift(&$cached, $filename);	
				}
			}
		}
	}
	
	function register_css_dependency($params) {
		$cssfiles = $params['files'];
		$op = $params['op'] ? $params['op'] : 'push';
		$cached = &$this->cache->get("css_files");
		
		if (FALSE == $cached) {
			$cached = array();
			$this->cache->set("css_files", &$cached);
		}
		
		$cssfiles = explode(",", $cssfiles);
		
		foreach($cssfiles as $cssfile) {
		$parts = explode(".", $cssfile);
		if (sizeof($parts) > 1) {
			if (strcmp('yui', $parts[0]) == 0) {
				$filename = "/assets/jscript/yui/{$parts[1]}/assets/{$parts[1]}.css";
			} elseif (strcmp('yui_sam', $parts[0]) == 0) {
				$filename = "/assets/jscript/yui/{$parts[1]}/assets/skins/sam/{$parts[1]}.css";
			} elseif (strcmp('ext', $parts[0]) == 0) {
				$filename = "/assets/jscript/ext/resources/css/{$parts[1]}.css";
			}
		} else {
			$filename = "/assets/css/{$cssfile}.css";
		}
			
			if (!in_array($filename, array_values($cached))) {
				if ('push' == $op) {
					$cached[] = $filename;
				} else {
					array_unshift(&$cached, $filename);	
				}
			}
			
		}
	}
	
	function get_imports($params, $ref) {
		$type = $params['type'] ? $params['type'] : 'js';
		$imports = $this->cache->get($type . '_files');
		$ref->assign($params['type'] . '_files', $imports);
	}
}

?>
