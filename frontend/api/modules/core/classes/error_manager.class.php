<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
 * Error Management Singleton object
 * Error Handling Facility  This object is responsible for triggering and handling
 * error events.
 * @package MADNET
 * @author Xavier Spriet
 */
class Error_Manager
{


  /**
   * Collection of errors
   * Errors are handled as simple data-structures. They contain
   * information such as error severity, error code, error message (i18n'd), error
   * handler, etc...
   * @access private
   * @var array $errors_collection
   */
  var $errors_collection = array();

  /**
   * Map of Error Codes to user messages.
   * @var array $error_map
   * @access private
   */
  var $error_map = array();

  /**
    * Error Manager constructor method
    *
    * Instanciate the registry and debugger
    *
    * @return Error_Manager
    */
  function Error_Manager() {
    $this->registry = Registry::get_registry();
    $this->debugger = $this->registry->get_singleton("core", "debugger");
  }

  /**
   * Adds a new error in the collection from a string.
   *
   * @param string $string
   * @return void
   * @access public
   */
  function err_from_string($string, $allow_dupes=TRUE)
  {
    if (TRUE == $allow_dupes) {
      $this->store_error($string);
    } else {
      $dupe = FALSE;
      foreach($this->errors_collection as $err) {
        if (strcmp($string, $err['message']) == 0) {
          $dupe = TRUE;
        }
      }
      if ($dupe == FALSE) {
        $this->store_error($string);
      }
    }
  } // end of member function err_from_string


  /**
   * Immediately stops processing and renders error information as JSON.
   *
   * @param integer $code
   * @param string $message
   * @param string $payload
   * @return void
   * @access public
   */
  function err_from_code($code, $message = null, $payload = array())
  {
    if ($code != DB_QUERY_ERROR) {
      header('HTTP/1.1 ' . $code . ' Unauthorized', true, 401);
      $json = array();
      $json["error"] = strip_tags($message);
      $json["payload"] = $payload;
      if (sizeof($this->errors_collection) == 0) {
        $json["errors"] = array(array("message" => $message));
      } else { 
        $json["errors"] = $this->errors_collection;
      }
      print json_encode($json);
      die();
    }
  } // end of member function err_from_code


  /**
    * Returns the message associated with an error code
    *
    * @param integer $code
    * @return string
    */
  function get_error_from_code($code) {
    return ($this->error_map[$code] ? $this->error_map[$code] : "Unknown Error {ref#" . $code . "}");
  }

  function add_error_to_map($code, $string) {
    $this->error_map[$code] = $string;
  }


  /**
   * Outputs directly to the user a template-based fatal-error and die()s
   *
   * @param hashtable $hash
   * @param string $template
   * @return void
   * @access public
   */
  function handle_fatal($message, $template = null)
  {
    $tpl = $template ? $template : 'fatal_error.tpl';
    $parser = new Parser(get_tpl_dir("core"));
    $parser->assign_by_ref("errors", $this->errors_collection);
    $parser->assign("message", $message);
    $parser->display($tpl);
    die();
  } // end of member function handle_fatal



  /** private **/


  /**
    *  Stores a new error in the collection.
    *
    * The parameters to this method are:
    * - Abstract: Basic title explaining what the error is.
    * - Message: Full description of the error
    * - Severity: Level of severity of the error
    * - Code: Optional - Code associated with the error
    * - Payload: Optional payload to attach to the error event (for the debugger).
    *
    * @param string  $message  Full description of the error
    * @param integer $severity Level of severity of the error
    * @param integer $code     Optional - Code associated with the error
    * @param string  $abstract Basic title explaining what the error is.
    * @param string  $payload  Optional payload to attach to the error event (for the debugger).
    * @return void
    *
    */
  function store_error($message, $severity = 0, $code = 0, $abstract = null, $payload = null) {
    array_push($this->errors_collection,
            array("abstract" => $abstract,
                "message"  => $message,
                 "severity" => $severity,
                 "code"     => $code));
    $this->debugger->add_hit($message, $abstract ? $abstract : "Error", $severity, $payload);
  }


  /**
    * Registers a new error code to the map
    *
    * @deprecated We are not currently using this architecture
    *
    * @param integer $code
    * @return void
    */
  function register_error($code) {
    if (!$this->isError($code)) {
      array_push($this->_errors, $code);
    }
  }

  /**
    * Returns true if a specific error code has been registered
    *
    * @deprecated - See deprecation warning for $this->register_error
    * @return void
    */
  function isError($code) {
    return in_array($code, $this->_errors);
  }

  /**
    * Attempts to find an error type  based on similar severity level
    *
    * @deprecated - See deprecation warning for $this->register_error
    * @return boolean
    *
    */
  function find_error_type($type) {
    foreach($this->errors_collection as $current) {
      if ($current['severity'] == $type) {
        return TRUE;
      }
    }
    return FALSE;
  }

} // end of Error_Manager


?>