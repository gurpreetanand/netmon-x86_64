<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
 * class SHM_Manager
 * Shared Memory Manager.  I would like this to be an OO wrapper around the
 * memcached library...
 */
class SHM_Manager
{

  /** Aggregations: */

  /** Compositions: */

   /*** Attributes: ***/

   var $err;

   var $registry;

   function SHM_Manager() {
      $this->registry = Registry::get_registry();
      $this->err = $this->registry->get_singleton("core", "error_manager");
   }

   function get_data_structure($base_key) {
      $sem_id = sem_get($base_key, 1024, 0666, TRUE);
      sem_acquire($sem_id);

      $shm_id = shmop_open($base_key, "a", 0, 0);
      if (!$shm_id) {
        $this->err->err_from_string("Unable to open semaphore to access shared memory at address $base_key");
        @sem_release($sem_id);
        @sem_remove($sem_id);
        return array();
      }

      $shm_size = shmop_size($shm_id);
      $data     = shmop_read($shm_id, 0, $shm_size);
      shmop_close($shm_id);

      @sem_release($sem_id);
      @sem_remove($sem_id);

      $structs  = explode ("\0", $data);
      $res      = array();


      foreach ($structs as $struct) {
        $fields = explode ("\n", $struct);
        if (count ($fields) == 1) {
          continue;
        }
        array_push($res, $fields);
      }
      return $res;
  }




} // end of SHM_Manager
?>
