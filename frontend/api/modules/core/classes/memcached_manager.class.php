<?php


/**
 * Memcached-based cache.
 */
class Memcached_Manager {
  
  /**
   * Singleton Registry
   * 
   * @var Registry registry
   */
   var $registry;
  
  /**
   * Error handler
   * 
   * @var Error_Manager err
   */
  var $err;
  
  /**
   * Debugger instance
   * 
   * @var Debugger debugger
   * @access private
   */
  var $debugger;
  
  /**
   * Memcached manager object
   *
   * @var cache
   * @access private
   */
  var $cache;
  

  function Memcached_Manager() {
    # Singleton Registry
    $this->registry = Registry::get_registry();

      # Error handler (MUST be a singleton)
    $this->err      = $this->registry->get_singleton("core", "error_manager");

    # Debugger
    $this->debugger = &$this->registry->get_singleton("core", "debugger");

    # Memcache interface
    try {
      $this->cache = new Memcache;
      $this->cache->connect('127.0.0.1', MEMCACHED_PORT);
    } catch (Exception $e) {
      $this->err->err_from_string("Unable to initialize cache manager");
    }
  }
    
  /**
    * Creates a new namespace $objName in the stack, and dump $objValue into it
    * @param string $name Key
    * @param mixed $value Payload (will be compressed if MEMCACHED_COMPRESS == TRUE
    * @param int $expire Number of seconds before the cache expires (default: 30)
    * 
    * @return boolean
    */
  function set($key, $value, $expire=30) {
    try {
      $this->cache->set($key, $value, MEMCACHED_COMPRESS, $expire);
      return TRUE;
    } catch (Exception $e) {
      $this->err->err_from_string("Unable to store data in memory cache");
      return FALSE;
    }
  }
    
  /**
    * Modifies the namespace $objName and change its value to $objValue
    * 
    * @param string $name Key
    * @return mixed
    */
  function get($key) {
      try {
        if ($payload = $this->cache->get($key)) {
          $this->debugger->add_hit("Memcache hit", NULL, NULL, $name);
        }
        return $payload;
      } catch (Exception $e) {
        return NULL;
      }
  }
  
  /**
    * Creates an advisory-lock against a key. Will use a resource-free spinlock
    * until $timeout is reached before returning FALSE if the key is already locked.
    * 
    * @param string $key Key
    * @param integer $timeout Timeout (in seconds) before giving up. 
    * @return boolean
    */
  function acquire_lock($key, $acquire_timeout=14, $expire=60) {
    $key .= '_lock';
    $locked = FALSE;
    $elapsed = 0;
    try {
        while (($elapsed < $acquire_timeout) && ($locked == FALSE)) {
      if (!($locked = $this->cache->add($key, NULL, FALSE, $expire))) {
          sleep(1);
      }
        }
        return $locked;
    } catch (Exception $e) {
      $this->err->err_from_string("Unable to acquire cache lock.");
    }
    return FALSE;
  }
    
  /**
    * Releases a lock against a key
    * 
    * @param string $key Key (will be hashed)
    * @return boolean
    */
  function release_lock($key) {
    try {
        return $this->cache->delete($key);
    } catch(Exception $e) {
        return FALSE;
    }
  }  
}
?>
