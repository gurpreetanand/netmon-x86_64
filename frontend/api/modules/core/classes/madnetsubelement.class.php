<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


/**
  * MadnetSubElement superclass
  *
  * The SubElement is typically used to handle the details and
  * logic behind subtle and complex relationship between Element
  * objects and the rest of the environment. In the user account
  * example, they will contain the logic behind the processing
  * of user to group relationship.
  *
  * This means our subElement would go in the user2group table,
  * find out more about the user object that has already been inserted
  * (through the META structure called $element in our context),
  * and perform all the INSERT based on that user_id and the POST data
  *
  * @package MADNET
  * @author Xavier Spriet
  */

class MadnetSubElement {
	
	/**
	  * Key to use as primary key for our primary table
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $pkey;
	/**
	  * Primary database table for the relationship
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $table;
	/**
	  * Whether the relationship is mandatory or not
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $mandatory;
	/**
	  * User doc for this element
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $doc;
	/**
	  * Friendly name for this field
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $friendly_name;
	/**
	  * Foreign Key (typically $this->element['pkey'])
	  *
	  * @var string $pkey
	  * @access private
	  */
	var $fkey;
	
	/**
	  * MadnetSubelement superclass constructor
	  *
	  * Instanciates all the aggregated objects and singletons
	  * then calls the init() method on the subclass.
	  *
	  * @return MadnetSubElement
	  */
	function MadnetSubElement() {
		# Instanciate the registry (Abstract Factory & Singleton)
		$this->registry = Registry::get_registry();
		
		# DB Manager (SHOULD be a singleton)
		$this->db     = $this->registry->get_singleton("core", "db_manager");
		# Error handler (MUST be a singleton)
		$this->err    = $this->registry->get_singleton("core", "error_manager");
		# Error handler (MUST be a singleton)
		$this->debugger    = $this->registry->get_singleton("core", "debugger");
		
		# This cannot be a singleton. All Element objects will need their own.
		#$this->params = new Params_Manager();
		# Continue the init process

		$this->init();

	}
	
	/**
	  * Does nothing
	  *
	  * This declaration is simply here to prevent the
	  * application to crash or interrupt the process if
	  * the subclass did not implement this interface properly.
	  *
	  * @return boolean
	  */
	function validate() { }
	/**
	  * Does nothing
	  *
	  * This declaration is simply here to prevent the
	  * application to crash or interrupt the process if
	  * the subclass did not implement this interface properly.
	  *
	  * @return boolean
	  */
	function insert()   { }
	/**
	  * Does nothing
	  *
	  * This declaration is simply here to prevent the
	  * application to crash or interrupt the process if
	  * the subclass did not implement this interface properly.
	  *
	  * @return boolean
	  */
	function init()     { }
	/**
	  * Does nothing
	  *
	  * This declaration is simply here to prevent the
	  * application to crash or interrupt the process if
	  * the subclass did not implement this interface properly.
	  *
	  * @return boolean
	  */
	function getVal()   { }

	
}


?>