<!-- {$smarty.template} ($Id$) -->

	<table width='100%' height='100%' align='center' border='0'>
		<tr>
			<td align='left' width='75px'><img src='/assets/images/info.png' width='48px' height='48px' /></td>
			<td align='left'>Netmon is now performing its Network Device Discovery process. You should receive an email alert once this process has been completed.</td>
		</tr>
	</table>

<!-- End of {$smarty.template} -->
