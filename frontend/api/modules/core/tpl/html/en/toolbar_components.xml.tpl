<xml>

<button id="home" label="Home" action="?module=mod_layout&amp;action=render_section&amp;layout=home" method="GET" icon="000" targetFrame="main">
	<panel  name="pnl_left" width="600" scrolling="auto" id="dashboard_summary"  tpl="blank_panel" />
	<panel name="pnl_right" width="*"   scrolling="no"   id="dashboard_activity" tpl="blank_panel" />
</button>

{if $perms.console_access.access_network_console}
<button id="network" label="Network" action="?module=mod_layout&amp;action=render_section&amp;layout=network&amp;ip=&amp;" method="GET" icon="001" targetFrame="main">
	<panel  name="pnl_left" width="60%" scrolling="no" id="network_vne"   tpl="blank_panel" />
	<panel name="pnl_right" width="32%" scrolling="no" id="network_tools" tpl="blank_panel" />
</button>
{/if}

{if $perms.console_access.access_services_console}
<button id="services" label="Services" action="?module=mod_layout&amp;action=render_section&amp;layout=services" method="GET" icon="006" targetFrame="main">
	<panel  name="pnl_left"  width="70%" scrolling="auto" id="services_view"  tpl="blank_panel" />
	<panel  name="pnl_right" width="30%" scrolling="no"   id="services_tools" tpl="blank_panel" />
</button>
{/if}

{if $perms.console_access.access_devices_console}
<button id="devices" label="Devices" action="?module=mod_layout&amp;action=render_section&amp;layout=devices" method="GET" icon="005" targetFrame="main">
	<panel name="pnl_left"   width="26%" scrolling="no"   id="devices_tree"    tpl="blank_panel" />
	<panel name="pnl_middle" width="49%" scrolling="auto" id="devices_default" tpl="blank_panel" />
	<panel name="pnl_right"  width="25%" scrolling="no"   id="devices_help"    tpl="blank_panel" />
</button>
{/if}

{if $perms.console_access.access_syslog_console}
<button id="syslog" label="Syslog" action="?module=mod_layout&amp;action=render_section&amp;layout=syslog" method="GET" icon="007" targetFrame="main">
	<panel   name="pnl_left" width="17%" scrolling="no"   id="syslog_tree"   tpl="blank_panel" />
	<panel name="pnl_middle" width="55%" scrolling="auto" id="syslog_default" tpl="blank_panel"/>
	<panel  name="pnl_right" width="28%" scrolling="no"   id="syslog_help"   tpl="blank_panel" />
</button>
{/if}

{if $perms.console_access.access_reports_console}
<button id="reports" label="Reports" action="?module=mod_layout&amp;action=render_section&amp;layout=reports" method="GET" icon="003" targetFrame="main">
	<panel name="pnl_left"   width="20%" scrolling="no"   id="reports_tree"    tpl="blank_panel" />
	<panel name="pnl_middle" width="56%" scrolling="auto" id="reports_default" tpl="blank_panel" />
	<panel name="pnl_right"  width="24%" scrolling="no"   id="reports_help"    tpl="blank_panel" />
</button>
{/if}

{if $perms.console_access.access_files_console}
<button id="files" label="Files" action="?module=mod_layout&amp;action=render_section&amp;layout=files" method="GET" icon="010" targetFrame="main">
	<panel name="pnl_left"   width="20%" scrolling="no"   id="fs_dir_tree"     tpl="blank_panel" />
	<panel name="pnl_middle" width="56%" scrolling="auto" id="fs_main"   tpl="blank_panel" />
	<panel name="pnl_right"  width="24%" scrolling="no"   id="fs_help" tpl="blank_panel" />
</button>
{/if}

{if $perms.administration.change_settings}
<button id="settings" label="Settings" action="?module=mod_layout&amp;action=render_section&amp;layout=settings" method="GET" icon="009" targetFrame="main">
	<panel name="pnl_left"   width="20%" scrolling="no"   id="settings_tree"    tpl="blank_panel" />
	<panel name="pnl_middle" width="50%" scrolling="auto" id="settings_default" tpl="blank_panel" />
	<panel name="pnl_right"  width="30%" scrolling="no"   id="settings_help"    tpl="blank_panel" />
</button>
{/if}

<button id="logout" label="Log Off" action="?module=mod_user&amp;action=logout" method="GET" icon="099" targetFrame="_top">
</button>

</xml>
