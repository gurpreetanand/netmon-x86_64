<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.event,yui.connection,keepalive,ext/adapter/ext/ext-base,ext/ext-all-debug"}
{import_css files="ext.ext-all"}


<base target="_blank">

{literal}
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
//-->
</script>
<script>
<!--

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_nbGroup(event, grpName) { //v6.0
  var i,img,nbArr,args=MM_nbGroup.arguments;
  if (event == "init" && args.length > 2) {
    if ((img = MM_findObj(args[2])) != null && !img.MM_init) {
      img.MM_init = true; img.MM_up = args[3]; img.MM_dn = img.src;
      if ((nbArr = document[grpName]) == null) nbArr = document[grpName] = new Array();
      nbArr[nbArr.length] = img;
      for (i=4; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
        if (!img.MM_up) img.MM_up = img.src;
        img.src = img.MM_dn = args[i+1];
        nbArr[nbArr.length] = img;
    } }
  } else if (event == "over") {
    document.MM_nbOver = nbArr = new Array();
    for (i=1; i < args.length-1; i+=3) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = (img.MM_dn && args[i+2]) ? args[i+2] : ((args[i+1])? args[i+1] : img.MM_up);
      nbArr[nbArr.length] = img;
    }
  } else if (event == "out" ) {
    for (i=0; i < document.MM_nbOver.length; i++) {
      img = document.MM_nbOver[i]; img.src = (img.MM_dn) ? img.MM_dn : img.MM_up; }
  } else if (event == "down") {
    nbArr = document[grpName];
    if (nbArr)
      for (i=0; i < nbArr.length; i++) { img=nbArr[i]; img.src = img.MM_up; img.MM_dn = 0; }
    document[grpName] = nbArr = new Array();
    for (i=2; i < args.length-1; i+=2) if ((img = MM_findObj(args[i])) != null) {
      if (!img.MM_up) img.MM_up = img.src;
      img.src = img.MM_dn = (args[i+1])? args[i+1] : img.MM_up;
      nbArr[nbArr.length] = img;
  } }
}
//-->
</script>
<script>
MM_preloadImages('assets/toolbar/home.gif','assets/toolbar/home_over.gif','assets/toolbar/network_over.gif','assets/toolbar/services_over.gif','assets/toolbar/devices_over.gif','assets/toolbar/environment_over.gif','assets/toolbar/reports_over.gif','assets/toolbar/settings_over.gif','assets/toolbar/logoff_over.gif');
</script>

{/literal}

{strip}
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
      <td width="166"><img src="assets/toolbar/netmon-logo.gif" width="166" height="68"></td>
	  <td>
	  	<a href="/?module=mod_layout&amp;action=render_section&amp;layout=home" target="main_window" onClick="MM_nbGroup('down','group1','dashboard','assets/toolbar/home_over.gif',1)" onMouseOver="MM_nbGroup('over','dashboard','assets/toolbar/home_over.gif','assets/toolbar/home_over.gif',1)" onMouseOut="MM_nbGroup('out')">
		  	<img src="assets/toolbar/home_over.gif" alt="Netmon Home Dashboard" name="dashboard" border="0" id="dashboard" onload="MM_nbGroup('init','group1','dashboard','assets/toolbar/home.gif',1)">
	  	</a>

	  	{if has_perm(access_services_console)}
	  	<a href="?module=mod_layout&amp;action=render_section&amp;layout=services" target="main_window" onClick="MM_nbGroup('down','group1','services','assets/toolbar/services_over.gif',1)" onMouseOver="MM_nbGroup('over','services','assets/toolbar/services_over.gif','assets/toolbar/services_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/services.gif" alt="Trackers Explorer" name="services" width="71" height="71" border="0" id="services">
	  	</a>
	  	{/if}

	  	{if has_perm(access_network_console)}
	  	<a href="?module=mod_layout&amp;action=render_section&amp;layout=network&amp;ip=&amp;" target="main_window" onClick="MM_nbGroup('down','group1','network','assets/toolbar/network_over.gif',1)" onMouseOver="MM_nbGroup('over','network','assets/toolbar/network_over.gif','assets/toolbar/network_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/network.gif" alt="Network Explorer" name="network" width="71" height="71" border="0" id="network">
	  	</a>
	  	{/if}



	  	{if has_perm(access_devices_console)}
	  	<a href="?module=mod_layout&amp;action=render_section&amp;layout=devices" target="main_window" onClick="MM_nbGroup('down','group1','devices','assets/toolbar/devices_over.gif',1)" onMouseOver="MM_nbGroup('over','devices','assets/toolbar/devices_over.gif','assets/toolbar/devices_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/devices.gif" alt="Devices Explorer" name="devices" width="71" height="71" border="0" id="devices">
	  	</a>
	  	{/if}

	  	{if has_perm(access_syslog_console)}
	  	<a href="?module=mod_layout&action=render_section&layout=syslog" target="main_window" onClick="MM_nbGroup('down','group1','syslog','assets/toolbar/syslog_over.gif',1)" onMouseOver="MM_nbGroup('over','syslog','assets/toolbar/syslog_over.gif','assets/toolbar/syslog_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/syslog.gif" alt="Event Logs" name="syslog" width="71" height="71" border="0" id="syslog">
	  	</a>
	  	{/if}


	  	{if has_perm(access_environment_console)}
	  	<a href="?module=mod_layout&action=render_section&layout=environment" target="main_window" onClick="MM_nbGroup('down','group1','environment','assets/toolbar/environment_over.gif',1)" onMouseOver="MM_nbGroup('over','environment','assets/toolbar/environment_over.gif','assets/toolbar/environment_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/environment.gif" alt="?module=mod_layout&amp;action=render_section&amp;layout=environment" name="environment" width="71" height="71" border="0" id="environment">
	  	</a>
	  	{/if}


	  	{if has_perm(access_reports_console)}
	  	<a href="?module=mod_layout&amp;action=render_section&amp;layout=reports" target="main_window" onClick="MM_nbGroup('down','group1','reports','assets/toolbar/reports_over.gif',1)" onMouseOver="MM_nbGroup('over','reports','assets/toolbar/reports_over.gif','assets/toolbar/reports_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/reports.gif" alt="Reports Explorer" name="reports" width="71" height="71" border="0" id="reports">
	  	</a>
	  	{/if}

	  	{if has_perm(access_files_console)}
	  	<a href="?module=mod_layout&action=render_section&layout=files" target="main_window" onClick="MM_nbGroup('down','group1','files','assets/toolbar/files_over.gif',1)" onMouseOver="MM_nbGroup('over','files','assets/toolbar/files_over.gif','assets/toolbar/files_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/files.gif" alt="Files Manager" name="files" width="71" height="71" border="0" id="files">
	  	</a>
	  	{/if}

	  	{if has_perm(change_settings)}
	  	<a href="?module=mod_layout&amp;action=render_section&amp;layout=settings" target="main_window" onClick="MM_nbGroup('down','group1','settings','assets/toolbar/settings_over.gif',1)" onMouseOver="MM_nbGroup('over','settings','assets/toolbar/settings_over.gif','assets/toolbar/settings_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/settings.gif" alt="Settings Explorer" name="settings" width="71" height="71" border="0" id="settings">
	  	</a>
	  	{/if}


	  	<a href="?module=mod_user&amp;action=logout" target="_top" onClick="MM_nbGroup('down','group1','logoff','assets/toolbar/logoff_over.gif',1)" onMouseOver="MM_nbGroup('over','logoff','assets/toolbar/logoff_over.gif','assets/toolbar/logoff_over.gif',1)" onMouseOut="MM_nbGroup('out')">
	  		<img src="assets/toolbar/logoff.gif" alt="Log Off" name="logoff" width="71" height="71" border="0" id="logoff">
	  	</a>
	  </td>
	  <td align="right">
        <div class="version">
          <div class="version-title">Version</div><div class="version-number">{$smarty.const.VERSION}</div>
        </div>
        <div class="current-user">Current User: <strong><span style="color: #0066CC;">{$smarty.session.first_name|capitalize|cat:" "|cat:$smarty.session.last_name|capitalize}</span></strong>&nbsp;&nbsp;&nbsp;</div>
      </td>
	</tr>
</table>
{/strip}

<script language="Javascript">
	keepAlive = new KeepAlive();
	keepAlive.init();
</script>

<!-- end of {$smarty.template} -->

