
<!-- {$smarty.template} ($Id$) -->

<div class="panel noPrint">
<form action="?module=mod_network&action=perform_traceroute" method="POST" onsubmit="showProgressIndicator('progress');">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td>Host (Name or IP)</td>
		{capture assign="host"}{$smarty.post.host}{/capture}
		<td>{input type="text" value="$host" name="host"}</td>
		<td>{input type="submit" class="button" value="Trace"}</td>
		<td><img id="progress" class="progress" src="/assets/progress.gif" style="display: inline"></td>
	</tr>
</table>
</form>
</div>

<!-- end of {$smarty.template} ($Id$) -->
