<graph {if $protocol == 'sflow'}yAxisMaxValue="100.01" yAxisMinValue="0" showLimits="1" subcaption="Estimated Protocol Distribution"{/if} chartLeftMargin="0" chartRightMargin="0" canvasBgDepth="2" canvasBaseDepth="1" caption="{if not $interface.interface}Local Traffic Sniffers{else}Interface {$interface.interface}: ({$interface.name}){/if}" divlinecolor="C5C5C5" numberSuffix="{if $protocol == 'netflow'}bps{else}{"%"|escape:'url'}{/if}" decimalPrecision="2" numdivlines="4" showAreaBorder="1" areaBorderColor="444444" animation="0" numberPrefix="" canvasBgColor="E1E1E1" canvasbordercolor="888888" canvasBorderThickness="2" showNames="1" numVDivLines="25" vDivLineAlpha="30" showAlternateHGridColor="1" alternateHGridColor="C9C9C9" formatNumberScale="1" rotateNames="1" bgcolor="f1f1f1" showLegend="1" >

<categories>
{foreach from=$times item="time"}{counter assign="tscount"}	
	<category name="{$time|date_format:'%H:%M'}" showname="{cycle values="1,0"}" />
{/foreach}
</categories>

{foreach from=$sets item="port" key="port_id"}
<dataset seriesName="{$port.port|resolve_port}" showAreaBorder="1" areaBorderThickness="1" color="{$colours[$port_id]}" showValues="0" areaAlpha="80">
{foreach from=$times item="time" key="time_id"}
{if $protocol == 'netflow'}
<set value="{$port.times[$time]/60|string_format:"%d"}" />
{else}
<set value="{$port.times[$time]|string_format:"%f"}" />
{/if}
{/foreach}
</dataset>
{/foreach}

</graph>