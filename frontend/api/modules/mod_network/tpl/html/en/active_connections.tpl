<!-- {$smarty.template} ($Id$) -->
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("connections").className = 'sort-table';
	var types = [null, 'CaseInsensitiveString', 'netmon_string_with_html', 'netmon_throughput'];
	var report = new SortableTable(document.getElementById("connections"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="/assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>Active Connections Snapshot: {$smarty.get.ip}</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>
{if $connections}
<div class="datagrid">
<table id="connections" width="100%" border="0" cellpadding="3" cellspacing="0">
	<thead>	
	<tr>
		<td width="16"><img src="assets/core/spacer.gif" width="16" height="16"></td>
		<td>Host</td>
		<td width="65">Port</td>
		<td width="65">Speed</td>
	</tr>
	</thead>
{foreach from=$connections item="entry"}
	<tr>
		<td width="16"><div title="{if $entry.direction == 'down'}{$entry.host} => {$smarty.get.ip}{else}{$smarty.get.ip} => {$entry.host}{/if}"><img src="assets/icons/{$entry.direction}load.gif" class="icon"></div></td>
		<td><a href="#" title="{$entry.host}" onClick="parent.parent.location = '?module=mod_layout&action=render_section&layout=network&ip={$entry.host}&store_request=2';">{$entry.host|resolve_ip}</a></td>
		<td width="65"><div align="center">
		{capture assign="missing"}{$entry.port|cat:"-"|cat:$entry.transport_layer}{/capture}
		<a title="{$entry.ports}" href="javascript:parent.showHelp('?module=mod_help&action=get_protocol_doc&transport_layer={$entry.transport_layer}&port_number={$entry.port}');" >
		{$entry.port|resolve_port|capitalize|default:$missing}
		</a></div></td>
		<td width="65"><div align="center">{$entry.rate}</div></td>
	</tr>
{/foreach}
</table>
</div>
{/if}

<!-- end of {$smarty.template} -->
