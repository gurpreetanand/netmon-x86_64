<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		{strip}
		<!-- {$smarty.template} ($Id$) -->
		{*import_js files="_debugger,_core" op="unshift"*}
		{import_js files="_debugger,_core,yui.yahoo,yui.dom,ext/adapter/ext/ext-base,ext/ext-all,draw2d/wz_jsgraphics,draw2d/mootools,draw2d/moocanvas,draw2d/draw2d,trackerTooltip,trackerConnector,topology_mapper,node_manager,topology_node"}
		{import_css files="ext.ext-all,ext.xtheme-gray,topology"}
		{get_imports type="js"}
		{get_imports type="css"}
		{/strip}
		<link rel="SHORTCUT ICON" href="/assets/icons/fav.ico">
		<title>Network Topology Map</title>
	</head>
	
	<!-- CSS Declarations -->
{foreach from=$css_files item="file"}
	<link type="text/css" rel="stylesheet" href="{$file}"  {if '/assets/css/_print.css' == $file}media="print"{/if} />
{/foreach}
	<!-- End of CSS Declarations -->
	
	<body scroll="no" id="mapper">
	
		<div id="loading-mask" style=""></div>
		<div id="loading">
			<div class="loading-indicator"><img src="/assets/progress_big_transparent.gif" width="32" height="32" style="margin-right:8px;" align="absmiddle"/><div id="status">Loading Dependencies</div></div>
		</div>
		
		<div id="map"><div id="workflow" style="position:relative;width:4000px;height:4000px;" ></div></div>


		<!-- Javascript Dependencies -->
	{foreach from=$js_files item="file"}
		<script src="{$file}"></script>
	{/foreach}
	<!-- End of Javascript Dependencies -->


		{literal}
		
		<!-- Netmon Topology Mapper -->
		<script language="JavaScript">
			/* Initialize the mapper object */
			var mapper = new Topology_Mapper();
			mapper.init();
			
			
			/* Remove the loading overlay */
			Ext.get('loading-mask').fadeOut({remove: true});
			
			/* Construct the UI */
			mapper.load_ui();
			window.onresize = mapper.maximize;
			
			/* Retrieve JSON data */
			mapper.ajax_load_topology();
			
		</script>
		<!-- End of Netmon Topology Mapper code -->
		
		{/literal}
	<!-- end of {$smarty.template} -->
	</body>
</html>