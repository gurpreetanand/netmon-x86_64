<!-- {$smarty.template} ($Id$) -->
{import_js files="AC_RunActiveContent"}
{literal}
<script type="JavaScript">
	setTimeout("window.location.reload()", 15000);
</script>
{/literal}

<!-- Button Row -->
	<div class="panel">

<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">

		<!-- Host: <input type="text" name="disk_ip" id="disk_ip" value="{$smarty.get.ip|default:""}" style="width: 100px;"  /> -->
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="parent.parent.pnl_right.showHelp('disk_windows');">

		<img src="assets/buttons/disks.gif" class="icon" title="Manage Disks" onClick="document.location='?module=mod_network&action=get_disks&root_tpl=blank_panel';">
		{input type="button" class="button" value="Add New Disk Tracker" onClick="parent.parent.pnl_right.showEditor('?module=mod_network&action=form_add_disk&root_tpl=blank_panel');"}
	</div>

{if $disks}
	{foreach from=$disks item="disk"}



	<div class="panel">
		<table width="100%" cellpadding="10" cellspacing="0">
			<tr>
				<td width="83"><div align="center">
				
					<script type="text/javascript">
					AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
					'width','63',
					'height','57',
					'src','assets/diskview?currentStatus={$disk.status|default:""}&animation=1&threshold={$disk.threshold|default:"0"}',
					'quality','high',
					'salign','LT',
					'pluginspage','http://www.macromedia.com/go/getflashplayer',
					'movie','assets/diskview?currentStatus={$disk.status|default:""}&animation=1&threshold={$disk.threshold|default:"0"}'
					 ); //end AC code
					</script>
				
					<br>
					{if $disk.total > 0}
					<strong>{$disk.used|capacity}</strong> of <strong>{$disk.total|capacity}</strong> 					{/if}
					</div>
				</td>

				<td>
					<div align="center">
					<strong>{$disk.name}</strong> on <strong>{if $disk.servername}{$disk.servername}{else}{$disk.ip|resolve_ip}{/if} ({$disk.ip})</strong><br /><br />
					Last Checked: {$disk.timestamp|date_format:"%b %e, %Y %H:%M:%S"}<br /><br />
					{if ($disk.message|replace:" ":"") != ""}
						Status Message: <strong>{$disk.message}</strong>
					{/if}
				</td>
				<td>
					<div align="center">
					<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_network&action=form_edit_disk&disk_type={$disk.type|lower}&id={$disk.srv_id}');">Edit</a><br>
					<a href="javascript:parent.parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type={$disk.type|lower|replace:"smb":"smb_disk"}_over_threshold&disk_type={$disk.type|lower}_servers&disk_id={$disk.srv_id}');">Alerts</a><br>
					<a href="#"onClick="if(confirm('Are you sure you want to remove this disk monitor? Click OK to confirm.')) {ldelim}parent.parent.pnl_right.showEditor('?module=mod_network&action=process_delete_disk&disk_type={$disk.type|lower}&disk_id={$disk.srv_id}');{rdelim}; return false;">Delete</a><br>
					</div>
				</td>
			</tr>
		</table>
	</div>


	{/foreach}
{else}
{"Netmon is not currently monitoring any remote storage partitions."|message_bar}
{/if}


<!-- end of {$smarty.template} -->
