<interfaces>
{foreach from=$interfaces item="interface"}
	<interface device_id="{$interface.device_id}" iface="{$interface.interface}" name="{$interface.description}" device_name="{$interface.label}" />
{/foreach}
</interfaces>