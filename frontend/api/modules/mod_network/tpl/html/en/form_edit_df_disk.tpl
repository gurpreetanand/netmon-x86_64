<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation"}
{literal}
<script language="Javascript">
hostname_success_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.responseText;
}

hostname_failure_handler = function(o) {
	hideProgressIndicator('hostname_progress');
	document.getElementById('hostname_field').value = o.document.getElementById('ip').value;
}

get_server_hostname = function() {
	ipElem = document.getElementById('ip');
	ip_addr = ipElem.value;
	
	url = "?module=mod_network&action=ajax_resolver_hook&root_tpl=blank&ip=" + ip_addr;
	
	showProgressIndicator('hostname_progress');
	YAHOO.util.Connect.asyncRequest('GET', url, {success:hostname_success_handler, failure:hostname_failure_handler}, null);
}
</script>
{/literal}
<div class="panel">
<form action="?module=mod_network&action=process_update_disk&disk_type=df&srv_id={$dm->get('srv_id')}" name="edit_disk_form" method="POST">
<table width="100%" cellpadding="3" cellspacing="0" border="0" align="center">
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;IP Address:</td>
		<td width="60%"><input onChange="get_server_hostname();" type="text" id="ip" name="ip" {param name="ip"} value="{$smarty.post.ip|default:$dm->get('ip')}"></td>
	</tr>
	<tr>
		<td align="right">Hostname:</td>
		<td><input type="text" {param name="servername"} id="hostname_field" value="{$smarty.post.servername|default:$dm->get('servername')}" /><img class="progress" id="hostname_progress" src="/assets/progress.gif" /></td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Partition Name:</td>
		<td width="60%"><input type="text" name="partition" {param name="partition"} value="{$smarty.post.partition|default:$dm->get('partition')}"> Port: <input type="text" size="3" name="port" {param name="port"} value="{$smarty.post.port|default:$dm->get('port')}">&nbsp;</td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Timeout:</td>
		<td width="60%"><input type="text" size="3" name="timeout" {param name="timeout"} value="{$smarty.post.timeout|default:$dm->get('timeout')}"> minute(s)</td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Interval:</td>
		<td width="60%"><input type="text" name="interval" size="3" {param name="interval"} value="{$smarty.post.interval|default:$dm->get('interval')}"> second(s) </td>
	</tr>
	<tr>
		<td align="right" width="40%">&nbsp;&nbsp;Threshold</td>
		<td width="60%"><input type="text" name="threshold" size="3" {param name="threshold"} value="{$smarty.post.threshold|default:$dm->get('threshold')}">%</td>
	</tr>
	<tr>
		<td colspan="2" align="center" width="100%">{input type="submit" class="button" value="Update Disk"}</td>
	</tr>
</table>
</form>
</div>
<!-- end of {$smarty.template} -->
