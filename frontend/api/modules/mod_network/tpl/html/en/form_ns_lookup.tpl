<!-- {$smarty.template} ($Id$) -->



<div class="panel noPrint">
<form action="?module=mod_network&action=perform_ns_lookup" method="POST">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td>Hostname</td>
		{capture assign="host"}{$smarty.post.host}{/capture}
		<td>{input type="text" value="$host" name="host"}</td>
	</tr>
	<tr>
		<td>Record Type</td>
		<td>{html_options name="type" options=$types}</td>
	</tr>
	<tr>
		<td colspan="2" align="center">{input type="submit" class="button" value="Lookup"}</td>
	</tr>
</table>
</form>
</div>

<!-- end of {$smarty.template} -->