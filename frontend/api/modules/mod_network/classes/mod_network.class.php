<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca) */


// mod_network.class.php

/**
  * Network MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */

Class Mod_Network extends MadnetModule {

  # Returns an XML document describing current IP conversations on the network.
  # This data is used for the VNE flash control to map out network activity.
  #
  function get_vne_data() {
    # NEW: Support for mini-VNE
    $device_id = intval($_GET['device_id']);
    $interface = intval($_GET['interface']);

    if ($device_id > 0 && $interface > 0) {
      $query = "SELECT shm_key FROM interfaces WHERE device_id = $device_id AND interface = $interface LIMIT 1";
      $res = $this->db->get_row($query);
      if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
        return;
      }
      $key = hexdec(base_convert($res['shm_key'], 10, 16));
    } else {
      $key = SHM_TOP_IP_STREAMS;
    }

    # Retrieve the Shared Memory data-structure at key SHM_TOP_IP_STREAMS
    # XS: Or at the shm_key found in the DB bound to the interface specified
    # through GET
    $this->shm = $this->registry->get_singleton("core", "shm_manager");
    $data = $this->shm->get_data_structure($key);



    /************** OUTPUT FILTERING **************/
    # Filters Manager singleton
    if (($_GET['host_filter']) || ($_GET['traffic_filter'])) {
      $this->filter   = $this->registry->get_singleton("core", "filters_manager");
    }

    # Validate and store host filter data
    if (($_GET['host_filter']) && ($this->filter->is_valid_filter("host", $_GET['host_filter']))) {
      $host_filter = $this->filter->get_filter_data("host", $_GET['host_filter']);
    } elseif ($_GET['host_filter'] == "selected") {
      $host_filter = array($_GET['ip']);
      # Erase it from the last request session var for future requests
      unset($_SESSION['request']['ip']);
    }
    
    # Validate and store traffic filter data
    if (($_GET['traffic_filter']) && ($this->filter->is_valid_filter("traffic", $_GET['traffic_filter']))) {
      $traffic_filter = $this->filter->get_filter_data("traffic", $_GET['traffic_filter']);
    }
    
    /*********** END OF OUTPUT FILTERING **********/

    $links = array();
    $nodes = array();
    $node_finder = array();

    $count_entries = 0;

    foreach($data as $current) {

      /*********** MORE OUTPUT FILTERING **********/

      # Make sure the source or dst IP aren't filtered
      if ($host_filter) {
        if((!in_array($current[0], $host_filter)) && (!in_array($current[1], $host_filter))) {
          continue;
        }
      }

      # Traffic filters are a little harder to fix because they rely on
      # Combination of transport layers and ports
      if ($traffic_filter) {
        $in_filter_flag = FALSE;

        # e.g. 80-tcp
        $src = trim(strtolower($current[2] . "-" . $current[4]));
        # e.g. 2245-tcp
        $dst = trim(strtolower($current[3] . "-" . $current[4]));


        foreach($traffic_filter as $tx) {
          # e.g. 80-tcp
          $cmp = trim(strtolower($tx['port'] . "-" . $tx['type']));

          # If our $src or $dst is the same as $cmp, we're gold, otherwise, we are out.
          if ((strcmp($cmp, $src) == 0) || (strcmp($cmp, $dst) == 0)) {
            $in_filter_flag = TRUE;
          }
        }

        # Next iteration of the foreach loop if we are out (before the array_push)
        if (!$in_filter_flag) {
          continue;
        }
      }

      /*********** END OF OUTPUT FILTERING **********/
      # Add our entry to the XML document
      $max = (intval($_GET['limit']) > 0) ? intval($_GET['limit']) : SHM_MAX_ENTRIES;
      if (++$count_entries > $max) {
        break;
      }



      $src = $this->get_host_data($current[0]);
      $dst = $this->get_host_data($current[1]);
      
      $src_node_index = -1;
      $dst_node_index = -1;

      if (!$node_finder[$src['ip']]) {
        $src_node_index = array_push($nodes,
          array(
            "name" => $src['ip'],
            "group" => 1,
            "info" => $src
          )
        ) - 1;
        $node_finder[$src['ip']] = $src_node_index;
      } else {
        $src_node_index = $node_finder[$src['ip']];
      }
      
      if (!$node_finder[$dst['ip']]) {
        $dst_node_index = array_push($nodes,
          array(
            "name" => $dst['ip'],
            "group" => 1,
            "info" => $dst
          )
        ) - 1;
        $node_finder[$dst['ip']]  = $dst_node_index;
      }  else {
        $dst_node_index = $node_finder[$dst['ip']];
      }
      
      array_push($links, array(
        "value" => $current[6],
        "source" => $src_node_index,
        "target" => $dst_node_index,
        "source_port" => $current[2],
        "target_port" => $current[3]
        )
      );
    }


    $payload = array(
      "nodes" => $nodes,
      "links" => $links,
      "key" => $key
    );

    print json_encode($payload, true);
  }

  function get_active_connections() {

    if ($_GET['ip']) {
      $shm = $this->registry->get_singleton("core", "shm_manager");

      # NEW: Support for mini-VNE
      $device_id = intval($_GET['device_id']);
      $interface = intval($_GET['interface']);

      if ($device_id > 0 && $interface > 0) {
        $query = "SELECT shm_key FROM interfaces WHERE device_id = $device_id AND interface = $interface LIMIT 1";
        $res = $this->db->get_row($query);
        if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
          return;
        }
        $key = hexdec(base_convert($res['shm_key'], 10, 16));
      } else {
        $key = SHM_TOP_IP_STREAMS;
      }

      $data = $shm->get_data_structure($key);

      $this->debugger->add_hit(sizeof($data) . " Entries loaded in SHM", NULL, NULL, vdump($data));

      $ds = array();

      foreach($data as $current) {
        $host = "";

        # If the target IP is the source point, we want to use the destination port
        if ($current[0] == $_GET['ip']) {
          $host = $current[1];
          $dir = "up";

          #$port = $current[3];
        # If the target IP is the destination, we want the source port.
        } elseif ($current[1] == $_GET['ip']) {
          $host = $current[0];
          $dir = "down";
          #$port = $current[2];
        }
        $port = min($current[3], $current[2]);
        $ports = $current[2] . " => " . $current[3];
        #$port = ($dir == "down") ? $current

        # If the previous if/elseif did not find a match, $host will be empty
        if (strlen($host) > 1) {
          array_push($ds, array("host" => $host, "rate" => $current[6],
                     "port" => $port, "transport_layer" => $current[4],
                     "direction" => $dir, "ports" => $ports));
        }

      }

      $parser = new Parser($this->tpl_dir);

      $parser->register_modifier("resolve_port", array(&$this, "_resolve_port"));

      $parser->assign_by_ref("connections", &$ds);

      # The Dashboard module has an IP resolver for the top activity panels so we will let smarty use it.
      $dashboard = require_module("mod_dashboard");
      $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));

      $index_content .= $parser->fetch("active_connections.tpl");
    }

  }

  function _resolve_port($int){
    $query = "SELECT name FROM protocols WHERE port = ".$this->db->escape($int);
    $res = $this->db->get_row($query);

    if((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return NULL;
    }

    return $res['name'];
  }

  function perform_portscan() {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      print '[]';
      return;
    }
    
    
    switch($_GET['scan_type']) {
      case com: $range = "-p 1-65535";               break;
      case cus: $range = "-p " . $_GET['port_list']; break;
      default:  $range = "-p 1-1024"; break;
    }

    $cmd = escapeshellcmd("sudo /usr/local/sbin/netscan $range " . $_GET['portscan_ip']." 2>&1");
    $this->debugger->add_hit("Portscan command: $cmd");

    $pipe = @popen($cmd, "r");

    if ($pipe) {
      $res = array();
      while ($val = fgets($pipe)) {
        array_push($res, $val);
      }
      @pclose($pipe);
    }
    
    $seq = '';
    foreach($res as $port) {
                        if (intval($port) > 0) {
                          $seq .= $this->db->escape(trim($port)) . ", ";
                        }
    }
    $seq = substr($seq, 0, strlen($seq)-2);

    # The query will fail if there are no elements in the IN clause.
    if (strcmp($seq, "") == 0) {
      $seq = "'-0'";
    }

    $query = "SELECT port, name FROM protocols WHERE port IN ($seq) GROUP BY port, name ORDER BY port ASC";
    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      print '[]';
      return;
    }

    # Now manually add the ports that have not been resolved.
    sort($res);
    foreach($res as $port) {
      $add_to_list = TRUE;
      foreach($result as $current) {
        if ($current['port'] == intval($port)) {
          $add_to_list = FALSE;
        }
      }
      if ($add_to_list == TRUE) {
        array_push($result, array("port" => trim($port), "name" => "Unknown"));
      }
    }

    print json_encode($result, true);
    return;
  }


  function get_disks(&$index_content) {
    require_class($this->module, "composite_disk_manager");
    $cm = new Composite_Disk_Manager();
    $res = $cm->get_all_ids();



    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("disks", &$res);

    # The Dashboard module has an IP resolver for the top activity panels so we will let smarty use it.
    $dashboard = require_module("mod_dashboard");
    $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));


    $index_content .= $parser->fetch("get_disks.tpl");
  }


  function form_add_disk(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    require_class($this->module, "composite_disk_manager");
    $cdm = new Composite_Disk_Manager();

    $parser->set_params($cdm->params);

    $index_content .= $parser->fetch("form_add_disk.tpl");
  }

  function form_edit_disk(&$index_content) {

    # Load managers in memory
    require_class($this->module, "smb_disk_manager");
    require_class($this->module, "df_disk_manager");

    switch(strtolower($_GET['disk_type'])) {
      case "df":  $dm = new DF_Disk_Manager(); break;
      case "smb": $dm = new SMB_Disk_Manager(); break;
      default: $this->err->err_from_string("Invalid disk selected"); return $this->get_disks($index_content);
    }

    if (!$dm->pop($_GET['id'])) {
      return $this->get_disks($index_content);
    }

    $parser = new Parser($this->tpl_dir);
    $parser->set_params($dm->params);
    $parser->assign("dm", &$dm);

    $index_content .= $parser->fetch("form_edit_".strtolower($_GET['disk_type'])."_disk.tpl");
    return;

  }

  function process_update_disk() {
    # Load managers in memory
    require_class($this->module, "smb_disk_manager");
    require_class($this->module, "df_disk_manager");

    switch(strtolower($_GET['disk_type'])) {
      case "df":
        $dm = new DF_Disk_Manager();
        break;
      case "smb":
        $dm = new SMB_Disk_Manager();
        break;
      default: 
        $this->err->err_from_code(400, "Invalid disk selected");
    }

    if ($dm->update()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to update disk");
    }
  }


  function process_add_disk(&$index_content) {

    # Load managers in memory
    require_class($this->module, "smb_disk_manager");
    require_class($this->module, "df_disk_manager");


    # Initialize the proper disk manager for insert
    switch($_GET['share_type']) {
      case "smb": 
        $dm = new SMB_Disk_Manager();
        break;
      case "df":
        $dm = new DF_Disk_Manager();
        break;
      default: 
        $this->err->err_from_code(400, "Invalid partition type selected");
        break;
    }

    # Call insert command on our disk manager
    if ($dm->insert()) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to create disk tracker");
    }
  }

  function get_host_data($ip) {

    $query = "SELECT hostname, node_type, ip, host_name_type
          FROM hosts
          WHERE ip = " . $this->db->escape($ip) . "
          ORDER BY timestamp DESC";

    $res = $this->db->select($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return array("ip" => $ip, "node_type" => "default", "hostname" => $ip);
    }

    foreach($res as $current) {
      if ($current['host_name_type'] == 'CUSTOM') {
        return $current;
      }
    }

    return $res[0];
  }


  function process_delete_disk() {

    # Load managers in memory
    require_class($this->module, "smb_disk_manager");
    require_class($this->module, "df_disk_manager");


    # Initialize the proper disk manager for insert
    switch(strtolower($_GET['disk_type'])) {
      case "smb": 
        $dm = new SMB_Disk_Manager();
        break;
      case "df":
        $dm = new DF_Disk_Manager();
        break;
      default: 
        $this->err->err_from_code(400, "Invalid partition type selected");
    }

    if ($dm->delete(intval($_GET['disk_id']))) {
      print '{}';
    } else { 
      $this->err->err_from_code(400, "Unable to delete disk");
    }
  }

  function ajax_auto_detect_smb_shares(&$index_content) {

    $username = $_GET['username'] ? "-u " . urldecode($_GET['username']) : "";
    $password = $_GET['password'] ? "-p " . urldecode($_GET['password']) : "";
    $domain =   $_GET['domain']   ? "-d " . urldecode($_GET['domain']) : "";
    $cmd = escapeshellcmd("/usr/local/sbin/smbscan $username $password $domain " . urldecode($_GET['ip']));
    $res = "";

    exec($cmd, $res);

    $this->debugger->add_hit("SMB Autodetect command: $cmd", NULL, NULL, vdump($res));

    sort($res);
    print json_encode($res, true);
  }

  function form_ns_lookup(&$index_content) {
    $parser = new Parser($this->tpl_dir);


    # RFC 1035
    $query_types = array("ANY"   => "[ANY] All Records",
               "A"     => "[A] Host Addresses",
               "CNAME" => "[CNAME] Alias",
               "NS"    => "[NS] Authoritative Name Servers",
               "MX"    => "[MX] Mail Exchange",
               "PTR"   => "[PTR] Domain Pointer",
               "SOA"   => "[SOA] Start of Authority",
               "HINFO" => "[HINFO] Host Information",
               "WKS"   => "[WKS] Well-Known services",
               "AXFR"  => "[AXFR] Zone Transfer");
    $parser->assign("types", $query_types);
    $index_content .= $parser->fetch("form_ns_lookup.tpl");
    return;
  }
  
  /**
   * Performs a traceroute
   *
   * @param pointer $index_content
   */
  function perform_traceroute() {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_code(400, "This operation is not available in Demo mode");
      print '[]';
      return;
    }
    
    $host = $_GET['host'];
    $cmd = escapeshellcmd("traceroute $host");
    $this->debugger->add_hit("Command", NULL, NULL, $cmd);
    
    $output = array();
    $return = array();
    
    exec($cmd, $output);
    
    foreach($output as $line) {
      $parts = array();
      if (eregi("^ *([0-9]+) +([^ ]+) +([^ ]+) +(.*)$", $line, $parts)) {
        $element = array();
        $element['hopcount'] = $parts[1];
        $element['hostname'] = $parts[2];
        $element['ip'] = str_replace(array("(", ")"), "", $parts[3]);
        $this->debugger->add_hit("Latencies", NULL, NULL, $parts[4]);
        $element['latencies'] = explode("  ", $parts[4]);
        
        array_push($return, $element);
      }
    }
    
    
                print json_encode($return, true);
    return;
  }

  function perform_ns_lookup(&$index_content) {
    
    if (has_perm("Guest/Demo Account")) {
      $this->debugger->add_hit("Demo mode enabled");
      $this->err->err_from_string("This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    $type = $_POST['type'];
    $host = $_POST['host'];


    $this->form_ns_lookup($index_content);

    $cmd = escapeshellcmd("sudo dig -t $type $host");

    # This will store the output of the command
    $output = array();

    # This array will store the data-structure to hand to the templating engine
    $final = array();

    # This is a temporary array to store elements from each line
    $tmp = array();


    exec($cmd, $output);

    # Separate the output in a multi-dimensional array for the templating engine
    foreach($output as $line) {
      if (strcmp(substr($line, 0, 1), ';') <> 0) {
        $line = str_replace(" ", "\t", $line);
        $tmp = explode("\t", $line);
        for ($i = 0; $i < sizeof($tmp); $i++) {
          if (empty($tmp[$i])) {
            unset($tmp[$i]);
          }
        }
        array_push($final, $tmp);
      }
    }

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("rows", $final);
    $index_content .= $parser->fetch("ns_lookup_results.tpl");

    return;
  }

  /**
   * Display the form for tcpdump capture
   *
   * @param pointer $index_content
   */
  function form_tcpdump(&$index_content) {
    $parser = new Parser($this->tpl_dir);

    $interfaces = array();
    $res = "";

    exec("sudo tcpdump -D", $res);
    foreach($res as $line) {
      eregi("[0-9]+\.([a-z0-9]+).*", $line, $parts);
      $interfaces[$parts[1]] = $parts[1];
    }

    $parser->assign("interfaces", $interfaces);
    $parser->assign("packets", array(100 => 100,
                     500 => 500,
                     1500 => 1500,
                     5000 => 5000,
                     10000 => 10000,
                     50000 => 50000,
                     100000 => 100000,
                     200000 => 200000));

    $index_content .= $parser->fetch("form_tcpdump.tpl");
  }
  
  
  /**
   * Displays the traceroute form
   *
   * @param pointer $index_content
   */
  function form_traceroute(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("form_traceroute.tpl");
  }

  /**
   * Runs tcpdump capture in the background and provide confirmation
   *
   * @param pointer $index_content
   */
  function perform_tcpdump() {
   $input = json_decode(file_get_contents("php://input"), true);

    
    if (has_perm("Guest/Demo Account")) {
      $this->err->err_from_code(400, "This operation is not available in Demo mode", FALSE);
      return FALSE;
    }
    
    
    $cmd_args = "";
    
    # Step 1: Determine the filename we want to use
    $filename = $input['interface'] . "_" . mktime() . ".cap";
    $cmd_args .= " --target $filename";

    # Step 2: Validate input
    $packets = intval($input['packets']);
    $cmd_args .= " -p $packets";

    if ($packets <= 0) {
      $this->err->err_from_code(400, "Incorrect number of packets specified.");
      return $this->form_tcpdump($index_content);
    }
    
    $ip = $input['ip_address'];
    if (strcmp("", $ip) <> 0) {
      $cmd_args .= " --ip $ip";
    }
    
    
    $port = $input['port'];
    if (strcmp("", $port) <> 0) {
      $cmd_args .= " --port $port";
    }
    
    $iface = $input['interface'];
    $cmd_args .= " -i $iface";
    
    # Step 2: Initiate the background capture
    #$fp = fopen("/var/captures/" . $filename, "w");
    #fclose($fp);
    


    $cmd = escapeshellcmd("sudo /apache/cli/capture.py $cmd_args");

    $this->debugger->add_hit("Shell command:", NULL, NULL, $cmd);
    $foo = FALSE;
    $status = proc_close(proc_open($cmd, array(), $foo));


    # Step 3: Run a VFS Sync and set the file as BUSY
    $mod = require_module("mod_vfs");

    $dir = $mod->fm->get_dir_info_by_path("/var/captures");

    $mod->fm->sync_directory($dir['id']);

    $query = "UPDATE fs_files SET busy = 't', label = " . $this->db->escape($input['label']) . " WHERE filename = " . $this->db->escape($filename);

    $res = $this->db->update($query);

    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_code(400, "Unable to lock the file");
    }

    # Step 4: Display a confirmation message.

    print '{}';
  }


  function get_xml_partitions(&$index_content) {
    require_class($this->module, "composite_disk_manager");
    $pm = new Composite_Disk_Manager();



    $devs = $pm->get_all_ids();

    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("devs", $devs);

    $dashboard = require_module("mod_dashboard");
    $parser->register_modifier("resolve_ip", array(&$dashboard, "resolve_ip"));

    $index_content .= $parser->fetch("xml_partitions.tpl");
    if (strcmp($_GET['root_tpl'], 'blank') == 0) {
      header("Content-type: application/xml");
    }
    return;
  }

  /**
   * AJAX hook for name-resolution service
   *
   * @param pointer $index_content
   */
  function ajax_resolver_hook(&$index_content) {
    $ip = $_GET['ip'];
    $time = intval($_GET['time']);

    if ($time <= 0) {
      $time = time();
    }

    $query = "SELECT host_lookup_by_time(" . $this->db->escape($ip) . ", " . $this->db->escape($time) . ") AS host";
    $res = $this->db->get_row($query);

    $index_content .= (strcmp($res['host'], "") > 0) ? $res['host'] : $ip;
    return;

  }

  /**
   * AJAX hook for port resolution service
   *
   * @param pointer $index_content
   */
  function ajax_resolve_port(&$index_content) {
    $port = $this->db->escape(intval($_GET['port']));

    if ($port <= 0) { $index_content .= "Unknown ($port)"; return; }
    $query = "SELECT name FROM protocols WHERE port = $port LIMIT 1";

    $res = $this->db->get_row($query);
    $index_content .= (strcmp($res['name'], "") > 0) ? $res['name'] : "Unknown ($port)";
  }


  /**
   * Displays the protocol breakdown graph
   *
   * @param pointer $index_content
   */
  function view_protocol_graph(&$index_content) {
    # device_id, interface_id
    $device = intval($_GET['device']);
    $interface = intval($_GET['interface']);

    if ($device == -1) {
      $res = array(
            "enable_netflow" => 't',
            "label" => "Local Analyzer",
            "ip_address" => "127.0.0.1",
            "interface" => "-1",
            "mac" => "00:00:00:00:00:00",
            "speed" => "100Mb/s"
            );
    } else {
      if ($device <= 0 || $interface <= 0) {
        $this->err->err_from_string("Invalid device/interface pair specified");
        return "Error";
      }

      $query = "SELECT a.*, b.label, b.ip_address, b.enable_netflow, b.enable_sflow
      FROM interfaces a, devices b
      WHERE b.id = a.device_id
      AND b.id = $device
      AND a.interface = $interface";

      $res = $this->db->get_row($query);

      if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
        $this->err->err_from_string("Unable to retrieve interface information");
        return "Error";
      }
    }


    $parser = new Parser($this->tpl_dir);
    if ($res['enable_netflow'] == 't') {
      $parser->assign("protocol", "netflow");
    } else {
      $parser->assign("protocol", "sflow");
    }
    
    $parser->assign("interface", $res);
    $index_content .= $parser->fetch("view_protocol_graph.tpl");
    return;
  }

  /**
   * Retrieves protocol utilization break-down for graphing system
   *
   * @param pointer $index_content
   */
  function graph_get_protocols(&$index_content) {
          $payload = $this->graph_get_protocols_payload();
          $parser = new Parser($this->tpl_dir);
          $parser->register_modifier("resolve_port", array(&$this, "_smarty_resolve_port"));
          
          foreach ($payload as $key => $value) {
            $parser->assign($key, $value);
          }

          $index_content .= $parser->fetch("xml_protocols_graph.tpl");

          if (strcmp($_GET['root_tpl'], 'blank') == 0) {
                  header("Content-type: application/xml");
          }
          return;
  }
  
  function graph_get_protocols_json() {
          $payload = $this->graph_get_protocols_payload();

          $payload = array(
            "timestamps" => $payload['timestamps'],
            "data" => $payload['sets']
          );

          foreach ($payload['data'] as &$set) {
            $set['key'] = $this->_smarty_resolve_port($set['port']);
            $set['values'] = $set['times'];
            unset($set['times']);
          }
          print json_encode($payload);
  }
  
  function graph_get_protocols_payload() {
    # device_id, interface_id
    $device_id = intval($_GET['device']);
    $interface = intval($_GET['interface']);
    $dir = $_GET['dir'];
    $time = time();

    $protocol = 'netflow';

    
    if ($device_id == -1) {
      $key = SHM_LOCAL_BREAKDOWN;
      $this->shm = $this->registry->get_singleton("core", "shm_manager");
      $data = $this->shm->get_data_structure($key);
    } else {
      if ($device_id <= 0 || $interface <= 0) {
        echo "Identifier Error"; return;
      }
      
      $query = "SELECT a.*, b.label, b.ip_address, b.enable_netflow, b.enable_sflow FROM
      interfaces a, devices b
      WHERE b.id = a.device_id
      AND a.device_id = $device_id
      AND a.interface = $interface
      LIMIT 1";
      $if_res = $this->db->get_row($query);
      if ((DB_QUERY_ERROR == $if_res) || (DB_NO_RESULT == $if_res)) {
        $data = NULL;
      } else {
        # Identify the proper key (result from shm +1)
        $key = hexdec(base_convert($if_res['shm_key']+1, 10, 16));

        # Retrieve the Shared Memory data-structure at key $key+1
        $this->shm = $this->registry->get_singleton("core", "shm_manager");
        $data = $this->shm->get_data_structure($key);
        
        if ($if_res['enable_sflow'] == 't') {
          $protocol = 'sflow';
        }
      }
    }
    

    # Now grab all entries from the DB.
    $query = "SELECT * FROM protocol_breakdown
    WHERE device = $device_id
    AND interface = $interface
    AND start_time > ((date_part('epoch', now()))::int4 - (3600 *
      (select int4(value) from daemonsconfig where var = 'breakdown_timeout' and daemon_id =
        (select id from daemons where name = 'flowd' limit 1)
      limit 1))
    )
    ORDER BY start_time, id DESC ";
    $res = $this->db->select($query);
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $sets = NULL;
      $times = NULL;
    } else {
      $sets = array();
      $times = array();
      $timestamps = array();

      # For large data sets, we don't show every single data point. (the client would choke trying to render them)
      $size = sizeof($res);
      $this->debugger->add_hit("Resultset size: $size");

      
      # Plot scaling
      $step = 1;
      if ($size > 60) {
        $step = ceil($size/60);
      }
      $this->debugger->add_hit("Step: $step");

      
      $junk_chars = array("{", "}");
      for($i = 0; $i < $size; $i += $step) {
        $current = $res[$i];
        array_push($times, $current['end_time']);
        array_push($timestamps, $current['end_time']);

        $ports_array = $current['ports'];
        $octets_array = $current['octets'];

        # Convert ports/octets from postgres arrays to php arrays
        $ports = array_map("trim", explode(",", str_replace($junk_chars, "", $ports_array)));
        $octets = array_map("trim", explode(",", str_replace($junk_chars, "", $octets_array)));

        # explode() returns an empty array even on failure so we need to make sure we have real data
        if (strcmp($ports[0], '') != 0) {
          $total = array_sum($octets);
          foreach($octets as $key => $val) {
            if ('netflow' == $protocol) {
              # We multiply by 8 to obtain bps instead of oct/s
              $val *= 8;
            } else {
              # We calculate the percentage of traffic going through this port
              $val = $val/$total*100;
            }
            # $ports[n] is associated with $octets[n]
            $port = $ports[$key];
            if (!is_array($sets[$port])) {
              $sets[$port] = array("port" => $port, "total" => $val, "times" => array($current['end_time'] => $val));
            } else {
              $sets[$port]['total'] += $val;
              $sets[$port]['times'][$current['end_time']] = $val;
            }
          }
        }
      }

      uasort($sets, array(&$this, "_sort_by_total"));
      $sets = array_slice($sets, 0, 10);
    }
    
    $payload = array(
                  "sets" => $sets,
                  "times" => $times,
                  "colours" => array("FF0000", "FF6600", "FFCC00", "DFDF00", "99CC00", "339999",
                                     "0066CC", "9933FF", "666666", "333333"),
                  "interface" => $if_res,
                  "protocol" => $protocol,
                  "timestamps" => $timestamps
    );
    return $payload;
  }

  function _sort_by_total($a, $b) {
    return $a['total'] < $b['total'];
  }


  /**
   * Shows the flash widget for the Mini-VNE
   *
   * @param pointer $index_content
   */
  function mini_vne(&$index_content) {

    $device = intval($_GET['device']);
    $interface = intval($_GET['interface']);

    if ($device <= 0 || $interface < 0) {
      $this->err->err_from_string("Invalid device/interface pair specified");
      return "Error";
    }

    $query = "SELECT a.name, b.ip_address, b.label
    FROM interfaces a, devices b
    WHERE b.id = a.device_id
    AND b.id = $device
    AND a.interface = $interface";

    $res = $this->db->get_row($query);



    $parser = new Parser($this->tpl_dir);
    $parser->assign_by_ref("row", $res);
    $index_content .= $parser->fetch("mini_vne.tpl");
  }

  /**
   * Outputs an XML document containing a list of all SHM-enabled SNMP Interfaces
   *
   * @param pointer $index_content
   */
  function get_vne_builders() {
    $payload = array(
      'host_filter' => array(),
      'traffic_filter' => array()
    );

    $query = "SELECT substring(a.description from 0 for 15) AS \"description\", a.interface, b.label, b.id AS \"device_id\"
    FROM interfaces a, devices b
    WHERE b.id = a.device_id
    AND a.enable_shm = true
    AND a.enable_logging = true
    ORDER BY b.label ASC, a.interface ASC";

    $res = $this->db->select($query);

    if (DB_NO_RESULT == $res || DB_QUERY_ERROR == $res) {
      $res = array();
    }
    
    $payload['interfaces'] = $res;
    
    $dom = new DOMDocument();
    $dom->load("/var/www/registry/filters/host.xml");
    $xpath = new DOMXpath($dom);
    $filters = $xpath->query("//filter");
    
    foreach ($filters as $element) {
      array_push($payload['host_filter'], $element->getAttribute('name'));
    }

    $dom = new DOMDocument();
    $dom->load("/var/www/registry/filters/traffic.xml");
    $xpath = new DOMXpath($dom);
    $filters = $xpath->query("//filter");
    
    foreach ($filters as $element) {
      array_push($payload['traffic_filter'], $element->getAttribute('name'));
    }

    require_class('mod_settings', 'localnets_manager');
    $lm = new LocalNets_Manager();
    $payload['localnets'] = $lm->get_all_ids(); 

    print json_encode($payload, true);
  }

  /**
   * Smarty modifier to resolve port numbers into protocol names
   *
   * @param pointer $index_content
   */
  function _smarty_resolve_port($port) {
    $port = intval($port);
    if ($port <= 0) { return "Unknown ($port)"; }

    $query = "SELECT name FROM protocols WHERE port = " . $this->db->escape($port);

    $res = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return "Unknown ($port)";
    }
    return $res['name'] . " ($port)";
  }
  
  /**
   * Renders the topology mapper panel on a blank window
   *
   * @param pointer $index_content
   */
  function topology(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch("topology_ui.tpl");
  }
  
  /**
   * Returns a JSON payload for creating the network topology map inside the topology panel
   *
   * @param pointer $index_content
   */
  function ajax_load_json_topology(&$index_content) {
    $cmd = "sudo python /apache/cli/mapper.py";
    $out = `$cmd`;
    $index_content .= $out;
    return;
  }
  
  function ajax_update_node_trackers(&$index_content) {
    $ip = $this->db->escape($_REQUEST['node']);
    $query = "SELECT 'proto' AS type, srv_id AS id, ip, status, latency, protocol, port FROM servers
    WHERE ip = $ip
    ORDER BY id DESC";
    
    $res = $this->db->select($query);
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $data = array();
    } else {
      $data = $res;
    }
    
    $query = "select 'SMB' AS type, 'smb_'||srv_id AS id, share AS name, srv_id, ip, servername, status,
    threshold, ((a.blocksize * a.total::int8) - (a.blocksize * a.available::int8))::int8 AS used,
    (a.blocksize * a.total::int8)::int8 AS total, timestamp, message
    FROM smb_servers a
    WHERE ip = $ip
    UNION
    select 'DF' AS type, 'df_'||srv_id AS id, partition AS name, srv_id, ip, servername, status,
    threshold, ((1024 * total::int8) - (1024 * available::int8))::int8 AS used, (1024 * total::int8)::int8 AS total,
    timestamp, message
    FROM df_servers b
    WHERE ip = $ip
    ORDER BY status DESC, srv_id ASC";
    
    $res = $this->db->select($query);
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      $disks = array();
    } else {
      $disks = $res;
    }
    
    $data = array_merge_recursive($data, $disks);
    
    echo json_encode($data);
  }
  
  function ajax_save_node_location(&$index_content) {
    $ip = $this->db->escape($_REQUEST['ip']);
    $x = $this->db->escape($_REQUEST['x']);
    $y = $this->db->escape($_REQUEST['y']);
    
    $query = "DELETE FROM topology_nodes WHERE node_ip = $ip";
    $this->db->delete($query);
    
    $query = "INSERT INTO topology_nodes(node_ip, node_x, node_y)
    VALUES($ip, $x, $y)";
    
    $res = $this->db->insert($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      echo "FAIL - $query";
    } else {
      echo "OK";
    }
  }
  
  /**
   * Saves a connection between 2 nodes
   *
   * @param pointer $index_content
   */
  function ajax_save_node_connection(&$index_content) {
    $parent = $this->db->escape($_REQUEST['parent']);
    $child = $this->db->escape($_REQUEST['child']);
    
    $query = "DELETE FROM topology_connections 
    WHERE (
      (parent_ip = $parent AND child_ip = $child)
      OR
      (child_ip = $parent AND parent_ip = $child)
    )";
    
    $this->db->delete($query);
    
    $query = "INSERT INTO topology_connections(parent_ip, child_ip)
    VALUES($parent, $child)";
    
    $res = $this->db->insert($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      echo 'FAIL';
    } else {
      echo 'OK';
    }
  }

}

?>
