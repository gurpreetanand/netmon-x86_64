<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca> and Jae Muzzin <jae@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */


// mod_settings.class.php

/**
  * Help Dialog MCO
  *
  * This module is used to retrieve help sections
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class mod_help extends MadnetModule {


	function get_help(&$index_content) {

		if (!file_exists($this->tpl_dir . "help_sections/" . $_GET['section'] . ".htm")) {
			$this->err->err_from_string("The help section " . mktitle($_GET['section']) . " is not available.");
			return "Error";
		}

		$parser = new Parser($this->tpl_dir);
		$parser->assign('netmonversion', VERSION);
		
		if (strcmp("intro", $_GET['section']) == 0) {
			$query = "SELECT devices FROM netmon";
			$res = $this->db->get_row($query);
			
			if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
				$devs = 0;
			} else {
				$devs = $res['devices'];
			}
			$parser->assign("devices", $devs);
		}

		$index_content .= $parser->fetch("help_sections/" . $_GET['section'] . ".htm");
		return "Help";
	}

	function get_help_toc(&$index_content){
		$parser = new Parser($this->tpl_dir);

		//parse xml
		require_once "XML/Unserializer.php";
		$res = `python /apache/cli/dbtoxml.py`;
		$this->xml = & new XML_Unserializer(array('complexType'=>'array','parseAttributes'=>TRUE,'attributesArray'=>FALSE,'contentName'=>'panels'));
		$this->xml->unserialize($res, FALSE);
		$struct=$this->xml->getUnserializedData();
		
		if (PEAR::isError($struct)) {
			$this->err->err_from_string("Error unserializing XML Data: " . $struct->getMessage());
			$parser->assign('tables', array());
		} else {
			for ($i=0;$i<count($struct['component']);$i++){
				for($j=0;$j<count($struct['component'][$i]['table']);$j++){
					if($struct['component'][$i]['table'][$j]['name']!=""){
						$tab[]=$struct['component'][$i]['table'][$j]['name'];
					}
				}
			}
			natsort($tab);
			$parser->assign_by_ref("tables", $tab);
		}
		$parser->assign('netmonversion', VERSION);
		$index_content .= $parser->fetch("help_sections/toc.tpl");
	}

	function get_table(&$index_content) {
		$table_name=$_GET['table_name'];
		$parser = new Parser($this->tpl_dir);

		//parse xml
		require_once "XML/Unserializer.php";
		$res = `/apache/cli/dbtoxml.py -c`;
		$this->xml = & new XML_Unserializer(array('complexType'=>'array','parseAttributes'=>TRUE,'attributesArray'=>FALSE,'contentName'=>'panels'));
		$this->xml->unserialize($res, FALSE);
		$struct=$this->xml->getUnserializedData();
		
		//find table
		for ($i=0;$i<count($struct['component']);$i++){
			for($j=0;$j<count($struct['component'][$i]['table']);$j++){
				if($struct['component'][$i]['table'][$j]['name']==$table_name){
					$table_struct = $struct['component'][$i]['table'][$j];
					break 2;
				}
			}
		}

		$parser->assign("table_name", $table_struct['name']);
		$parser->assign("table_desc", $table_struct['desc']);
		$parser->assign_by_ref("cols", $table_struct['column']);
	
		$index_content .= $parser->fetch("help_sections/table.tpl");
		return "Help";
	}

	function get_database_reference(&$index_content) {
		$parser = new Parser($this->tpl_dir);

		//parse xml
		require_once "XML/Unserializer.php";
		$res = `/apache/cli/dbtoxml.py -c`;
		$this->xml = & new XML_Unserializer(array('complexType'=>'array','parseAttributes'=>TRUE,'attributesArray'=>FALSE,'contentName'=>'panels'));
		$this->xml->unserialize($res, FALSE);
		$struct=$this->xml->getUnserializedData();
		
		for ($i=0;$i<count($struct['component']);$i++){
			for($j=0;$j<count($struct['component'][$i]['table']);$j++){
				if($struct['component'][$i]['table'][$j]['name']!=""){
					$tab[]=$struct['component'][$i]['table'][$j];
				}
			}
		}
		foreach ($tab as $key => $row) {
    			$name[$key]  = $row['name'];
		}
		array_multisort($name, $tab);
		$parser->assign_by_ref("tables", $tab);
		
		$index_content .= $parser->fetch("help_sections/db_reference.tpl");
		return "Help";
	}


	function submit_bug(&$index_content) {
		$query = "SELECT * FROM netmon";
		$res = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$this->err->handle_fatal("Unable to retrieve registration information. The database server may be down or your registration key is invalid.");
			return "Error";
		}


		$parser = new Parser($this->tpl_dir);
		$parser->assign_by_ref("ds", $res);
		$index_content .= $parser->fetch("bug_report.htm");
		return "Submit Bug";
	}

	function request_support(&$index_content) {
		$query = "SELECT * FROM netmon";
		$res = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
			$this->err->handle_fatal("Unable to retrieve registration information. The database server may be down or your registration key is invalid");
			return "Error";
		}

		$parser = new Parser($this->tpl_dir);
		$parser->assign_by_ref("ds", $res);
		$index_content .= $parser->fetch("support.htm");
		return "Request Product Support";
	}

	function browse_proto_dic(&$index_content) {

		$parser = new Parser($this->tpl_dir);

		$index_content .= $parser->fetch("proto_dic.htm");
		return "Browse Protocol Dictionary";
	}

	function get_nrk(&$index_content) {

		$parser = new Parser($this->tpl_dir);

		$index_content .= $parser->fetch("nrk.htm");
		return "Browse Netmon Resource Kit";
	}

	function get_nrk_item(&$index_content) {

		if (!file_exists($this->tpl_dir . "nrk/" . $_GET['doc'] . "")) {
			$this->err->err_from_string("Could not locate this NRK document.");
			return "Error";
		}

		$parser = new Parser($this->tpl_dir);

		$index_content .= $parser->fetch("nrk/" . $_GET['doc'] . "");
		return "NRK";
	}


	function get_protocol_doc(&$index_content) {
		$transport_layer = $_GET['transport_layer'];
		$port_number     = intval($_GET['port_number']);


		if ((strlen($transport_layer) == 0) || ($port_number <= 0)) {
			$this->err->err_from_string("Invalid protocol request");
			return "Error";
		}

		$filename = $port_number . "_" .  strtolower($transport_layer). ".htm";
		if (!file_exists($this->tpl_dir . "protocols/" . $filename)) {
			//$this->err->err_from_string("The specified port is not in the protocol dictionary. <a target=\"_blank\" href=\"http://www.google.com/search?hl=en&q=$transport_layer+$port_number\">Google it</a>.");
			$filename = "unknown.tpl";
			//return "Error";
		}


		$parser = new Parser($this->tpl_dir);
		$index_content .= $parser->fetch("protocols/" . $filename);
		return "Protocol Definition";
	}

	function get_rss(&$index_content) {
		require_once(INCLUDE_PATH . "XML/RSS.php");

		$feeds = array("http://www.netmon.ca/news/rss.xml",
					   "http://www.microsoft.com/technet/security/bulletin/secrss.aspx",
					   "http://www.securityfocus.com/rss/vulnerabilities.xml",
					   "http://feeds.feedburner.com/NetworkBuzz?format=xml",
					   "http://www.debian.org/security/dsa-long"
					   );

		$rss = array();

		foreach($feeds as $current) {
			$tmp = array();
			$parser = new XML_RSS($current);
			$parser->parse();

			$tmp['info'] = $parser->getChannelInfo();
			$tmp['items'] = $parser->getItems();
			array_push($rss, $tmp);
		}


		$tpl = new Parser($this->tpl_dir);
		$tpl->assign_by_ref("rss", $rss);
		$index_content .= $tpl->fetch("get_rss.tpl");
	}

	/**
	 * Generates a snapshot of the system and send it to the Netmon support team
	 *
	 * @param pointer $index_content
	 */
	function send_snapshot(&$index_content) {
		$arr = array();
		$return = "";
		$cmd = "sudo python " . ROOT . "/cli/snapshot.py 1> /dev/null";
		exec($cmd, $arr, $return);

		$index_content .= message_bar("A snapshot of this system has been sent to the Netmon Technical Support department.");
		$index_content .= join("<br />", $arr);
		$this->debugger->add_hit("Command", NULL, NULL, $cmd);
	}
	


}


?>
