<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Netmon Evasion Primer</title>
<link href="/assets/lib_css.css" rel="stylesheet" type="text/css">
<link href="/assets/lib_css_printable.css" rel="stylesheet" type="text/css" media="print">
</head>
<body class="iframe">
<div class="panel white">
<h1>Netmon Evasion Primer</h1>
<p>When network users know that their activities are being monitored, or even if they suspect it, you can be sure that a few of them are going to take countermeasures. People have all sorts of reasons for attempting to mask their network activty: some are relatively benign, and others considerably less so. Bob in Accounting may not want you to know that he's chatting with his brother overseas on his lunch break, while Shane in Engineering may have set up a large-scale P2P hub, distributing the latest DVD movie releases to hundreds of people - courtesy of your Internet pipe.</p>
<p>Depending on your network usage policy (you do have one, don't you?) many of these activities may be of concern to you as a network administrator. This whitepaper focuses on the most commonly used techniques to get around a network sniffer, and how each approach works from a high level. If you know what to look for, most of these tactics can be spotted relatively easily, and appropriate countermeasures can be taken. </p>
<h2>Tactic #1 : The Physical Bypass</h2>
<p>The simplest way to escape Netmon's sniffer altogether is to physically relocate the network connection. Many companies use separate switches or hubs for LANs, DMZs and external networks. If your network is configured in this fashion, then bypassing Netmon becomes  simply a matter of moving a single network cable upward in the hierarchy, and into a physical port which is not being monitored. In fact, many DSL modems ship with two or more built-in ports, providing a perfect opportunity for an extra client or two to sit at the very edge of the network. </p>
<p>This tactic can be dangerous, because it often involves placing an ordinary workstation onto a less protected DMZ or outside network. Many workstations are not sufficiently secured for this level of exposure, and this practice could easily provide a point of entry for external attacks.</p>
<p>While this approach is indeed the simplest, it also has a significant drawback:  it requires physical access to the server room. If your company restricts access to these areas (and it ought to) then this type of threat can be virtually eliminated. In addition, there needs to be a higher-level device available with an open port. </p>
<h3>Identification</h3>
<ul>
  <li>Ensure all of the cables which run to upper-level switches, hubs, routers and firewalls belong to legitimate hosts. </li>
  <li>A physical bypass can become evident for what isn't there: namely, a complete lack of traffic information for a particular host. If you've got 27 regular staff in Department A, and you're only seeing traffic data for 26 of them, you're going to want to find out why.</li>
  <li>Compare SNMP traffic data to Netmon's live traffic reports. Ensure there's a relatively close match between the traffic levels reported by your key routing devices, and Netmon's live traffic data. If your router is reporting 1.25 Mbps of outbound traffic over the SNMP protocol, and the network sniffer shows your most active host communicating at 10 kbps, you need to determine where all that extra traffic is coming from. </li>
  </ul>
<h3>Countermeasure(s)</h3>
<ul>
  <li>Perform a periodic physical audit of your routers, firewalls and switches. Ensure that new cabling can be traced back to legitimate network requirements.</li>
  </ul>
<h2>Tactic #2 : The Proxy Server</h2>
<p>A proxy server specializes in processing network requests on your behalf. It acts as a go-between, relaying messages between the client and a remote server.</p>
<p>A proxy provides a nifty privacy advantage to its user, in that it can mask the true origin of a request to the remote server, and also obscure the destination of a request to the local network.</p>
<p> <span class="inlineHeading">Example:</span> Mark uses an HTTP proxy service. When he makes a request to http://www.espn.com, the request is actually routed to his proxy server instead of espn.com. The proxy server then makes its own request to http://www.espn.com, receives the web page, and forwards it to Mark. As far as the website owner is concerned, his visitor came from the proxy server. As far as Mark's network administrator is concerned, he received a web page from somewhere else. </p>
<p>There are dozens or hundreds of proxy services on the Internet, often marketed as 'Web Anonymizers'. Some are free, offering only basic HTTP proxy services, while others charge a fee and expand availability to a wider range of TCP ports.</p>
<p>Of course, the proxy server itself is in the priveleged position of knowing <em>all</em> of the particulars of every network transaction, which ought to make consumers of such services a little wary - especially the 'free' ones.</p>
<p>With high-speed Internet  now commonplace in the home, it's now trivial for a  power user to set up their own proxy service right from their home computer, using widely available software, eliminating the need to use a third-party proxy service altogether.</p>
<h3>Identification</h3>
<ul>
  <li>In many cases, the usage of proxy servers can be identified easily. If all of Mark's port 80 (HTTP) requests are going to somehost.somedomain.com, you can be pretty certain a proxy service is being used. Be suspicious of disproportionately large amounts of unexplained traffic to one particular host.</li>
  <li>Home broadband modems often identify themselves via DNS, and Netmon can resolve their names. These names tend to be easy to tell apart from normal Internet destinations, as they often contain the name of the ISP, along with elaborate numbering/lettering combinations - needed by the ISP in order to manage many thousands of nodes.</li>
  </ul>
<h2>Tactic #3 : Network Tunneling &amp; Port Spoofing </h2>
<p>Tunneling generally boils down to the technique of layering a higher level protocol within another high layer protocol. To the casual observer, the traffic appears to be of one type (FTP, for example) while in fact it carries the encapsulated traffic of an entirely different protocol. Often the encapsulated traffic is encrypted, further obscuring the true nature of the payload.</p>
<p>Tunneling can be difficult to spot if it is done by a skilled individual who exercises care and caution. Nevertheless, it can still nevertheless be identified with sharp observation, especially over time.</p>
<p>Port spoofing is the practice of sending a non-standard type of traffic over a standard TCP/IP port. For example, it is relatively easy for a competent network professional to send HTTP traffic (which usually goes over TCP port 80) over TCP port 21 instead. (TCP port 21 is usually used for an FTP data stream.) This technique may often be used in conjunction with a proxy server, in order to create the illusion of a legitimate data stream of one type or another. </p>
<h3>Identification</h3>
<ul>
  <li>Tunneled traffic creates a higher network overhead than normal network conversations, as it has to bear the added weight of both high-level protocols. Thus, it is more difficult to keep these transactions at a low profile. The key to identification lies in repetition: recurring traffic to any one host should always warrant a second look.</li>
  <li>It is possible to use a packet-by-packet sniffer such as Ethereal or tcpdump to analyze the actual contents of packets going across the wire. Unfortunately, if the traffic is encrypted, packet inspection will not provide much information. </li>
  <li>Port spoofing can often be identified with a basic understanding of the various protocols used. For example, FTP traffic tends to be sustained over a period of time, while HTTP requests tend to occur in short bursts, so I'd be suspicious of a large number of intermittent FTP connections. Port spoofing is often done over common service ports, because the very idea is to blend in with surrounding traffic. </li>
  </ul>
<h3>Countermeasure(s)</h3>
<ul>
  <li>Using a Layer 4 switch can prevent tunneling and port spoofing. A Layer 4 switch can identify legitimate traffic, and enforce policy-based rules in much the same way a firewall protects Layer 3. To learn more about layer 4 switches, talk to your Netmon Channel Partner.</li>
  </ul>
<h2>Tactic #4 : Remote Desktop Applications </h2>
<p>There are a variety of products available which provide the ability to operate a remote host via remote control, such as Microsoft Remote Desktop, PCAnywhere, GoToMyPC.com, and VNC. By using this type of software, it is possible to engage in network activity from a remote machine, so that all suspicious activity appears to originate from the remote host. Often, this remote host is entirely out of reach of the monitoring system.</p>
<h2>Identification</h2>
<ul>
  <li>Remote desktop applications can be spotted relatively easily, as most of them communicate over well known TCP ports. Some of them also generate a great deal of network traffic. VNC-based systems are</li>
</ul>
<h2>Tactic # 5 : The Layered Approach</h2>
<p>A skilled and determined individual may choose to employ some or all of these tactics together, further clouding the situation. It can be extremely difficult to track the activities of a patient professional. If you suspect that there's more going on than meets the eye, talk to your Netmon vendor about a more in-depth security audit.</p>
<h2>Other Approaches</h2>
<p>Other approaches are less common, and often have limited applications:</p>
<p><span class="inlineHeading">Google Cache</span> Clever use of Google's caching feature can permit access to web content which violates network usage policies. Be observant of excessive use of Google resources.</p>
<h2>Conclusion</h2>
<p>When monitoring measures are in place, you can virtually count on efforts to manipulate these systems. As with many of these kinds of tasks, the best defense is often a good offense: regular, proactive monitoring. Developing a close familiarity with the type and nature of the traffic going across your network is a critical element to identifying many of the measures described in this article. When in doubt, consult with your Netmon partner.</p>
</div>
</body>
</html>
