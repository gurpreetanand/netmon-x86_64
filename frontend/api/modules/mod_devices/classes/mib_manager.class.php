<?php

/**
 * MIB Manager class
 *
 * @author Xavier Spriet <xavier@netmon.ca>
 * @copyright Netmon Inc.
 * @version $Id$
 */
class MIB_Manager  {

  /**
   * Error Manager Singleton Object
   *
   * @var Error_Manager
   */
  var $err;

  /**
   * Registry singleton Object
   *
   * @var Registry
   */
  var $registry;

  /**
   * Upload Manager Singleton Object
   *
   * @var Upload_Manager
   */
  var $upload_manager;

  /**
   * Debugger Singleton Object
   *
   * @var Debugger
   */
  var $debugger;

  /**
   * DB Manager Singleton Object
   *
   * @var DB_Manager
   */
  var $db;

  /**
   * Will be set to TRUE if browse_mib determines we can write vals to the device.
   *
   * @var boolean
   */
  var $can_write;
  
  /**
   * Determine whether or not SNMP-write has been tested or not
   * 
   * @var boolean
   */
   var $snmpwrite_tested;

  /**
   * MIB Manager Constructor
   *
   * @return MIB_Manager
   */
  function MIB_Manager() {
    /**
     * This block instanciates our singleton objects
     */

    $this->registry = Registry::get_registry();

    $this->err = $this->registry->get_singleton("core", "error_manager");

    $this->debugger = $this->registry->get_singleton("core", "debugger");

    $this->upload_manager = $this->registry->get_singleton("core", "upload_manager");

    $this->db = $this->registry->get_singleton("core", "db_manager");
    
    $this->memcache = $this->registry->get_singleton("core", "memcached_manager");

    /**
     * Paths libsmi uses for mib parsing
     */
    $this->mibs_path = "/usr/local/share/mibs/";
    $this->site_path = $this->mibs_path . "site/";
    
    $this->snmpwrite_tested = $this->can_write = FALSE;
  }

  /**
   * Returns a list of MIB files located in /usr/local/share/mibs/site
   * sorted by mtime, descending
   *
   * @return array
   */
  function get_all_MIBs() {

    $mibs = array();

    # Add all the MIB files to our array
    if ($dir = opendir($this->site_path)) {
      while (($file = readdir($dir)) !== false) {
        if (($file != ".") && ($file != "..")) {
          array_push($mibs, $file);
        }
      }
    }

    closedir($dir);

    usort($mibs, array(&$this, "_cmp_file_stats"));

    # Returns a list of MIB files located in /usr/local/mib/site sorted by date in descending order
    return $mibs;
  }
  
  /**
     * Runs a CLI command, and attempts to use memcache if USE_SNMP_CACHE is TRUE
     * @param string $cmd CLI Command to run
     * @return mixed
     */
    function cli($cmd) {
        if (USE_SNMP_CACHE == TRUE) {
            if (($output = $this->memcache->get($cmd)) === FALSE) {
        $this->memcache->acquire_lock($cmd);
                $output = trim(`$cmd`);
                try {
                    $this->memcache->set($cmd, $output, 45);
                } catch (Exception $e) {
                    $this->err->err_from_string("Unable to cache data. Performance is likely to degrade very quickly.");
                }
                $this->memcache->release_lock($cmd);
            }
            return $output;
        } else {
            return trim(`$cmd`);
        }
    }

  /**
   * Translate a numeric OID into its textual representation
   *
   * @param string $oid
   * @return string
   */
  function translate_oid($oid) {
    
    $oid = eregi_replace('\[(.+)\]', ".\\1", $oid);
    $oid = escapeshellarg(trim($oid));

    # Use smiquery from libsmi instead of snmptranslate (MUCH faster)
    #$cmd = "SMIPATH=\"/usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs\" smiquery node $oid";
    $cmd = "sudo nice -n2  snmptranslate -w25 -PduRc -Td -O q -m ALL -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs $oid 2> /dev/null";
    $this->debugger->add_hit("translate_oid Command", NULL, NULL, $cmd);
    return $this->cli($cmd);
  }

  /**
   * Returns the numeric OID value associated with the OID passed in as argument
   *
   * @param string $oid
   * @return string
   */
  function translate_oid_numeric($oid) {
    $oid = eregi_replace('\[([0-9]+)\]', ".\\1", $oid);
    $oid = eregi_replace('\[(.+)\]', ".\\1", $oid);

    
    $parts = array();
    /*
    if (eregi(".*\"(.*)\".*", $oid, $parts)) {
      $seq = array();
      for ($i = 0; $i < strlen($parts[1]); $i++) {
        # Do not try to get the ASCII value of the index if it is an integer.
        if (strcmp(strval($parts[1][$i], intval($parts[1][$i]))) == 0) {
          array_push($seq, $parts[1][$i]);
        } else {
          array_push($seq, ord($parts[1][$i]));
        }
      }
      $oid = eregi_replace("(.*)\"(.*)\"(.*)", "\\1" . implode(".", $seq) . "\\3", $oid);
    }
    */
    
    $oid = escapeshellarg(trim($oid));

    $cmd = "sudo nice -n2  snmptranslate -Ir -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -PduRc -O qn $oid 2> /dev/null";
    $this->debugger->add_hit("Command", NULL, NULL, $cmd);
    return $this->cli($cmd);
    
    /**
     * snmptranslate tends to fail while attempting to translate
     * longer OIDs with spaces, brackets, etc... so we try to do it ourself...
     */
    /*
    if (strcmp("", $val) == 0) {
      
    }
    */
  }

  /**
   * Converts a numeric OID into a string
   *
   * @param string $oid
   * @return string
   */
  function translate_oid_name($oid) {
    $oid = escapeshellarg(trim($oid));

    $cmd = "sudo nice -n2  snmptranslate -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -PduRc -O qs $oid 2> /dev/null";
    $this->debugger->add_hit("Command", NULL, NULL, $cmd);
    return trim(`$cmd`);
  }

  /**
   * Uses the snmpget CLI utility to retrieve an SNMP value from a device
   *
   * @param string $ip
   * @param string $community
   * @param string $oid
   * @return mixed
   */
  function snmp_get($ip, $community, $oid) {
    $ip = escapeshellarg($ip);
    $community = escapeshellarg($community);
    $oid = escapeshellarg(trim($oid));

    $versions = array('1', '2c');

    foreach($versions as $version) {
        # New: Do not make snmpget keep the description of all loaded OID in RAM and make it ignore OID conflicts
        #$cmd = "sudo nice -n2 /usr/bin/snmpget -v$version -c $community -PeuR -Ir -Ov -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs $ip $oid 2> /dev/null";
        $cmd = "sudo /usr/local/bin/snmpget -v$version -c$community -Ir -OvU $ip $oid 2> /dev/null";
        $this->debugger->add_hit("Command", NULL, NULL, $cmd);
        $regex = "(.*)Wrong Type(.*):(.*)";
        $ret = eregi_replace($regex, "\\1\\3", $this->cli($cmd));
        
        # If snmpproxyd is running, we should never get this.
        if ($version == '2c') {
          if ((strcmp("No Such Object available on this agent at this OID", $ret) == 0) ||
            (strcmp("No Such Instance currently exists at this OID", $ret) == 0)) {
              $ret = NULL;
            }
        }
        
        $data = explode(" ", $ret);


        if (strcmp("", $ret) <> 0) {
          break;
        } else {
          $this->debugger->add_hit("This device does not support snmpget using v$version");
        }
    }

    # All attempts were unsuccessful.
    if (strcmp("", $ret) == 0) {
      #$this->err->err_from_string("No response from $ip");
      return "N/A";
    }
    unset($data[0]);
    $this->debugger->add_hit($oid, NULL, NULL, vdump($data));
    return trim(implode(" ", $data));
  }

  /**
   * Emulates the behaviour of PHP's snmp_walk() call but using NET-SNMP instead of UCD-SNMP
   *
   * @param string $ip
   * @param string $community
   */
  function snmp_walk($ip, $community, $domain) {
    
    
    /*
    # oid: .1.3.6.1.2.1.1.1.0
    $oid = "1.3.6.1.2.1.1.4.0";
    $contact_val = $this->snmp_get($ip, $community, $oid);
    if (FALSE == $this->snmpwrite_tested) {
      if (('N/A' == $contact_val) || ('Object Identifier' == $contact_val)) {
        $this->can_write = FALSE;
        $this->debugger->add_hit("SNMP Write Disabled (OID returned N/A)");
      } else {
        if (@snmpset($ip, $community, $oid, "s", "Netmon_Test") == TRUE) {
          snmpset($ip, $community, $oid, "s", $contact_val);
          $this->can_write = TRUE;
          $this->debugger->add_hit("SNMP Write Enabled");
        } else {
          $this->can_write = FALSE;
          $this->debugger->add_hit("SNMP Write Disabled (snmpset failed)");
        }
      }
      $this->snmpwrite_tested = TRUE;
    }
    */
    
    $ip = escapeshellarg($ip);
    $community = escapeshellarg($community);
    
    
    #$binaries = array('/usr/bin/snmpwalk -m ALL -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -PeuRc');
    $binaries = array('/usr/bin/snmpwalk', '/usr/bin/snmpwalk -m ALL -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -PeuRc');
    $versions = array('1', '2c', '3');


    foreach($versions as $version) {
      foreach($binaries as $bin) {
        # Same changes as for snmpget (don't maintain OID descriptions in RAM and ignore conflicts)
        $cmd = "sudo nice -n2  $bin -Ir -v$version -c $community -Ci -OU -Cc $ip $domain 2> /dev/null";
        $this->debugger->add_hit("Command", NULL, NULL, $cmd);
        $data = explode("\n", $this->cli($cmd));

        if (sizeof($data) > 1) {
          # Break out of 2 nested foreach levels when we get data.
          break 2;
        } else {
          $this->debugger->add_hit("This device does not support $bin using v$version");
        }
      }
    }

    # All attempts were unsuccessful.
    if (sizeof($data) < 2) {
      $this->err->err_from_string("No response from $ip");
      return array();
    }


    $result = array();
    unset($data[sizeof($data)-1]);

    foreach($data as $row) {
      $tmp = explode("=", $row);
      if (sizeof($tmp) == 1) {
        continue;
      }
      $mib = $tmp[0];

      $result[$mib] = trim($tmp[1]);
      unset($tmp);
      unset($mib);
    }
    unset($data);
    #$this->debugger->add_hit("SNMPWalk Data", NULL, NULL, vdump($result));
    return $result;
  }


  /**
   * Associates a MIB to a device.
   *
   * @param string $mib
   * @param int $device_id
   */
  function browse_mib($device_id, $domain) {


    require_class("mod_devices", "device_manager");
    $dm = new device_manager();

    $dm->pop($device_id);

    #$vals = @snmprealwalk($dm->get("ip_address"), $dm->get("snmp_community"), NULL, 80000, 2);
    $vals = $this->snmp_walk($dm->get("ip_address"), $dm->get("snmp_community"), $domain);

    if (!is_array($vals)) {
      $this->err->err_from_string("Netmon was unable to retrieve the SNMP MIB tree for this device.");
      return FALSE;
    }
    #var_dump($vals);

    $result = array();
    # Ex.   'SNMPv2-SMI::mib-2.47.1.2.1.1.4.1' => 'STRING: "public"'
    # Ex.   'SNMPv2-MIB::sysServices.0' => 'INTEGER: 72'
    foreach($vals as $key => $val) {
      # Retrieve data-type and payload
      $v_parts = explode(":", $val);
      $datatype = $v_parts[0];
      unset($v_parts[0]);
      $payload = implode(":", $v_parts);

      if (eregi(".*Wrong Type.*", $datatype)) {
        $tmp = explode(":", $payload);
        $datatype = $tmp[0];
        $payload = $tmp[1];
        unset($tmp);
      }

      # Retrieve MIB and OID
      $k_parts = explode("::", $key);
      $mib = $k_parts[0];
      $oid = $k_parts[1];
      unset($k_parts);
      /**
       * @todo Find a better way to determine if snmpset is enabled
       */
/*
      if (strcmp($oid, "sysContact.0") == 0) {
        $val = $payload;
        if (@snmpset($dm->get('ip_address'), $dm->get('snmp_community'), $key, "s", "Test") == TRUE) {
          $this->can_write = TRUE;
          snmpset($dm->get('ip_address'), $dm->get('snmp_community'), $key, "string", $val);
        } else {
          $this->can_write = FALSE;
        }
      }
*/

      $this->push_oid($result, $mib, $oid, $payload, $datatype);
      unset($payload);
      unset($datatype);
      unset($oid);
      unset($mib);
    }
    #$this->debugger->add_hit("Walk Structure", NULL, NULL, vdump($result));
    return $result;
  }
  
  function snmp_table($ip, $community, $table, $convert_hex=TRUE) {
        $ip = escapeshellarg($ip);
        $community = escapeshellarg($community);
        $table = escapeshellarg(trim($table));

        $versions = array('2c', '1');

        foreach($versions as $version) {

                if (TRUE == $convert_hex){
                    $opt = '-Oa';
                }
                
                $cmd = "sudo nice -n2 snmptable -M /usr/share/snmp/mibs:/usr/local/share/mibs/site:/usr/local/share/mibs/ietf:/usr/local/share/mibs/iana:/usr/local/share/mibs/irtf:/usr/local/share/mibs/tubs -v$version $ip -c $community $opt -Peu -Cf '|' -Cilb $table 2> /dev/null";
                $this->debugger->add_hit("Command", NULL, NULL, $cmd);
                $ret = explode("\n", $this->cli($cmd));
                if (sizeof($ret) > 1) {
                    break;
                } else {
                    $this->debugger->add_hit("This device does not support snmptable using v$version");
                }
        }

        # All attempts were unsuccessful.
        if (sizeof($ret) < 2) {
            #$this->err->err_from_string("No response from $ip");
            return array();
        }
        
        
        if (strcmp($ret[2], '') == 0) {
            $headers = explode("|", $ret[3]);
            //unset($ret[3]);
        } else {
            $headers = explode("|", $ret[2]);
        }

        array_splice($ret, 0, 3, NULL);

        
        $this->debugger->add_hit("Headers", NULL, NULL, vdump($headers));        
        $out = array();
        foreach($ret as $row) {
            if (strcmp('', trim($row)) == 0) {
                continue;
            } else {
                $this->debugger->add_hit("Row:", NULL, NULL, vdump($row));
                $row = explode("|", $row);
                $newrow = array();
                for ($i = 0; $i < sizeof($headers); $i++) {
                    $newrow[$headers[$i]] = $row[$i];
                }
                $out[] = $newrow;
            }
        }


        $this->debugger->add_hit($table, NULL, NULL, vdump($out));
        return $out;
    }



  /**
   * Recursively add an OID to the MIB collection for SNMP Walks
   *
   * @param array $result
   * @param array $mib
   * @param array $oid
   * @param string $payload
   * @param string $type
   */
  function push_oid(&$result, $mib, $oid, $payload, $type) {
    if (!is_array($result[$mib])) {
      $result[$mib] = array("mib" => $mib);
    }
    array_push($result[$mib], array("oid" => $oid, array("payload" => $payload, "type" => $type)));
  }

  /**
   * Returns the XML document associated with the MIB associated with the specified device
   *
   * @param string $device_ip
   * @return mixed
   * @deprecated MIB2xml has been dropped
   */
  function get_device_MIB_XML($device_ip) {
    # Returns the XML for the MIB browser for the specified device
    $query = "SELECT a.xml FROM mib2xml a, snmpagents b WHERE a.id = b.mib_id AND b.ip = " . $this->db->escape($device_ip);

    $res = $this->db->get_row($query);

    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return FALSE;
    }
    return $res['xml'];
  }

  /**
   * Uploads a MIB file
   *
   * @return bool
   */
  function upload_mib() {
    if (!$file = $this->upload_manager->retrieve_file("mib")) {
      $this->debugger->add_hit("File entry", NULL, NULL, vdump($file));
      $this->err->err_from_string("The MIB file was not uploaded, or its size exceeds the maximum allowed upload size.");
      return FALSE;
    }

    # Then simply move the file...
    if (!move_uploaded_file($file['tmp'], $this->site_path . basename($file['name']))) {
      $this->err->err_from_string("Unable to move uploaded file to " . $this->site_path . basename($file['name']) . ".");
      return FALSE;
    }
    
    $cmd = "extracttraps " . escapeshellarg($this->site_path . basename($file['name']));
    exec($cmd);

    /*
    #require_class("mod_settings", "mod_settings");
    $ms = require_module("mod_settings");
    $ms->_sig_procmond("stop snmpproxyd");
    sleep(1);
    $ms->_sig_procmond("start snmpproxyd");
    
    
    # We must parse any MIB on upload in order to have the OID translation of
    # imported MIBs in mib2xml table as libsmi doesn't parse imports, simply grabs its datatypes.
    $cmd = "mib2xml -m" . escapeshellarg($this->site_path . basename($file['name']));
    $this->debugger->add_hit("mib2xml command:", NULL, NULL, $cmd);
    $out = array();
    $return = FALSE;
    exec($cmd, $out, $return);

    if ($return == 1) { // Error
      $this->err->err_from_string("Unable to parse MIB file\n " . implode("\n ", $out) . ".");
      return FALSE;
    }
    */
    return TRUE;
  }

  /**
   * Returns the content of a MIB file if it exists
   *
   * @param string $file
   * @return mixed
   */
  function get_mib_content($file) {
    if (file_exists($this->site_path . $file)) {
      return file_get_contents($this->site_path . $file);
    } else {
      $this->err->err_from_string("The file $file does not exist. It may have been deleted.");
      return FALSE;
    }
  }

  /**
   * Deletes a MIB file if it exists
   *
   * @param string $file
   * @return mixed
   */
  function delete_mib($file) {
    if (file_exists($this->site_path . $file)) {
      unlink($this->site_path . $file);
    }
    return TRUE;
  }

  /**
   * This internal function is a usort() call handler to sort files by last mtime
   *
   * @param string $file_a
   * @param string $file_b
   * @return int
   * @access private
   */
  function _cmp_file_stats($file_a, $file_b) {
    # Return the newest file
    $a = filemtime($this->site_path . $file_a);
    $b = filemtime($this->site_path . $file_b);

    if ($a == $b) {
      return 0;
    }

    return ($a < $b) ? -1 : 1;
  }

}

?>
