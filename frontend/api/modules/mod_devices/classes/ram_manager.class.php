<?php


class Ram_Manager {
  function get($device) {
    require_class("mod_devices", "mib_manager");
    $this->device = $device;
    
    $this->mib_manager = new MIB_Manager();

    switch ($this->device["profile"]) {
      case "dash_windows_snmp_inf_iis":
      case "dash_windows_snmp_inf_exch07":
      case "dash_windows_snmp_inf_exch":
      case "dash_windows_snmp_inf_sql":
      case "dash_windows_snmp_inf_adv":
      case "dash_windows_snmp_inf_std":
      case "dash_linux_adv":
      case "dash_linux_generic":
        return $this->windows();
        break;
      case "dash_cisco_asa":
      case "dash_cisco_generic":
      case "dash_cisco_firewall":
      case "dash_cisco_router":
        return $this->cisco();
        break;
      case "dash_hp_ap":
      case "dash_hp_switch":
        return $this->hp_switch();
      case "dash_hp_msm":
        return $this->hp_msm();
      default:
        return array();
        break;
    }
  }
  
  function windows() {
    $payload = array(
      "oid" => ".1.3.6.1.4.1.9600.1.1.2.3.0",
      "label" => "Memory Available (MB)"
    );

    # Retrieve device info from the DB
    if (!$this->device) {
      $this->err->err_from_code(400, "Unable to retrieve profile for the specified device.");
      return FALSE;
    }

    $devs = $this->mib_manager->snmp_table($this->device['ip_address'], $this->device['snmp_community'], "HOST-RESOURCES-MIB::hrStorageTable");
    
    $size = 0;
    $used = 0;
    
    foreach($devs as $dev) {
      if (
        (strcmp($dev['Type'], 'HOST-RESOURCES-TYPES::hrStorageRam') == 0) ||
        (strcmp($dev['Type'], 'HOST-RESOURCES-MIB::hrStorageTypes.2') == 0)
      ) {
        $size += $dev['Size'];
        $used += $dev['Used'];
      }
    }
    
    if (($size == 0) || ($used == 0)) {
      $payload["percent"] = 100;
    } else {
      $payload["percent"] = (floatval($used)/floatval($size))*100;
    }

    return $payload;
  }
  
  function cisco() {
    $payload = array();
    $com = $this->device['snmp_community'];
    $devs = $this->mib_manager->snmp_table($this->device['ip_address'], $this->device['snmp_community'], "CISCO-MEMORY-POOL-MIB::ciscoMemoryPoolTable");
    //var_dump($devs);
    
    $used = 0;
    $free = 0;
    
    foreach($devs as $dev) {
      //$size += $dev['ciscoMemoryPoolUsed'] + $dev['ciscoMemoryPoolFree'];
      $used_buf = explode(" ", $dev['Used']);
      $used += $used_buf[0];
      $free_buf = explode(" ", $dev['Free']);
      $free += $free_buf[0];
    }
    
    if (($free == 0) && ($used == 0)) {
      $payload["percent"] = "N/A";
    } else {
      $payload["percent"] = (floatval($used)/(floatval($used) + floatval($free)))*100;
    }
    return $payload;
  }
  
  function hp_switch() {
    $payload = array();

    $devs = $this->mib_manager->snmp_table($this->device['ip_address'], $this->device['snmp_community'], "NETSWITCH-MIB::hpGlobalMemTable");
    //var_dump($devs);
    
    $used = 0;
    $free = 0;
    
    foreach($devs as $dev) {
      //$size += $dev['ciscoMemoryPoolUsed'] + $dev['ciscoMemoryPoolFree'];
      $used_buf = explode(" ", $dev['AllocBytes']);
      $used += $used_buf[0];
      $free_buf = explode(" ", $dev['FreeBytes']);
      $free += $free_buf[0];
    }
    
    if (($free == 0) && ($used == 0)) {
        $payload["percent"] =  "N/A";
    } else {
        $payload["percent"] = (floatval($used)/(floatval($used) + floatval($free)))*100;
    }
    return $payload;
  }
  
  function hp_msm() {
    $payload = array(
      "oid" => "COLUBRIS-USAGE-INFORMATION-MIB::coUsInfoRamFree.0",
      "label" => "Memory Available (MB)"
    );
    $ip = $this->device['ip_address'];
    $com = $this->device['snmp_community'];
    $total = $this->mib_manager->snmp_get($ip, $com, "COLUBRIS-USAGE-INFORMATION-MIB::coUsInfoRamTotal.0");
    $free = $this->mib_manager->snmp_get($ip, $com, "COLUBRIS-USAGE-INFORMATION-MIB::coUsInfoRamFree.0");
    
    if (($free == 0) && ($total == 0)) {
        $payload["percent"] = "N/A";
    } else {
        $payload["percent"] = (1.0 - (floatval($free)/(floatval($total))))*100;
    }
    return $payload;
  }
 
}

?>