<?php


class SNMP_Interface_Manager extends MadnetElement {

  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "interfaces";
  
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "id";
  
  /**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_devices";
  
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  function init() {
    $this->params->add_primitive("description", "string", TRUE, "Name", "Name");
    $this->params->add_primitive("homedisplay", "pg_bool", TRUE, "Homepage Display", "Display this Monitor on the home dashboard");
    $this->params->add_primitive("enable_logging", "pg_bool", TRUE, "Enable SNMP Logging", "Enable SNMP Logging");
    $this->params->add_primitive("enable_shm", "pg_bool", TRUE, "Enable Real-time Probing", "Enable Real-time Probing");
  }

  # Returns a data structure containing all the interfaces linked to a device.
  # The device's IP address must be specified through $_GET['ip']
  function get_all_interfaces() {

    $ip = $this->db->escape($_GET['ip']);

    $query = "SELECT a.interface, a.mac, a.description
    FROM interfaces a, devices b
    WHERE a.device_id = b.id
    AND b.ip_address = $ip
    ORDER BY interface ASC";


    $res = $this->db->select($query);

    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return FALSE;
    }

    return $res;

  }

  function get_device_interfaces($device_id) {
    $id = $this->db->escape($device_id);

                $query = "SELECT * 
                FROM interfaces
                WHERE device_id = $id
                ORDER BY interface ASC";


                $res = $this->db->select($query);

                if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
                        return FALSE;
                }

                return $res;

  }


  function pop($ip, $interface) {
    $ip = $this->db->escape($ip);
    $interface = $this->db->escape(intval($interface));

    $query = "select a.*, b.ip_address, b.label AS \"device_name\", b.snmp_community, b.interval,
    b.enable_snmp, b.enable_netflow, b.enable_sflow
    FROM interfaces a, devices b
    WHERE a.device_id = b.id
    AND b.ip_address = $ip
    AND a.interface = $interface";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->params->setval($key, $value);
      }
      return TRUE;
    }
  }

}
?>
