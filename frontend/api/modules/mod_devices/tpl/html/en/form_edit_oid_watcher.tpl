
<!-- {$smarty.template} ($Id$) -->

{literal}
<script language="Javascript">
	function checkbox_update(checkbox, fieldname) {
		document.getElementById(fieldname).value = (checkbox.checked == true) ? "true" : "false";
	}
</script>
{/literal}


<div class="panel">
<form action="?module=mod_devices&action=process_update_oid_watcher&id={$owm->get('id')}" method="POST" name="oid_form">
<input type="hidden" name="device_id" value="{$owm->get('device_id')}" />
<input type="hidden" name="oid" value="{$owm->get('oid')}" />
<input type="hidden" name="datatype" value="{$owm->get('datatype')|escape:html}" />
<input type="hidden" name="enable_logging" id="enable_logging" value="{if ($smarty.post.enable_logging == "true") or ($owm->get('enable_logging') == 't')}true{else}false{/if}" />
<input type="hidden" name="homedisplay" id="homedisplay" value="{if ($smarty.post.homedisplay == "true") or ($owm->get('homedisplay') == 't')}true{else}false{/if}" />
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">Edit OID Tracker<br />{$owm->get('oid_name')|default:$owm->get('oid')|escape:html}</th>
		</tr>
		<tr>
			<td>Label</td>
			<td><input type="text" {param name="label" class="input"} value="{$smarty.post.label|default:$owm->get('label')|escape:html}" /></td>
		</tr>
		<tr>
			<td>Sample Every</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$owm->get('interval')|default:3600}" /> seconds</td>
		</tr>
		<tr>
			<td>Enable Logging</td>
			<td><input type="checkbox" onchange="checkbox_update(this, 'enable_logging');" name="chk_logging" value="true" {if ($smarty.post.enable_logging == "true") or ($owm->get('enable_logging') == 't')}checked{/if} /></td>
		</tr>
		<tr>
			<td>Display on Home Dashboard</td>
			<td><input type="checkbox" onchange="checkbox_update(this, 'homedisplay');" name="chk_home" value="true" {if ($smarty.post.homedisplay == "true") or ($owm->get('homedisplay') == 't')}checked{/if} /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Update Tracker"}</td>
		</tr>
	</table>

</form>
</div>
<!-- end of {$smarty.template} -->
