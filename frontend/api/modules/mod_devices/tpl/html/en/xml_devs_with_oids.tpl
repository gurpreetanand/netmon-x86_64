<devices>
{if $devs}
{foreach from=$devs item="device"}
	<device ip="{$device.ip_address}" label="{$device.label}" id="{$device.id}">
	{foreach from=$device.oids item="oid"}
		<oid ip="{$device.ip_address}" label="{$oid.label}" id="{$oid.id}" category="{$oid.cat}" />
	{/foreach}
	</device>
{/foreach}
{/if}
</devices>