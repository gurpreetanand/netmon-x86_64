<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
{/literal}

</script>


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> MAC: <span class="gray2">{$device.mac|default:"Unresolved"}</span>
		
</td>
	    <td rowspan="2"><div align="right"><img src="/assets/devices/cisco.gif" alt="Logo: Cisco" width="82" height="42"></div></td>
	</tr>
	<tr>
		<td>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: {ajax_oid class="gray2" frequency="0" oid="ENTITY-MIB::entPhysicalDescr.1"} 
       </td>
    </tr>
	<!--
	<tr>
      <td colspan="2"> <img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> IOS Version: {ajax_oid class="gray2" frequency="0" oid=".1.3.6.1.4.1.9.9.25.1.1.1.2.5"} &nbsp;&nbsp;&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> Serial #: {ajax_oid class="gray2" frequency="0" oid="ENTITY-MIB::entPhysicalSerialNum.1"} </td>
    </tr>
	-->
	<tr>
      <td colspan="2"><hr /></td>
    </tr>
	<tr>
	<td colspan="2">
	<img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="600000" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}
	
	</td>
	</tr>
</table>


</fieldset>
		{capture assign="dev_id"}{$device.id}{/capture}
		<!--{input type="button" class="button" value="Delete Device" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=delete_snmp_device&id=$dev_id');"}-->



<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td width="75%" valign="top">
	

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>CPU</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td>
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal5sec.1&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal5sec.1&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">CPU Utilization <a href="javascript:addOIDTracker({$device.id}, 'CISCO-PROCESS-MIB::cpmCPUTotal5sec.1', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a><br>
	    <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> (5 sec) </p>
		  </div></td>
	    <td><div align="right">
	        <div align="center" style="margin-top: 5px;">
	          <script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal1min.1&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal1min.1&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script>
	          </div>
	        <p align="center" style="margin-bottom:0px;">CPU Utilization <a href="javascript:addOIDTracker({$device.id}, 'CISCO-PROCESS-MIB::cpmCPUTotal1min.1', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a><br>
	          <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> (1 min) </p>
	        </div></td>
	    <td><div align="right">
            <div align="center" style="margin-top: 5px;">
              <script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal5min.1&refresh=30&url={$smarty.const.TOP_DOMAIN}',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=CISCO-PROCESS-MIB::cpmCPUTotal5min.1&refresh=30&url={$smarty.const.TOP_DOMAIN}'
			 ); //end AC code
			</script>
            </div>
            <p align="center" style="margin-bottom:0px;">CPU Utilization <a href="javascript:addOIDTracker({$device.id}, 'CISCO-PROCESS-MIB::cpmCPUTotal5min.1', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a><br>
              <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> (5 min) </p>
	        </div></td>
	</tr>
</table>


</fieldset>

	</td>
	<td width="25%" valign="top">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>RAM</strong>&nbsp;</legend>

<table border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
    <td valign="middle">
        <div align="center" style="margin-top: 5px;">
              <script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge_inverse?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_cisco_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_inverse?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_cisco_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script>
            </div>
        <p align="center" style="margin-bottom:0px;">RAM Utilization</p>        
		</td>
    </tr>
</table>


</fieldset>

	
	</td>
	</tr>
</table>





<fieldset>
<legend>&nbsp;<img src="/assets/icons/disconnect.png" width="16" height="16" class="icon"> <strong>Firewall Connections &amp; Buffers</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
	    <td><img src="/assets/icons/connect_success.png" width="16" height="16" class="icon"> Current Connections: {ajax_oid class="blue2" frequency="300000" oid="CISCO-FIREWALL-MIB::cfwConnectionStatValue.protoIp.currentInUse" trackable=true} of {ajax_oid class="gray2" frequency="1200000" oid="CISCO-FIREWALL-MIB::cfwConnectionStatValue.protoIp.high" trackable=true} maximum observed since system startup.</td>
  </tr>
	<tr>
		<td><hr /></td>
    </tr>
	<tr>
	<td><img src="/assets/icons/package_go.png" width="16" height="16" class="icon"> 1,550 Byte Blocks: {ajax_oid class="blue2" frequency="300000" oid="CISCO-FIREWALL-MIB::cfwBufferStatValue.1550.free" trackable=true} free of {ajax_oid class="gray2" frequency="0" oid="CISCO-FIREWALL-MIB::cfwBufferStatValue.1550.maximum" trackable=true}total blocks available. </td>
	</tr>
</table>


</fieldset>

</div>


{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}
<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();
    </script>


  <!-- end of {$smarty.template} -->



