
<!-- {$smarty.template} ($Id$) -->

<fieldset>
<legend>&nbsp;<img src="/assets/icons/database_connect.png" width="16" height="16" class="icon"> <strong>Database Server</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>

{foreach from=$data item="val" key="key"}
{counter assign="item_count"}
		<td>{$key}</td>
{if $key}
		<td><span class="blue2">{$val}</span></td>
{/if}
{if $item_count % 2 == 0}</tr><tr>{/if}
{/foreach}
</tr>
</table>
</fieldset>


<!-- end of {$smarty.template} ($Id$) -->