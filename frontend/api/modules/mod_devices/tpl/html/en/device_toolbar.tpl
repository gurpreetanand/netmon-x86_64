
<!-- {$smarty.template} ($Id$) -->

<div class="panel noPrint">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="/assets/buttons/button_resource_centre.gif" width="24" height="24" class="icon" title="Device Home Dashboard" alt="Device Home Dashboard" onClick="location.href='?module=mod_devices&action=dashboard&ip={$smarty.get.ip|default:$smarty.get.id|to_ip}'"> 
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="/assets/buttons/notes.gif" width="24" height="24" class="icon" title="Device Notes" alt="Device Notes" onClick="location.href='?module=mod_devices&action=manage_device_notes&id={$smarty.get.ip|default:$smarty.get.id|to_id}'">
	<img src="/assets/buttons/interface.gif" width="24" height="24" class="icon" title="Network Activity" alt="Network Activity" onClick="location.href='?module=mod_devices&action=dashboard&profile=snmp&ip={$smarty.get.ip|default:$smarty.get.id|to_ip}'">
	<img src="/assets/buttons/log.gif" width="24" height="24" class="icon" title="Events and Logs" alt="Events and Logs" onClick="location.href='?module=mod_syslog&action=view_syslog_data&ip={$smarty.get.ip|default:$smarty.get.id|to_ip}'">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="/assets/buttons/snmp_walk.gif" width="24" height="24" class="icon" title="Browse SNMP MIBs" alt="Browse SNMP MIBs" onClick="showProgressIndicator('progress'); location.href='?module=mod_devices&action=snmp_walk&domain=iso&id={$smarty.get.ip|to_id|default:$smarty.get.id}&class=white'">
	<img src="/assets/buttons/enterprise_mibs.gif" width="24" height="24" class="icon" title="Browse Enterprise MIBs" alt="Browse Enterprise MIBs" onClick="showProgressIndicator('progress'); location.href='?module=mod_devices&action=snmp_walk&domain=enterprises&id={$smarty.get.ip|to_id|default:$smarty.get.id}&class=white'">
	<img src="/assets/buttons/oid_trackers.gif" width="24" height="24" class="icon" title="SNMP Object (OID) Trackers" alt="SNMP Object (OID) Trackers" onClick="location.href='?module=mod_devices&action=manage_oid_watchers&id={$smarty.get.ip|to_id|default:$smarty.get.id}'">
	<img src="/assets/buttons/snmp_traps.gif" width="24" height="24" class="icon" title="SNMP Trap Messages" alt="SNMP Trap Messages" onClick="location.href='?module=mod_devices&action=view_traps&ip={$smarty.get.ip|default:$smarty.get.id|to_ip}'">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('device_toolbar');">
	<img class="progress" id="progress" src="/assets/progress.gif" />
</div>

<!-- end of {$smarty.template} ($Id$) -->
