
<!-- {$smarty.template} ($Id$) -->

<div class="datagrid">
<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th align="left">Database Name</th>
		<th>Cache Hit Ratio</th>
		<th>Active Trans</th>
		<th>Trans/Sec</th>
		<th>Size</th>
	</tr>

{foreach from=$databases item="db"}
	<tr>
		<td>{$db.name}</td>
		<td align="center">{$db.cache_hit}%</td>
		<td align="center">{$db.transactions}</td>
		<td align="center">{$db.tps}</td>
		<td align="center">{$db.size|process_size}</td>
	</tr>
{/foreach}
</table>
</div>


<!-- end of {$smarty.template} ($Id$) -->
