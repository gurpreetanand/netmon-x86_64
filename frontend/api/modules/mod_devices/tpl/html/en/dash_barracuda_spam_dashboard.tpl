<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
{/literal}

</script>


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}



<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>System Name: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"}  &nbsp; Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/barracuda.gif" width="74" height="48" style="margin-right: 5px;"></div></td>
	</tr>
	<tr>
		<td>
		
	      <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="0" oid="DISMAN-EXPRESSION-MIB::sysUpTimeInstance"}&nbsp;
      </td>
    </tr>
	<tr>
	  <td colspan="2"> System Description: {ajax_oid class="unresolved" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}</td>
    </tr>
	<tr>
	  <td colspan="2"><img src="/assets/icons/chart_bar.gif" width="16" height="16" class="icon"> Load Average 1 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.1" trackable=true} 5 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.2"} 15 min: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::laLoad.3" trackable=true}</td>
    </tr>
</table>


</fieldset>


<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> <strong>Mail Queue</strong>&nbsp;</legend>
<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Inbound Mail: {ajax_oid class="blue" frequency="15000" oid="UCD-SNMP-MIB::extResult.1" trackable=true text="messages in the queue."}</p>
<p><strong><img src="/assets/icons/email_go.png" width="16" height="16" class="icon"></strong> Outbound Mail: {ajax_oid class="blue" frequency="15000" oid="UCD-SNMP-MIB::extResult.2" trackable=true text="messages in the queue."}</p>
<p><strong><img src="/assets/icons/email_error.png" width="16" height="16" class="icon"></strong> Bounced Mail: {ajax_oid class="blue" frequency="15000" oid="UCD-SNMP-MIB::extResult.3" trackable=true text="messages in the queue."}</p>
</fieldset>


<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td width="50%" valign="top">
	

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>CPU</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td width="82">
		  <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&oid=UCD-SNMP-MIB::ssCpuIdle.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&mod=inverse',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&oid=UCD-SNMP-MIB::ssCpuIdle.0&refresh=30&url={$smarty.const.TOP_DOMAIN}&mod=inverse'
			 ); //end AC code
			</script></div>
	  <p align="center" style="margin-bottom:0px;">CPU Utilization</p><a href="javascript:addOIDTracker({$device.id}, 'UCD-SNMP-MIB::ssCpuIdle.0', 'INTEGER')"><img src="/assets/icons/tracker_add.gif" width="11" height="18" class="icon" title="Track This Item" alt="Track This Item" border="0"></a>
		  </div></td>
	    <td valign="top"><p>Processes: {ajax_oid class="blue" frequency="30000" oid="HOST-RESOURCES-MIB::hrSystemProcesses.0" trackable=true}</p>
	        <p>Idle: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuIdle.0" trackable=true text="%"}</p>
	        <p>System: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuSystem.0" trackable=true text="%"}</p>
	        <p>User: {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::ssCpuUser.0" trackable=true text="%"}</p>
	      
	      </td>
	</tr>
</table>


</fieldset>

	</td>
	<td width="50%" valign="top">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/cpu.gif" width="16" height="16" class="icon"> <strong>RAM</strong>&nbsp;</legend>

<table border="0" cellpadding="3" cellspacing="0">
  <tr>
    <td width="82" valign="middle"><div align="right">
        <div align="right"><div align="center" style="margin-top: 5px;"><script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','72',
			'height','63',
			'bgcolor','D4D0C8',
			'src','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank',
			'quality','high',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge?device_id={$device.id}&refresh=30&url={$smarty.const.TOP_DOMAIN}&mode=custom&request=%3Fmodule=mod_devices%26action=ajax_proxy%26profile=dash_windows_generic%26proc=ajax_get_ram%26ip={$smarty.get.ip}%26root_tpl=blank'
			 ); //end AC code
			</script></div>
        <p align="center" style="margin-bottom:0px;">RAM</p>
    </div></td>
    <td valign="middle"><p>{ajax_oid class="blue" frequency="15000" oid="UCD-SNMP-MIB::memAvailReal.0" trackable=true} Kbytes available of {ajax_oid class="blue" frequency="30000" oid="UCD-SNMP-MIB::memTotalReal.0" trackable=true text="total Kbytes"}</p></td>
  </tr>
</table>


</fieldset>

	
	</td>
	</tr>
</table>




</div>




{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}

<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();
</script>


<!-- end of {$smarty.template} -->

