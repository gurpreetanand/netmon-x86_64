<!-- {$smarty.template} ($Id$) -->

{"DEPRECATED"|message_bar}

<div class="panel">
	<img src="assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_autodisc');" title="Help" alt="Help" />

</div>
{if $hosts}
<div class="datagrid center">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<th>Host</th>
		<th>Description</th>
		<th>Actions</th>
	</tr>
{foreach from=$hosts item="host"}
	<tr>
		<td>{$host.ip}</td>
		<td>{if $host.sysdescr != ""}{$host.sysdescr}{else}No description available.{/if}</td>
		{capture assign="community"}{$host.community|escape:url}{/capture}
		{capture assign="ip"}{$host.ip}{/capture}
		<td><div align="left">{input type="button" class="button" value="Add" onClick="javascript:parent.pnl_right.showEditor('?module=mod_devices&action=form_create_snmp_device&ip_address=$ip&snmp_community=$community');"}</div></td>
	</tr>
{/foreach}
</table>
</div>
{/if}
<!-- end of {$smarty.template} -->
