<!-- {$smarty.template} ($Id$) -->
<div class="noPrint">
<div class="titlebar">
	MIB Definition - {$smarty.get.mib_file}
</div>


<div class="panel">
	<img src="assets/separator_double.gif" width="10" height="21" hspace="2" class="icon">
	<img src="assets/buttons/button_back.gif" width="24" height="24" class="icon" onClick="document.location.href='?module=mod_devices&action=manage_mibs'" title="Back to MIB File Manager">
	<img src="assets/buttons/button_help.gif" title="View Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('snmp_mibupload');">
	<img src="assets/buttons/button_print.gif" title="Print this page" width="24" height="24" class="icon" onClick="print();">
</div>
</div>

<div class="white">
	<pre>
		{$file_content}
	</pre>
</div>
<!-- end of {$smarty.template} -->
