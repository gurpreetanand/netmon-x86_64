
<!-- {$smarty.template} ($Id$) -->


<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th>&nbsp;</th>
		<th>Virtual MAC <span style="font-weight: normal;"><a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=VRRP-MIB%3A%3AvrrpOperVirtualMacAddr.0.7.100&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></span></th>
		<th>State <span style="font-weight: normal;"><a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=VRRP-MIB%3A%3AvrrpOperState.0.7.100&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></th>
		<th>IP Address <span style="font-weight: normal;"><a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=VRRP-MIB%3A%3AvrrpOperPrimaryIpAddr.0.7.100&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></th>
		<th>Priority <span style="font-weight: normal;"><a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=VRRP-MIB%3A%3AvrrpOperPriority.0.7.100&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></th>
		<th><div align="left">Uptime <span style="font-weight: normal;"><a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=VRRP-MIB%3A%3AvrrpOperVirtualRouterUpTime.0.7.100&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></div></th>
	</tr>
{foreach from=$vrrp_sessions item="vrrp_entry"}
	<tr>
		<td><div align="center"><img src="/assets/icons/interface.gif" width="16" height="16"></div></td>
		<td align="center"><span class="gray2">{$vrrp_entry.VirtualMacAddr}</span></td>
		<td align="center"><span class="blue2">{$vrrp_entry.State}</span></td>
		<td align="center"><span class="gray2">{$vrrp_entry.MasterIpAddr}</span></td>
		<td align="center"><span class="blue2">{$vrrp_entry.Priority}</span></td>
		<td align="left"><img src="/assets/icons/mib_timetick.png" border="0" class="icon"> {$vrrp_entry.VirtualRouterUpTime}</td>
	</tr>
{/foreach}
</table>

<!-- end of {$smarty.template} ($Id$) -->