<!-- {$smarty.template} ($Id$) -->

{literal}
<script language="Javascript">
	function toggleHomeDisplay() {
		if (document.snmp_interface.home_display_check.checked) {
			document.snmp_interface.homedisplay.value = "true";
		} else {
			document.snmp_interface.homedisplay.value = "false";
		}
	}

	function toggleLogging() {
		if (document.snmp_interface.enable_logging_check.checked) {
			document.snmp_interface.enable_logging.value = "true";
		} else {
			{/literal}
			{if (($sim->get('enable_netflow') == 't') or ($sim->get('enable_sflow') == 't'))}
			document.snmp_interface.enable_shm_check.checked = false;
			toggleSHM();
			{/if}
			{literal}
			document.snmp_interface.enable_logging.value = "false";
		}
	}

	function toggleSHM() {

		if (document.snmp_interface.enable_shm_check.checked) {
			document.snmp_interface.enable_logging_check.checked = true;
			toggleLogging();
			document.snmp_interface.enable_shm.value = "true";
		} else {
			document.snmp_interface.enable_shm.value = "false";
		}
	}
</script>
{/literal}


<div class="panel">
<form name="snmp_interface" action="?module=mod_devices&action=update_snmp_interface&id={$sim->get('id')}" method="POST">

{capture assign="homedisplay_checked"}
	{if ($smarty.post.homedisplay == "true")}
		checked
	{elseif (($sim->get('homedisplay') == "t") and (not $smarty.post.homedisplay))}
		checked
	{/if}
{/capture}

{capture assign="logging_checked"}
	{if ($smarty.post.enable_logging == "true")}
		checked
	{elseif (($sim->get('enable_logging') == "t") and (not $smarty.post.enable_snmp))}
		checked
	{/if}
{/capture}

{capture assign="shm_checked"}
	{if ($smarty.post.enable_shm == "true")}
		checked
	{elseif (($sim->get("enable_shm") == "t") and (not $smarty.post.enable_shm))}
		checked
	{/if}
{/capture}


<input type="hidden" name="homedisplay" value="false">
<input type="hidden" name="enable_logging" value="false">
<input type="hidden" name="enable_shm" value="false">
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">Edit SNMP Interface</th>
		</tr>
		<tr>
			<td>Label</td>
			<td><input type="text" {param name="description" class="input"} value="{$smarty.post.description|default:$sim->get('description')}"></td>
		</tr>
		<tr>
			<td>Display on Home Dashboard</td>
			<td><input type="checkbox" {$homedisplay_checked} name="home_display_check" value="checked" onClick="toggleHomeDisplay();"></td>
		</tr>
		{if $sim->get("enable_snmp") == "t"}
		<tr>
			<td>Enable SNMP Logging</td>
			<td><input type="checkbox" {$logging_checked} name="enable_logging_check" value="checked" onClick="toggleLogging();"></td>
		</tr>
		{if $sim->get('enable_netflow') == 't'}
		<tr>
			<td>Enable NetFlow</td>
			<td>
				<input type="checkbox" {$shm_checked} name="enable_shm_check" value="checked" onClick="toggleSHM();">
			</td>
		</tr>
		{/if}
		{if $sim->get('enable_sflow') == 't'}
		<tr>
			<td>Enable sFlow</td>
			<td>
				<input type="checkbox" {$shm_checked} name="enable_shm_check" value="checked" onClick="toggleSHM();">
			</td>
		</tr>
		{/if}
		{/if}
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Update Device"}</td>
		</tr>
	</table>

</form>
</div>

<script language="Javascript">
	toggleHomeDisplay();
	toggleLogging();
	{if $sim->get('enable_netflow') == 't'}
	toggleSHM();
	{/if}
</script>

<!-- end of {$smarty.template} -->
