{import_css files="_print"}
<!-- {$smarty.template} ($Id$) -->


<script language="Javascript">
parent.pnl_right.showEditor("?module=mod_devices&action=form_create_note&id={$smarty.get.id}");
</script>

<div class="titlebar noPrint">
	Notes Explorer
</div>

<div class="noPrint">
{include file="device_toolbar.tpl"}
</div>

<div class="panel noPrint">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	{input type="submit" class="button" value="Add New Note"  onClick="parent.pnl_right.showEditor('?module=mod_devices&action=form_create_note&id=`$smarty.get.id`');"}
	<img src="assets/buttons/button_help.gif" alt="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('notes_manager');" title="Help" alt="Help" />

	<img src="assets/buttons/button_print.gif" alt="Print this page" width="24" height="24" class="icon" onclick="window.print();" title="Print this page" />
</div>


{if $notes}
<div class="datagrid">
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="3">

{foreach from=$notes item="note"}
<tr>
	<td valign="top">
	<p><span style="color: #0066CC; font-weight: bold; font-family: Tahoma, sans-serif; font-size: 11px;"><img src="assets/icons/note.gif" width="16" height="16" class="icon">&nbsp;&nbsp;{$note.subject} by {$note.owner_id|resolve_user_id|capitalize}</span> &nbsp; &nbsp; <a href="javascript:parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_note&id={$note.id}');">Edit</a> |
	<a href="#" onClick="if (confirm('Are you sure you wish to delete this note?')) {ldelim} parent.pnl_right.showEditor('?module=mod_devices&action=process_delete_note&id={$note.id}'); {rdelim}">Delete</a></p>
	<p><span class="inlineHeading">Created:</span>{$note.created|date_format:"%b %e, %Y %H:%M:%S"}  {if $note.created <> $note.last_modified}&nbsp; &nbsp; &nbsp;<span class="inlineHeading">Last Modified:</span> {$note.last_modified|date_format:"%b %e, %Y %H:%M:%S"}{/if}</p>
	<p>{$note.note|strip_tags|nl2br}</p>
	</td>
	</tr>
{/foreach}
</table>
</div>
{else}
{"There are no notes associated with this device at the moment."|message_bar}
{/if}

<!-- end of {$smarty.template} -->
