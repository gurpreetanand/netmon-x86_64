<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}


<script>
var oid_handler = new AJAX_OID({$device.id});
var device_id = {$device.id};

{literal}
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}
{/literal}

</script>


<div class="titlebar">
	<a name="top"></a>{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}



<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>System Name: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysName.0"}  &nbsp; Location: {ajax_oid class="gray2" frequency="0" oid="SNMPv2-MIB::sysLocation.0"}</td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/checkpoint.gif" width="172" height="42" style="margin-right: 5px;"></div></td>
	</tr>
	<tr>
		<td>
		
	      <img src="/assets/icons/mib_timetick.png" width="16" height="16" class="icon"> System Uptime: {ajax_oid class="gray2" frequency="0" oid="DISMAN-EVENT-MIB::sysUpTimeInstance"}&nbsp;
      </td>
    </tr>
	<tr>
	  <td colspan="2"> System Description: {ajax_oid class="unresolved" frequency="0" oid="SNMPv2-MIB::sysDescr.0"}</td>
    </tr>
</table>


</fieldset>

<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/connect.png" width="16" height="16" class="icon"> TCP Connection Summary</strong>&nbsp;</legend>

<table width="100%" cellpadding="3" cellspacing="0" border="0">
<tr>
	<td><img src="/assets/icons/connect_success.png" width="16" height="16" class="icon"> Active Opens: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpActiveOpens.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpActiveOpens.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
	<td><img src="/assets/icons/connect_fail.png" width="16" height="16" class="icon"> Attempt Fails: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpAttemptFails.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpAttemptFails.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
</tr>
<tr>
	<td><img src="/assets/icons/connect_success.png" width="16" height="16" class="icon"> Passive Opens: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpPassiveOpens.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpPassiveOpens.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
	<td><img src="/assets/icons/connect_fail.png" width="16" height="16" class="icon"> Established Resets: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpEstabResets.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpEstabResets.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
</tr>
<tr>
	<td><img src="/assets/icons/connect_success.png" width="16" height="16" class="icon"> Established Connections: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpCurrEstab.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpCurrEstab.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
	<td><img src="/assets/icons/connect_fail.png" width="16" height="16" class="icon"> TCP Retransmissions: {ajax_oid class="blue2" frequency="60000" oid="TCP-MIB::tcpRetransSegs.0" trackable=true}<a class="noPrintInline" href="javascript: GetMIBDetail('?module=mod_devices&action=translate_snmp_oid&oid=TCP-MIB%3A%3AtcpRetransSegs.0&root_tpl=blank_panel&class=white');" style="color: #0066CC; text-decoration: underline;">[?]</a></td>
</tr>

</table>

</fieldset>

<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/drive_network.gif" width="16" height="16" class="icon"> Resources</strong>&nbsp;</legend>

{ajax_proxy dash="dash_windows_generic" proc="ajax_get_storage_devices" params="ip="|cat:$smarty.get.ip freq="120000" div="storage_devs"}

</fieldset>


<fieldset>
<legend>&nbsp;<strong><img src="/assets/icons/cluster.png" width="16" height="16" class="icon"> VRRP Interface Status</strong>&nbsp;</legend>

{ajax_proxy dash="dash_checkpoint" proc="ajax_get_vrrp_sessions" params="ip="|cat:$smarty.get.ip freq="120000" div="vrrp_sess"}

</fieldset>


</div>
<div class="titlebar">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
	<td><a name="process"></a>Process Summary</td>
		<td align="right"><img src="assets/icons/back_to_top.gif" class="button" width="14" height="10" onClick="location.href='#top';" title="Back to Top"></td>
	</tr>
</table>
</div>

<div class="panel" id="process_list">
	<img src="assets/core/separator_double.gif" width="10" height="21" class="icon"> 
	<a href="#" onClick="document.getElementById('process_list').style.display='none';document.getElementById('process_list_iframe').style.display='block'; processes.location.href='/?module=mod_devices&action=ajax_proxy&profile=dash_windows_generic&proc=ajax_get_windows_processes&ip={$smarty.get.ip}&root_tpl=blank_panel';">Click here to see a list of processes.</a>
</div>



<div id="process_list_iframe" style="display:none;">
	<iframe height="150px" id="processes" name="processes" width="100%" frameborder="0"></iframe>
</div>

{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}

<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();
</script>


<!-- end of {$smarty.template} -->

