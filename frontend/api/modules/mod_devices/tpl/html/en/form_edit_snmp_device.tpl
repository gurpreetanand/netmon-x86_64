<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">

	function toggleEnableSNMP() {
		if (document.dev_form.enable_snmp_check.checked) {
			document.dev_form.enable_snmp.value = "true";


		} else {
			document.dev_form.enable_snmp.value = "false";
			document.dev_form.enable_netflow_check.checked = false;
			toggleEnableNetflow();
			document.dev_form.enable_sflow_check.checked = false;
			toggleEnablesFlow();
		}
	}

	function toggleEnableNetflow() 
	{
		if (document.dev_form.enable_netflow_check.checked)
		{
			document.dev_form.enable_snmp_check.checked = true;
			toggleEnableSNMP();

			document.dev_form.enable_sflow_check.checked = false;
			toggleEnablesFlow()
			document.dev_form.enable_netflow.value = "true";
		} else {
			document.dev_form.enable_netflow_check.checked = false;
			document.dev_form.enable_netflow.value = "false";
		}
	}

	function toggleEnablesFlow() 
	{
		if (document.dev_form.enable_sflow_check.checked) 
		{
		
			document.dev_form.enable_snmp_check.checked = true;
			toggleEnableSNMP();
			document.dev_form.enable_netflow_check.checked = false;
			toggleEnableNetflow();
			document.dev_form.enable_sflow.value = "true";
		} else {
			document.dev_form.enable_sflow_check.checked= false;
			document.dev_form.enable_sflow.value = "false";
		}
	
	}
</script>
{/literal}
<div class="panel">
<form action="?module=mod_devices&action=update_snmp_device&id={$sdm->get('id')}" method="POST" name="dev_form">
<input type="hidden" name="enable_netflow" value="false">
<input type="hidden" name="enable_snmp" value="false">
<input type="hidden" name="enable_sflow" value="false">

{capture assign="enable_snmp_checked"}
	{if $smarty.post.enable_snmp == "true"}checked{/if}
	{if ($sdm->get("enable_snmp") == "t") and (!$smarty.post.enable_snmp)}checked{/if}
{/capture}

{capture assign="enable_netflow_checked"}
	{if $smarty.post.enable_netflow == "true"}checked{/if}
	{if ($sdm->get("enable_netflow") == "t") and (!$smarty.post.enable_netflow)}checked{/if}
{/capture}

{capture assign="enable_sflow_checked"}
	{if $smarty.post.enable_sflow == "true"}checked{/if}
	{if ($sdm->get("enable_sflow") == "t") and (!$smarty.post.enable_sflow)}checked{/if}
{/capture}

	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<th colspan="2" align="center">Edit SNMP Device</th>
		</tr>
		<tr>
			<td>Device Dashboard<!-- [{$sdm->get('profile')}] --></td>
			<td>{html_options options=$profiles selected=$smarty.post.profile name="profile" selected=$sdm->get('profile')}</td>
		</tr>
		<tr>
			<td>IP Address</td>
			<td><input type="text" {param name="ip_address" class="input"} value="{$smarty.post.ip_address|default:$sdm->get('ip_address')}"> </td>
		</tr>
		<tr>
			<td>Label</td>
			<td><input type="text" {param name="label" class="input"} value="{$smarty.post.label|default:$sdm->get('label')}"></td>
		</tr>
		<tr>
			<td>Sample Every</td>
			<td><input size="4" type="text" {param name="interval" class="input"} value="{$smarty.post.interval|default:$sdm->get('interval')|default:180}">&nbsp; seconds</td>
		</tr>
		<tr>
			<td>Community String</td>
			<td><input type="text" {param name="snmp_community" class="input"} value="{$smarty.post.snmp_community|default:$sdm->get('snmp_community')}"></td>
		</tr>
		<tr>
			<td>Port</td>
			<td><input type="text" {param name="snmp_port" class="input"} value="{$smarty.post.snmp_port|default:$sdm->get('snmp_port')|default:'161'}"></td>
		</tr>
		<tr>
			<td>Enable SNMP</td>
			<td><input type="checkbox" name="enable_snmp_check" onClick="toggleEnableSNMP();" value="checked" {$enable_snmp_checked}></td>
		</tr>
		<tr>
			<td>Enable NetFlow</td>
			<td>
				<input type="checkbox" name="enable_netflow_check" onClick="toggleEnableNetflow();" value="checked" {$enable_netflow_checked}>
			</td>
		</tr>	
		<tr>
			<td>Enable sFlow</td>
			<td>
				<input type="checkbox" name="enable_sflow_check" onClick="toggleEnablesFlow();" value="checked" {$enable_sflow_checked}>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">{input class="button" type="submit" value="Update Device"} &nbsp; <input type="button" class="button" value="Delete Device" onClick="if (confirm('Are you sure you wish to delete this device?')){ldelim}parent.showEditor('?module=mod_devices&action=delete_snmp_device&id={$sdm->get('id')}');{rdelim}"></td>
		</tr>
	</table>

</form>
</div>

<script language="Javascript">
toggleEnableSNMP();
toggleEnableNetflow();
</script>

<!-- end of {$smarty.template} -->
