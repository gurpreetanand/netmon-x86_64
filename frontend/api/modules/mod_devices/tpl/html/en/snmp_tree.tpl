<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.utilities,yui.connection,yui.animation,yui.dom,ext/adapter/ext/ext-base,ext/ext-all-debug"}
{import_css files="ext.ext-all"}



{literal}


<style type="text/css">
/*  Toolbar container */
.x-toolbar{
	border-color:#a9bfd3;
    border-style:solid;
    border-width:0 0 1px 0;
    display: block;
	padding:2px;
	background:  none;
	border-style:none;
    position:relative;
    zoom:1;
}

/* Buttons in normal state */
.x-toolbar .x-btn-left {
	background:none;
	border: 1px outset #000000;
	border-right-width: 0px;
}

.x-toolbar .x-btn-right{
	background:none;
	border: 1px outset #000000;
	border-left-width: 0px;
}

.x-toolbar .x-btn-center{
	background:none;
	padding:0 0;
	border: 1px outset #000000;
	border-right-width: 0px;
	border-left-width: 0px;
}

/* Buttons mouseover effect */
.x-toolbar .x-btn-over .x-btn-left{
	background: none;
	border: 1px solid #0A246A;
	border-right-width: 0px;
	background-color: #B6BDD2;
}

.x-toolbar .x-btn-over .x-btn-right{
	background: none;
	border: 1px solid #0A246A;
	border-left-width: 0px;
	background-color: #B6BDD2;
}
.x-toolbar .x-btn-over .x-btn-center{
	background: none;
	border: 1px solid #0A246A;
	border-right-width: 0px;
	border-left-width: 0px;
	background-color: #B6BDD2;
}


</style>
{/literal}

<div id="tree-div"></div>
{literal}
<script>

var tree;
var newNodes = [];

Ext.QuickTips.init();


function AdjustTreeframe(){
	var treeDiv = Ext.query('#tree-div')[0];
	
	treeDiv.style.height = YAHOO.util.Dom.getClientHeight() + "px";
	//treeDiv.style.width = YAHOO.util.Dom.getClientWidth() + "px";
}

window.onresize = AdjustTreeframe;
AdjustTreeframe();

manageNewNodeGroup = function(index) {
	node = newNodes[index];
	parent.parent.pnl_middle.location = '?module=mod_devices&action=manage_device_group&gid='+node.id;
}




Ext.onReady(function(){	
    // shorthand
    var Tree = Ext.tree;
	
	Ext.Ajax.timeout = 280000;
    
    // Menu Toolbar
    var mnu = new Ext.Toolbar({
    	id: 'deviceTree',
    	ctCls: 'panel',
    	style: 'border-style:none',
    	autoWidth: true,
   		items:[{
    			text: 'New Group',
            	icon: '/assets/icons/folderclosed.gif',
            	cls: 'x-btn-text-icon',
            	handler: function(){
            		
            		index = newNodes.length;
	                var node = root.insertBefore(new Ext.tree.TreeNode({
	                    text:'New Group',
	                    href: 'javascript:manageNewNodeGroup('+index+');',
	                    allowDrag:false
	                }), root.item(0));
	                
	                
	                newNodes.push(node);
	                
	                tree.getSelectionModel().select(node);
	                setTimeout(function(){
	                	var ge = createEditor(index);
		               	ge.editNode = node;
		               	ge.startEdit(node.ui.textNode);
	               	}, 10);
            	}
            	
           	}, {
           		id: 'new_devices_button',
           		text: 'New Device',
           		icon: '/assets/icons/device.gif',
           		cls: 'x-btn-text-icon',
           		handler: function() {
           			parent.parent.pnl_right.showEditor('?module=mod_devices&action=form_create_snmp_device');
           		}
           		
           	}, {
           		id: 'new_mibs_button',
           		text: 'MIBs',
           		icon: '/assets/icons/snmp_walk.gif',
           		cls: 'x-btn-text-icon',
           		handler: function() {
           			parent.parent.pnl_middle.location = '?module=mod_devices&action=manage_mibs';
           		}
           	}, new Ext.Toolbar.Separator, {
           		icon: '/assets/icons/help.png',
           		cls: 'x-btn-icon',
           		handler: function() {
           			parent.parent.pnl_right.showHelp('snmp_device_explorer');
           		}
           	}	
           	]
       });
       
       
    	
	// Tree Control
    tree = new Tree.TreePanel({
    	el: 'tree-div',
    	split: true,
        autoScroll:true,
        animate:false,
        border: false,
        enableDD:true,
        autoScroll: true,
        rootVisible:false,
        containerScroll: true, 
        margins: '5 0 5 5',
        tbar: mnu,
        split: true,
        ddGroup: 'treeDDGroup',
        loader: new Tree.TreeLoader({
            url:'index.php?module=mod_devices&action=json_get_devices&root_tpl=blank'
        })
    });
    
    // Root node
    var root = new Tree.AsyncTreeNode({
        text: 'Devices',
        draggable:false,
        id:'source'
    });
    tree.setRootNode(root);


	// Edit node
	createEditor = function() {
	
	    var ge = new Ext.tree.TreeEditor(tree, {
	        allowBlank:false,
	        blankText:'A name is required',
	        selectOnFocus:true
	    });
	    
	    ge.on('beforecomplete', function(){
	      query = '?module=mod_devices&action=ajax_create_device_group&root_tpl=blank';
	      payload = 'group_name='+encodeURIComponent(this.getValue());
	      callback_arg = {context: 'new_group', node: this.editNode};
	      callback = {success:success_handler,failure:failure_handler,argument:callback_arg};
	      this.editNode.disable();
	      YAHOO.util.Connect.asyncRequest('POST', query, callback, payload);
	      return true;
	    });
	    
	    ge.on('complete', function() {
	    	this.suspendEvents();
	    	this.disable();
	    	this.destroy();
	    	this.editNode = null;
	    	this.tree = null;
	    	console.log(this);
	    });
	    
	    return ge;
	}

	
	
	

	// D&D Handling
	tree.on('nodedrop', function(ev) {
		// Group it's going to
		if ('append' == ev.point) {
			gid = ev.target.id;
			gname = ev.target.text;
		} else {
			// No group or didn't drag on group name.
			gid = ev.target.parentNode.id;
			gname = ev.target.parentNode.text;
			if ('source' == gid) {
				gid = 0;
			}
		}
	
		// Dropped Node info
		nid = ev.dropNode.id;
		nname = ev.dropNode.text;
		
		// Craft our AJAX query
		query = '/?module=mod_devices&action=ajax_update_device_node&node_id='+nid+'&gid='+gid+'&root_tpl=blank';
		callback_arg = {context: 'nodedrop', node: ev.dropNode};
		callback = {success:success_handler,failure:failure_handler,argument:callback_arg};
		YAHOO.util.Connect.asyncRequest('GET', query, callback, null);
		
		return true;
	});
	
	// AJAX success callback
	success_handler = function(o) {
		var context = o.argument['context'];
		if ('nodedrop' == context) {
			if ('OK' != o.responseText) {
				failure_handler(o);
			}
		} else if ('new_group' == context) {
			res = o.responseXML;
			
			if (res) {
				status = res.documentElement.getElementsByTagName('status')[0].getAttribute('value');
				if ('OK' == status) {
					id = res.documentElement.getElementsByTagName('id')[0].getAttribute('value');
					o.argument['node'].id = id;
					o.argument['node'].enable();
					o.argument['node'].draggable = true;
					console.log(o.argument['node']);
				} else {
					failure_handler(o);
				}
			} else {
				failure_handler(o);
			}
		}
	}
	
	// AJAX failure callback
	failure_handler = function(o) {
		var context = o.argument['context'];
		if ('nodedrop' == context) {
			console.log("Error: Unable to move node");
		} else if ('new_group' == context) {
			
			if (o.responseXML) {
				err = o.responseXML.documentElement.getElementsByTagName('error')[0].getAttribute('value');
			} else {
				err = "No response from server";
			}
			
			alert("Unable to create new device group: "+err);
			o.argument['node'].remove(); 
		}
	}
	
	// This should resolve a bug in the Ext tree where if you change the ID of a node,
	// getNodeById() on that tree will not work with the new ID.
	getTreeNodeById = function(id) {
		for (i = 0; i < newNodes.length; i++) {
			if (newNodes[i].id == id) {
				return newNodes[i];
			}
		}
		return tree.getNodeById(id);
	}
	
	
	
	
	
	tree.render();
    root.expand(false, false);
    


});



</script>
{/literal}

<!-- end of {$smarty.template} -->