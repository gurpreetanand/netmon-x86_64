
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}

{literal}
<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}



</script>
{/literal}

<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}


<div class="panel">
<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> IP Address: <span class="gray2">{$device.ip_address}</span>
	    &nbsp;&nbsp;&nbsp;
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> Mac Address: <span class="gray2">{$device.mac|default:"<strong>Unresolved</strong>"}</span>
		
</td>
	</tr>

	
	
	

	<tr>
		<td>
		<img src="/assets/icons/tag_red.png" width="16" height="16" class="icon"> 
	  System Description: <span title="{$device.sysdescr|default:"N/A"}" class="gray2">{$device.sysdescr|default:"N/A"|truncate:70}</span>
</td>
	</tr>
	
</table>


</fieldset>

	

	
		{capture assign="dev_id"}{$device.id}{/capture}
		<!--{input type="button" class="button" value="Delete Device" onClick="parent.pnl_right.showEditor('?module=mod_devices&action=delete_snmp_device&id=$dev_id');"}-->

	</div>

<div class="panel">
{if $oids.temp1_online == "online(1)"}
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_temp?lowthreshold={$oids.temp1_lowthresh}&hithreshold={$oids.temp1_highthresh}&sensorname={$oids.temp1_name|escape:"url"|truncate:17}&unit=F&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeTempDegree.0&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?lowthreshold={$oids.temp1_lowthresh}&hithreshold={$oids.temp1_highthresh}&sensorname={$oids.temp1_name|escape:"url"|truncate:17}&unit=F&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeTempDegree.0&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
{/if}
{if $oids.hum1_online == "online(1)"}
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_humidity?lowthreshold={$oids.hum1_lowthresh}&hithreshold={$oids.hum1_highthresh}&sensorname={$oids.hum1_name|escape:"url"|truncate:23}&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeHumidityPercent.0&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_humidity?lowthreshold={$oids.hum1_lowthresh}&hithreshold={$oids.hum1_highthresh}&sensorname={$oids.hum1_name|escape:"url"|truncate:23}&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeHumidityPercent.0&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
{/if}
</div>

<div class="panel">
{if $oids.temp2_online == "online(1)"}
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_temp?lowthreshold={$oids.temp2_lowthresh}&hithreshold={$oids.temp2_highthresh}&sensorname={$oids.temp2_name|escape:"url"|truncate:17}&unit=F&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeTempDegree.1&refresh=1&enviro=0&url={$smarty.server.SERVER_NAME}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_temp?lowthreshold={$oids.temp2_lowthresh}&hithreshold={$oids.temp2_highthresh}&sensorname={$oids.temp2_name|escape:"url"|truncate:17}&unit=F&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeTempDegree.1&refresh=1&enviro=0&url={$smarty.server.SERVER_NAME}'
	 ); //end AC code
	</script>
{/if}
{if $oids.hum2_online == "online(1)"}
	<script type="text/javascript">
	AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
	'width','240',
	'height','80',
	'src','assets/flash/gauge_humidity?lowthreshold={$oids.hum2_lowthresh}&hithreshold={$oids.hum2_highthresh}&sensorname={$oids.hum2_name|escape:"url"|truncate:23}&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeHumidityPercent.1&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}',
	'quality','high',
	'salign','LT',
	'pluginspage','http://www.macromedia.com/go/getflashplayer',
	'movie','assets/flash/gauge_humidity?lowthreshold={$oids.hum2_lowthresh}&hithreshold={$oids.hum2_highthresh}&sensorname={$oids.hum2_name|escape:"url"|truncate:23}&device_id={$device.id}&oid=SPAGENT-MIB%3A%3AsensorProbeHumidityPercent.1&refresh=1&enviro=0&url={$smarty.const.TOP_DOMAIN}'
	 ); //end AC code
	</script>
{/if}
</div>



{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');

oid_handler.init();


</script>



<!-- end of {$smarty.template} -->
