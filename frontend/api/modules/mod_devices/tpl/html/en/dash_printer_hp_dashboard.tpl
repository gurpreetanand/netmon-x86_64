
<!-- {$smarty.template} ($Id$) -->

{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.connection,yui.animation,ajax_oid"}
{literal}

<script>
var oid_handler = new AJAX_OID({/literal}{$device.id}{literal});

function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

function successHandler(o) {
	document.getElementById(o.argument).innerHTML = o.responseText;
}

function failureHandler(o) { }

// this function is called from a <SPAN> or <DIV> block
function UpdateElement (elementid, oid, frequency){
	YAHOO.util.Connect.asyncRequest('GET', '/?module=mod_devices&action=ajax_get_oid_value&id={/literal}{$device.id}{literal}&oid=' + oid + '&root_tpl=blank', {success:successHandler, failure:failureHandler, argument:elementid}, null);
	setTimeout("UpdateElement('" + elementid + "', '" + oid + "', " + frequency + ")", frequency);
}

function UpdateStatusElement (elementid, oid, frequency){
	YAHOO.util.Connect.asyncRequest('GET', '/?module=mod_devices&action=ajax_get_oid_value&id={/literal}{$device.id}{literal}&oid=' + oid + '&root_tpl=blank', {success:successStatusElement, failure:failureHandler, argument:elementid}, null);
	setTimeout("UpdateStatusElement('" + elementid + "', '" + oid + "', " + frequency + ")", frequency);
}

function successStatusElement(o) {
	if(o.responseText == 1){
		document.getElementById(o.argument).className = "red";
		document.getElementById(o.argument).innerHTML = "Problem detected.";
	} else {
		document.getElementById(o.argument).className = "";
	}
	CheckPaperStatus();
}

function CheckPaperStatus(){
	
	paperOut = document.getElementById("gdStatusPaperOut").innerHTML;
	paperJam = document.getElementById("gdStatusPaperJam").innerHTML;
	lowToner = document.getElementById("gdStatusTonerLow").innerHTML;
	
	if(paperOut == "" && paperJam == "" && lowToner == ""){
		document.getElementById("paperStatus").className = "blue2";
		document.getElementById("paperStatus").innerHTML = "No problems detected.";
	} else {
		document.getElementById("paperStatus").className = "";
		document.getElementById("paperStatus").innerHTML = "";
	}
	
}

</script>
{/literal}


<div class="titlebar">
	{$device.label} ({$smarty.get.ip})
</div>

{include file="device_toolbar.tpl"}

<div class="panel">

<fieldset>
<legend>&nbsp;<img src="/assets/icons/tag_blue.png" width="16" height="16" class="icon"> <strong>Device Information</strong>&nbsp;</legend>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>Device Name: {ajax_oid class="gray2" frequency="0" oid="HOST-RESOURCES-MIB::hrDeviceDescr.1"}  Serial No: {ajax_oid class="gray2" frequency="0" oid="Printer-MIB::prtGeneralSerialNumber.1"} </td>
	    <td rowspan="2" valign="top"><div align="right"><img src="/assets/devices/hp.gif" alt="Logo: HP" width="51" height="36" align="middle"></div></td>
	</tr>
	<tr>
		<td>
		
	      <span class="panel">Status Message(s): {ajax_oid class="blue2" frequency="30000" oid="JETDIRECT3-MIB::gdStatusDisplay.0"} {ajax_oid class="blue2" frequency="30000" oid="JETDIRECT3-MIB::npSysStatusMessage.0"} {ajax_oid class="blue2" frequency="30000" oid="JETDIRECT3-MIB::npSysState.0"}</span></td>
    </tr>
	<tr>
	  <td colspan="2"> Print Status: <span class="blue2" id="paperStatus"></span>
	<span id="gdStatusPaperOut"></span> 
	<span id="gdStatusPaperJam"></span> 
	<span id="gdStatusTonerLow"></span> 
 </td>
    </tr>
</table>


</fieldset>


<fieldset>
	<legend>&nbsp; <img src="/assets/icons/printer.png" width="16" height="16" class="icon"> <strong>Supply Status</strong>&nbsp;</legend>


{foreach from=$supplies item="supply"}

<div style="margin: 7px;">
			<script type="text/javascript">
			AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0',
			'width','400',
			'height','21',
			'src','assets/flash/gauge_printersupply?desc={$supply.supply_desc|escape:'url'}&supply_max={$supply.supply_max}&supply_level={$supply.supply_level}',
			'quality','high',
			'salign','LT',
			'pluginspage','http://www.macromedia.com/go/getflashplayer',
			'movie','assets/flash/gauge_printersupply?desc={$supply.supply_desc|escape:'url'}&supply_max={$supply.supply_max}&supply_level={$supply.supply_level}'
			 ); //end AC code
			</script>
</div>
{/foreach}

</fieldset>



</div>



{madnet_action module="mod_dashboard" action="get_oid_monitors" device_id=$device.id}


<script language="Javascript">
	parent.pnl_right.showEditor('?module=mod_devices&action=form_edit_snmp_device&id={$device.id}');
	oid_handler.init();
	UpdateStatusElement('gdStatusPaperJam', 'JETDIRECT3-MIB::gdStatusPaperJam.0',  30000);
	UpdateStatusElement('gdStatusPaperOut', 'JETDIRECT3-MIB::gdStatusPaperOut.0',  30000);
	UpdateStatusElement('gdStatusTonerLow', 'JETDIRECT3-MIB::gdStatusTonerLow.0',  30000);
	
	//CheckPaperStatus();
	
</script>


<!-- end of {$smarty.template} -->
