<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.treeview"}
{literal}
<script>
function GetMIBDetail(url){
	parent.pnl_right.document.getElementById('iframe_help').src = url;
	activePanel = parent.pnl_right.document.getElementById("Netmon_Help");
	parent.pnl_right.activatePanel(activePanel);
}

function addOidWatcher(oid, datatype) {
	url = "?module=mod_devices&action=form_create_oid_watcher&device_id={/literal}{$smarty.get.id}{literal}&oid_txt=" + escape(oid) + "&type=" + datatype + "&root_tpl=blank_panel";
	parent.pnl_right.showEditor(url);
}
</script>
{/literal}
<div class="printOnly">
	<table width="100%"  border="0" cellpadding="0">
	  <tr>
		<td rowspan="2"><img src="assets/logo_print.gif" width="145" height="60" /></td>
		<td><div align="right"><h1>SNMP MIB Report: {$dm->get('label')} ({$dm->get('ip_address')} - {array_size array=$mib} elements)</h1></div></td>
	  </tr>
	  <tr>
	  	<td><div align="right">{$smarty.now|date_format:"%Y-%m-%d %H:%M:%S"}</div></td>
	  </tr>
	</table>
	<br /><br />
</div>
<div class="noPrint">
<div class="titlebar">
	Management Information Base (MIB) - {$dm->get('label')} ({$dm->get('ip_address')} - {array_size array=$mib} elements)
</div>

{include file="device_toolbar.tpl"}


<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page">
		<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('snmp_mibs');">
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		{input type="button" class="button" name="expand_all" value="Expand All" onClick="tree.expandAll();" }
		{input type="button" class="button" name="expand_all" value="Collapse All" onClick="tree.collapseAll();" }
		<img src="assets/core/separator_double.gif" width="10" height="21" class="icon">
		{*
		{if $can_write}{$oid.label}
		<span class="green"><strong>SNMP Write Enabled</strong></span>
		{else}
		<span class="gray2"><strong>SNMP Write Disabled</strong></span>
		{/if}
		*}
</div>
</div>

<div id="treeDiv1" class="tree" style="margin-left: 9px"></div>

<script language="Javascript">
{literal}
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	//tree.setExpandAnim(YAHOO.widget.TVAnim.FADE_IN);
	//tree.setCollapseAnim(YAHOO.widget.TVAnim.FADE_OUT);
	var root = tree.getRoot();
{/literal}
{if $mib}
{foreach from=$mib item="entry"}
	{assign var="mibname" value=$entry.mib|replace:"-":"_"}
	{counter name="mib_counter" assign="mib_number"}
	myobj = {ldelim} label: "<img src=\"assets/icons/mib_group.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> {$entry.mib|escape:javascript}", id:"mib_{$mib_number}" {rdelim} ;
	var mib_{$mib_number} = new YAHOO.widget.TextNode(myobj, root, false);

	{foreach from=$entry item="oid" key="key"}

		{if strcmp($key, "mib") != 0}
			{assign var="oidname" value=$oid.oid|replace:"-":"_"|replace:".":"_"|replace:" ":"_"|replace:"[":""|replace:"]":""}

			{foreach from=$oid key="oidkey" item="payload"}
				{counter name="oid_counter" assign="oid_number"}
				{if strcmp($oidkey, "oid") !=  0}
					{capture assign="rendered"}{render type=$payload.type payload=$payload.payload}{/capture}
					{capture assign="full_oid"}{$entry.mib}::{$oid.oid}{/capture}
					myobj = {ldelim} label: '<img src=\"assets/icons/mib_table.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> {$oid.oid|escape:javascript|nl2br}: {$rendered|escape:javascript|nl2br} &nbsp;&nbsp;&nbsp;[<a class="noPrintInline" href="javascript: GetMIBDetail(\'?module=mod_devices&action=translate_snmp_oid&oid={$full_oid|trim|escape:url}&root_tpl=blank_panel&class=white\');" style="color: #0066CC; text-decoration: underline;">Info</a>&nbsp;|&nbsp;<a class="noPrintInline" href="javascript: addOidWatcher(\'{$full_oid|trim|escape:url}\', \'{$payload.type|escape:javascript}\');" style="color: #0066CC; text-decoration: underline;">Add Tracker</a>]', id:"oid_{$oid_number}_payload" {rdelim} ;
					var oid_{$oid_number}_payload = new YAHOO.widget.TextNode(myobj, mib_{$mib_number}, false);
				{/if}
			{/foreach}
		{/if}
	{/foreach}
{/foreach}
{/if}
tree.draw();
{rdelim}

treeInit();
</script>

