<!-- {$smarty.template} ($Id$) -->
<graph caption="{$oid->get('label')}" subcaption="on {$oid->get('device_label')}" divlinecolor="C5C5C5" numberSuffix="" decimalPrecision="2" {if $smarty.get.showxaxis == 1}numdivlines="6"{else}numdivlines="3"{/if} showAreaBorder="1" areaBorderColor="444444" numberPrefix="" canvasBgColor="E1E1E1" canvasbordercolor="888888" canvasBorderThickness="2" showNames="{$smarty.get.showxaxis|default:0}" rotateNames="1" numVDivLines="25" vDivLineAlpha="30" showAlternateHGridColor="1" alternateHGridColor="C9C9C9" formatNumberScale="1" bgcolor="" animation="{$smarty.get.animation|default:0}" {if $smarty.get.showxaxis == 1}anchorRadius="5"{else}anchorRadius="3"{/if} showShadow="1" lineThickness="3" yAxisMinValue="{$min}" yAxisMaxValue="{$max}" >
   <categories>
	{foreach from=$points item="point"}
	{counter assign="tscount"}
	  <category name="{$point.timestamp}" {if $tscount % 2 == 0 && $smarty.get.showxaxis == 1}showName="1"{else}showName="0"{/if} />
	{/foreach}
   </categories>
   <dataset seriesname="{$oid->get('label')}" color="FF6600" showValues="0" showAreaBorder="1" areaBorderColor="FF6600" anchorsides="5" anchorBorderThickness="1" anchorBgColor="FFD9BF" >
	{foreach from=$points item="point"}
	<set value="{$point.message}" />
	{/foreach}
   </dataset>
</graph>
<!-- end of {$smarty.template} -->
