{import_js files="yui.yahoo,yui.event,yui.utilities,yui.connection,yui.dom}


<div class="titlebar">Group Delete Operation:</div>
<div id="message"></div>




<script language="Javascript">

	var gid = {$smarty.get.gid};
	var delete_devices = {if $smarty.get.delete_devices == '1'}true{else}false{/if};
	var rootNode = parent.pnl_left.iframe_snmp_explorer.tree.getRootNode();
	var groupNode = parent.pnl_left.iframe_snmp_explorer.getTreeNodeById(gid);
	var childNodes = [];
	
	{literal}
	
	
	// Iterates through nodes, updating them in turn
	updateTree = function(myNode) {
		if (!myNode.isExpanded()) {
			myNode.expand();
		}
		
		myNode.eachChild(updateNode);
		
		for (i = 0; i < childNodes.length; i++) {
			rootNode.appendChild(childNodes[i]);
		}
		myNode.remove();
		document.getElementById('message').innerHTML = '{/literal}{"This device group has been deleted successfully"|message_bar}{literal}';
	}
	
	// Registers a node in our child nodes array
	updateNode = function(arg_node) {
		if (arg_node) {
			if (!delete_devices) {
				childNodes.push(arg_node);
			}
		}
		return true;
	}
	
	ajax_delete = function() {
		//callback_arg = {context: 'new_group', node: this.editNode};
		callback = {success:success_handler,failure:failure_handler};
		delete_devices_url = (delete_devices == true) ? '&delete_devices=1' : '';
		query="/?module=mod_devices&action=ajax_delete_device_group&root_tpl=blank&gid="+gid+delete_devices_url;
		YAHOO.util.Connect.asyncRequest('GET', query, callback);
	}
	
	// AJAX query success handler
	success_handler = function(o) {
		// lalala
		res = o.responseXML;
			
		if (res) {
			status = res.documentElement.getElementsByTagName('status')[0].getAttribute('value');
			if ('OK' == status) {
				return updateTree(groupNode);
			}
		}
		failure_handler(o);
	}
	
	// AJAX query failure handler
	failure_handler = function(o) {
		document.getElementById('message').innerHTML = '{/literal}{"An error has occured while attempting to delete this device."|message_bar}{literal}';
	}
	
	
	
	
	
	
	
	
	
	
	// Prepare the group node
	groupNode.ensureVisible();
	groupNode.enable();
	groupNode.collapseChildNodes();
	
	
	// Ensure the child nodes have been loaded, then call updateTree()
	if (!groupNode.isExpanded()) {
		groupNode.expand(false, true, ajax_delete);
	} else {
		ajax_delete();
	}
	
	
	
	
	
	{/literal}

</script>
