
<!-- {$smarty.template} ($Id$) -->
<div class="panel white">
<h1><img src=" assets/icons/mib_table.gif" width="16" height="16" align="absmiddle"> OID: {$smarty.get.oid}</h1>

{if $desc}
{capture assign="description"}{$desc|escape:html|regex_replace:"/.*DESCRIPTION\t&quot;(.*)&quot;.*/si":"\$1"}{/capture}
{if $description != $desc|escape:html}
{capture assign="description"}{$description|strip|wordwrap:36:"\n"}{/capture}

<h2>Description</h2>
<p>{$description}</p>

{/if}
<h2>Definition</h2>
<pre>
{$desc|escape:html|default:"No information is available for this particular OID"|wordwrap:36:"\n":false|replace:"::":"\n"|regex_replace:"/DESCRIPTION\t&quot;(.*)&quot;/si":" "}
</pre>
{else}
<p>Netmon was unable to retrieve information about the specified OID.</p>
{/if}
</div>
<!-- end of {$smarty.template} -->
