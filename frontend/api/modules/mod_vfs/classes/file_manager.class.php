<?php


/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.6b
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */

/**
 * File management class. Maintains a database of files and directories available
 * and synchronizes it to real locations on the file system. This system also
 * abstracts on top of a traditional file system in that the leaf nodes can be
 * located anywhere on the underlying OS while all being centralized in the same
 * DB. This means that if your MIB parser needs to be reading from /usr/share/mibs/site
 * and your back-up tool needs to write to /var/backups, the system will be able to
 * make both of those paths look like the are leafs to the same node, without having
 * to follow symlinks. The system does not follow symlinks due to potential security
 * headaches.
 * 
 * Additionally, this system stores access permission informations for directories
 * that are specific to Madnet/Netmon, and that information does not belong in the FS.
 */
class File_Manager {
	/**
	 * Error Manager
	 *
	 * @var Error_Manager
	 */
	var $err;
	
	
	/**
	 * DB Manager
	 *
	 * @var DB_Manager
	 */
	var $db;
	
	/**
	 * Debugger
	 *
	 * @var Debugger
	 */
	var $debugger;
	
	/**
	 * Registry
	 *
	 * @var Registry
	 */
	var $registry;
	
	/**
	 * Class constructor
	 *
	 * @return File_Manager
	 */
	function File_Manager() {
		
		# Fetch the registry instance
		$this->registry = Registry::get_registry();
		
		# Collect composite objects
		$this->debugger = $this->registry->get_singleton("core", "debugger");
		$this->err = $this->registry->get_singleton("core", "error_manager");
		$this->db = $this->registry->get_singleton("core", "db_manager");
	 }
	 
	 /**
	  * Returns a hash-table filled with directory entries from the DB
	  *
	  * @return mixed
	  */
	 function get_directories() {
	 	$query = "SELECT * FROM fs_directories ORDER BY label ASC";
	 	$res = $this->db->select($query);
	 	
	 	if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
	 		return FALSE;
	 	}
	 	
	 	return $res;
	 	
	 }
	 
	 /**
	  * Performs any required corrective action to compensate for
	  * users potentially tampering with the file system directly, which
	  * would cause the DB and the file-system to become out-of-sync.
	  *
	  * @param int $id
	  * @return bool
	  */
	 function sync_directory($id) {
	 	
	 	
	 	# 1 - Retrieve access info for the directory in the DB
	 	$dir = $this->get_dir_info($id);
	 	
	 	
	 	# 2 - Call $this->get_dir_files to retrieve the files we know about
	 	$db_files = $this->get_dir_files($id);
	 	
	 	
	 	# 3 - Open up the directory and itterate through it and add any missing file
	 	# to the DB. Also store all files in an array for step #4
	 	$dir_handle = opendir($dir['real_path']);
	 	
	 	if (!$dir_handle) {
	 		$this->err->err_from_string("Unable to open specified directory");
	 		return FALSE;
	 	}
	 	
	 	$files_struct = array();
	 	
	 	while (($file_iter = readdir($dir_handle)) !== FALSE) {
	 		if (((strcmp($file_iter, ".") <> 0) && (strcmp($file_iter, "..") <> 0)) && (is_readable($dir['real_path'] . "/" . $file_iter))) {
		 		array_push($files_struct, $file_iter);
		 		if (!$this->_in_dir_files($db_files, $file_iter)) {
		 			$this->add_file($id, $file_iter);
		 			$this->debugger->add_hit("Adding file $file_iter");
	 			}
	 		}
	 	}
	 	
	 	closedir($dir_handle);
	 	
	 	# 4 - Now proceed in the other direction and find any file in the DB that
	 	# no longer exists, and remove them from the DB.
	 	foreach($db_files as $current) {
	 		if ((!in_array($current['filename'], $files_struct)) || (!is_readable($dir['real_path'] . "/" . $current['filename']))) {
	 			$this->debugger->add_hit("Removing " . $current['filename']);
	 			$this->remove_file($id, $current['id']);
	 		}
	 	}
	 	
	 	
	 	# 5 - Return
	 	return TRUE;
	 	
	 }
	 
	 /**
	  * Iterates through a list of files associated to a directory to find
	  * a match for the specified filename
	  *
	  * @param array $dir_files
	  * @param string $filename
	  */
	 function _in_dir_files($dir_files, $filename) {
	 	$in_dir = FALSE;
	 	
	 	foreach($dir_files as $file) {
	 		#var_dump($file['filename']); echo '<hr>'; var_dump($filename); echo '<hr>';
	 		if (strcmp($file['filename'], $filename) == 0) {
	 			$in_dir = TRUE;
	 		}
	 	}
	 	
	 	$this->debugger->add_hit("File $filename " . (($in_dir == FALSE) ? "does not exist" : "exists") . " in directory");
	 	
	 	return $in_dir;
	 }
	 
	 /**
	  * Returns a hash-table of all files located in a directory from
	  * the DB's perspective.
	  *
	  * @param int $directory_id
	  * @return array
	  */
	 function get_dir_files($directory_id) {

	 	$this->db->disable_caching();
	 	
	 	# 1- Retrieve the files associated with that directory in the DB
	 	$query = "SELECT * FROM fs_files WHERE directory_id = " . $this->db->escape($directory_id);
	 	$res = $this->db->select($query);
	 	$this->debugger->add_hit("Results:", NULL, NULL, vdump($res));
	 	
	 	if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
	 		$res = array();
	 	}
	 	
	 	
	 	$dir = $this->get_dir_info($directory_id);
	 	for ($i = 0; $i < sizeof($res); $i++) {
	 		$res[$i]['mtime'] = @filemtime($dir['real_path'] . '/' . $res[$i]['filename']);
	 		$res[$i]['type'] = $this->_get_filetype($dir['real_path'] . "/" . $res[$i]['filename']);
	 		$res[$i]['size'] = @filesize($dir['real_path'] . '/' . $res[$i]['filename']);
	 	}
	 	
	 	$this->db->restore_previous_state();
	 	$this->debugger->add_hit("File entries in DB:", NULL, NULL, vdump($res));
	 	usort($res, array($this, "_sort_by_mtime"));
	 	return $res;
	 }
	 
	 /**
	  * Custom sorting function
	  *
	  * @param array $file_a
	  * @param array $file_b
	  */
	function _sort_by_mtime($file_a, $file_b) {
		return ($file_a['mtime']  < $file_b['mtime']) ? -1 : (($file_a['mtime'] == $file_b['mtime']) ? 0 : 1);
	}
	 
	 	 
	 /**
	  * Returns the content of a file.
	  *
	  * @param int $id
	  */
	 function get_file($id) {
	 	$query = "SELECT (a.real_path || '/' || b.filename) AS filename, a.id AS dir_id,
	 			b.label, b.description FROM fs_directories a, fs_files b
	 			WHERE a.id = b.directory_id
	 			AND b.id = " . $this->db->escape($id) . "
	 			LIMIT 1";
	 	$res = $this->db->get_row($query);
	 	
	 	if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
	 		return FALSE;
	 	}

	 	return array("filename" => $res['filename'],
	 			"label"  => $res['label'],
	 			"description" => $res['description'],
	 			"path" => $res['filename'],
	 			"dir_id" => $res['dir_id']);
	 	
	 	
	 }
	 
	 /**
	  * Returns all meta-data relevant to the specified directory
	  *
	  * @param int $id
	  * @return mixed
	  */
	 function get_dir_info($id) {
	 	
	 	$query = "SELECT * FROM fs_directories WHERE id = " . $this->db->escape($id);

	 	$res = $this->db->get_row($query);
	 	
	 	if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
	 		return FALSE;
	 	}
	 	
	 	return $res;
	 }

	 /**
	  * Updates metadata pertaining to a specific file
	  *
	  * @param int $id
	  * @param string $filename
	  * @param string $label
	  * @param string $description
	  * @return bool
	  */
	 function update_file_info($id, $filename, $label, $description) {
	 	$auto = func_get_args();
	 	unset($auto['id']);
	 	
	 	$res = $this->db->auto_update("fs_files", $auto, "id = " . $this->db->escape($id));
	 	
	 	return (DB_QUERY_ERROR != $res);
	 }
	 
	 /**
	  * Updates metada pertaining to specified directory
	  * Note: This will likely be a 4.0 feature.
	  *
	  * @param unknown_type $id
	  */
	 function update_dir_info($id) {
	 	# We can make good use of the autoupdate feature here.
	 }
	 
	 /**
	  * Adds a new file entry in the DB
	  *
	  * @param int $dir_id
	  * @param string $filename
	  * @param string $label
	  * @param string $description
	  */
	 function add_file($dir_id, $filename, $label = "Unknown", $description = "") {
	 	$auto = func_get_args();
	 	$auto = array("directory_id" => $dir_id, "filename" => $filename,
	 				  "label" => $label, "description" => $description);
	 	
	 	$res = $this->db->auto_insert("fs_files", $auto);
	 	
	 	return (DB_QUERY_ERROR != $res);
	 	
	 }
	 
	 /**
	  * Removes a file from the database. If the file physically exists,
	  * it will need to be deleted by your application code. The scope of
	  * this class is to maintain an accurate representation of the shape of
	  * the file-system in the DB and not vice-versa.
	  *
	  * @param int $dir_id
	  * @param int $file_id
	  */
	 function remove_file($dir_id, $file_id) {
	 	$query = "DELETE FROM fs_files WHERE directory_id = " . $this->db->escape($dir_id) . "
	 	AND id = " . $this->db->escape($file_id);
	 	
	 	$res = $this->db->delete($query, TRUE);
	 	
	 	if ((FALSE == $res) || (DB_QUERY_ERROR == $res)) {
	 		return FALSE;
	 	}
	 	
	 	return $res;
	 }
	 
	# This method is used to determine whether the action associated to a
	# file should be viewed or downloaded. Binary files are downloaded, ascii
	# files are viewed, and anything else will have both choices.
	function _get_filetype($file) {
		$parts = @pathinfo($file);
		$ext = $parts['extension'];
		
		$bin_ext = array("pgz", "cap", "tgz", "tbz", "tbz2", "gz", "bin", "jpg", "gif",
						 "png", "zip");
						 
		$ascii_ext = array("txt", "log", "", "mib", "xml", "html", "htm", "sql");
		
		if (in_array($ext, $bin_ext)) {
			return "bin";
		}
		
		if (in_array($ext, $ascii_ext)) {
			return "ascii";
		}
		
		return "unknown";
		
	}
	
	function get_dir_info_by_path($path) {

	 	$query = "SELECT * FROM fs_directories WHERE real_path = " . $this->db->escape($path);

	 	$res = $this->db->get_row($query);
	 	
	 	if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
	 		return FALSE;
	 	}
	 	
	 	return $res;
	}
}


?>
