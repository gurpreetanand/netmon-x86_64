
<!-- {$smarty.template} ($Id$) -->
{if !$smarty.get.expanded}
	<div class="noPrint">
	<div class="titlebar">
		Viewing File: - {$file.label} ({$file.filename})
	</div>
	<div class="panel">
		<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">
		<img src="assets/buttons/button_back.gif" width="24" height="24" class="icon" onClick="history.go(-1);" title="Back to File List">
		<img src="assets/buttons/button_help.gif" title="View Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('viewing_files');">
		<img src="assets/buttons/button_print.gif" title="Print this page" width="24" height="24" class="icon" onClick="print();">
		
		<img src="assets/buttons/button_reports.gif" title="Display in new window" width="24" height="24" class="icon" onClick="window.open(document.location + '&expanded=1', 'Saved File', 'toolbar=no,scrollbars=yes');">
		
	</div>
	</div>
{/if}
	{if ($file.filename|regex_replace:"/.*\.(.*)/si":"\$1")  == "html"}
		{$file.content}
	{else}
	<strong>Type: {$file.filename|regex_replace:"/.*\.(.*)/si":"\$1"}</strong>
	<div class="white">
		<pre>{$file.content|wordwrap:90:"<br />":true}</pre>
	</div>
	{/if}

<!-- end of {$smarty.template} -->
