<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.treeview"}
{literal}
<script>
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	var root = tree.getRoot();
{/literal}

{if $dirs}
	{foreach from=$dirs item="dir"}

		   var myobj = {ldelim} label: "<div title=\"{$dir.notes|escape:javascript}\"><img src=\"assets/icons/folderopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> {$dir.label}</div>", id:"tree_{$dir.id}", href:"?module=mod_vfs&action=view_dir_files&dir_id={$dir.id}", target:"pnl_middle" {rdelim} ;
		   var tree_{$dir.id} = new YAHOO.widget.TextNode(myobj, root, false);


	
	{/foreach}
{else}
	{"Netmon is currently not managing any directories."|message_bar}
{/if}

{literal}

   tree.onExpand = function(node) {
      //alert(node.data.id + " was expanded");
   }

   tree.onCollapse = function(node) {
      //alert(node.data.id + " was collapsed");
   }

   tree.draw();
   
}
</script>
{/literal}

<div id="treeDiv1" class="tree" style="margin-left: 9px;"></div>
<script>treeInit();</script>


<!-- end of {$smarty.template} -->

