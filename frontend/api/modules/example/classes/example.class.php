<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    3.5a
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
  */



/**
  * Example MCO
  *
  * This module is used to perform any application-level change and management
  *
  *
  * @package MADNET
  * @author Xavier Spriet
  */
Class Example extends MadnetModule {

	/**
	  * Initializes all module variables
	  *
	  * @return void
	  */
	function init() {
		require_class($this->module, "example_manager");
	}


	/**
	 * Example Method
	 *
	 * @param pointer $index_content
	 * @return string
	 */
	function example_method(&$index_content) {
		return;
		#$index_content .= "Hello, World";
		#return "Page Title";
	}
}

?>