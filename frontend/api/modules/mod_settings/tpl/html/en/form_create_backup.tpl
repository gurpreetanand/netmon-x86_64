
<!-- {$smarty.template} ($Id$) -->
{import_css files="yui_sam.treeview,yui.calendar,yui.container"}
{import_js files="yui.yahoo,backup_lib,yui.utilities,yui.event,yui.dom,yui.animation,yui.calendar,yui.container,yui.dragdrop,yui.treeview,TaskNode"}
<style type="text/css">
{literal}
DIV.backup_panel {
	border-left: 1px solid White;
	border-top: 1px solid White;
	border-bottom: 1px solid #808080;
	background-color: #D4D0C8;
	padding: 4px;
	font-family: Tahoma, sans-serif;
	font-size: 11px;
}


.ygtvspacer { height: 10px; width: 18px; }
.ygtvcheck0 { background: url(/assets/images/check/check0.gif) 0 0 no-repeat; width:16px; height:22px; cursor:pointer }
.ygtvcheck1 { background: url(/assets/images/check/check1.gif) 0 0 no-repeat; width:16px; height:22px; cursor:pointer }
.ygtvcheck2 { background: url(/assets/images/check/check2.gif) 0 0 no-repeat; width:16px; height:22px; cursor:pointer }


#wait.yui-panel .bd { 
			background-color: #D4D0C8; 
			padding: 0px;
			border: none;
			text-align: center;
		}
		
		#wait.yui-panel .hd { 
			background-color: #333399;
			background-image: url('/assets/core/bg_titlebar.gif'); 
			background-position: top left; 
			background-repeat: repeat-x; 
			padding: 6px;
			border: none;
			text-align: center;
			vertical-align: middle;
		}

{/literal}
</style>

<div class="titlebar">New Netmon Backup</div>
<div class="backup_panel">
<span class="backup_panel"><img src="/assets/core/separator_double.gif" class="icon"></span>{input type="button" onClick="document.location = '?module=mod_settings&action=manage_backups';" value="Manage Backups" class="button"}
</div>
<form name="backup_form" action="?module=mod_settings&action=launch_backup_process&root_tpl=blank_panel" method="POST" onSubmit='return validate_backup_form();'>
<input type="hidden" name="tables" value="" />
<input type="hidden" name="date_from" value="" />
<input type="hidden" name="date_to" value="" />

<div class="titlebar">Step 1 - Select a Backup Destination</div>
<div class="backup_panel">
	<img src="/assets/core/separator_double.gif" class="icon">
	Choose a Backup Location:  
	<select id="destination" name="destination" onChange="toggle_destination(this.options[this.selectedIndex].value);">
		<option value="">-</option>
		<option value="dest_div_local">Local (on your Netmon device)</option>
		<option value="dest_div_smb">Remote (SMB share on your Network)</option>
	</select>
</div>
</div>
	
	<div class="backup_panel" style="display: none" id="dest_div_local">
		<img src="/assets/core/separator_double.gif" class="icon"> 
		Label: {input type="text" name="label"}
	</div>
	
	<div class="backup_panel" style="display: none" id="dest_div_smb">
	<br />
	<table align="center" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td align="right">Host IP Address: {input id="smb_ip_address" type="text" name="smb_ip_address" onKeyUp="update_smb_button();"}</td>
			<td align="right">Username: {input id="smb_username" type="text" name="smb_username"}</td>
		</tr>
		<tr>
			<td align="right">Domain: {input id="smb_domain" type="text" name="smb_domain"}</td>
			<td align="right">Password: {input id="smb_password" type="password" name="smb_password"}</td>
		</tr>
		<tr>
			<td colspan="2">Select Share: 
				<div id="smb_lookup" style="display: inline">
					<input type="button" onClick="ajax_get_smb_shares();" value="Find Shares on this host" />
				</div>
				<div id="smb_success_div" style="visibility: hidden; display: none">
					<img id="smb_success"  class="icon" src="/assets/icons/up.gif" style="display: inline" />
					<select name="smb_share" id="smb_share_dropdown">
						<option></option>
					</select>
				</div>
				<div id="smb_failure_div" style="visibility: hidden; display: none">
					<img id="smb_success"  class="icon" src="/assets/icons/down.gif" style="display: inline" />
					Unable to establish a connection to the specified share.
				</div>
				
				<img id="smb_progress" class="progress" src="/assets/progress.gif" style="display: inline" />
				
				
			</td>
		</tr>
	</table>
	</div>




	<div class="titlebar">Step 2 - Select Data to Back Up</div>
	<div class="white">
	
	<div id="db_tree_div" name="db_tree_div">
		<p><span class="blue2">Analyzing Database Structure... please wait.</span></p><img id="db_tree_progress" src="/assets/progress_transparent.gif" class="progress" style="display: inline" />
	</div>
	
	</div>
	
	{*
	<div class="titlebar" style="display: block">Step 3 - Select Date Range</div>
	<div class="backup_panel">
		<input type="checkbox" name="all_time" value="true"> All Time
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th>From</th>
				<th>To</th>
			</tr>
			<tr>
			<td><div id="dateFromDiv" style="float: none;"></div></td>
			<td><div id="dateToDiv" style="float: none;"></div></td>
			<td></td>
			</tr>
		</table>
	</div>
	*}

	
	</div>
	<div class="titlebar">Step 3 - Other Items</div>
	<div class="backup_panel">
	Select user to notify when the backup operation completes: {html_options name="notify" options=$users selected=$smarty.session.email}
	</div>
	<div class="backup_panel">
	<input type="checkbox" name="archive_mode" value="true"> <strong>Delete</strong> historical data backed up during the backup operation.
	</div>
<div class="backup_panel">
 {input type="submit" value="Launch Backup Operation Now" class="button"}
</div>

{*
<div id="progressDialog">
	<div align="center"><img src="/assets/progress_big_transparent.gif"></div>
</div>
*}


</form>
<script language="Javascript">
	update_smb_button();
	showProgressIndicator('db_tree_progress');
	ajax_get_db_components();

	{*
	cal_from = new YAHOO.widget.Calendar("cal_from","dateFromDiv", null, "{$smarty.now|date_format:"%m/%d/%Y"}");
	cal_to = new YAHOO.widget.Calendar("cal_to","dateToDiv", null, "{$smarty.now|date_format:"%m/%d/%Y"}");
		
	cal_from.maxDate = new Date();
	cal_to.maxDate = new Date();
	
	cal_from.render();
	cal_to.render();
	*}
	
	{literal}
	
		
		
		
	showProgressDialog = function() {
		YAHOO.namespace("netmon.panels");
		
		YAHOO.netmon.panels.wait = new YAHOO.widget.Panel("wait",
		{
			width:       '240px',
			fixedcenter: true,
			close:       false,
			modal:       true,
			draggable:   true,
			visible:     true,
			effect:      {effect:YAHOO.widget.ContainerEffect.FADE, duration:0.5}
		}
		);
		
		YAHOO.netmon.panels.wait.setHeader("Initializing. Please Wait...");
		YAHOO.netmon.panels.wait.setBody('<img src="/assets/loading.gif" alt="Loading" />');
		
		YAHOO.netmon.panels.wait.render(document.body);
		
		YAHOO.netmon.panels.wait.show();
	}
	
	
	{/literal}
	
</script>
</div>
<!-- end of {$smarty.template} ($Id$) -->
