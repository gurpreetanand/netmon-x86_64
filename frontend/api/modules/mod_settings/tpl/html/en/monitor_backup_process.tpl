{import_js files="yui.yahoo,yui.utilities,yui.connection,AC_RunActiveContent"}
<!-- {$smarty.template} ($Id$) -->
<script language="Javascript" src="/assets/jscript/backup_lib.js" type="text/javascript"></script>

<div class="titlebar">Backup Operation Status</div>

Backup Status: <span id="status" class="grey"></span><br />
<br />

<div class="datagrid">
<table width="100%" align="center" border="0" id="eventsGrid">
	<thead>
		<tr>
			<td>Time</td>
			<td>Event</td>
		</tr>
	</thead>
	<tbody>
	
	</tbody>
</table>
</div>

<script language="Javascript">
	fetch_backup_events({$id}, "eventsGrid", 0);
</script>
<!-- end of {$smarty.template} ($Id$) -->
