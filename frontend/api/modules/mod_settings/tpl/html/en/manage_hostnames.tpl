<!-- {$smarty.template} ($Id$) -->


<div class="titlebar">
	Manage Hostname Database
</div>

<div class="panel">
{capture assign="ip"}{$smarty.post.search_key|default:''}{/capture}
	{input type="button" class="button" value="Add New Host" onClick="parent.pnl_right.showEditor('?module=mod_settings&action=form_create_hostname&root_tpl=blank_panel&name=' + document.hostname_form.search_key.value);"}
	<img src="assets/buttons/button_help.gif" alt="Help" title="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=host_mgmt&root_tpl=blank');" />
</div>
<div class="panel">
<form name="hostname_form" action="index.php" method="GET" onSubmit="parent.pnl_right.showEditor('?module=mod_settings&action=form_create_hostname&name=' + document.hostname_form.search_key.value);">
<input type="hidden" name="module" value="mod_settings">
<input type="hidden" name="action" value="manage_hostnames">
<input type="hidden" name="search" value="1">
	&nbsp;Search Text/IP Address: {input class="string" name="search_key" type="text" id="key" size="15" tabindex="2" value="$ip" style="width: 160px;"} 
	<select name="filter" type="text" id="filter">
		<option {if !$smarty.post.filter}selected{/if} value="">Search All Sources</option>
		<option {if $smarty.post.filter == "SMB"}selected{/if} value="SMB">{"SMB"|translate_type}</option>
		<option {if $smarty.post.filter == "DNS"}selected{/if} value="DNS">{"DNS"|translate_type}</option>
		<option {if $smarty.post.filter == "HTTP"}selected{/if} value="HTTP">{"HTTP"|translate_type}</option>
		<option {if $smarty.post.filter == "CUSTOM"}selected{/if} value="CUSTOM">{"CUSTOM"|translate_type}</option>
	</select>
	{input type="submit" class="button" value="Search" tabindex="6"}
	</form>
</div>

{include file=list_hostnames.tpl}

{literal}
<script language="Javascript">
	//document.hostname_form.search_key.focus();
</script>
{/literal}
<!-- end of {$smarty.template} -->
