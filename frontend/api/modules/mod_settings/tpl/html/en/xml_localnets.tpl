<xml>
<localnets>
{if $networks}
{foreach from=$networks item="network"}
		<network id="{$network.id}" start="{$network.network}" end="{$network.broadcast}" label="{$network.label}" />
{/foreach}
{/if}
</localnets>
</xml>