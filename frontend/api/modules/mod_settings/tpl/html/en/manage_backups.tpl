<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.connection,backup_lib"}
{import_js files="sortabletable,sort_lib"}
{literal}
<script language="Javascript">

init_sort = function() {
	document.getElementById("backups").className = 'sort-table';
	var types = [null, 'netmon_date', 'CaseInsensitiveString', null];
	var report = new SortableTable(document.getElementById("backups"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}


</script>
{/literal}
<div class="titlebar">Netmon Database Backups</div>
<div class="datagrid">
<table width="100%" align="center" border="0" cellspacing="0" cellpadding="2" id="backups">
	<thead>
	<tr>
		<td>&nbsp;</td>
		<td>Initiated</td>
		<td>Status</td>
		<td>Action</td>
	</tr>
	</thead>
{foreach from=$backups item="backup"}
	<tr>
		<td><a href="?module=mod_settings&action=monitor_backup_process&id={$backup.id}&root_tpl=blank_panel" title="Monitor"><img src="/assets/icons/backup.gif" border="0" /></a></td>
		<td>{$backup.init_timestamp|date_format:"%b %e, %Y %H:%M:%S"} by {$backup.owner}</td>
		<td>{$backup.status}</td>
		<td><a href="?module=mod_settings&action=monitor_backup_process&id={$backup.id}&root_tpl=blank_panel" title="Monitor">Monitor</a> | 
		<a href="?module=mod_settings&action=clear_backup_process&id={$backup.id}&root_tpl=blank_panel" title="Monitor">Clear</a></td>
	</tr>
{/foreach}
</table>
</div>
<!-- end of {$smarty.template} -->
