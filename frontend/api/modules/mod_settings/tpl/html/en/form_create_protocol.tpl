<!-- {$smarty.template} ($Id$) -->


<div class="empty">
<form action="?module=mod_settings&action=process_create_protocol" method="POST">
	<br />
	<div align="center"><strong>Add New Port Label</strong></div>
	<br />
	
	<table width="100%" cellspacing="0" cellpadding="5" align="center" border="0">
		<tr>
			<td><div align="right">Transport Layer:</div></td>
			<td>
			{capture assign="selected"}{$smarty.post.protocol|default:""}{/capture}
			<select name="protocol">
				<option value="TCP" {if $selected == "TCP"}selected{/if}>TCP</option>
				<option value="UDP" {if $selected == "UDP"}selected{/if}>UDP</option>
			</select>			
			</td>
		</tr>
		<tr>
			<td><div align="right">Port Number:</div></td>
			<td><input name="port" type="text" class="input" value="{$smarty.post.port}"  style="width: 35px;" {param}></td>
		</tr>
		<tr>
			<td><div align="right">Label:</div></td>
			<td><input name="name" type="text" class="input" value="{$smarty.post.name}" size="25" {param}></td>
		</tr>
	</table>

	<p align="center">{input class="button" type="submit" value="Create Port Label"}</p>

</form>
</div>


<!-- end of {$smarty.template} -->
