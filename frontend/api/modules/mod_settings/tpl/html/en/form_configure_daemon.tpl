
<!-- {$smarty.template} ($Id$) -->
{literal}
<script language="Javascript">

	// Javascript doesn't support finding an option element by value, only by index
	// This function performs a search-by-value to set the selected Option in a <select>
	// by its value.
	function setDefaultDD_Val(drop_down, defaultVal) {
		for (i = 0; i < drop_down.length; i++) {
			if (drop_down.options[i].value == defaultVal) {
				drop_down.selectedIndex = i;
			}
		}
	}

</script>
{/literal}
<div class="datagrid center">
{if $fields}
<div class="titlebar">
	{$fields[0].description} ({$fields[0].name}) Configuration
</div>
<table width="100%" align="center" cellpadding="2" cellspacing="0" border="0">
<tr>
	<th>Variable</th>
	<th>Description</th>
	<th>Value</th>
	<th>Actions</th>
</tr>
{foreach from=$fields item="field"}
{if $field.meta.type != "hidden"}
<form method="POST" action="?module=mod_settings&action=process_update_daemon_var&daemon={$smarty.get.daemon}&var={$field.var}" name="form_{$field.var}">
<tr>
	<td>{$field.var}</td>
	<td>{$field.meta.label}</td>
	<td>
			{if $field.meta.options}
				{html_options options=$field.meta.options name=$field.var selected=$field.value}
			{else}
			{input type="text" class="text" name=$field.var value=$field.value}
			{/if}
	</td>
	<td>
		{capture assign="onclick"}{if $field.meta.options}setDefaultDD_Val(document.forms.form_{$field.var}['{$field.var}'], {$field.meta.default});{else}document.forms.form_{$field.var}['{$field.var}'].value = '{$field.meta.default|escape:"javascript"}'{/if}{/capture}
		{input type="button" value="Default" onClick="$onclick"}
		{input type="submit" value="Update"}
	</td>
</tr>
</form>
{/if}
{/foreach}
</table>
{/if}

</div>
<!-- end of {$smarty.template} -->
