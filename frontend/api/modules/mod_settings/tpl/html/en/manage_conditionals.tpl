<!-- {$smarty.template} ($Id$) -->

<div class="titlebar">
Manage Conditionals
</div>

<div class="panel">
	{input type="button" class="button" value="Add New Conditional" onClick="parent.pnl_right.showEditor('?module=mod_settings&action=form_create_conditional&root_tpl=blank_panel');"}
	<img src="assets/buttons/button_help.gif" alt="Help" title="Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('?module=mod_help&action=get_help&section=conditionals_setup&root_tpl=blank');" />
</div>
<div class="datagrid center">

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<th>IP Address </th>
		<th>Name </th>
		<th>Action</th>
	</tr>

    {if $ids}
    	{foreach from=$ids item="id"}
    	<tr>
	    	<td>{$id.ip}</td>
	    	<td>{$id.name}</td>
	    	<td><a href="#" onClick="if (confirm('Are you sure you wish to delete the conditional named {$id.name}')) {ldelim} parent.pnl_right.showEditor('?module=mod_settings&action=delete_conditional&id={$id.id}'); {rdelim}">Delete</a></td>
	    </tr>
    	{/foreach}
    </table>
    {else}
    	</table>
    	{"There are currently no conditionals registered in the database"|message_bar}
    {/if}
    
    
<!-- end of {$smarty.template} -->