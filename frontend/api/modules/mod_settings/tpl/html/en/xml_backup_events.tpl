{if $backup}
<backup status="{$backup.status}">
{foreach from=$backup.events item="event"}
	<event time="{$event.timestamp|date_format:"%b %e, %Y %H:%M:%S"}" timestamp="{$event.timestamp}" payload="{$event.event}" />
{/foreach}
</backup>
{/if}