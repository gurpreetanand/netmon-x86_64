{if $commands}
<table cellpadding="3" cellspacing="0" border="0">
  <tr>
    <th width="16">&nbsp;</th>
    <th>Label</th>
    <th>Command</th>
    <th><div align="center">Action</div></th>
  </tr>
  {foreach from=$commands item="command"}
  {capture assign="async"}{if $command.async == 'f'}sync{else}async{/if}{/capture}
  <tr>
  	<td>
  		<img id="delete_{$command.id}_progress" src="/assets/progress_transparent.gif" class="progress" />
  		<img src="/assets/icons/thread_{$async}.gif" width="16" height="16" class="icon" title="Process Command {$async|mktitle}hronously" alt="Process Command {$async|mktitle}hronously" />
  		{if $command.perform_on_failure == 't'}
		<img src="/assets/icons/icon_error.gif" width="16" height="16" class="icon" title="Perform on Failure" alt="Perform on Failure" />
  		{/if}
  		{if $command.perform_on_recovery == 't'}
  		<img src="/assets/icons/up.gif" width="16" height="16" class="icon" title="Perform on Recovery" alt="Perform on Recovery" />
  		{/if}
  	</td>
   	<td>{$command.label}</td>
   	<td><tt>{$command.command|escape:html}</tt></td>
   	<td><div align="center"><a href="javascript:show_edit_container(); fill_edit_container('{$command.id}', '{$command.label|escape:javascript|escape:quotes}', '{$command.command|escape:javascript|replace:'"':'&quot;'|escape:quotes|strip}', '{$command.timeout|escape:javascript}', '{$command.async|escape:javascript}', '{$command.perform_on_failure|default:f}', '{$command.perform_on_recovery|default:f}');">Edit</a> | 
   		<a href="javascript:if (confirm('Are you sure you wish to delete the command {$command.label|escape:javascript}?')) {ldelim} ajax_delete_command('{$command.id}'); {rdelim}">Del</a>
	</div></td>
   </tr>
{/foreach}
</table>
{/if}
