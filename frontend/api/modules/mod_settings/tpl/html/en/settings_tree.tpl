<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.treeview"}
<!-- Execution of the code that actually builds the specific tree.
     The variable foldersTree creates its structure with calls to
	 gFld, insFld, and insDoc -->
{literal}
<script>
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	var root = tree.getRoot();

   var myobj = { label: "<img src=\"assets/icons/gear.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Initial Setup Tasks", id:"initialsetup" } ;
   var initialsetup = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/gear.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Define Network Range(s)", id:"localnets", href:"?module=mod_settings&action=manage_localnets&root_tpl=blank_panel", target:"pnl_middle" } ;
   var localnets = new YAHOO.widget.TextNode(myobj, initialsetup, false);

   myobj = { label: "<img src=\"assets/icons/gear.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Configure SNMP Discovery", id:"snmpauto", href:"?module=mod_settings&action=form_configure_daemon&daemon=snmpautod", target:"pnl_middle" } ;
   var snmpauto = new YAHOO.widget.TextNode(myobj, initialsetup, false);

   myobj = { label: "<img src=\"assets/icons/gear.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Set Up Netmon Users", id:"usersetup", href:"?module=mod_user&action=manage_users&root_tpl=blank_panel", target:"pnl_middle" } ;
   var usersetup = new YAHOO.widget.TextNode(myobj, initialsetup, false);

	// Registration data
   var myobj = { label: "<img src=\"assets/icons/user_gray.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Registration Data", id:"registration" } ;
   var registration = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/icon_get_world.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> License Key", id:"localnets", href:"?module=mod_settings&action=form_edit_license&root_tpl=blank_panel", target:"pnl_middle" } ;
   var license = new YAHOO.widget.TextNode(myobj, registration, false);
   
   myobj = { label: "<img src=\"assets/icons/report.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Device Report", id:"localnets", href:"?module=mod_settings&action=device_report&root_tpl=blank_panel", target:"pnl_middle" } ;
   var dev_report = new YAHOO.widget.TextNode(myobj, registration, false);
   // End of registration data section

   myobj = { label: "<img src=\"assets/icons/conditionals.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert Conditionals", id:"conditionals", href:"?module=mod_settings&action=manage_conditionals&root_tpl=blank_panel", target:"pnl_middle" } ;
   var conditionals = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/gear.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert Testing Utility", id:"localnets", href:"?module=mod_settings&action=form_test_alerts&root_tpl=blank_panel", target:"pnl_middle" } ;
   var localnets = new YAHOO.widget.TextNode(myobj, initialsetup, false);

   myobj = {label: '<img src="assets/icons/alert.gif" class="icon" style="vertical-align: middle" border="0">Manage Alerts', id:'alerts', href:'?module=mod_settings&action=manage_alerts&root_tpl=blank_panel', target:'pnl_middle' };
   new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/script_code_red.png\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert Message Templates", id:"alert_templates" } ;
   var alert_templates = new YAHOO.widget.TextNode(myobj, root, false);
   
   


{/literal}
{foreach from=$alert_types item="type"}

   myobj = {ldelim}label: "<img src=\"assets/icons/script_code_red.png\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> {$type.description|capitalize}", id:"conditionals", href:"?module=mod_settings&action=setup_alert_tpl&id={$type.id}", target:"pnl_middle" {rdelim} ;
   var template = new YAHOO.widget.TextNode(myobj, alert_templates, false);
{/foreach}

{literal}

   myobj = { label: "<img src=\"assets/icons/backup.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Database Backup", id:"datamgmt", href:"?module=mod_settings&action=form_create_backup&root_tpl=blank_panel", target:"pnl_middle" } ;
   var datamgmt = new YAHOO.widget.TextNode(myobj, root, false);

   var myobj = { label: "<img src=\"assets/icons/filters.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Filter Collections", id:"filters" } ;
   var filters = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/filters.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Traffic Filters", id:"filter_traffic", href:"?module=mod_settings&action=get_traffic_filters&root_tpl=blank_panel", target:"pnl_middle" } ;
   var filter_traffic = new YAHOO.widget.TextNode(myobj, filters, false);

   myobj = { label: "<img src=\"assets/icons/filters.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Host Filters", id:"filter_host", href:"?module=mod_settings&action=get_host_filters&root_tpl=blank_panel", target:"pnl_middle" } ;
   var filter_host = new YAHOO.widget.TextNode(myobj, filters, false);

   myobj = { label: "<img src=\"assets/icons/device.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Hostname Database", id:"hostnames", href:"?module=mod_settings&action=manage_hostnames&root_tpl=blank_panel", target:"pnl_middle" } ;
   var hostnames = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/localnet.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Network Range(s)", id:"localnets", href:"?module=mod_settings&action=manage_localnets&root_tpl=blank_panel", target:"pnl_middle" } ;
   var localnets = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/services.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Services", id:"procmond", href:"?module=mod_settings&action=manage_netmon_services&root_tpl=blank_panel", target:"pnl_middle" } ;
   var procmond = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/netmon.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Update Service", id:"updates", href:"?module=mod_settings&action=manage_updates&root_tpl=blank_panel", target:"pnl_middle" } ;
   var updates = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/protodic.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Port Label Database", id:"protodic", href:"?module=mod_settings&action=manage_protocols&root_tpl=blank_panel", target:"pnl_middle" } ;
   var protodic = new YAHOO.widget.TextNode(myobj, root, false);

   var myobj = { label: "<img src=\"assets/icons/groups.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Users & Groups", id:"usermgmt" } ;
   var usermgmt = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/users.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Manage User Accounts", id:"users", href:"?module=mod_user&action=manage_users&root_tpl=blank_panel", target:"pnl_middle" } ;
   var users = new YAHOO.widget.TextNode(myobj, usermgmt, false);

   myobj = { label: "<img src=\"assets/icons/groups.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Manage Account Groups", id:"groups", href:"?module=mod_user&action=manage_groups&root_tpl=blank_panel", target:"pnl_middle" } ;
   var groups = new YAHOO.widget.TextNode(myobj, usermgmt, false);


   tree.onExpand = function(node) {
      //alert(node.data.id + " was expanded");
   }

   tree.onCollapse = function(node) {
      //alert(node.data.id + " was collapsed");
   }

   tree.draw();

}
</script>
{/literal}


<div id="treeDiv1" class="tree" style="margin-left: 9px;"></div>
<script>treeInit();</script>
<!-- end of {$smarty.template} -->
