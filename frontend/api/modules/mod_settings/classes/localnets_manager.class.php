<?php


class LocalNets_Manager extends MadnetElement {
  
  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  public $table = "localnets";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  public $pkey = "id";/**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  public $module = "mod_settings";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  public $element = __CLASS__;
  
  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  public $meta;
  
  function init() {
    $this->params->add_primitive("network",     "ip_address",           TRUE,   "Start IP",      "Network Address");
    $this->params->add_primitive("broadcast",   "ip_address",           TRUE,   "End IP",        "Network Broadcast");
    $this->params->add_primitive("label",       "string",               FALSE,  "Label",         "Network Name");
    $this->params->add_primitive("enable_snmp_discovery", "pg_bool", TRUE, "SNMP Probing", "Scan this network periodically for new SNMP devices");
    $this->params->add_primitive("enable_portscan", "pg_bool", TRUE, "Scan for open Ports", "Periodically scan this network and look for new open ports");
  }
  
  
  
  /**
    * Returns an array containing the user ID of every user account in the DB
    *
    * @return mixed
    */
  public function get_all_ids() {
    $query = "SELECT * FROM {$this->table} ORDER BY network, broadcast ASC";
    $result = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      return $result;
    }
  }
  
  public function pre_insert($id = null) {
    $net = $this->params->getVal("network");
    $broad = $this->params->getVal("broadcast");
    
    if (sprintf("%u", ip2long($broad)) <= sprintf("%u", ip2long($net))) {
      $this->err->err_from_string("Invalid End IP address. The End IP address must be higher than the beginning IP address");
      return FALSE;
    }
    
    $query = "SELECT id FROM {$this->table} WHERE network = " . 
         $this->db->escape($net) .
             " AND broadcast = " . $this->db->escape($broad);
             
    if ($id) { $query .= "AND id != " . $this->db->escape($id); }
    
    
    $result = $this->db->select($query);
    
    if ((!is_array($result)) || ((is_array($result)) && (sizeof($result) == 0))) {
      return TRUE;
    } else {
      $this->err->err_from_string("A local network with the same parameters already exists");
      return FALSE;
    }
  }

  public function post_insert($id = null) {
    $label = $this->params->getVal("label");
    $net = $this->params->getVal("network");
    $broad = $this->params->getVal("broadcast");

    $start = ip2long($net);
    $end = ip2long($broad);

    $dom=new DOMDocument();
    $dom->load("/var/www/registry/filters/host.xml");

    $doc = $dom->getElementsByTagName("filters")->item(0);

    if ($dom->getElementsByTagName("filters")->length == 0) {
      // Filter collection is messed, add back the root attribute we expect.
      $doc = $dom->createElement("filters");
      $dom->appendChild($doc);
    }

    // <filter name="LAN" type="exclusive">
    $filter = $dom->createElement("filter");
    $doc->appendChild($filter);

    $name = $dom->createAttribute("name");
    $name->value = $label;

    $filter->appendChild($name);

    $type = $dom->createAttribute("type");
    $type->value = "exclusive";

    $filter->appendChild($type);

    for ($i = $start; $i <= $end; $i++) {
      $param = $dom->createElement("param");
      $param->appendChild($dom->createTextNode(long2ip($i)));
      $filter->appendChild($param);
    }

    $dom->save("/var/www/registry/filters/host.xml");
    return TRUE;
  }
  
  
  public function pre_update($id) {
    return $this->pre_insert($id);
  }

  public function post_update($id) {
    return (
      $this->pre_delete($id) &&
      $this->post_insert($id)
    );
  }

  public function pre_delete($id = null) {
    $sql = "SELECT label FROM localnets WHERE id = " . intval($this->db->escape($id));
    $res = $this->db->select($sql);

    $label = $res[0]['label'];

    $dom=new DOMDocument();
    $dom->load("/var/www/registry/filters/host.xml");


    $xpath = new DOMXpath($dom);

    $to_nuke = $xpath->query("//filters/filter[@name='" . $label . "']");

    foreach ($to_nuke as $element) {
      $element->parentNode->removeChild($element);
    }

    $dom->save("/var/www/registry/filters/host.xml");

    return TRUE;
  }
}


?>
