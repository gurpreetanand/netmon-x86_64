<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.dom,yui.utilities,yui.animation"}
{import_js files="sortabletable,sort_lib"}
{literal}


<script language="Javascript">

function SetAllCheckBoxes(FormName, FieldName, CheckValue)
{
	if(!document.forms[FormName])
		return;
	var objCheckBoxes = document.forms[FormName].elements[FieldName];
	if(!objCheckBoxes)
		return;
	var countCheckBoxes = objCheckBoxes.length;
	if(!countCheckBoxes)
		objCheckBoxes.checked = CheckValue;
	else
		// set the check value for all check boxes
		for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;
}

function toggle_fields(FormName, FieldName) {
	if(!document.forms[FormName]) { return; }
	
	var objFields = document.forms[FormName].elements[FieldName];
	
	if(!objFields) { return; }
	objFields.disabled = !objFields.disabled;
}

function toggleDates(toggleAction){
	toggle_fields('frm_syslogview', 'start_date[Date_Day]');
	toggle_fields('frm_syslogview', 'start_date[Date_Month]');
	toggle_fields('frm_syslogview', 'start_date[Date_Year]');
	toggle_fields('frm_syslogview', 'end_date[Date_Day]');
	toggle_fields('frm_syslogview', 'end_date[Date_Month]');
	toggle_fields('frm_syslogview', 'end_date[Date_Year]');
}

/**
Note: We keep a count of rows deleted so far because the indexes
of the table rows change each time you delete one, so you need to
subtract the count to the index if you want to delete the right row.
*/
var boxNum = 0;
var currentBox = 0;


var trans_attr = {
	opacity: { to: 0 },
	duration: 0.6
};


deleteRow = function(event, data, payload) {
	rowObj = payload['rowObj'];
	boxId = payload['boxId'];
	rowObj.parentNode.deleteRow(rowObj.sectionRowIndex);
	//console.log("Current: "+boxId+" - Total: "+boxNum);
	if (boxId == boxNum) {
		window.setTimeout("confirmDeletion()", 1000);
	}
}

confirmDeletion = function() {
	delDiv = document.getElementById('delete_confirm');
	
	delDiv.style.opacity = 0;
	var confirmTransaction = new YAHOO.util.Anim(delDiv, {opacity: {to: 1}, duration: 1});
	confirmTransaction.animate();
}

ajax_success = function(o) {
	for (i = 0; i < o.argument.length; i++) {
		currentBox++;
		row = document.getElementById(o.argument[i]);
		idx = row.sectionRowIndex;
		tbdy = row.parentNode;

		if (boxNum < 8) {
			var transition = new YAHOO.util.Anim(row, trans_attr);		
			transition.onComplete.subscribe(deleteRow, {'rowObj': row, 'boxId': currentBox});
			transition.animate();
		} else {
			tbdy.deleteRow(idx);
		}
		
	}
	
	if (boxNum >= 8) {
		confirmDeletion();
	}
}

ajax_failure = function(o) {
	// do nothing (for now)
}

var callback = {success: ajax_success, failure: ajax_failure};

function delete_events() {
	document.getElementById('delete_confirm').style.opacity = 0;
	document.getElementById('delete_confirm').style.filter = "alpha(opacity=0)";
	payload = '';
	currentBox = 0;
	var objCheckBoxes = document.forms['syslogfilter'].elements['msg_id[]'];
	
	_rows = [];
	
	// This fixes a bug that prevented itteration of checkboxes if
	// only one row was present in the syslog results dialog.
	if (!objCheckBoxes.length) {
		objCheckBoxes = [objCheckBoxes];
	}
	
	
	for (i = 0; i < objCheckBoxes.length; i++) {
		if (objCheckBoxes[i].checked == true) {
			val = objCheckBoxes[i].value.split('|');
			_rows.push(objCheckBoxes[i].parentNode.parentNode.id);
			payload += '&msg_id[]=' + val[1];
		}
	}
	boxNum = _rows.length;

	
	
	callback = {success: ajax_success, failure: ajax_failure, argument: _rows};
	YAHOO.util.Connect.asyncRequest('POST', '?module=mod_syslog&action=ajax_delete_syslog_entries&root_tpl=blank', callback, payload);
}

init_sort = function() {
	document.getElementById("events_tbl").className = 'sort-table';
	var types = [null, 'netmon_date', 'CaseInsensitiveString', 'CaseInsensitiveString', 'CaseInsensitiveString', 'CaseInsensitiveString'];
	var report = new SortableTable(document.getElementById("events_tbl"), types);
	report.sort(1, true);
}
addEvent(window, "load", init_sort);


function addEvent(elm, evType, fn, useCapture)
// addEvent and removeEvent
// cross-browser event handling for IE5+,  NS6 and Mozilla
// By Scott Andrew
{
  if (elm.addEventListener){
    elm.addEventListener(evType, fn, useCapture);
    return true;
  } else if (elm.attachEvent){
    var r = elm.attachEvent("on"+evType, fn);
    return r;
  } else {
    alert("Handler could not be removed");
  }
}



</script>

{/literal}


<div class="noPrint">
<div class="titlebar">
	Event Log Viewer
</div>
{if ($smarty.get.src == 'device')}
	{include file="device_toolbar.tpl"}
{/if}

{if $entries}

<div class="panel">
	<img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon">  
   {input name="btn_select_all" type="button" class="button" id="btn_select_all" value="Check All" onClick="SetAllCheckBoxes('syslogfilter', 'msg_id[]', true);"} 
   {input name="btn_clear_all" type="button" class="button" id="btn_clear_all" value="Uncheck All" onClick="SetAllCheckBoxes('syslogfilter', 'msg_id[]', false);"} 
	{input name="btn_delete_selected" type="button" class="button" id="btn_delete_selected" value="Delete Selected" onClick="document.getElementById('form_action').value='Delete'; delete_events();"}
   <img src="assets/core/separator_double.gif" width="10" height="21" hspace="2" class="icon"> 
	<img src="assets/buttons/button_print.gif" width="24" height="24" class="icon" onClick="print();" title="Print this page"> 
	<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" title="Show Help" onClick="parent.pnl_right.showHelp('syslog_viewer');">
</div>
</div>

<div class="panel" style="height: 22px; padding: 0px; valign: middle">
<div  style="background-color: #FFFF80; height: 22px; opacity: 0; filter: alpha(opacity=0)"  id="delete_confirm"><img src="assets/icons/info.gif" width="16" height="16" align="absmiddle">&nbsp;Log events have been deleted successfully.</div>
</div>

<div class="datagrid center">
<!-- <form name="syslogfilter" id="syslogfilter" method="post" action="?module=mod_syslog&action=delete_syslog_entries"> -->
<form name="syslogfilter" id="syslogfilter" onSubmit="delete_events(); return false;">
<input type="hidden" name="form_action" id="form_action" value="" />
<table width="100%" border="0" cellpadding="3" cellspacing="0" id="events_tbl">
<thead>
	<tr>
		<td>&nbsp;</td>
		<td>Timestamp</td>
		<td>Client</td>
		<td>Facility</td>
		<td>Severity</td>
		<td>Message</td>
	</tr>
</thead>
<tbody>
{counter name='ctr' start=-1 skip=1 assign="count" print=false}
	{foreach from=$entries item="entry"}
	{counter name='ctr'}
	<tr id='row_{$count}'>
		<td><input type="checkbox" name="msg_id[]" value="{$count}|{$entry.msg_id}"></td>
		<td>{$entry.timestamp|date_format:"%b %e, %Y %H:%M:%S"}</td>
		<td>{$entry.ip|resolve_ip} ({$entry.ip})</td>
		<td><span class="gray2" style="border: 1px solid #666666; background-color: #999999;">{$entry.facility|get_facility_by_id}</span></td>
		<td><span style="border: 1px solid #333333;" class="{if $entry.severity|get_severity_by_id == "ALERT"}red{elseif $entry.severity|get_severity_by_id == "CRITICAL"}red{elseif $entry.severity|get_severity_by_id == "EMERGENCY"}red{elseif $entry.severity|get_severity_by_id == "WARNING"}yellow{elseif $entry.severity|get_severity_by_id == "ERROR"}yellow{else}gray2{/if}">{$entry.severity|get_severity_by_id}</span></td>
		<td><div align="left" style="width: 200px; overflow: auto;">{$entry.message|regex_replace:"/(.*)%([^:]+):(.*)/si":"\$1<a href='http://www.cisco.com/pcgi-bin/search/search.pl?searchPhrase=%25\$2' target='_blank'>%\$2</a>\$3"}</div></td>
	</tr>
	{/foreach}
{/if}
</tbody>
</table>
</form>

{if !$entries}
 {"No results were found in the SYSLOG database."|message_bar}
{/if}

</div>

<!-- end of {$smarty.template} -->
