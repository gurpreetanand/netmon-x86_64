<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom,yui.element"}
{literal}

<style>
SPAN.customDate {
	display: none;
}
</style>

<script language="Javascript">

parent.document.getElementById("Netmon_Syslog_Manager").innerHTML = "Event Log Search";


function toggle_fields(FormName, FieldName) {
	if(!document.forms[FormName]) { return; }
	
	var objFields = document.forms[FormName].elements[FieldName];
	
	if(!objFields) { return; }
	//if (objFields.disabled) { alert("enabling"); }
	objFields.disabled = !objFields.disabled;
}

function toggleDates(){
	hiddenElements = YAHOO.util.Dom.getElementsByClassName("customDate");
	if(document.search.dfield[1].checked){
	 for(j=0; j<hiddenElements.length; j++){
		 hiddenElements[j].style.display = 'block';
	 }
	} else {
	 for(j=0; j<hiddenElements.length; j++){
		 hiddenElements[j].style.display = 'none';
	 }
	}
	
	toggle_fields('search',    'start_date[Date_Day]');
	toggle_fields('search',  'start_date[Date_Month]');
	toggle_fields('search',   'start_date[Date_Year]');
	
	toggle_fields('search',    'start_time[Time_Hour]');
	toggle_fields('search',  'start_time[Time_Minute]');
	
	toggle_fields('search',      'end_date[Date_Day]');
	toggle_fields('search',    'end_date[Date_Month]');
	toggle_fields('search',     'end_date[Date_Year]');
	
	toggle_fields('search',    'end_time[Time_Hour]');
	toggle_fields('search',  'end_time[Time_Minute]');
}

</script>

{/literal}
<div class="panel">
<form name="search" target="pnl_middle" action="?module=mod_syslog&action=view_syslog_data&root_tpl=blank_panel&id=syslog_default" method="POST">
<table width="100%" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td>Time Range:</td>
		<td>
			<input type="radio" name="dfield" value="1" onClick="toggleDates();" label="All Dates" checked="true" /> All Dates
		    <input type="radio" name="dfield" value="2" label="Custom" onClick="toggleDates();" /> Custom
		</td>
	</tr>
	<tr>
		<td><span class="customDate">Start Date/Time:</span></td>
		<td>
			<span class="customDate">{html_select_date month_format="%b" all_extra="style=\"vertical-align:absmiddle\" disabled=\"true\"" start_year=-5 end_year="+0" field_array=start_date}<br />
			{html_select_time all_extra="style=\"vertical-align:absmiddle\" disabled=\"true\"" time="0:00" display_seconds=false field_array=start_time}</span>
		</td>
	</tr>
	<tr>
		<td><span class="customDate">End Date/Time:</span></td>
		<td>
			<span class="customDate">{html_select_date month_format="%b" all_extra="style=\"vertical-align:absmiddle\" disabled=\"true\"" start_year=-5 end_year="+0" field_array=end_date}<br />
			{html_select_time all_extra="style=\"vertical-align:absmiddle\" disabled=\"true\"" time="23:59" display_seconds=false field_array=end_time}</span>
		</td>
	</tr>
	
	<tr>
		<td>Keyword/RegEx:</td>
		<td>{input type="text" class="text" name="key"}</td>
	</tr>
	<tr>
		<td colspan="2">

		<table width="100%" cellpadding="3" cellspacing="0" border="0">
			<tr>
			<td valign="top">
			Facility:<br />
			{html_options options=$facilities name="facility[]" multiple="true" size="5"}
			
			</td>
			<td rowspan="2" valign="top">
			Host:<br />
{html_options options=$clients name="clients[]" multiple="true" size="12"}			</td>
			</tr>
			<tr>
			<td valign="top">Severity:<br />
{html_options options=$severities name="severity[]" multiple="true" size="5"} 
			</td>
			</tr>
		</table>

		
		</td>
	  </tr>
	<tr>
		<td colspan="2" align="center">Show {html_options name="limit" options="$limits"}  results</td>
	</tr>

	
	<tr>
		<td colspan="2" align="center">{input type="submit" class="button" value="Search Now"} <img id="progress" class="progress" src="/assets/progress.gif"></td>
	</tr>
</table>
</form>
</div>
<script language="Javascript">
	//toggleDates();
</script>
<!-- end of {$smarty.template} -->
