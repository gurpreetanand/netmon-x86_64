<!-- {$smarty.template} ($Id$) -->

<div class="titlebar">
	Manage Syslog Clients
</div>

<div class="panel">
	<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">
	{input name="Button" type="button" class="button" value="Add New Syslog Client" onClick="parent.pnl_right.showEditor('?module=mod_syslog&action=form_create_syslog_client');"}
	<img src="/assets/buttons/button_help.gif" alt="Show Help" width="24" height="24" class="icon" onClick="parent.pnl_right.showHelp('syslog_manager');" title="Show Help" />
</div>

<div class="datagrid center">

{if $clients}
<table align="center" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<th>Client</th>
		<th>Facility</th>
		<th>Min. Severity</th>
		<th>Actions</th>
	</tr>
{foreach from=$clients item="client"}
	<tr>
		<td>{$client.ip|resolve_ip} {if $client.ip|resolve_ip != $client.ip}({$client.ip}){/if}</td>
		<td>{$client.facility|resolve_facility}</td>
		<td>{$client.severity|resolve_severity}</td>
		<td>	<a href="javascript:parent.pnl_right.showEditor('?module=mod_syslog&action=form_edit_syslog_client&syslog_id={$client.syslog_id}');">Edit</a> |
			<a href="javascript:parent.pnl_right.showEditor('?module=mod_syslog&action=delete_syslog_client&syslog_id={$client.syslog_id}');">Del</a> |
			<a href="javascript:parent.pnl_right.showEditor('?module=mod_alerts&action=form_create_alert&alert_type=syslog_msg&client_id={$client.syslog_id}');">Alerts</a>
		</td>
	</tr>
{/foreach}
</table>
{/if}

</div>

<!-- end of {$smarty.template} -->
