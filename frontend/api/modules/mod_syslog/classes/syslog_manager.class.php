<?php


class Syslog_Manager extends MadnetElement {

	/**
	  * Database table associated with this subclass
	  *
	  * @var $table
	  * @access protected
	  */
	var $table = "syslog_access";
	/**
	  * Name of the primary key in the table
	  *
	  * @var string $pkey
	  * @access protected
	  */
	var $pkey = "syslog_id";/**
	  * Name of the module this MadnetElement subclass belongs to
	  *
	  * @var string $module
	  * @access protected
	  */
	var $module = "mod_syslog";
	/**
	  * Name of the class containing the business logic for this Element
	  *
	  * @var string $element
	  * @access protected
	  */
	var $element = __CLASS__;

	/**
	  * Meta-structure (see MadnetElement for more info)
	  *
	  * @var hashtable $meta
	  * @access private
	  */
	var $meta;

	function init() {
		$this->params->add_primitive("ip",          "ip_address",        TRUE,   "IP Address", "IP Address");
		$this->params->add_primitive("facility",    "integer",           TRUE,   "Facility",   "Facility");
		$this->params->add_primitive("severity",    "integer",           TRUE,   "Severity",   "Severity");
	}


	# Check for dups
	function pre_insert($id = null) {

		/**
		  * Determine whether we are allowed to consume this IP slot
		  */
		if (NULL != $id) {
			$old_ip = $this->getBit($id, "ip");
		} else {
			$old_ip = NULL;
		}

		$core = require_module("core");

		if (!$core->check_device_limit($this->params->primitives['ip']['value'], $old_ip)) {
			$this->err->err_from_string("Unable to create Syslog Server entry: You have exceeded the number of devices allowed in your license.");
			return FALSE;
		}
		/**
		  * at this point, we know we can consume an IP slot
		  */

		$ip = $this->db->escape($this->params->primitives['ip']['value']);


		$query = "SELECT {$this->pkey} FROM {$this->table} WHERE ip = $ip";

		$res = $this->db->get_row($query);

		if ((is_array($res)) && ($res[$this->pkey] <> intval($id))) {
			$this->err->err_from_string("A device with the IP address " . htmlentities($ip) . " is already in the syslog database");
			$success = FALSE;
		}

		return TRUE;
	}

	function pre_update($id) {
		/**
		  * Determine whether we are allowed to consume this IP slot
		  */
		if (NULL != $id) {
			$old_ip = $this->getBit($id, "ip");
		} else {
			$old_ip = NULL;
		}

		$core = require_module("core");

		if (!$core->check_device_limit($this->params->primitives['ip']['value'], $old_ip)) {
			$this->err->err_from_string("Unable to create Syslog Server entry: You have exceeded the number of devices allowed in your license.");
			return FALSE;
		}
		/**
		  * at this point, we know we can consume an IP slot
		  */

		return TRUE;

	}

	function pop($id) {
		$id = $this->db->escape($id);

		$query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";

		$result = $this->db->get_row($query);

		if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
			return FALSE;
		} else {
			foreach($result as $key => $value) {
				$this->params->setval($key, $value);
			}
			return TRUE;
		}
	}

	function pre_delete($id) {
		$query = "DELETE FROM syslog WHERE ip = (SELECT ip FROM syslog_access WHERE syslog_id = " . $this->db->escape($id) . ")";
		return $this->db->delete($query);
	}

  function get_entries_by_ip($ip, $offset = 0, $limit = 50) {
    $ip = $this->db->escape($ip);
    $offset = intval($offset);
    $limit = intval($limit);

    $query = "SELECT * FROM syslog WHERE ip={$ip} LIMIT {$limit} OFFSET {$offset}";
    $res = $this->db->select($query);

    if (DB_QUERY_ERROR == $res) {
      return NULL;
    } elseif (DB_NO_RESULT == $res) {
      return NULL;
    } else {
      return $res;
    }
  }

}
?>
