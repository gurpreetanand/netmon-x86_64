<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script>
function AdjustIframes(){	
	document.getElementById("dir_tree").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("titlebar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

<div class="titlebar" id="titlebar0">
	Folder Explorer
</div>

	
<!-- Sub-Window (Inline Frame) -->
<iframe src="?module=mod_vfs&action=get_dir_tree&root_tpl=blank_panel&class=white" id="dir_tree" width="100%" height="440px" name="dir_tree" frameborder="0"></iframe>
	
<script>
AdjustIframes();
parent.parent.toolbar.MM_nbGroup('down','group1','files','assets/toolbar/files_over.gif',1);
</script>

<!-- end of {$smarty.template} -->
