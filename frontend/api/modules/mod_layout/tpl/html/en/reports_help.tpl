<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script language="Javascript">

function showHelp(subject){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showEditor(dest){
	document.getElementById("iframe_report_editor").src = dest;
	activatePanel(document.getElementById("Netmon_Report_Editor"));
}
	
function AdjustIframes(){	
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("Netmon_Report_Editor").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}


<!-- Report Editor -->
		<div id="Netmon_Report_Editor" class="titlebar_collapsed" onclick="activatePanel(this)">
		 	Report Manager
		</div>
		<div class="panel, closed">
			<!-- Sub-Window (Inline Frame) -->
			<iframe src="?module=mod_layout&action=render_panel&root_tpl=blank_panel" id="iframe_report_editor" name="iframe_report_editor" width="100%" height="380px" frameborder="0"></iframe>
		</div>

{include file="help_panel.tpl"}

<script language="Javascript">
//setInitPanel();
activePanel = document.getElementById("Netmon_Help");
openPanel(activePanel);
AdjustIframes();
</script>

<!-- end of {$smarty.template} -->
