
<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
<!-- Help Panel -->
<div>
	<!-- Menu header box -->
	<div id="Netmon_Help" class="titlebar_collapsed" onclick="activatePanel(this)"> Help &amp; Resources </div>
	
	<div class="panel, closed">
	
		<!-- Button Row -->
		<div class="panel" id="toolbar0">
			<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">
			<img src="assets/buttons/button_resource_centre.gif" alt="Resource Centre" width="24" height="24" class="icon" onClick="document.getElementById('iframe_help').src='?module=mod_help&action=get_help&section=intro&root_tpl=blank_panel'" title="Netmon Help & Resource Center" />
			<img src="assets/buttons/button_toc.gif" alt="Table of Contents" width="24" height="24" class="icon" onClick="document.getElementById('iframe_help').src='?module=mod_help&action=get_help_toc&section=toc&root_tpl=blank_panel'" title="Online User Guide" />
			<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">
			<img src="assets/buttons/button_back.gif" alt="Go to previous page" width="24" height="24" class="icon" onClick="iframe_help.history.go(-1)" title="Go to previous page" />
			<img src="assets/buttons/button_forward.gif" alt="Go to next page" width="24" height="24" class="icon" onClick="iframe_help.history.go(+1)" title="Go to next page" />
			<img src="assets/buttons/button_print.gif" alt="Print this page" width="24" height="24" class="icon" onclick="iframe_help.focus(); iframe_help.print();" title="Print this page" />
			<img class="progress" id="progress" src="/assets/progress.gif" />
		</div>
		<!-- Sub-Window (Inline Frame) -->

		<iframe src="?module=mod_help&action=get_help&section=intro&root_tpl=blank_panel" id="iframe_help" name="iframe_help" width="100%" height="410" frameborder="0"></iframe>
	
	</div>
</div>

<!-- end of {$smarty.template} ($Id$) -->
