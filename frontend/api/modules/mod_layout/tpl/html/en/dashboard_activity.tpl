<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.utilities,yui.dom"}
{literal}
<script>

function showHelp(subject){
	document.getElementById("iframe_help").src = "?module=mod_help&action=get_help&section=" + subject;
	activatePanel(document.getElementById("Netmon_Help"));
}

function showlastRefreshed(objid){
	myDate = new Date();
	document.getElementById(objid).innerHTML = myDate.toString();
}

openingDate = new Date();
openingDate = openingDate.toString();

function disableButtons(){
	document.getElementById('checkall').disabled = true;
	document.getElementById('uncheckall').disabled = true;
	document.getElementById('clear').disabled = true;
	document.getElementById('goback').style.display = "inline";
}

function enableButtons(){
	document.getElementById('checkall').disabled = false;
	document.getElementById('uncheckall').disabled = false;
	document.getElementById('clear').disabled = false;
}

function AdjustIframes(){	
	document.getElementById("iframe_help").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("New_Hosts").offsetHeight - document.getElementById("Top_Activity_Snapshot").offsetHeight - document.getElementById("Top_Web_Destinations").offsetHeight - document.getElementById("Top_Web_Users").offsetHeight - document.getElementById("Top_Ethernet").offsetHeight - document.getElementById("Netmon_Help").offsetHeight - document.getElementById("toolbar0").offsetHeight - 6;
}
window.onresize = AdjustIframes;


</script>
{/literal}

<!-- Top Activity Snapsnot Panel -->
<div>

	<!-- Menu header box -->
	<div id="New_Hosts" class="titlebar_collapsed" onclick="iframe_new_hosts.location='?module=mod_dashboard&action=get_discovered_hosts&root_tpl=blank_panel'; showlastRefreshed('ts_new_hosts'); activatePanel(this)">
		Recently Discovered Hosts
	</div>
	<div class="panel, closed">
		<div class="panel">
			<span id="goback"><img src="assets/buttons/button_back.gif" width="24" height="24" class="icon" onClick="document.getElementById('iframe_new_hosts').scrolling='yes';iframe_new_hosts.location.href='?module=mod_dashboard&action=get_discovered_hosts&root_tpl=blank_panel'; showlastRefreshed('ts_new_hosts'); enableButtons();" style="display: none" title="Back to List"></span>
			<a href="?module=mod_alerts&action=form_create_alert&alert_type=new_host" target="iframe_new_hosts" onclick="disableButtons();document.getElementById('iframe_new_hosts').scrolling='yes';"><img src="assets/buttons/alerts.gif" width="24" height="24" class="icon" border="0" title="Configure Alerts" alt="Configure Alerts"></a>
			{input type="button" name="checkall" id="checkall" class="button" value="Check All" onClick="iframe_new_hosts.SetAllCheckBoxes('hosts_form', 'host_ids[]', true)"}
			{input type="button" name="uncheckall" id="uncheckall" class="button" value="Uncheck All" onClick="iframe_new_hosts.SetAllCheckBoxes('hosts_form', 'host_ids[]', false)"}
			{input type="button" name="clear" id="clear" class="button" value="Clear Selected" onClick="iframe_new_hosts.document.forms['hosts_form'].submit()"}
			<!--<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_new_hosts.focus();iframe_new_hosts.print();" title="Print this page"> -->
			<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('network_autodiscovery');" title="Show Help" alt="Show Help">
		</div>

		<!-- Sub-Window (Inline Frame) -->
		<iframe scrolling="no" src="?module=mod_dashboard&action=get_discovered_hosts&root_tpl=blank_panel" id="iframe_new_hosts" name="iframe_new_hosts" width="100%" height="200" frameborder="0"></iframe>
		<div class="panel">
			Last Refreshed: <span id="ts_new_hosts"><script>document.write(openingDate)</script></span>
		</div>
	</div>


	<!-- Menu header box -->
	<div id="Top_Activity_Snapshot" class="titlebar_collapsed" onclick="iframe_top_conversations.location.reload(); showlastRefreshed('ts_top_conversations'); activatePanel(this)">
		Top Activity Snapshot
	</div>
	<div class="panel, closed">
		<div class="panel">
			<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="iframe_top_conversations.location.reload(); showlastRefreshed('ts_top_conversations');" title="Refresh">
			<!--<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_top_conversations.focus();iframe_top_conversations.print();" title="Print this page"> -->
			<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('panel_top_activity');" title="Show Help">
		</div>
		
		<!-- Sub-Window (Inline Frame) -->
		<iframe scrolling="no" src="?module=mod_dashboard&action=get_top_conversations&root_tpl=blank_panel" id="iframe_top_conversations" name="iframe_top_conversations" width="100%" height="200" frameborder="0"></iframe>
		<div class="panel">
			Last Refreshed: <span id="ts_top_conversations"><script>document.write(openingDate)</script></span>
		</div>
	</div>
</div>

<!-- Top Web Destinations -->
<div>
	<!-- Menu header box -->
	<div id="Top_Web_Destinations" class="titlebar_collapsed" onclick="iframe_top_webdest.location.reload(); showlastRefreshed('ts_top_webdest'); activatePanel(this)">
		Top Web Destinations
	</div>
	<div class="panel, closed">
		<div class="panel">
			<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="iframe_top_webdest.location.reload(); showlastRefreshed('ts_top_webdest');" title="Refresh">
			<!--<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_top_webdest.focus();iframe_top_webdest.print();" title="Print this page"> -->
			<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('panel_top_webdest');" title="Show Help">
		</div>
	
		<!-- Sub-Window (Inline Frame) -->
		<iframe src="?module=mod_dashboard&action=get_top_web_destinations&root_tpl=blank_panel" id="iframe_top_webdest" name="iframe_top_webdest" width="100%" height="200" frameborder="0" scrolling="no"></iframe>

		<div class="panel">
			Last Refreshed: <span id="ts_top_webdest"><script>document.write(openingDate)</script></span>
		</div>

		</div>
	</div>
</div>

<!-- Top Web Users -->
<div>
	<!-- Menu header box -->
	<div id="Top_Web_Users" class="titlebar_collapsed" onclick="iframe_top_webusers.location.reload(); showlastRefreshed('ts_top_webusers'); activatePanel(this)">
		Top Web Users
	</div>
	<div class="panel, closed">
		<div class="panel">
			<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="iframe_top_webusers.location.reload(); showlastRefreshed('ts_top_webusers');" title="Refresh">
			<!--<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_top_webusers.focus();iframe_top_webusers.print();" title="Print this page"> -->
			<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('panel_top_webusers');" title="Show Help">
		</div>
	
		<!-- Sub-Window (Inline Frame) -->
		<iframe scrolling="no" src="?module=mod_dashboard&action=get_top_web_users&root_tpl=blank_panel" id="iframe_top_webusers" name="iframe_top_webusers" width="100%" height="200" frameborder="0"></iframe>

		<div class="panel">
			Last Refreshed: <span id="ts_top_webusers"><script>document.write(openingDate)</script></span>
		</div>

		</div>
	</div>
</div>


<!-- Top Ethernet Protocols Snapsnot Panel -->
<div>
	<!-- Menu header box -->
	<div id="Top_Ethernet" class="titlebar_collapsed" onclick="iframe_ethernet.location.reload(); showlastRefreshed('ts_top_ethernet'); activatePanel(this)">
		Top Ethernet Protocols
	</div>
	<div class="panel, closed">
		<div class="panel">
			<img src="assets/buttons/button_refresh.gif" width="24" height="24" class="icon" onClick="iframe_ethernet.location.reload(); showlastRefreshed('ts_top_ethernet');" title="Refresh">
			<img src="assets/buttons/button_back.gif" alt="Go to previous page" width="24" height="24" class="icon" onClick="iframe_ethernet.history.go(-1)" title="Go to previous page" /> 
			<img src="assets/buttons/button_forward.gif" alt="Go to next page" width="24" height="24" class="icon" onClick="iframe_ethernet.history.go(+1)" title="Go to next page" />
			<!--<img src="assets/buttons/button_print.gif" class="icon" onClick="iframe_ethernet.focus();iframe_ethernet.print();" title="Print this page"> -->
			<img src="assets/buttons/button_help.gif" width="24" height="24" class="icon" onClick="showHelp('panel_top_ethernet');" title="Show Help">
		</div>

		<!-- Sub-Window (Inline Frame) -->
		<iframe scrolling="no" src="?module=mod_dashboard&action=get_top_ethernet_protocols&root_tpl=blank_panel" id="iframe_ethernet" name="iframe_ethernet" width="100%" height="200" frameborder="0"></iframe>
		<div class="panel">
			Last Refreshed: <span id="ts_top_ethernet"><script>document.write(openingDate)</script></span>
		</div>
	</div>
</div>



{include file="help_panel.tpl"}

<script language="Javascript">
setInitPanel();
AdjustIframes();
</script>


<!-- end of {$smarty.template} -->
