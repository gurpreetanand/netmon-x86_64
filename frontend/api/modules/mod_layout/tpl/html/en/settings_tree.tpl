<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom"}
{literal}
<script>
function AdjustIframes(){	
	document.getElementById("tree_settings").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("titlebar0").offsetHeight - 4;
}
window.onresize = AdjustIframes;
</script>
{/literal}

<!-- Menu header box -->
<div class="titlebar" id="titlebar0">
	Settings Explorer
</div>

<!-- Sub-Window (Inline Frame) -->
<iframe src="?module=mod_settings&action=get_settings_tree&root_tpl=blank_panel&class=white" id="tree_settings" width="100%" height="440px" name="tree_settings" frameborder="0"></iframe>

<script>
AdjustIframes();
parent.parent.toolbar.MM_nbGroup('down','group1','settings','assets/toolbar/settings_over.gif',1);
</script>

<!-- end of {$smarty.template} -->
