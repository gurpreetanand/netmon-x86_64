<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.dom,yui.element"}
<div class="titlebar" id="servicesTitleBar">
	Trackers Explorer
</div>

<!-- Menu header box -->
<div class="panel" id="toolbar1">

<img src="/assets/core/separator_double.gif" width="10" height="21" class="icon">

<img class="icon" src="/assets/buttons/button_resource_centre.gif" width="24" height="24" border="0" onClick="iframe_main.location.href='?module=mod_services&action=get_system_status&tpl=blank_tpl&root_tpl=blank_panel'" alt="Trackers Home Page" title="Trackers Home Page" />

	{input type="button" class="button" onClick="iframe_main.location.href='?module=mod_services&action=dump_all_services&tpl=blank_tpl&root_tpl=blank_panel'" value="ALL Trackers"} 

	{input type="button" class="button" onClick="iframe_main.location.href='?module=mod_services&type=icmp&action=get_services&tpl=blank_tpl&root_tpl=blank_panel'" value="PING Trackers"} 
	
	{input type="button" class="button" onClick="iframe_main.location.href='?module=mod_services&type=tcp&action=get_services&tpl=blank_tpl&root_tpl=blank_panel'" value="TCP Service Trackers"} 

	{input type="button" class="button" onClick="iframe_main.location.href='?module=mod_network&action=get_disks&root_tpl=blank_panel'" value="Disk Trackers"} 
	
	{input type="button" class="button" onClick="iframe_main.location.href='?module=mod_services&action=manage_urls&root_tpl=blank_panel'" value="URL Trackers"}

	{*input type="button" class="button" onClick="iframe_main.location.href='?module=mod_services&action=manage_oids&root_tpl=blank_panel'" value="SNMP Object (OID) Trackers"*}

</div>

<!-- Sub-Window (Inline Frame) -->
<iframe src="?module=mod_services&action=get_system_status&tpl=blank_tpl&root_tpl=blank_panel" id="iframe_main" name="iframe_main" width="100%" height="600px" frameborder="0"></iframe>
<script language="Javascript">
	document.getElementById("iframe_main").height = YAHOO.util.Dom.getClientHeight() - document.getElementById("servicesTitleBar").offsetHeight - document.getElementById("toolbar1").offsetHeight - 6; 
	//document.getElementById("home").style.className = 'button_over';
</script>

<!-- end of {$smarty.template} -->
