<?php

/**
  * @author     Xavier Spriet <xavier@netmon.ca>
  * @version    4.5-r2
  * @access     public
  * @link       http://www.netmon.ca
  * @copyright  Copyright (c) 2007, Netmon Inc. (netmon.ca)
  */

/**
  * Alert handler to command association manager
  *
  * This class handles the process of managing the relationship
  * between alert handlers and their associated commands.
  *
  * @package MADNET
  * @author Xavier Spriet
  */
class sub_handler_command extends MadnetSubElement {
  
  /**
    * META structure of the MadnetElement object we are bound to
    *
    * @var $element
    * @access private
    */
  var $element;
  
  /**
    * Class constructor method. Instanciates all class attributes.
    *
    * @return sub_account_group
    * @access public
    */
  public function __construct() {
    parent::MadnetSubElement();
    
    $this->table = "alert_handler2command";
    $this->fkey = "command_id";
    $this->pkey = "handler_id";
    $this->mandatory = FALSE;
    $this->doc = "Commands associated with this alert";
    $this->friendly_name = "Custom Commands";
    
    $this->debugger->add_hit(__CLASS__ . " instanciated");
  }
  
  /**
    * Validates the data found in the POST stack
    *
    * Creates a structure based on the POST stack filled with all group IDs.
    * It then ensures that none of the values in the structure are null or
    * non-integer, and return TRUE if everything is valid, FALSE otherwise.
    *
    * This provides a fine-grained control over SQL injection
    *
    * @return boolean
    */
  function validate() {

    if ((strcmp('', $GLOBALS['json_post'][__CLASS__]) == 0) && ($this->mandatory == FALSE)) {
      return TRUE;
    }
  
    $ids = explode(",", $GLOBALS['json_post'][__CLASS__]);
    
    if ((!is_array($ids)) && ($this->mandatory == TRUE)) {
      $this->err->err_from_string("The alert relationship is a mandatory field");
      return FALSE;
    }
    
    // Since we only accept numeric entries, this will detect invalid ones.
    for($i = 0; $i < sizeof($ids); $i++) {
      $ids[$i] = (intval($ids[$i]) > 0);
    }
    
    
    $this->debugger->add_hit("Sub IDs: ", NULL, NULL, vdump($ids));
    $success = ((intval($this->element['pkey_value']) > 0) && (!in_array(FALSE, $ids)));
    $this->debugger->add_hit("Validate returns " . ($success ? "true" : "false"));
    return $success;
    
  }
  
  /**
    * Inserts the relationship in the database
    *
    * Builds a hash-table based on the user ID that was just created
    * (since the MadnetElement will update $this->element['pkey_value'] with that data)
    * and autoinserts it in the DB.
    *
    * @return boolean
    */
    
  function insert() {
    
    $success = TRUE;
    
    if ((strcmp('', $GLOBALS['json_post'][__CLASS__]) == 0) && ($this->mandatory == FALSE)) {
      return TRUE;
    }
    
        
    $handler_id = intval($this->element['pkey_value']);
      
    $data = explode(",", $GLOBALS['json_post'][__CLASS__]);
    
    if ($handler_id == 0) {
      $this->debugger->add_hit("Alert Handler ID has not been passed by reference", NULL, NULL, vdump($this->element));
      $this->err->err_from_string("Unable to register handler commands");
      return FALSE;
    }
    
    
    # Go through all our parameters (arrays of handler => command) and INSERT the rels in the DB
    foreach($data as $id) {
      $auto = array("handler_id" => $handler_id,
                "command_id" => $id);
      if (DB_QUERY_ERROR == $this->db->auto_insert($this->table, $auto)) {
        $success = FALSE;
      }  
    }
    
    
    
    
    # The $handler_id should be the same accross all elements so the var is already set correctly
    if (!$success) {
      # Undo
      $this->db->delete("DELETE FROM " . $this->table . " WHERE handler_id = " . $this->db->escape($handler_id));
      return FALSE;
    }
    
    $this->debugger->add_hit(__FUNCTION__ . " returns " . vdump($success));
    
    return $success;
  }
  
  
  /**
    * Deletes all command relationships with the specified handler and re-creates them
    *
    * It is MUCH simpler to start from scratch and call $this->insert than to do a diff of
    * a resultset with POST data.
    *
    * @return boolean
    */
  function update() {
    $query = "DELETE FROM {$this->table} WHERE {$this->pkey} = {$this->element['pkey_value']}";
    
    $result = $this->db->delete($query);
    $this->debugger->add_hit("Result of $query", NULL, NULL, vdump($result));
    
    if (DB_QUERY_ERROR == $result) {
      $this->err->err_from_code(DB_QUERY_ERROR, "Unable to delete old records", $result->getMessage());
      return FALSE;
    }
    
    return $this->insert();
    
  }
  
  /**
    * Returns an array containing all the handler to command rels for that user.
    *
    * @param integer $fkey_id
    * @return mixed
    */
  function getVal($pkey_id) {
    $query = "SELECT {$this->fkey} FROM {$this->table} WHERE {$this->pkey} = " . $this->db->escape($pkey_id);
    
    $result = $this->db->select($query);
    
    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    }
    
    $tmp = array();
    
    
    
    foreach($result as $current) {
      array_push($tmp, $current[$this->fkey]);
    }
    return $tmp;
    

    
  }
  
  /**
    * Deletes all relationships pertaining to a user account
    *
    * @return boolean
    */
  function delete() {
    $query = "DELETE FROM {$this->table} WHERE {$this->pkey} = {$this->element['pkey_value']}";
    
    $res = $this->db->delete($query);
    
    
    if (DB_QUERY_ERROR == $res) {
      $this->err->err_from_string("Unable to remove command relationships.");
      return FALSE;
    }
    
    return TRUE;
    
  }
  
  /**
    * Returns smarty array of all relationship names
    *
    * @return array
    */
    /*
  function get_groups() {
    
    $query = "SELECT id, label FROM commands ORDER BY label ASC";
    $res = $this->db->select_smarty($query, "id", "group_name");
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array();
    } else {
      return $res;
    }
  }
  */
  
  
  
  
}

?>
