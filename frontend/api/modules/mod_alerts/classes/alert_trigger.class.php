<?php


class Alert_Trigger extends MadnetElement {

  /**
    * Database table associated with this subclass
    *
    * @var $table
    * @access protected
    */
  var $table = "alert_triggers";
  /**
    * Name of the primary key in the table
    *
    * @var string $pkey
    * @access protected
    */
  var $pkey = "trigger_id";
  /**
    * Name of the module this MadnetElement subclass belongs to
    *
    * @var string $module
    * @access protected
    */
  var $module = "mod_alerts";
  /**
    * Name of the class containing the business logic for this Element
    *
    * @var string $element
    * @access protected
    */
  var $element = __CLASS__;

  /**
    * Meta-structure (see MadnetElement for more info)
    *
    * @var hashtable $meta
    * @access private
    */
  var $meta;

  function init() {
    $this->params->add_primitive("trigger_threshold",    "integer", TRUE,  "Threshold", "Threshold");
    $this->params->add_primitive("trigger_timeout",      "integer", TRUE,  "Timeout",   "Timeout");
    $this->params->add_primitive("reference_table_name", "string",  TRUE,  "Item Type", "Item Type");
    $this->params->add_primitive("reference_pkey_val",   "integer", TRUE,  "Item ID",   "Item ID");
    $this->params->add_primitive("pattern",              "string",  FALSE, "Pattern",   "Pattern");
    $this->params->add_primitive("label",                "string",  FALSE, "Label",     "Label");
    $this->params->add_primitive("comp_exp",             "string",  FALSE, "Comparison Operator");
  }



  /**
    * Returns an array containing the user ID of every user account in the DB
    *
    * @return mixed
    */
  function get_all_ids() {
    $query = "SELECT {$this->pkey} FROM {$this->table}";
    $result = $this->db->select($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      return $result;
    }
  }

  function pre_insert($id = NULL) {
    # We do allow duplicate alerts so not a lot of checking required here.
    return TRUE;
  }

  function pre_update($id) {
    return $this->pre_insert($id);
  }

  function post_insert() {
    $GLOBALS['json_post']['trigger_id'] = $this->meta['pkey_value'];
    return TRUE;
  }

  function post_update($id) {
    return $this->post_insert();
  }



  function pop($id) {
    $id = $this->db->escape($id);

    $query = "SELECT * FROM {$this->table} WHERE {$this->pkey} = $id";

    $result = $this->db->get_row($query);

    if ((DB_QUERY_ERROR == $result) || (DB_NO_RESULT == $result)) {
      return FALSE;
    } else {
      foreach($result as $key => $value) {
        $this->params->setval($key, $value);
      }
      return TRUE;
    }
  }

  function pre_delete($id) {
    $query = "DELETE FROM alert_handlers WHERE trigger_id = " . $this->db->escape($id);
    return $this->db->delete($query);
  }
  /*
  function delete($id) {
    
    
    
    $query = "DELETE FROM alert_triggers WHERE trigger_id = " . $this->db->escape($id);
    if (!$this->db->delete($query)) {
      return FALSE;
    }
    
    return TRUE;
  }
  */

}
?>