<?php

/**
 * @author     Xavier Spriet <xavier@netmon.ca>
 * @version    3.5a
 * @access     public
 * @link       http://www.netmon.ca
 * @copyright  Copyright (c) 2005, Netmon Inc. (netmon.ca)
 */

// mod_alerts.class.php


/**
 * Alerts MCO
 *
 * This module is used to perform any application-level change and management
 *
 *
 * @package MADNET
 * @author Xavier Spriet
 */
class mod_alerts extends MadnetModule {
  
  /**
   * Data structure used for caching of NS lookups
   */
  var $cache = array () ;
  
  var $alert_types = array ( "web_alert_down" , "df_over_threshold" , "smb_disk_over_threshold" , "srv_service_down" , "snmp_iface_above_threshold" , "syslog_msg" , "new_host" , "open_ports" , "port_protocol_traffic" , "snmp_trap" , "snmp_oid_match" ) ;
  
  /**
   * Main handler for the alert creation form.
   *
   * @param pointer $index_content
   */
  function form_create_alert () {
    $type = strtolower ( $_GET [ 'alert_type' ] ) ;
    
    if (! in_array ( $type, $this->alert_types )) {
      $this->err->err_from_code(400, "Invalid Error Type");
    }
    
    $payload = array();
    
    # The composite alert manager is an alert trigger and
    # an alert handler object merged into one for the purpose
    # of validation and form rendering.
    require_class ( $this->module, "composite_alert_manager" ) ;
    $cam = & new Composite_Alert_Manager ( ) ;
    
    $params = $this->_get_alert_params ( $type );

    $payload['cam'] = $cam->params;

    # Dynamic based on the type of alert because we don't want the user
    # to be able to forge indirect SQL queries from the alert daemon.
    $payload['alert_params'] = $params;
    
    # Fill out all the drop-downs and assign all variables
    $payload["medias"] = $this->_get_alert_medias();
    $payload["type"] =  $type;
    $payload["users"] = $this->_get_alert_users();
    $payload["threshold_unit"] = $this->_get_threshold_unit ( $type );
    $payload["commands"] = $this->_get_alert_commands ( $type );
    
    # This one is only used for services but the performance hit is minimal
    $payload["conditionals"] = $this->_get_alert_conditionals ();
    
    # Register the comparison expressions in the template
    $payload["comp_exps"] = $this->_get_comparative_expressions ();
    
    # List all similar alerts
    $payload["alerts"] = $this->_get_other_alerts ( $params );
    $payload["snmp_directions"] = $this->_get_snmp_directions ();
    # Grab latencies for the specified service
    if ($type == "srv_service_down") {
      $srv_id = $_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'srv_id' ] ;
      $payload["latencies"] = $this->_get_alert_latencies ( $srv_id );
    }
  
    print json_encode($payload, true);

  }

  
  function form_create_alert_orig ( &$index_content ) {
    $type = strtolower ( $_GET [ 'alert_type' ] ) ;
    
    if (! in_array ( $type, $this->alert_types )) {
      $this->err->err_from_string ( "Invalid alert type: " . htmlentities ( $type ) ) ;
    }
    
    # The composite alert manager is an alert trigger and
    # an alert handler object merged into one for the purpose
    # of validation and form rendering.
    require_class ( $this->module, "composite_alert_manager" ) ;
    $cam = & new Composite_Alert_Manager ( ) ;
    
    $parser = & new Parser ( $this->tpl_dir ) ;
    $parser->set_params ( $cam->params ) ;
    
    
    # Dynamic based on the type of alert because we don't want the user
    # to be able to forge indirect SQL queries from the alert daemon.
    $params = $this->_get_alert_params ( $type ) ;
    $parser->assign ( "alert_params", $params ) ;
    
    # Fill out all the drop-downs and assign all variables
    $parser->assign ( "medias", $this->_get_alert_medias () ) ;
    $parser->assign ( "type", $type ) ;
    $parser->assign ( "users", $this->_get_alert_users () ) ;
    $parser->assign ( "threshold_unit", $this->_get_threshold_unit ( $type ) ) ;
    $parser->assign ( "commands", $this->_get_alert_commands ( $type ) ) ;
    
    if ($type == "syslog_msg") {
      $mod = & require_module ( "mod_syslog" ) ;
      $parser->assign ( "severities", $mod->severities ) ;
      require_class("mod_syslog", "Syslog_Manager");
      $sm = new Syslog_Manager();
      $sm->fetch ( $params['reference_pkey_val'] ) ;
      $parser->assign_by_ref("sm", $sm);
    } elseif ($type == "snmp_oid_match") {
      require_class ( "mod_devices", "oid_watcher_manager" ) ;
      $owm = & new OID_Watcher_Manager ( ) ;
      $owm->fetch ( $params['reference_pkey_val'] ) ;
      $parser->assign_by_ref ( "owm", $owm ) ;
    }
    
    # This one is only used for services but the performance hit is minimal
    $parser->assign ( "conditionals", $this->_get_alert_conditionals () ) ;
    
    # Register the comparison expressions in the template
    $parser->assign ( "comp_exps", $this->_get_comparative_expressions () ) ;
    
    
    
    # List all similar alerts
    $parser->assign ( "alerts", $this->_get_other_alerts ( $params ) ) ;
    $parser->assign ( "snmp_directions", $this->_get_snmp_directions () ) ;
    # Grab latencies for the specified service
    if ($type == "srv_service_down") {
      $srv_id = $_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'srv_id' ] ;
      $parser->assign ( "latencies", $this->_get_alert_latencies ( $srv_id ) ) ;
    }
    
    # Dynamically insert extension depending on the type of alert
    if (is_readable ( $this->tpl_dir . "/form_component_" . $type . ".tpl" )) {
      $parser->assign ( "component", $type ) ;
    }
    
    $index_content .= $parser->fetch ( "form_create_alert.tpl" ) ;
    return ;
  }

  
  
  /**
   * Returns a list of all commands associated with the specified type of alerts
   * 
   * @return array list of matching command entries
   */
  function _get_alert_commands ( $alert_type ) {
    $query = "SELECT label, command, id FROM alert_commands " . "WHERE alert_type_id = " . "(SELECT id FROM alert_types WHERE name = upper(" . $this->db->escape ( $alert_type ) . "))" . " ORDER BY label ASC" ;
    $res = $this->db->select ( $query ) ;
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULT == $res)) {
      return array () ;
    }
    
    return $res ;
  }
  
  /**
   * Returns a list of all usable medias for dispatching alerts
   *
   * @return array
   */
  function _get_alert_medias () {
    $query = "SELECT id, name FROM alert_medias ORDER BY name ASC" ;
    $res = $this->db->select_smarty ( $query, "id", "name" ) ;
    
    if (is_array ( $res )) {
      foreach ( $res as $key => $val ) {
        $res [ $key ] = ucwords ( $val ) ;
      }
    }
    
    return $res ? $res : array () ;
  }
  
  /**
   * Returns a list of all users
   *
   * @return array
   */
  function _get_alert_users () {
    $query = "SELECT id, (COALESCE(last_name, '') || ', '::varchar || COALESCE(first_name, '')) AS \"name\" FROM users ORDER BY name ASC" ;
    $res = $this->db->select_smarty ( $query, "id", "name" ) ;
    return $res ? $res : array () ;
  }
  
  /**
   * This generates a list of fields that the templating engine will forward from GET to POST
   *
   * @param string $type
   * @return array
   */
  function _get_alert_params ( $type ) {
    switch ( $type) {
      
      case "web_alert_down" :
        return array ( "reference_table_name" => "urls" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'url_id' ]) ) ;
      
      case "df_over_threshold" :
        return array ( "reference_table_name" => "df_servers" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'disk_id' ]) ) ;
      
      case "smb_disk_over_threshold" :
        return array ( "reference_table_name" => "smb_servers" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'disk_id' ]) ) ;
      
      case "srv_service_down" :
        return array ( "reference_table_name" => "servers" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'srv_id' ]) ) ;
      
      case "open_ports" :
        return array ( "reference_table_name" => "scan_log" , "reference_pkey_val" => "-1" ) ;
      
      case "syslog_msg" :
        return array ( "reference_table_name" => "syslog_access" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'client_id' ]) ) ;
      
      case "snmp_iface_above_threshold" :
        return array ( "reference_table_name" => "interfaces" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'interface_id' ]) ) ;
      
      case "new_host" :
        return array ( "reference_table_name" => "arptable" , "reference_pkey_val" => '-1' ) ;
      
      case "port_protocol_traffic" :
        return array ( "reference_table_name" => "protocols" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'protocol_id' ]) ) ;
      
      case "snmp_trap" :
        return array ( "reference_table_name" => "snmpoids" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'trap_id' ]) ) ;
      
      case "snmp_oid_match" :
        return array ( "reference_table_name" => "oids" , "reference_pkey_val" => ($_GET [ 'reference_pkey_val' ] ? $_GET [ 'reference_pkey_val' ] : $_GET [ 'oid_id' ]) ) ;
    }
  }
  
  /**
   * Generate a list of latencies that could be used as threshold for
   * a specified service.
   *
   * @param integer $server_id
   * @return array
   */
  function _get_alert_latencies ( $server_id ) {
    $query = "SELECT timeout FROM servers WHERE srv_id = " . $this->db->escape ( $server_id ) ;
    $res = $this->db->get_row ( $query ) ;
    
    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return ;
    }
    
    $return = array () ;
    
    require_class ( "mod_reports", "report_manager" ) ;
    #require_class("mod_reports", "uptime_report_manager");
    #$rep = new Uptime_Report_Manager();
    

    # We write it here first so it is the first element, then we overwrite it
    # because the for loop will overwrite it to whatever seconds value it comes
    # up to.
    $return [ intval ( $res [ 'timeout' ] ) * 60 * 1000 ] = "Service Down" ;
    $return [ 10 ] = "10ms" ;
    for ( $i = 60 ; $i <= 1500 ; $i += 60 ) {
      #$arr = array($i);
      $return [ $i ] = $i . " ms" ;
    }
    
    $return [ intval ( $res [ 'timeout' ] ) * 60 * 1000 ] = "Service Down" ;
    return $return ;
  
  }
  
  /**
   * returns a list of available conditionals from the DB
   *
   * @return array
   */
  function _get_alert_conditionals () {
    $query = "SELECT cond_id, name FROM conditionals ORDER BY name ASC" ;
    $res = $this->db->select_smarty ( $query, "cond_id", "name" ) ;
    return $res ? $res : array () ;
  }
  
  /** 
   * Registers a new alert trigger and matching alert handler in the database
   *
   * @param pointer $index_content
   * @return mixed
   */
  function process_create_alert(&$index_content ) {
    require_class ( $this->module, "alert_trigger" ) ;
    require_class ( $this->module, "alert_handler" ) ;
    
    $at = & new Alert_Trigger ( ) ;
    $ah = & new Alert_Handler ( ) ;
    
    if ($at->insert ()) {
      if ($ah->insert ()) {
        print '{}';
      } else {
        $this->err->err_from_code(400, "Unable to create alert handler");
      }
      $at->delete ( $_POST [ 'trigger_id' ] ) ;
    } else { 
      $this->err->err_from_code(400, "Unable to create alert trigger");
    }
  }
  
  function process_update_alert() {
    
    $id = intval ( $_GET [ 'trigger_id' ] ) ;
    if ($id <= 0) {
      $index_content .= "Invalid alert identifier. Please refresh the alerts form and try again." ;
      return;
    }
    
    // Retrieve the alert handler ID:
    $query = "SELECT id FROM alert_handlers WHERE trigger_id = " . $this->db->escape($id);
    $res = $this->db->get_row($query);
    
    if ((DB_QUERY_ERROR == $res) || (DB_NO_RESULTS == $res)) {
      $this->err->err_from_code(400, "Unable to fetch the associated handler ID.");
    }
    $handler_id = $res['id'];
    
    require_class ( $this->module, "alert_trigger" ) ;
    require_class ( $this->module, "alert_handler" ) ;
    
    $at = & new Alert_Trigger ( ) ;
    $ah = & new Alert_Handler ( ) ;
    
    //$at->fetch($id);
    
    if ($at->update($id)) {
      if ($ah->update ($handler_id)) {
        print '{}';
        return;
      }
    }
    $this->err->err_from_code(400, "Unable to update alert" ) ;
  }
  
  /**
   * Returns a list of alerts bound to the same trigger criteria.
   *
   * @param array $params
   * @return mixed
   */
  function _get_other_alerts ( $params ) {
    $query = "SELECT (u.last_name || ', '::varchar || u.first_name) AS \"name\",
          u.id AS \"user_id\", m.id AS \"media_id\", t.pattern AS \"pattern\",
          h.id, m.name AS \"media\", t.trigger_threshold AS \"threshold\", t.label,
          t.trigger_id, h.conditional_id, t.comp_exp
          FROM users u, alert_handlers h, alert_medias m, alert_triggers t
          WHERE t.trigger_id = h.trigger_id
          AND m.id = h.media_id
          AND u.id = h.user_id
          AND reference_table_name = " . $this->db->escape ( $params [ 'reference_table_name' ] ) . "
          AND reference_pkey_val = " . $this->db->escape ( $params [ 'reference_pkey_val' ] ) ;
    $res = $this->db->select ( $query ) ;
    
    if ((DB_NO_RESULT == $res) || (DB_QUERY_ERROR == $res)) {
      return ;
    }
    
    return $res ;
  }
  
  /**
   * Returns the metric used to represent thresholds
   * depending on the specific type of alert one is creating
   *
   * @param string $type
   * @return mixed
   */
  function _get_threshold_unit ( $type ) {
    switch ( $type) {
      case "smb_disk_over_threshold" :
      case "df_disk_over_threshold"  :
        return "%" ;
      case "srv_service_down" :
        return " ms" ;
      default :
        return NULL ;
    }
  }
  
  
  /**
   * Get the direction of the bandwidth an alert could be triggered for.
   *
   * @return array
   */
  function _get_snmp_directions () {
    return array ( "IN" => "IN" , "OUT" => "OUT" , "ANY" => "ANY" ) ;
  }
  
  /**
   * Returns a list of comparison expressions that the alert system
   * can use to compare to the specified threshold to determine whether
   * or not to trigger the alert.
   *
   * @return array
   */
  function _get_comparative_expressions () {
    return array ( ">" => "greater than" , ">=" => "greater than or equal to" , "==" => "equal to" , "!=" => "not equal to" , "<=" => "lower than or equal to" , "<" => "lower than" ) ;
  }
  
  /**
   * Removes an alert trigger and its associated
   * allert handler from the database
   *
   * @param pointer $index_content
   */
  function delete_alert() {
    $id = intval ( $_GET [ 'handler_id' ] ) ;
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid alert identifier. Please refresh the alerts form and try again." ) ;
      return ;
    }
    
    require_class ( $this->module, "alert_trigger" ) ;
    require_class ( $this->module, "alert_handler" ) ;
    
    $at = & new Alert_Trigger ( ) ;
    $ah = & new Alert_Handler ( ) ;
    
    if ($ah->delete ( $id )) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to delete alert");
    }
  }
  
  /**
   * AJAX handler for deleting alerts from the database
   * @param $index_content
   */
  function ajax_delete_alert(&$index_content) {
    //$_GET['root_tpl'] = 'blank';
    $id = intval ( $_GET [ 'trigger_id' ] ) ;
    if ($id <= 0) {
      echo "Invalid alert identifier. Please refresh the alerts form and try again." ;
      return;
    }
    
    require_class($this->module, "alert_trigger");
    
    $at = new Alert_Trigger();
    if ($at->delete($id)) {
      echo 'OK';
    } else {
      echo 'The database server encountered an error while trying to delete the alert';
    }
  }
  
  /**
   * Toggle the 'active' bit for an alert trigger in the database (pause/resume it)
   * @param $index_content
   */
  function toggle_alert(&$index_content) {
    $_GET['root_tpl'] = 'blank';
    $id = intval ( $_GET [ 'trigger_id' ] ) ;
    if ($id <= 0) {
      echo "Invalid alert identifier. Please refresh the alerts form and try again." ;
      return;
    }
    
    $query = "UPDATE alert_triggers SET active = not(active) WHERE trigger_id = " . $this->db->escape($id);
    if ($this->db->update($query)) {
      echo 'OK';
      return;
    } else {
      echo 'The database server encountered an error while trying to toggle this alert.';
      return;
    }
  }
  
/**
   * Main handler for the alert creation form.
   *
   * @param pointer $index_content
   */
  function get_alert_data() {
    
    $id = intval ( $_GET [ 'trigger_id' ] ) ;
    if ($id <= 0) {
      return array();
    }
    
    
    $types = array('oids' => 'snmp_oid_match',
      'servers' => 'srv_service_down',
      'urls' => 'web_alert_down',
      'df_servers' => 'df_over_threshold',
      'smb_servers' => 'smb_disk_over_threshold',
      'scan_log' => 'open_ports',
      'syslog_access' => 'syslog_msg',
      'interfaces' => 'snmp_iface_above_threshold',
      'arptable' => 'new_host',
      'protocols' => 'port_protocol_traffic',
      'snmpoids' => 'snmp_trap'
    );
    
    require_class($this->module, "composite_alert_manager");
    $cam = new Composite_Alert_Manager();
    if (!$cam->fetch_by_trigger_id($id)) {
      return array();
    }

    $type = $types[$cam->get('reference_table_name')];

    $payload = array(
    # Fill out all the drop-downs and assign all variables
      "medias" => $this->_get_alert_medias(),
      "type" => $type,
      "users" => $this->_get_alert_users(),
      "threshold_unit" => $this->_get_threshold_unit($type),
      "trigger_id" => $cam->get('trigger_id'),
      "commands" => $this->_get_alert_commands($type),
      "params" => array(
        "reference_table_name" => $cam->get('reference_table_name'),
        "reference_pkey_val" => $cam->get('reference_pkey_val'))
    );
    
    
    # This one is only used for services but the performance hit is minimal
    $parser->assign ( "conditionals", $this->_get_alert_conditionals () ) ;
    
    # Register the comparison expressions in the template
    $parser->assign ( "comp_exps", $this->_get_comparative_expressions () ) ;
    
    # Dynamic based on the type of alert because we don't want the user
    # to be able to forge indirect SQL queries from the alert daemon.
    //$params = $this->_get_alert_params ( $type ) ;
    
    $parser->assign ( "alert_params", $params ) ;

    # List all similar alerts
    $parser->assign ( "snmp_directions", $this->_get_snmp_directions () ) ;
    # Grab latencies for the specified service
    if ($type == "srv_service_down") {
      // TODO: Fix latencies code
      $srv_id = $cam->get('reference_pkey_val');
      $parser->assign ( "latencies", $this->_get_alert_latencies ( $srv_id ) ) ;
    }
    
    # Dynamically insert extension depending on the type of alert
    if (is_readable ( $this->tpl_dir . "/form_component_" . $type . ".tpl" )) {
      $parser->assign ( "component", $type ) ;
    }
    
    $index_content .= $parser->fetch ( "form_edit_alert.tpl" ) ;
    return ;
  }
  
  function form_schedule_maintenance(&$index_content) {
    $parser = new Parser($this->tpl_dir);
    $index_content .= $parser->fetch('form_schedule_maintenance.tpl');
  }

  
  /**
   * Creates a new alert maintenance window in the database based on POST data.
   * @param $index_content
   */
  function schedule_maintenance() {
    require_class($this->module, 'maintenance_manager');
    $mm = new Maintenance_Manager();
    $opt_xml = ""; 
    $GLOBALS['json_post'] = json_decode(file_get_contents("php://input"), true);
    $GLOBALS['json_post']['event_type'] = 'maintenance';
    
    if (strcmp("", $GLOBALS['json_post']['schedule_year']) <> 0) {
        $parts = explode('/', $GLOBALS['json_post']['schedule_year']);
        $GLOBALS['json_post']['schedule_day'] = $parts[0];
        $GLOBALS['json_post']['schedule_month'] = $parts[1];
    }
    
    $GLOBALS['json_post']['notify_users'] =  is_array($GLOBALS['json_post']['notify_users']) ? join(",", $GLOBALS['json_post']['notify_users']) : '';
    
    $trigger_ids = $_GET['trigger_ids'];
    
    foreach (explode(",", $trigger_ids) as $trigger_id) {
      $GLOBALS['json_post']['trigger_id'] = intval($trigger_id);
      if (!$mm->insert()) {
        $this->err->err_from_code(400, "Unable to pause alert");
      }
    }
    print '{}';
  }
  
  /**
   * Deleted an alert maintenance window from the database.
   * @param $index_content
   */
  function delete_alert_maintenance() {
    $id = intval($_GET['id']);
    
    if ($id <= 0) {
      $this->err->err_from_code(400, "Invalid alert maintenance window ID specified. Aborting.");
      return;
    }
    
    require_class($this->module, 'maintenance_manager');
    $mm = new Maintenance_Manager();
    if ($mm->delete($id)) {
      print '{}';
    } else {
      $this->err->err_from_code(400, "Unable to delete the maintenance schedule.");
    }
  }
  
  /**
   * Performs bulk management operations on alerts (mass pause, resume, delete)
   * @param pointer $index_content
   */
  function bulk_manage(&$index_content) {
    if (empty($_POST['trigger_ids'])) {
      print '{}';
    }
    
    $triggers = join(',', array_map('intval', $_POST['trigger_ids']));
    if (strcmp($_POST['action'], 'pause') == 0) {
      $query = "UPDATE alert_triggers 
        SET active = '0' 
          WHERE trigger_id IN (".$triggers.")";
      $res = $this->db->update($query);
      if ((DB_QUERY_ERROR) == $res) {
        $this->err->err_from_code(400, "Unable to resume the selected alerts due to a database error.");
      }
    } elseif (strcmp($_POST['action'], 'resume') == 0) {
      $query = "UPDATE alert_triggers 
        SET active = '1' 
          WHERE trigger_id IN (".$triggers.")";
      $res = $this->db->update($query);
      if ((DB_QUERY_ERROR) == $res) {
        $this->err->err_from_code(400, "Unable to resume the selected alerts due to a database error.");
      }
    # There is no foreign key linking the alert handlers and triggers tables,
    # so we must clean the tables up manually for the time being.
    } elseif (strcmp($_POST['action'], 'delete') == 0) {
      $queries = array(
# There actually is a foreign key for alert_handler2command and alert_maintenance.
//        "DELETE FROM alert_handler2command WHERE handler_id IN (SELECT id FROM alert_handlers WHERE trigger_id IN (".$triggers."))",
        "DELETE FROM alert_handlers WHERE trigger_id IN (".$triggers.")",
        "DELETE FROM alert_triggers WHERE trigger_id IN (".$triggers.")");
      
      $this->db->start_transaction();
      foreach($queries as $query) {
        $res = $this->db->delete($query);
        
        if (DB_QUERY_ERROR == $res) {
          $this->db->restore_previous_state();
          $this->err->err_from_code(400, "Unable to delete the alert due to a database transaction error.");
        }
      }
      
      $res = $this->db->commit_transaction();
      $this->db->restore_previous_state();
        
      if (TRUE !== $res) {
        $this->err->err_from_code("Error while committing database transaction.");
      }

    }
    
    print '{}';
  }
  
}

?>
