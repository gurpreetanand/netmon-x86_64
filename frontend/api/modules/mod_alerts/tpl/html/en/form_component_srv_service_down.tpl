<!-- {$smarty.template} ($Id$) -->

<tr>
	<td>Max Latency</td>
	{capture assign="threshold"}{if $cam}{$cam->get('trigger_threshold')}{else}{$smarty.post.trigger_threshold}{/if}
	<td>{html_options name="trigger_threshold" options=$latencies selected=$threshold}</td>
</tr>
<tr>
	<td>Conditional</td>
	<td>
		<select name="conditional_id">
			<option value="" label="-">-</option>
			{capture assign="conditional"}{if $cam}{$cam->get('conditional_id')}{else}{$smarty.post.conditional_id}{/if}
			{html_options options=$conditionals selected=$conditional}
		</select>

	</td>
</tr>
<!-- end of {$smarty.template} -->
