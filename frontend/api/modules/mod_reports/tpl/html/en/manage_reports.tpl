{literal}
<script language="Javascript">
	confirmDeleteReport = function(label,id) {
		if (confirm("Are you sure you wish to delete the report '"+label+"'?")) {
			document.location = "?module=mod_reports&action=delete&id="+id+"&root_tpl=blank_panel";
		}
	}
</script>
{/literal}

<div class="titlebar">
	Manage Saved &amp; Scheduled Reports
</div>

<div class="datagrid">
<table width="100%" align="center" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<td>Report Label</td>
			<td><div align="center">Type</div></td>
			<td><div align="center">Actions</div></td>
		</tr>
	</thead>
	<tbody>
		{foreach from=$reports item="report"}
		<tr>
			<td><a href="?module=mod_reports&action=form&type={$report.report_type}&id={$report.id}"><img src="/assets/icons/report.gif" width="16" height="16" class="icon" border="0"></a> <a href="?module=mod_reports&action=form&type={$report.report_type}&id={$report.id}">{$report.label}</a></div></td>
			<td><div align="center">{$report.report_type} <a href="javascript:parent.pnl_right.showHelp('report_{$report.report_type}')">[?]</a></div></td>
			<td>
			<div align="center">
			<a href="?module=mod_reports&action=form&type={$report.report_type}&id={$report.id}">Edit</a> 
			| 
			<a href="javascript: confirmDeleteReport('{$report.label|escape:"javascript"}', {$report.id});">Delete</a>
			</div></td>
		</tr>
		{/foreach}
	</tbody>
</table>
</div>