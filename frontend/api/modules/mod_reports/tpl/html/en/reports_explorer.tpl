<!-- {$smarty.template} ($Id$) -->
{import_js files="yui.yahoo,yui.event,yui.treeview"}
{literal}
<script>
var tree;
function treeInit() {
	tree = new YAHOO.widget.TreeView("treeDiv1");
	var root = tree.getRoot();

{/literal}

var myobj = {ldelim} label: "<img src=\"assets/icons/folderopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Saved & Scheduled Reports", href:"?module=mod_reports&action=manage_reports&root_tpl=blank_panel", target: "pnl_middle", id:"custom" {rdelim} ;
var custom = new YAHOO.widget.TextNode(myobj, root, false);


	var myobj = {ldelim} label: "<img src=\"assets/icons/folderopen.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Completed Reports", id:"saved", href:"?module=mod_vfs&action=view_dir_files&dir_id={$dir_id}", target:"pnl_middle" {rdelim} ;
	var saved = new YAHOO.widget.TextNode(myobj, root, false);

{literal}

   myobj = { label: "<img src=\"assets/icons/report.gif\" class=\"icon\"  border=\"0\"> Network Activity Report", id:"net_activity", href:"?module=mod_reports&action=form&type=network_activity", target:"pnl_middle" } ;
   var net_activity = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/report.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Conversation Report", id:"conversations", href:"?module=mod_reports&action=form&type=conversation", target:"pnl_middle" } ;
   var conversations = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/ie.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Web Traffic Report", id:"web_traffic", href:"?module=mod_reports&action=form&type=web_traffic", target:"pnl_middle" } ;
   var conversations = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/mib_timetick.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> UP/DOWN Time Report", id:"updown", href:"?module=mod_reports&action=form&type=up_downtime", target:"pnl_middle" } ;
   var updown = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/interface.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Bandwidth Activity Report", id:"bandwidth", href:"?module=mod_reports&action=form&type=bandwidth_activity", target:"pnl_middle" } ;
   var bandwidth = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/interface.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Bandwidth Consumption Report", id:"utilization", href:"?module=mod_reports&action=form&type=bandwidth_consumption", target:"pnl_middle" } ;
   var bandwidth = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/disk.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Disk Activity Report", id:"disks", href:"?module=mod_reports&action=form&type=disk_activity", target:"pnl_middle" } ;
   var disks = new YAHOO.widget.TextNode(myobj, root, false);

   new YAHOO.widget.TextNode({
	   label: "<img src=\"assets/icons/email.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Email Traffic Inspection",
	   id: "email_traffic",
	   href: "?module=mod_reports&action=form&type=email_traffic",
	   target: "pnl_middle"
   }, root, false);
   
   new YAHOO.widget.TextNode({
	   label: "<img src=\"assets/icons/email_go.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Email Traffic Statistics",
	   id: "email_summary",
	   href: "?module=mod_reports&action=form&type=email_summary",
	   target: "pnl_middle"
   }, root, false);

   
   myobj = { label: "<img src=\"assets/icons/report.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Latency Report", id:"latency", href:"?module=mod_reports&action=form&type=latency", target:"pnl_middle" } ;
   var latency = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = {label: '<img src="/assets/icons/oid_tracker.gif" class="icon" style="vertical-align: middle" border="0" /> OID Tracker Report', id: "oids", href:"?module=mod_reports&action=form&type=oid_tracker", target: "pnl_middle" };
   var oids = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = {label: '<img src="/assets/icons/network.gif" class="icon" style="vertical-align: middle" border="0" /> URL Tracker Report', id: "urls", href:"?module=mod_reports&action=form&type=url_tracker", target: "pnl_middle" };
   var oids = new YAHOO.widget.TextNode(myobj, root, false);
   
   myobj = { label: "<img src=\"assets/icons/report.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Port Scan Report", id:"netscan", href:"?module=mod_reports&action=process&type=portscan", target:"pnl_middle" } ;
   var netscan = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/alert.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Alert History Report", id:"alerts", href:"?module=mod_reports&action=form&type=alert_history", target:"pnl_middle" } ;
   var alerts = new YAHOO.widget.TextNode(myobj, root, false);

   myobj = { label: "<img src=\"assets/icons/users.gif\" class=\"icon\" style=\"vertical-align: middle\" border=\"0\"> Netmon Login Report", id:"logins", href:"?module=mod_reports&action=form&type=netmon_login", target:"pnl_middle" } ;
   var logins = new YAHOO.widget.TextNode(myobj, root, false);



   tree.onExpand = function(node) {
      //alert(node.data.id + " was expanded");
   }

   tree.onCollapse = function(node) {
      //alert(node.data.id + " was collapsed");
   }

   tree.draw();
   
}
</script>
{/literal}


<div id="treeDiv1" class="tree" style="margin-left: 9px;"></div>
<script>
treeInit();
</script>


<!-- end of {$smarty.template} -->
