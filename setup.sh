# Install Genetic Dependencies
echo "Installing Dependencies ..."
apt-get install -y build-essential nmap memcached python-dev python-pip sendmail squirrelmail traceroute > /dev/null
pip install --index=https://pypi.python.org/simple/ psycopg2 Cheetah
echo "Dependencies: DONE"

# Install Apache2
echo "Installing Apache2 ..."
apt-get install -y apache2 > /dev/null
echo "Apache2: DONE"

# Install PHP5
echo "Installing PHP5 ..."
apt-get install -y php5 libapache2-mod-php5 > /dev/null
apt-get install -y php5-mysql php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt php5-memcache php5-ming php5-ps php5-pspell php5-recode php5-snmp php5-sqlite php5-tidy php5-xmlrpc php5-xsl php5-memcache php5-memcached php5-pgsql > /dev/null
echo "PHP5: DONE"

# Installing Support library for Daenoms
echo "Installing Support Libraries for Daenoms ..."
apt-get install -y g++ libevent1-dev libdumbnet-dev libpcre++-dev libmemcached-dev libdbus-c++-dev libjsoncpp-dev libdbus-1-dev libsmi2-dev libpcap0.8-dev libcurl4-openssl-dev libsnmp-dev libboost-dev libpq-dev > /dev/null

ln -s /usr/lib/libdbus-c++-1.a /usr/lib/i386-linux-gnu/libdbus-c++-1.a > /dev/null
ln -s /usr/lib/libmemcached.a /usr/lib/i386-linux-gnu/libmemcached.a > /dev/null
ln -s /usr/lib/libjsoncpp.a /usr/lib/i386-linux-gnu/libjsoncpp.a > /dev/null
echo "Daenom Support Libraries: DONE"

# Copy Frontend Files to /var/www/
echo "Setting-Up PHP Frontend files at /var/www/ ..."
cp -r $HOME/Desktop/netmon/frontend/* /var/www/ > /dev/null
ln -sT /var/www /apache > /dev/null
echo "PHP Frontend Files Setup: DONE"

# Setting-Up PostgresSQl 8.4 netmon35 database
echo "Setting Up netmon35 Database ..."
cd $HOME/Desktop/netmon
psql -d netmon35 -U root < netmon.pgsql
echo "netmon35 Database Setup: DONE"

# Copying netmon Configuration Files
echo "Setting-Up netmon Configuration files at /etc/ ..."
cp -r $HOME/Desktop/netmon/system/etc/netmon* /etc/ > /dev/null
cp -r $HOME/Desktop/netmon/system/etc/init.d/netmon /etc/init.d/ > /dev/null
cp -r $HOME/Desktop/netmon/system/etc/cron.d/* /etc/cron.d/ > /dev/null
cp $HOME/Desktop/netmon/system/etc/dbus-1/system.d/* /etc/dbus-1/system.d/ > /dev/null
mv /etc/rc.local /etc/rc.local.bak
touch /etc/rc.local
chmod 0755 /etc/rc.local
cat $HOME/Desktop/netmon/system/etc/rc.local > /etc/rc.local
echo "netmon Configuration Files Setup: DONE"

# Compiling Daemons Source Files
echo "Compiling Daemons from Source Files ..."
cd $HOME/Desktop/netmon/daemons
make clean > /dev/null
make > /dev/null
echo "Daemons Source Files Compiling: DONE"

# Installing Daemons
echo "Installing Daemons ..."
cd $HOME/Desktop/netmon/daemons
make install > /dev/null
echo "Daemons Installation: DONE"

cp $HOME/Desktop/netmon/daemons/netscan/netscan /usr/local/sbin/ > /dev/null
cp $HOME/Desktop/netmon/daemons/netscan/netscand /usr/local/sbin/ > /dev/null

# Starting netmon
echo "Starting Daemons ..."
netmond && procmond > /dev/null
echo "Daemons: Running"
echo "Restarting Apache2 ..."
service apache2 restart > /dev/null
echo "Apache2: Running"
echo "Restarting PostgresSQL ..."
service postgresql restart > /dev/null
echo "PostgresSQL: Running"


